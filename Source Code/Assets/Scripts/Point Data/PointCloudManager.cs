﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using CommonFramework.DataStream;
using CommonFramework.MessageSystem;
using CommonFramework.Environment;
using CommonFramework.LineDrawer;

namespace CommonFramework.PointCloud
{
    /// <summary>
    /// PointCloudManager controls the asset management of point cloud data objects
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class PointCloudManager : MonoBehaviour, IMessageable
    {
        private const string messageableName = "PointCloudManager";
        private Queue<IMessage> pendingMessages = new Queue<IMessage>();

        private IPointCloudGameObjectFactory factory = new PointCloudRendererGameObjectFactory();
        private BufferedPointDataSource dataSource = new BufferedPointDataSource();
        private PointCloudRenderer pointRenderer = null;
        private List<PointCloudRenderer> renderers = new List<PointCloudRenderer>();

        private bool drawingLines = true;

        /// <summary>
        /// The number of samples that should be made to all point cloud renderer generations managed by this renderer.
        /// The number of samples affects the radius generated. The closer to one the quicker it performs. The more
        /// higher the value the computation time per cloud point increases linearly. The default is 5 which is in general
        /// a suitable value for performance and looks.
        /// </summary>
        public int NearestNeighbourSamples = 5;

        /// <summary>
        /// A flag that indicates if the renderers should use a fixed radius with for the splats.
        /// </summary>
        public bool HasFixedRadius = false;

        /// <summary>
        /// A flag that indicates if the renderers should still use the KDTree for storage when HasFixedRadius is set to true.
        /// </summary>
        public bool UseTree = false;

        /// <summary>
        /// The size of the radius to use when the HasFixedRadius is set to true.
        /// </summary>
        public float FixedRadius = 1.0f;

        /// <summary>
        /// A flag that indicates if the manager should listen to cloud data sent through the message system.
        /// </summary>
        public bool ListenToIncomingData = true;

        /// <summary>
        /// A flag that indicates if the renderers should draw the grid lines of the spatial tree.
        /// </summary>
        public bool RenderTreeLines = true;

        /// <summary>
        /// The collection of models to use as point cloud data sources
        /// </summary>
        public List<string> ModelDataSources = new List<string>();

        // Use this for variable assignment
        void Awake()
        {
            // Register the PointCloudManager with the messaging system and transcriber pumps
            MessageHandler.Instance.RegisterMessageable(this);
            PacketTranscriber.Instance.Register(new CloudPointMessageFactory());

            // Create game object from global data source
            GameObject gameObject = factory.CreatePointCloudRenderer("DataStream_CloudRenderer", dataSource);
            Transform transform = gameObject.GetComponent<Transform>();
            pointRenderer = gameObject.GetComponent<PointCloudRenderer>();
            pointRenderer.NearestNeighbourSamples = NearestNeighbourSamples;
            pointRenderer.HasFixedRadius = HasFixedRadius;
            pointRenderer.FixedRadius = FixedRadius;
            pointRenderer.UseTree = UseTree;
            pointRenderer.RenderTreeLines = RenderTreeLines;
            renderers.Add(pointRenderer);
            transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            pendingMessages.Enqueue(new GameObjectMessage(gameObject));

            // Loop through specified model names
            for (int i = ModelDataSources.Count - 1; i >= 0; i--)
            {
                // Create a game object from a mesh
                string name = ModelDataSources[i];
                AssetMessage message = new AssetMessage(name, name + "_MeshCloudDataSource", Vector3.zero, Quaternion.identity, new Vector3(0.0f, 0.0f, 0.0f));
                message.AssetInstancedEvent += message_AssetInstancedEventMultiFilter;
                pendingMessages.Enqueue(message);
            }
        }

        private void message_AssetInstancedEventMultiFilter(EnvironmentManager manager, GameObject instance)
        {
            MeshFilter[] filters = instance.GetComponentsInChildren<MeshFilter>();
            GameObject gameObject = factory.CreatePointCloudRenderer(instance.name + "_CloudRenderer", new MeshPointDataSource(filters));
            PointCloudRenderer pointRenderer = gameObject.GetComponent<PointCloudRenderer>();
            Transform transform = gameObject.GetComponent<Transform>();
            pointRenderer.NearestNeighbourSamples = NearestNeighbourSamples;
            pointRenderer.HasFixedRadius = HasFixedRadius;
            pointRenderer.FixedRadius = FixedRadius;
            pointRenderer.UseTree = UseTree;
            pointRenderer.RenderTreeLines = RenderTreeLines;
            renderers.Add(pointRenderer);
            transform.localScale = new Vector3(100.0f, 100.0f, 100.0f);
            pendingMessages.Enqueue(new GameObjectMessage(gameObject));
        }

        void Update()
        {
            //renderer.Clear();
        }

        void OnGUI()
        {
            bool prevDrawingLines = drawingLines;
            drawingLines = GUI.Toggle(new Rect(5, 5, 85, 30), drawingLines, " Draw KD Tree Lines");

            if (prevDrawingLines != drawingLines)
                pendingMessages.Enqueue(new DrawLineStatusMessage(drawingLines));
        }

        /// <summary>
        /// Informs the PointCloudManager that it needs to handle and process an IMessage.
        /// </summary>
        /// <param name="message">The IMessage that the PointCloudManager needs to process.</param>
        public void ReceiveMessage(IMessage message)
        {
            if(ListenToIncomingData && message.MessageName == CloudPointMessage.CloudPointMessageName)
            {
                CloudPointMessage cloudPointMessage = (CloudPointMessage)message;
                if(cloudPointMessage.CloudPoint != null) dataSource.Add(cloudPointMessage.CloudPoint);
            }
        }

        // ----- Properties ----- //

        /// <summary>
        /// Gets the pending outgoing IMessage queue that needs to be processed by other componenets for the PointCloudManager.
        /// </summary>
        public Queue<IMessage> PendingMessages
        {
            get { return pendingMessages; }
        }

        /// <summary>
        /// Gets the identifier name of the IMessageable component, PointCloudManager.
        /// </summary>
        public string MessageableName
        {
            get { return messageableName; }
        }
    }
}