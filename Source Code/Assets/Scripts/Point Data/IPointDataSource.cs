﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace CommonFramework.PointCloud
{
    /// <summary>
    /// IPointDataSource defines the interface for components that provide a stream of point data
    /// </summary>
    public interface IPointDataSource
    {
        /// <summary>
        /// Reads the data currently available through the IPointDataSource
        /// </summary>
        /// <returns>The data read from the data source</returns>
        IEnumerable<CloudPoint> ReadAvailablePointData();

        // ----- Properties ----- //

        /// <summary>
        /// Gets if point data is available through the IPointDataSource
        /// </summary>
        bool PointDataAvailable { get; }
    }
}
