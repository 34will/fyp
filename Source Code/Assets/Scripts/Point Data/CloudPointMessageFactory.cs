﻿using UnityEngine;
using System;
using System.Text;

using CommonFramework.DataStream;
using CommonFramework.MessageSystem;

namespace CommonFramework.PointCloud
{
    /// <summary>
    /// CloudPointTranscriber converts incoming data into CloudPointMessages.
    /// </summary>
    public class CloudPointMessageFactory : IPacketMessageFactory
    {
        /// <summary>
        /// Create message creates a new instance of CloudPointMessage converting the passed Packet.
        /// </summary>
        /// <param name="packet">The Packet to to convert into a CloudPointMessage.</param>
        /// <returns>A new CloudPointMessage instance.</returns>
        public IMessage CreateMessage(Packet packet)
        {
            if(packet.Data.Length != 12)
                return null;
            else
            {
                Vector3 position = new Vector3(BitConverter.ToSingle(packet.Data, 0),
                                               BitConverter.ToSingle(packet.Data, 4),
                                               BitConverter.ToSingle(packet.Data, 8));
                Vector3 normal = position.normalized * -1.0f;
                CloudPoint point = new CloudPoint(position, normal, Vector4.zero, new Color(0.705f, 0.698f, 0.168f));
                return new CloudPointMessage(point);
            }
        }
    }
}
