﻿using UnityEngine;
using System;
using System.Diagnostics.CodeAnalysis;

namespace CommonFramework.PointCloud
{
    /// <summary>
    /// PointCloudRendererGameObjectFactory implements IPointCloudGameObjectFactory creating game objects that render IPointDataSources using a special
    /// point data shader within Unity.
    [ExcludeFromCodeCoverage]
    /// </summary>
    public class PointCloudRendererGameObjectFactory : IPointCloudGameObjectFactory
    {
        /// <summary>
        /// Creates a new GameObject that renders an IPointDataSource using a specialised shader.
        /// </summary>
        /// <param name="name">The name to assign the GameObject.</param>
        /// <param name="dataSource">The IPointDataSource the render component should use.</param>
        /// <returns>A new named GameObject with a PointCloudRenderer attached.</returns>
        public GameObject CreatePointCloudRenderer(string name, IPointDataSource dataSource)
        {
            GameObject gameObject = new GameObject(name);
            PointCloudRenderer renderer = gameObject.AddComponent<PointCloudRenderer>();
            renderer.DataSource = dataSource;
            return gameObject;
        }
    }
}