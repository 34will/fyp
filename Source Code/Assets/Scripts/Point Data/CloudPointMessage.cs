﻿using System;
using System.Collections.Generic;
using System.Text;

using CommonFramework.MessageSystem;

namespace CommonFramework.PointCloud
{
    /// <summary>
    /// CloudPointMessage defines an IMessage that encapsulates a CloudPoint that is to be sent through the message system.
    /// </summary>
    public class CloudPointMessage : IMessage
    {
        public const string CloudPointMessageName = "CloudPointMessage";
        private IMessageable source = null;
        private CloudPoint point = null;

        /// <summary>
        /// Creates a new CloudPointMessage encapsulating the specified CloudPoint.
        /// </summary>
        /// <param name="point">The CloudPoint for the CloudPointMessage to encapsulate.</param>
        /// <param name="source">The source IMessageable that is sending the CloudPointMessage.</param>
        public CloudPointMessage(CloudPoint point, IMessageable source = null)
        {
            this.point = point;
            this.source = source;
        }

        // ----- Properties ----- //

        /// <summary>
        /// Gets the name of the message type identifier, CloudPointMessage.
        /// </summary>
        public string MessageName
        {
            get { return CloudPointMessageName; }
        }

        /// <summary>
        /// Sets or gets the source IMessageable componenent.
        /// </summary>
        public IMessageable Source
        {
            set { source = value; }
            get { return source; }
        }

        /// <summary>
        /// Gets the encapsulated CloudPoint contained within this CloudPointMessage.
        /// </summary>
        public CloudPoint CloudPoint
        {
            get { return point; }
        }
    }
}
