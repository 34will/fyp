﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Xml;

using CommonFramework.MessageSystem;
using CommonFramework.Logging;

namespace CommonFramework.PointCloud
{
    /// <summary>
    /// PointCloudCRUDRecorder is a component that records any CloudPointMessage that is being passed to a CRUD file.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class PointCloudCRUDRecorder : MonoBehaviour, IMessageable
    {
        private const string messageableName = "PointCloudCRUDRecorder";
        private Queue<IMessage> pendingMessages = new Queue<IMessage>();

        private XmlDocument document = new XmlDocument();
        private XmlNodeList baseElements = null;
        private XmlNode packetsElement = null;
        private XmlNode fileInfoElement = null;
        private bool started = false;

        /// <summary>
        /// OutputFilename is the name of the file that the recorded point data will be written to.
        /// </summary>
        public string OutputFilename = "recording.crud";

        /// <summary>
        /// A flag that indicates whether the PointCloudCRUDRecorder should rewrite the output file, or append to the file.
        /// If the flag is set to true and an existing file exists with the OutputFilename then it will be cleared
        /// before recording begins.
        /// </summary>
        public bool Rewrite = true;

        /// <summary>
        /// A flag that indicates whether the PointCloudCRUDRecorder should write received CloudPointMessages to file.
        /// </summary>
        public bool Record = true;

        private void Start()
        {
            // Registers this class with the MessageHandler so it can receive CloudPointMessages
            MessageHandler.Instance.RegisterMessageable(this);
            started = true;

            // Load in the document as long as we don't want to destroy it
            try
            {
                if (!Rewrite && File.Exists(OutputFilename))
                    document.Load(OutputFilename);
            }
            catch(System.Exception ex)
            {
                pendingMessages.Enqueue(new LogMessage("Failed to load existing CRUD document:\n" + ex.Message, LogClassification.Error, this));
                Rewrite = true;
            }

            // The document structure needs to be made
            if(Rewrite)
            {
                document.LoadXml("<crud>" +
                                 "<fileinfo>" +
                                 "<title>Point Cloud CRUD Recording</title>" +
                                 "<info>A recording of a point cloud data stream</info>" +
                                 "<starttimestamp mode=\"epoch\" long=\"" + DateTime.Now.Ticks + "\" />" +
                                 "</fileinfo>" +
                                 "<packets></packets></crud>");
            }

            // Read in the document information ready for processing
            XmlNode root = document.FirstChild;
            if (root == null) return;
            baseElements = root.ChildNodes;

            // Get the packet elements
            foreach (XmlNode element in baseElements)
            {
                switch(element.Name.ToString())
                {
                    case "packets": packetsElement = element; break;
                    case "fileinfo": fileInfoElement = element; break;
                }
            }
        }

        private void OnDestroy()
        {
            if (started && Record)
            {
                // Create the end timestamp node
                XmlNode node = document.CreateElement("endtimestamp");
                XmlAttribute mode = document.CreateAttribute("mode");
                XmlAttribute timestamp = document.CreateAttribute("long");
                mode.InnerText = "epoch";
                timestamp.InnerText = DateTime.Now.Ticks.ToString();
                node.Attributes.Append(mode);
                node.Attributes.Append(timestamp);
                fileInfoElement.AppendChild(node);

                document.Save(OutputFilename);
            }
        }

        /// <summary>
        /// Informs the PointCloudCRUDRecorder that it needs to handle and process an IMessage.
        /// </summary>
        /// <param name="message">The IMessage that the PointCloudCRUDRecorder needs to process.</param>
        /// <remarks>
        /// The only message that the PointCloudCRUDRecorder recognises is CloudPointMessage.
        /// Any received CloudPointMessage that is passed to this PointCloudCRUDRecorder will be
        /// processed into the CRUD recording.
        /// </remarks>
        public void ReceiveMessage(IMessage message)
        {
            if(Record && message.MessageName == CloudPointMessage.CloudPointMessageName)
            {
                CloudPointMessage pointMessage = (CloudPointMessage)message;
                if(pointMessage != null)
                {
                    Vector3 position = pointMessage.CloudPoint.Position;
                    XmlNode node = document.CreateElement("packet");
                    node.InnerXml = "<timestamp mode=\"epoch\" long=\"" + DateTime.Now.Ticks + "\" />" +
                                    "<position x=\"" + position.x + "\" y=\"" + position.y + "\" z=\"" + position.z + "\" />";
                    packetsElement.AppendChild(node);
                }
            }
        }

        // ----- Properties ----- //

        /// <summary>
        /// Gets the pending outgoing IMessage queue that needs to be processed by other componenets for the PointCloudCRUDRecorder.
        /// </summary>
        public Queue<IMessage> PendingMessages
        {
            get { return pendingMessages; }
        }

        /// <summary>
        /// Gets the identifier name of the IMessageable component, PointCloudCRUDRecorder.
        /// </summary>
        public string MessageableName
        {
            get { return messageableName; }
        }
    }
}