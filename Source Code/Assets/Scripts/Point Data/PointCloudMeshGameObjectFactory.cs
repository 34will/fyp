﻿using UnityEngine;
using System.Collections;
using System.Diagnostics.CodeAnalysis;

namespace CommonFramework.PointCloud
{
    /// <summary>
    /// PointCloudMeshGameObjectFactory is used to create unity GameObjects using IPointDataSources as a conventional mesh
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class PointCloudMeshGameObjectFactory : IPointCloudGameObjectFactory
    {
        /// <summary>
        /// Creates a new GameObject with a render component to render the specified IPointDataSource as a conventional polygonal mesh
        /// </summary>
        /// <param name="name">The name to assign the new unity GameObject</param>
        /// <param name="dataSource">The IPointDataSource to use when creating the mesh</param>
        /// <returns>A GameObject with a Mesh and an updating component attached created from the IPointDataSource</returns>
        public GameObject CreatePointCloudRenderer(string name, IPointDataSource dataSource)
        {
            GameObject gameObject = new GameObject(name);

            // TODO - Create a Mesh and create relevant components to add to the gameObject
            //        Add a special component to update the mesh when the IPointDataSource changes

            return gameObject;
        }
    }
}