﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using UnityEngine;

namespace CommonFramework.PointCloud
{
    /// <summary>
    /// MeshPointSource is an IPointDataSource implementation that uses a Unity Mesh object to source the point data.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class MeshPointDataSource : IPointDataSource
    {
        private List<CloudPoint> data = null;
        private bool available = true;

        /// <summary>
        /// Creates a new MeshPointSource from a Unity Mesh object.
        /// </summary>
        /// <param name="mesh">The Mesh point data source, where the vertices represent the point data to use.</param>
        public MeshPointDataSource(Mesh mesh)
            : this(mesh, Color.white) { }

        /// <summary>
        /// Creates a new MeshPointSource from a Unity Mesh object.
        /// </summary>
        /// <param name="mesh">The Mesh point data source, where the vertices represent the point data to use.</param>
        /// <param name="colour">The colour to give the cloud points of the mesh.</param>
        public MeshPointDataSource(Mesh mesh, Color colour)
        {
            data = new List<CloudPoint>(mesh.vertexCount);
            for (int i = mesh.vertexCount - 1; i >= 0; i--)
                data.Add(new CloudPoint(mesh.vertices[i], mesh.normals[i], mesh.tangents[i], colour));
            available = data.Count > 0;
        }

        /// <summary>
        /// Creates a new MeshPointDataSource from a enumeration of Unity MeshFilters.
        /// </summary>
        /// <param name="meshfilters">The MeshFilter enumeration containing all the point data to use.</param>
        public MeshPointDataSource(IEnumerable<MeshFilter> filters)
        {
            // Get a point count for speed
            int count = 0;
            foreach (MeshFilter filter in filters)
                count += filter.mesh.vertexCount;

            // Load the point data into memory
            data = new List<CloudPoint>(count);
            foreach (MeshFilter filter in filters)
            {
                MeshRenderer renderer = filter.gameObject.GetComponent<MeshRenderer>();
                Material[] materials = renderer.materials;
                Mesh mesh = filter.sharedMesh;
                if (materials.Length != mesh.subMeshCount)
                    throw new Exception("Incompatible number of mesh materials in mesh renderer");
                for (int i = 0; i < mesh.subMeshCount; i++)
                {
                    Material material = materials[i];
                    Texture2D texture = material.mainTexture as Texture2D;
                    Color colour = material.color;
                    HashSet<int> indices = new HashSet<int>(mesh.GetIndices(i));
                    foreach(int j in indices)
                    {
                        if(texture != null)
                        {
                            Vector2 uv = mesh.uv[j];
                            colour = texture.GetPixelBilinear(uv.x, uv.y);
                        }
                        data.Add(new CloudPoint(mesh.vertices[j], mesh.normals[j], mesh.tangents[j], colour));
                    }
                }
            }
        }

        /// <summary>
        /// Creates a new MeshPointDataSource from a enumeration of Unity MeshFilters.
        /// </summary>
        /// <param name="meshfilters">The MeshFilter enumeration containing all the point data to use.</param>
        /// <param name="colour">The colour to give the cloud points of the mesh.</param>
        public MeshPointDataSource(IEnumerable<MeshFilter> filters, Color colour)
        {
            // Get a point count for speed
            int count = 0;
            foreach (MeshFilter filter in filters)
                count += filter.mesh.vertexCount;

            // Load the point data into memory
            data = new List<CloudPoint>(count);
            foreach(MeshFilter filter in filters)
            {
                Mesh mesh = filter.mesh;
                for (int i = mesh.vertexCount - 1; i >= 0; i--)
                    data.Add(new CloudPoint(mesh.vertices[i], mesh.normals[i], mesh.tangents[i], colour));
            }
            available = count > 0;
        }

        /// <summary>
        /// Reads the data currently available through the data source.
        /// </summary>
        /// <returns>The data read from the data source.</returns>
        public IEnumerable<CloudPoint> ReadAvailablePointData()
        {
            if (available)
            {
                available = false;
                return data;
            }
            else throw new Exception("No point data available to read");
        }

        // ----- Properties ----- //

        /// <summary>
        /// Gets if point data is available through the data source.
        /// </summary>
        public bool PointDataAvailable
        {
            get { return available; }
        }
    }
}
