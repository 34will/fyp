﻿using UnityEngine;
using System;
using System.Collections.Generic;

using CommonFramework.Maths;

namespace CommonFramework.PointCloud
{
    /// <summary>
    /// StaticPointDataSource is an implementation of IPointDataSource that does not change once loaded.
    /// </summary>
    public class BufferedPointDataSource : IPointDataSource
    {
        private List<CloudPoint> buffer = new List<CloudPoint>();

        /// <summary>
        /// Reads the data currently available through the data source.
        /// </summary>
        /// <returns>The data read from the data source.</returns>
        public IEnumerable<CloudPoint> ReadAvailablePointData()
        {
            CloudPoint[] temp = buffer.ToArray();
            buffer.Clear();
            return temp;
        }

        /// <summary>
        /// Adds a single point of data to the buffer data source.
        /// </summary>
        /// <param name="point">The point of data to add to the data source.</param>
        public void Add(Vector3 point)
        {
            Vector3 axis = Vector3.up;
            Vector3 normal = Vector3.Normalize(point);
            Vector3 tangent = Vector3.Normalize(Vector3.Cross(axis, normal));
            buffer.Add(new CloudPoint(point, normal, tangent, Color.white));
        }

        /// <summary>
        /// Adds a single point of data to the buffer data source.
        /// </summary>
        /// <param name="point">The point of data to add to the data source.</param>
        public void Add(CloudPoint point)
        {
            buffer.Add(point);
        }

        /// <summary>
        /// Adds a range of point data to the buffer data source.
        /// </summary>
        /// <param name="points">The enumeration of points to add to the buffer</param>
        public void AddRange(IEnumerable<CloudPoint> points)
        {
            buffer.AddRange(points);
        }

        /// <summary>
        /// Adds a range of point data to the buffer data source.
        /// </summary>
        /// <param name="points">The enumeration of points to add to the buffer</param>
        public void AddRange(IEnumerable<Vector3> points)
        {
            foreach (Vector3 point in points)
                Add(point);
        }

        // ----- Properties ----- //

        /// <summary>
        /// Gets if point data is available through the data source.
        /// </summary>
        public bool PointDataAvailable
        {
            get { return buffer.Count > 0; }
        }

        /// <summary>
        /// Gets the number of points currently stored in the buffer.
        /// </summary>
        public int Count
        {
            get { return buffer.Count; }
        }
    }
}
