﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

using CommonFramework.Containers.Spatial;
using CommonFramework.LineDrawer;
using CommonFramework.Maths;
using CommonFramework.MessageSystem;
using CommonFramework.Logging;

namespace CommonFramework.PointCloud
{
    /// <summary>
    /// PointCloudRenderer defines a render component that renders point cloud data to the graphics hardware.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class PointCloudRenderer : MonoBehaviour
    {
        private const string materialFilename = "PointCloudMaterial";
        private const float overlapFactor = 1.0f;

        // Buffers
        private List<int> indices = new List<int>();
        private List<Vector3> vertices = new List<Vector3>();
        private List<Vector3> normals = new List<Vector3>();
        private List<Vector4> tangents = new List<Vector4>();
        private List<Color> colours = new List<Color>();

        // Mesh information
        private Transform transform = null;
        private Mesh mesh = new Mesh();
        private MeshFilter filter = null;

        // Source data
        private IPointDataSource dataSource = null;
        private KDTree<CloudPoint> tree = new KDTree<CloudPoint>(3, 10);

        // Sample information
        private bool hasFixedRadius = false;
        private bool useTree = false;
        private bool renderTreeLines = false;
        private float fixedRadius = 1.0f;
        private int nearestNeighbourSamples = 5;

        void Awake()
        {
            // Adds the mesh to the game object
            filter = gameObject.AddComponent<MeshFilter>();
            filter.mesh = mesh;

            // Get the transform for the renderer
            transform = GetComponent<Transform>();

            // Assign the shader to the mesh renderer
            Material material = Resources.Load<Material>(materialFilename);
            MeshRenderer renderer = gameObject.AddComponent<MeshRenderer>();
            renderer.material = material;
        }

        void Update()
        {
            // Acquire new data
            if (dataSource != null && dataSource.PointDataAvailable)
                AddRange(dataSource.ReadAvailablePointData());

            // Update the mesh
            if (vertices.Count != mesh.vertexCount)
            {
                UpdateRadius(nearestNeighbourSamples);

                // Set the input data
                mesh.vertices = vertices.ToArray();
                mesh.normals = normals.ToArray();
                mesh.tangents = tangents.ToArray();
                mesh.colors = colours.ToArray();

                // Set the indices
                for (int i = indices.Count; i < vertices.Count; i++)
                    indices.Add(i);
                mesh.SetIndices(indices.ToArray(), MeshTopology.Points, 0);
            }

            // Send messages to draw grid lines
            if(renderTreeLines)
            {
                MessageHandler.Instance.SendMessage(new ClearLinesMessage());

                float scaleX = transform.localScale.x;
                float scaleY = transform.localScale.y;
                float scaleZ = transform.localScale.z;

                foreach(LineSegmentND segment in tree.SubTreeAxes)
                {
                    Vector3 a = new Vector3(segment.A.GetDimension(0) * scaleX, segment.A.GetDimension(1) * scaleY, segment.A.GetDimension(2) * scaleZ);
                    Vector3 b = new Vector3(segment.B.GetDimension(0) * scaleX, segment.B.GetDimension(1) * scaleY, segment.B.GetDimension(2) * scaleZ);
                    MessageHandler.Instance.SendMessage(new DrawLineMessage(a, b, Color.magenta));
                }
            }
        }

        private void UpdateRadius(int number)
        {
            Transform transform = GetComponent<Transform>();
            for (int i = vertices.Count - 1; i >= 0; i--)
            {
                Vector4 tangent = tangents[i];
                if (hasFixedRadius) tangent.w = fixedRadius;
                else
                {
                    SortedList<float, IPositionableND> nearest = tree.NearestNeighbours(vertices[i].ToFloat3(), number);
                    float lengthSquared = nearest.ElementAt(nearest.Count - 1).Key;
                    tangent.w = (float)Math.Sqrt(lengthSquared) * transform.localScale.x * overlapFactor;
                }
                tangents[i] = tangent;
            }
        }

        /// <summary>
        /// Adds a collection of points to the cloud data.
        /// </summary>
        /// <param name="vertices">The vertices to add to the data cloud.</param>
        public void AddRange(IEnumerable<CloudPoint> vertices)
        {
            // Perform per vertex calculations
            Vector3 axis = Vector3.up;
            foreach (CloudPoint vertex in vertices)
            {
                // Calculate normal and tangent
                this.vertices.Add(vertex.Position);
                this.normals.Add(vertex.Normal);
                this.tangents.Add(Vector3.Normalize(Vector3.Cross(axis, vertex.Normal)));
                this.colours.Add(vertex.Colour);

                // Add the points to the oc-tree (Only position information needed to calculate radius)
                if(!hasFixedRadius || useTree)
                    tree.Insert(vertex);
            }
        }

        internal void Clear()
        {
            indices.Clear();
            vertices.Clear();
            normals.Clear();
            tangents.Clear();
            colours.Clear();
        }

        // ----- Properties ----- //

        /// <summary>
        /// Sets or gets the IPointDataSource used by the PointCloudRenderer to acquire data.
        /// </summary>
        public IPointDataSource DataSource
        {
            set { dataSource = value; }
            get { return dataSource; }
        }

        /// <summary>
        /// Sets or gets the number of samples the nearest neighbour calculation will use to generate the radius.
        /// Changes to this value will only take effect once new data is added and the update of the MonoBehaviour is
        /// called by Unity.
        /// </summary>
        public int NearestNeighbourSamples
        {
            set { nearestNeighbourSamples = value; }
            get { return nearestNeighbourSamples; }
        }

        /// <summary>
        /// Sets or gets a flag, inidcating if the radius of the renderer should be fixed.
        /// </summary>
        public bool HasFixedRadius
        {
            set { hasFixedRadius = value; }
            get { return hasFixedRadius; }
        }

        /// <summary>
        /// Sets or gets the radius of the splats when the HasFixedRadius flag is set to true.
        /// </summary>
        public float FixedRadius
        {
            set { fixedRadius = value; }
            get { return fixedRadius; }
        }

        /// <summary>
        /// Sets or gets whether the KDTree should be used for storage if HasFixedRadius is set to true.
        /// </summary>
        public bool UseTree
        {
            set { useTree = value; }
            get { return useTree; }
        }

        /// <summary>
        /// Sets or gets if the PointCloudRenderer should draw line axes for the KDTree.
        /// This is useful for debugging.
        /// </summary>
        public bool RenderTreeLines
        {
            set { renderTreeLines = value; }
            get { return renderTreeLines; }
        }

        /// <summary>
        /// Gets the tree used for lookup storage.
        /// </summary>
        public KDTree<CloudPoint> Tree
        {
            get { return tree; }
        }
    }
}