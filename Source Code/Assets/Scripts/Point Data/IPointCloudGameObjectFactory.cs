﻿using UnityEngine;
using System;

namespace CommonFramework.PointCloud
{
    /// <summary>
    /// IPointCloudGameObjectFactory defines the interface for a factory the creates a type of point cloud renderer for a certain data source.
    /// </summary>
    public interface IPointCloudGameObjectFactory
    {
        /// <summary>
        /// Creates a GameObject with the relevant components attached to render an IPointDataSource.
        /// </summary>
        /// <param name="name">The name to assign the GameObject.</param>
        /// <param name="dataSource">The IPointDataSource the relevant render component will use when rendering.</param>
        /// <returns>A new GameObject with the relevant renderer attached.</returns>
        GameObject CreatePointCloudRenderer(string name, IPointDataSource dataSource);
    }
}
