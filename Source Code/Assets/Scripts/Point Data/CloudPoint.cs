﻿using UnityEngine;
using System;

using CommonFramework.Maths;

namespace CommonFramework.PointCloud
{
    /// <summary>
    /// CloudPoint is an implementation of IPositionable3D and IPositionableND. It represents a single point of data in a point cloud
    /// data set, storing position, normal, tangent.
    /// </summary>
    public class CloudPoint : IPositionable3D, IPositionableND
    {
        private Vector3 position;
        private Vector3 normal;
        private Vector4 tangent;
        private Color colour;

        /// <summary>
        /// Creates a new PointCloud with the specified position, normal and tangent.
        /// </summary>
        /// <param name="position">The position of the data point.</param>
        /// <param name="normal">The normal of the data point.</param>
        /// <param name="tangent">The tangent of the data point, a normalised vector of the cross product of up and the normal.</param>
        /// <param name="colour">The colour of the cloud point.</param>
        public CloudPoint(Vector3 position, Vector3 normal, Vector4 tangent, Color colour)
        {
            this.position = position;
            this.normal = normal;
            this.tangent = tangent;
            this.colour = colour;
        }

        /// <summary>
        /// Creates a new PointCloud with the specified position, normal and tangent.
        /// </summary>
        /// <param name="position">The position of the data point.</param>
        /// <param name="normal">The normal of the data point.</param>
        /// <param name="tangent">The tangent of the data point, a normalised vector of the cross product of up and the normal.</param>
        /// <param name="colour">The colour of the cloud point.</param>
        public CloudPoint(Float3 position, Float3 normal, Float3 tangent, Float3 colour)
        {
            this.position = new Vector3(position.X, position.Y, position.Z);
            this.normal = new Vector3(normal.X, normal.Y, normal.Z);
            this.tangent = new Vector4(tangent.X, tangent.Y, tangent.Z, 1.0f);
            this.colour = new Color(colour.X, colour.Y, colour.Z);
        }

        /// <summary>
        /// Sets the spatial value for a specified dimension of the CloudPoint position.
        /// </summary>
        /// <param name="dimension">The zero-based index of the dimension to set the spatial value for.</param>
        /// <param name="value">The spatial value to set on the specified dimension.</param>
        /// <remarks>This function will throw an exception if a dimension less than zero or above 2 is specified.</remarks>
        public void SetDimension(int dimension, float value)
        {
            if (dimension == 0) position.x = value;
            else if (dimension == 1) position.y = value;
            else if (dimension == 2) position.z = value;
            else throw new System.Exception("CloudPoint does not support the " + dimension + " dimension");
        }

        /// <summary>
        /// Gets the spatial value for a specified dimension of the CloudPoint position.
        /// </summary>
        /// <param name="dimension">The zero-based index of the dimension to get the spatial value for.</param>
        /// <returns>The spatial value of the specified dimension.</returns>
        /// <remarks>This function will throw an exception if a dimension less than zero or above 2 is specified.</remarks>
        public float GetDimension(int dimension)
        {
            if (dimension == 0) return position.x;
            else if (dimension == 1) return position.y;
            else if (dimension == 2) return position.z;
            else throw new System.Exception("CloudPoint does not support the " + dimension + " dimension");
        }

        /// <summary>
        /// Creates a nicely formatted string version of the CloudPoint, An example output could be:
        /// CloudPoint[position(1.0, 1.5, 2.0), normal(2.5, 3.0, 3.5), tangent(4.0, 4.5, 5.0, 5.5), colour(0.3, 0.6, 0.9, 1.0)]
        /// </summary>
        /// <returns>A nicely formatted string representation of the CloudPoint.</returns>
        public override string ToString()
        {
            return String.Format("CloudPoint[position{0}, normal{1}, tangent{2}, colour({3})]", position, normal, tangent, colour);
        }

        // ----- Properties ----- //

        /// <summary>
        /// Gets the number of dimensions of the CloudPoint, which is always 3.
        /// </summary>
        public int Dimensions
        {
            get { return 3; }
        }

        /// <summary>
        /// Sets or gets the X spatial position of the CloudPoint.
        /// </summary>
        public float X
        {
            set { position.x = value; }
            get { return position.x; }
        }

        /// <summary>
        /// Sets or gets the Y spatial position of the CloudPoint.
        /// </summary>
        public float Y
        {
            set { position.y = value; }
            get { return position.y; }
        }

        /// <summary>
        /// Sets or gets the Z spatial position of the CloudPoint.
        /// </summary>
        public float Z
        {
            set { position.z = value; }
            get { return position.z; }
        }

        /// <summary>
        /// Gets the position of the CloudPoint.
        /// </summary>
        public Vector3 Position
        {
            get { return position; }
        }

        /// <summary>
        /// Gets the normal of the CloudPoint.
        /// </summary>
        public Vector3 Normal
        {
            get { return normal; }
        }

        /// <summary>
        /// Gets the tangent to the normal of the CloudPoint.
        /// </summary>
        public Vector4 Tangent
        {
            get { return tangent; }
        }

        /// <summary>
        /// Gets the colour of the CloudPoint.
        /// </summary>
        public Color Colour
        {
            get { return colour; }
        }

        /// <summary>
        /// Sets or gets the spatial value position on a paticular dimension.
        /// </summary>
        /// <param name="dimension">The dimension to set or get the spatial value for.</param>
        /// <returns>The spatial value for a paticular dimension.</returns>
        public float this[int dimension]
        {
            set
            {
                if (dimension == 0) position.x = value;
                else if (dimension == 1) position.y = value;
                else if (dimension == 2) position.z = value;
                else throw new System.Exception("CloudPoint does not support the " + dimension + " dimension");
            }
            get
            {
                if (dimension == 0) return position.x;
                else if (dimension == 1) return position.y;
                else if (dimension == 2) return position.z;
                else throw new System.Exception("CloudPoint does not support the " + dimension + " dimension");
            }
        }
    }
}
