using UnityEngine;
using System.Collections.Generic;

namespace CommonFramework.MessageSystem
{
    /// <summary>
    /// IMessagable defines the contract for an object that is capable of sending and receiving messages.
    /// </summary>
    public interface IMessageable
    {
        /// <summary>
        /// Informs the IMessagable that it should handle and process the passed IMessage.
        /// </summary>
        /// <param name="message">The IMessage that the IMessagable should handle.</param>
        void ReceiveMessage(IMessage message);

        // ----- Properties ----- //

        /// <summary>
        /// Gets a Queue of the IMessages that this IMessagable wants to dispatch.
        /// </summary>
        Queue<IMessage> PendingMessages { get; }

        /// <summary>
        /// Gets the identifier to use when addressing the IMessageable.
        /// </summary>
        string MessageableName { get; }
    }
}