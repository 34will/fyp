﻿using UnityEngine;
using System.Collections.Generic;
using System;
using CommonFramework.Logging;

namespace CommonFramework.MessageSystem
{
    /// <summary>
    /// MessageHandler is a singleton that is responsible for the distribution of messages around the system.
    /// IMessageables can be registered and removed from the MessageHandler to change the clients that receive
    /// IMessages.
    /// </summary>
    public class MessageHandler
    {
        private static MessageHandler instance = new MessageHandler();
		private List<IMessageable> messageables = new List<IMessageable>();

        /// <summary>
        /// Informs the message handler to process messages on registered IMessageables.
        /// </summary>
        public void Update()
        {
            // Iterate over all the messageables with messages to send
            foreach (IMessageable source in messageables)
            {
                while (source.PendingMessages.Count > 0)
                {
                    //  Once a message is found send it out to all the other messageables
                    IMessage message = source.PendingMessages.Dequeue();
                    foreach (IMessageable target in messageables)
                    {
                        // Don't send it to itself
                        if (target != source)
                            target.ReceiveMessage(message);
                    }
                }
            }
        }

        /// <summary>
        /// Registers an IMessageable subscriber to the messaging system.
        /// </summary>
        /// <param name="messageable">The IMessageable to add to the messaging system.</param>
        public void RegisterMessageable(IMessageable messageable)
        {
            // Cannot add the messageable if it is already indexed
            if (messageables.Contains(messageable)) return;
            else messageables.Add(messageable);
        }

        /// <summary>
        /// Removes a subscriber from the MessageHandler if it exists in the registered subscribers.
        /// </summary>
        /// <param name="messageable">The IMessageable to remove from the messaging system.</param>
        public void RemoveMessageable(IMessageable messageable)
        {
            messageables.Remove(messageable);
        }

        /// <summary>
        /// Broadcasts an IMessage to all the IMessageables registered with the MessageHandler.
        /// </summary>
        /// <param name="message">The IMessage to broadcast to the IMessageables.</param>
        public void SendMessage(IMessage message)
        {
            foreach (IMessageable messageable in messageables)
                messageable.ReceiveMessage(message);
        }

        /// <summary>
        /// Sends an IMessage to all IMessageables whos MessageableName matches that of the passed identifier.
        /// </summary>
        /// <param name="message">The IMessage to send to the target IMessageable.</param>
        /// <param name="targetMessageable">The MessageableName identifier of the target IMessageable.</param>
        public void SendMessage(IMessage message, string targetMessageable)
        {
            try
            {
                foreach (IMessageable messageable in messageables)
                {
                    if (messageable.MessageableName == targetMessageable)
                        messageable.ReceiveMessage(message);
                }
            }
            catch (Exception e)
            {
                MessageHandler.Instance.SendMessage(new LogMessage(string.Format("Error: {0}", e.Message)));
            }
        }

        // ----- Properties ----- //

        /// <summary>
        /// Gets the singleton instance of the MessageHandler.
        /// </summary>
        public static MessageHandler Instance
        {
            get { return instance; }
        }

		public int NumberOfRegisteredIMessageables
		{
			get { return messageables.Count; }
		}
    }
}