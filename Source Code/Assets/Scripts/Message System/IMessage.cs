using UnityEngine;
using System.Collections;

namespace CommonFramework.MessageSystem
{
    /// <summary>
    /// IMessage defines the minimum contract for a message implemetation to be used with IMessageable.
    /// </summary>
    public interface IMessage
    {
        /// <summary>
        /// Gets the named identifier of the IMessage.
        /// </summary>
        string MessageName { get; }

        /// <summary>
        /// Gets the source IMessageable object the message was sent from.
        /// </summary>
        IMessageable Source { get; set; }
    }
}