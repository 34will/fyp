﻿using System;
using System.Diagnostics.CodeAnalysis;

using UnityEngine;

namespace CommonFramework.MessageSystem
{
    /// <summary>
    /// MessagePump is a MonoBehaviour that wraps the MessageHandler singleton for use in Unity.
    /// </summary>
	[ExcludeFromCodeCoverage]
	public class MessagePump : MonoBehaviour
    {
        /// <summary>
        /// Starts the MessageHandler singleton indicating the directory it executes in.
        /// </summary>
        void Start()
        {
            //Debug.Log("Start on MessagePump called in: \n" + Assembly.GetExecutingAssembly().Location);
        }

        /// <summary>
        /// Calls the Update function of the MessageHandler singleton.
        /// </summary>
        void Update()
        {
            MessageHandler.Instance.Update();
        }
    }
}