﻿using System;
using System.Collections.Generic;

using UnityEngine;

namespace CommonFramework
{
	/// <summary>
	/// MonotoneChainConvexHullComparer is used in sorting a list of 2D points by x, then y, for use with Andrew's Monotone Chain Convex Hull Algorithm.
	/// </summary>
	public class MonotoneChainConvexHullComparer : IComparer<Vector2>
	{
		/// <summary>
		/// Compares the two provided vectors, returning a value indicating their relative values.
		/// </summary>
		/// <param name="a">The first Vector2 to compare.</param>
		/// <param name="b">The second Vector2 to compare.</param>
		/// <returns>A signed integer where: a value less than zero means that a.x is less than b.x, or if they are equal, that a.y is less than b.y, a value of zero means a.x is equal to b.x and a.y is equal to b.y, and a value greater than zero means that a.x is greater than b.x, or if they are equal, that a.y is greater than b.y.</returns>
		public int Compare(Vector2 a, Vector2 b)
		{
			int xComp = a.x.CompareTo(b.x);
			return xComp == 0 ? a.y.CompareTo(b.y) : xComp;
		}
	}
}
