﻿using UnityEngine;
using System.Diagnostics.CodeAnalysis;

namespace CommonFramework
{
	public static class GUIHelper
	{
		[ExcludeFromCodeCoverage]
		public static float FloatSliderAndTextBox(Vector2 pos, float inputValue, float min, float max, float sliderWidth = 100.0f, float textBoxWidth = 40.0f, string ToStringFormat = "0.00")
		{
			float value = GUI.HorizontalSlider(new Rect(pos.x, pos.y + 5.0f, sliderWidth, 21), inputValue, min, max);
			string valueString = GUI.TextField(new Rect(pos.x + 5.0f + sliderWidth, pos.y, textBoxWidth, 21), value.ToString(ToStringFormat));
			float outValue;
			if(float.TryParse(valueString, out outValue) && outValue >= min && outValue <= max)
				value = outValue;

			return value;
		}

		[ExcludeFromCodeCoverage]
		public static int IntSliderAndTextBox(Vector2 pos, int inputValue, int min, int max, float sliderWidth = 100.0f, float textBoxWidth = 40.0f)
		{
			int value = Mathf.RoundToInt(GUI.HorizontalSlider(new Rect(pos.x, pos.y + 5.0f, sliderWidth, 21), inputValue, min, max));
			string valueString = GUI.TextField(new Rect(pos.x + 5.0f + sliderWidth, pos.y, textBoxWidth, 21), value.ToString());
			int outValue;
			if(int.TryParse(valueString, out outValue) && outValue >= min && outValue <= max)
				value = outValue;

			return value;
		}
	}
}
