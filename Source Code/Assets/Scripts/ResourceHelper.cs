﻿using System;
using System.IO;
using System.Xml;
using System.Diagnostics.CodeAnalysis;

using UnityEngine;

using CommonFramework.MessageSystem;
using CommonFramework.Logging;

namespace CommonFramework
{
    [ExcludeFromCodeCoverage]
    public static class ResourceHelper
    {
        /// <summary>
        /// Loads a Resource file as an XMLDocument.
        /// </summary>
        /// <param name="filenameWithoutExt">Path relative to Resources folder and name of file without extension.</param>
        /// <param name="errorMessageType">Log Message classification if file cannot be loaded.</param>
        /// <returns>Loaded XmlDocument or null the file cannot be found or loaded.</returns>
        public static XmlDocument LoadXMLResource(string filenameWithoutExt, LogClassification errorMessageType = LogClassification.Error)
        {
            if (filenameWithoutExt == null || filenameWithoutExt == "")
            {
                MessageHandler.Instance.SendMessage(new LogMessage("Trying to load null or empty filename.", errorMessageType));
                return null;
            }

            TextAsset file = Resources.Load<TextAsset>(filenameWithoutExt);
            if (file == null || file.bytes == null || file.bytes.Length <= 0)
            {
                MessageHandler.Instance.SendMessage(new LogMessage("File " + filenameWithoutExt + " is non-existant or empty or an incompatible file type.", errorMessageType));
                return null;
            }

            MemoryStream ms = new MemoryStream(file.bytes);
            if (ms == null || !ms.CanRead)
            {
                MessageHandler.Instance.SendMessage(new LogMessage("File " + filenameWithoutExt + " cannot be converted into a MemoryStream.", errorMessageType));
                return null;
            }

            XmlDocument osmDoc = new XmlDocument();
            osmDoc.Load(ms);

            return osmDoc;
        }
    }
}
