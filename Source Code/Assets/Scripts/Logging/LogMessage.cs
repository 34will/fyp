﻿using UnityEngine;
using System.Collections;
using System.Diagnostics.CodeAnalysis;

using CommonFramework.MessageSystem;

namespace CommonFramework.Logging
{
    /// <summary>
    /// LogMessage is an implementation of IMessage that is detected primarily by the Logging system to report events in the system.
    /// </summary>
    public class LogMessage : IMessage
    {
        internal const string messageName = "LogMessage";
        private IMessageable source = null;
        private string message = "";
        private LogClassification classification = LogClassification.Information;

        /// <summary>
        /// Creates a new LogMessage with the specified message, log classification of Information and no source IMessageable component
        /// </summary>
        /// <param name="message">The message to log</param>
        public LogMessage(string message)
            : this(message, LogClassification.Information, null) { }

        /// <summary>
        /// Creates a new LogMessage with the specified message, classification with no source IMessageable component
        /// </summary>
        /// <param name="message">The message to log</param>
        /// <param name="classification">The classification of the message</param>
        public LogMessage(string message, LogClassification classification)
            : this(message, classification, null) { }

        /// <summary>
        /// Creates a new LogMessage with the specified message and source IMessageable component defaulting to Information log classification
        /// </summary>
        /// <param name="message">The message to log</param>
        /// <param name="source">The source IMessageable component</param>
        public LogMessage(string message, IMessageable source)
            : this(message, LogClassification.Information, source) { }

        /// <summary>
        /// Creates a new LogMessage with the specified message, classification and source IMessageable component
        /// </summary>
        /// <param name="message">The message to log</param>
        /// <param name="classification">The classification of the message</param>
        /// <param name="source">The source IMessageable component</param>
        public LogMessage(string message, LogClassification classification, IMessageable source)
        {
            this.message = message;
            this.classification = classification;
            this.source = source;
        }

        // ----- Properties ----- //

        /// <summary>
        /// Gets the string message that needs to be logged
        /// </summary>
        public string Message
        {
            get { return message; }
        }

        /// <summary>
        /// Gets the LogClassification that the LogMessage is associated with
        /// </summary>
        public LogClassification Classification
        {
            get { return classification; }
        }

        /// <summary>
        /// Gets the name of the IMessage, LogMessage
        /// </summary>
        [ExcludeFromCodeCoverage]
        public string MessageName
        {
            get { return messageName; }
        }

        /// <summary>
        /// Sets or Gets the source IMessageable component of the IMessage
        /// </summary>
        public IMessageable Source
        {
            set { source = value; }
            get { return source; }
        }
    }
}