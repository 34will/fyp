﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

using CommonFramework.MessageSystem;

namespace CommonFramework.Logging
{
    [ExcludeFromCodeCoverage]
    public class ConsoleLogger : MonoBehaviour, IMessageable
    {
        internal const string messageableName = "ConsoleLogger";
        private Queue<IMessage> pendingMessages = new Queue<IMessage>();

        void Awake()
        {
            MessageHandler.Instance.RegisterMessageable(this);
        }

        /// <summary>
        /// Informs the ConsoleLogger that it needs to handle and process an IMessage. The logger expects messages of type LogMessage
        /// </summary>
        /// <param name="message">The IMessage that the ConsoleLogger needs to process.</param>
        public void ReceiveMessage(IMessage message)
        {
            if (message.MessageName == "LogMessage")
            {
                // Converts the IMessage to the LogMessage
                LogMessage logMessage = message as LogMessage;
                if (logMessage == null) return;

                // Print the message to the console
                Console.WriteLine(logMessage.Classification.ToString() + ": " + logMessage.Message);
            }
        }

        // ----- Properties ----- //

        /// <summary>
        /// Gets the queue of pending IMessages that need to be read from the ConsoleLogger
        /// </summary>
        public Queue<IMessage> PendingMessages
        {
            get { return pendingMessages; }
        }

        /// <summary>
        /// Gets the name messageable name of the IMessageable, ConsoleLogger
        /// </summary>
        public string MessageableName
        {
            get { return messageableName; }
        }
    }
}