﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using UnityEngine;

using CommonFramework.MessageSystem;

namespace CommonFramework.Logging
{
	[ExcludeFromCodeCoverage]
    public class UnityLogger : MonoBehaviour, IMessageable
    {
        internal const string messageableName = "UnityLogger";
        private Queue<IMessage> pendingMessages = new Queue<IMessage>();

        void Awake()
        {
            MessageHandler.Instance.RegisterMessageable(this);
        }

        /// <summary>
        /// Informs the UnityLogger that it needs to handle and process an IMessage. The logger expects messages of type LogMessage.
        /// </summary>
        /// <param name="message">The IMessage that the UnityLogger needs to process.</param>
        public void ReceiveMessage(IMessage message)
        {
            if(message.MessageName == "LogMessage")
            {
                // Converts the IMessage to the LogMessage
                LogMessage logMessage = message as LogMessage;
                if (logMessage == null) return;

                // Print the message to the Unity console
                switch(logMessage.Classification)
                {
                    case LogClassification.Error:
                        Debug.LogError(logMessage.Message); break;
                    case LogClassification.Exception:
                        Debug.LogException(new Exception(logMessage.Message)); break;
                    case LogClassification.Warning:
                        Debug.LogWarning(logMessage.Message); break;
                    default:
                        Debug.Log(logMessage.Message); break;
                }
            }
        }

        // ----- Properties ----- //

        /// <summary>
        /// Gets the queue of pending IMessages that need to be read from the UnityLogger.
        /// </summary>
        public Queue<IMessage> PendingMessages
        {
            get { return pendingMessages; }
        }

        /// <summary>
        /// Gets the name messageable name of the IMessageable, UnityLogger.
        /// </summary>
        public string MessageableName
        {
            get { return messageableName; }
        }
    }
}
