﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;

namespace CommonFramework.Logging
{
    /// <summary>
    /// LogClassification specifies an enumeration of values that a LogMessage can be associated with
    /// </summary>
    public enum LogClassification
    {
        /// <summary>Specifies that the log message is to provide information to the output</summary>
        Information,
        /// <summary>Specifies that the log message is to inform that a warning has been triggered</summary>
        Warning,
        /// <summary>Specifies that the log message is to inform that an error has occured</summary>
        Error,
        /// <summary>Specifies that the log message is to inform that a crash has occured</summary>
        Crash,
        /// <summary>Specifies that the log message is to inform that an exception has been thrown</summary>
        Exception,
        /// <summary>Specifies that the log message is to assert information</summary>
        Assert
    }
}
