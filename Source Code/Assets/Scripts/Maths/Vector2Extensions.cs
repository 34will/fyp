﻿using System;

using UnityEngine;

namespace CommonFramework.Maths
{
	/// <summary>
	/// Provides extension methods for the Vector2 struct.
	/// </summary>
	public static class Vector2Extensions
	{
		/// <summary>
		/// Returns a Vector3 with the X and Y components set to the X and Y components of this, and the Z component set to the passed value.
		/// </summary>
		/// <param name="value">The Vector2 to convert.</param>
		/// <param name="z">Z value.</param>
		/// <returns>A Vector3 of { this.x, this.y, z }.</returns>
		public static Vector3 ToV3XY(this Vector2 value, float z = 0.0f)
		{
			return new Vector3(value.x, value.y, z);
		}

		/// <summary>
		/// Returns a Vector3 with the X and Y components set to the Y and X components of this, and the Z component set to the passed value.
		/// </summary>
		/// <param name="value">The Vector2 to convert.</param>
		/// <param name="z">Z value.</param>
		/// <returns>A Vector3 of { this.y, this.x, z }.</returns>
		public static Vector3 ToV3YX(this Vector2 value, float z = 0.0f)
		{
			return new Vector3(value.y, value.x, z);
		}

		/// <summary>
		/// Returns a Vector3 with the X and Z components set to the X and Y components of this, and the Y component set to the passed value.
		/// </summary>
		/// <param name="value">The Vector2 to convert.</param>
		/// <param name="y">Y value.</param>
		/// <returns>A Vector3 of { this.x, y, this.y }.</returns>
		public static Vector3 ToV3XZ(this Vector2 value, float y = 0.0f)
		{
			return new Vector3(value.x, y, value.y);
		}

		/// <summary>
		/// Returns a Vector3 with the X and Z components set to the Y and X components of this, and the Y component set to the passed value.
		/// </summary>
		/// <param name="value">The Vector2 to convert.</param>
		/// <param name="y">Y value.</param>
		/// <returns>A Vector3 of { this.y, y, this.x }.</returns>
		public static Vector3 ToV3ZX(this Vector2 value, float y = 0.0f)
		{
			return new Vector3(value.y, y, value.x);
		}

		/// <summary>
		/// Returns a Vector3 with the Y and Z components set to the X and Y components of this, and the X component set to the passed value.
		/// </summary>
		/// <param name="value">The Vector2 to convert.</param>
		/// <param name="x">X value.</param>
		/// <returns>A Vector3 of { x, this.x, this.y }.</returns>
		public static Vector3 ToV3YZ(this Vector2 value, float x = 0.0f)
		{
			return new Vector3(x, value.x, value.y);
		}

		/// <summary>
		/// Returns a Vector3 with the Y and Z components set to the Y and X components of this, and the X component set to the passed value.
		/// </summary>
		/// <param name="value">The Vector2 to convert.</param>
		/// <param name="x">X value.</param>
		/// <returns>A Vector3 of { x, this.y, this.x }.</returns>
		public static Vector3 ToV3ZY(this Vector2 value, float x = 0.0f)
		{
			return new Vector3(x, value.y, value.x);
		}

		public static Vector2 Tangent(this Vector2 value)
		{
			return new Vector2(-value.y, value.x);
		}
	}
}
