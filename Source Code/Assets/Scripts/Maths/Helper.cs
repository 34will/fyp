﻿using System;
using System.Collections.Generic;

using UnityEngine;

namespace CommonFramework.Maths
{
	/// <summary>
	/// Helper provides useful maths functions not provided by the default System.Math class.
	/// </summary>
	public static class Helper
	{
		private static System.Random random = new System.Random();

		/// <summary>
		/// The double value of the mathematical constant √2.
		/// </summary>
		public static readonly double RootTwo = 1.41421356237;

		/// <summary>
		/// The float value of the mathematical constant √2.
		/// </summary>
		public static readonly float RootTwoF = 1.41421356237f;

		/// <summary>
		/// The double value of the mathematical constant 1/√2.
		/// </summary>
		public static readonly double OneOverRootTwo = 0.70710678118;

		/// <summary>
		/// The float value of the mathematical constant 1/√2.
		/// </summary>
		public static readonly float OneOverRootTwoF = 0.70710678118f;

		/// <summary>
		/// The double value of the mathematical constant π / 2.
		/// </summary>
		public static readonly double HalfPi = 1.57079632679;

		/// <summary>
		/// The double value of the mathematical constant π.
		/// </summary>
		public static readonly double Pi = 3.14159265359;

		/// <summary>
		/// The double value of double the mathematical constant 3π / 2.
		/// </summary>
		public static readonly double ThreeHalfsPi = 4.71238898038;

		/// <summary>
		/// The double value of double the mathematical constant π.
		/// </summary>
		public static readonly double TwoPi = 6.28318530718;

		/// <summary>
		/// The double value to multiply an angle stored in radians by to convert it to degrees.
		/// </summary>
		public static readonly double RadiansToDegreesMultiplier = 180.0 / Pi;

		/// <summary>
		/// The double value to multiply an angle stored in degrees by to convert it to radians.
		/// </summary>
		public static readonly double DegreesToRadiansMultiplier = Pi / 180.0;

		/// <summary>
		/// A Vector2 with all dimensions set to float.NaN.
		/// </summary>
		public static readonly Vector2 NaV2 = new Vector2(float.NaN, float.NaN);

		/// <summary>
		/// A Vector3 with all dimensions set to float.NaN.
		/// </summary>
		public static readonly Vector3 NaV3 = new Vector3(float.NaN, float.NaN, float.NaN);

		/// <summary>
		/// Checks if the supplied Vector2 is MathHelper.NaV.
		/// </summary>
		/// <param name="v">Vector2 to check.</param>
		/// <returns>True if v is NaV, false if not.</returns>
		public static bool IsNaV(Vector2 v)
		{
			return (float.IsNaN(v.x) && float.IsNaN(v.y));
		}

		/// <summary>
		/// Checks if any elements of the supplied Vector2 are set to float.NaN.
		/// </summary>
		/// <param name="v">Vector2 to check.</param>
		/// <returns>True if any element of v is float.NaN, false if not.</returns>
		public static bool HasNaN(Vector2 v)
		{
			return (float.IsNaN(v.x) || float.IsNaN(v.y));
		}

		/// <summary>
		/// Checks if the supplied Vector2 has all elements set to float.PositiveInfinity or float.NegativeInfinity.
		/// </summary>
		/// <param name="v">Vector2 to check.</param>
		/// <returns>True if all elements of v are float.PositiveInfinity or float.NegativeInfinity, false if not.</returns>
		public static bool IsInfinite(Vector2 v)
		{
			return (float.IsInfinity(v.x) && float.IsInfinity(v.y));
		}

		/// <summary>
		/// Checks if any elements of the supplied Vector2 are set to float.PositiveInfinity or float.NegativeInfinity.
		/// </summary>
		/// <param name="v">Vector2 to check.</param>
		/// <returns>True if any element of v is float.PositiveInfinity or float.NegativeInfinity, false if not.</returns>
		public static bool HasInfinite(Vector2 v)
		{
			return (float.IsInfinity(v.x) || float.IsInfinity(v.y));
		}

		/// <summary>
		/// Checks if the supplied Vector2 is valid by performing the IsNaV, IsInfinite and HasInfinite checks.
		/// </summary>
		/// <param name="v">Vector2 to check.</param>
		/// <returns>True if v fails all of the checks, false if any of them are true.</returns>
		public static bool IsValid(Vector2 v)
		{
			return !(IsNaV(v) || HasNaN(v) || IsInfinite(v) || HasInfinite(v));
		}

		/// <summary>
		/// Checks if the supplied Vector2 has all elements set to 0.0f.
		/// </summary>
		/// <param name="v">Vector2 to check.</param>
		/// <returns>True if all elements of v are 0.0f, false if not.</returns>
		public static bool IsZero(Vector2 v)
		{
			return v.x == 0.0f && v.y == 0.0f;
		}

		/// <summary>
		/// Checks if the supplied Vector2 has all elements set to 1.0f.
		/// </summary>
		/// <param name="v">Vector2 to check.</param>
		/// <returns>True if all elements of v are 1.0f, false if not.</returns>
		public static bool IsOne(Vector2 v)
		{
			return v.x == 1.0f && v.y == 1.0f;
		}

		/// <summary>
		/// Checks if the supplied Vector2 has all elements set to -1.0f.
		/// </summary>
		/// <param name="v">Vector2 to check.</param>
		/// <returns>True if all elements of v are -1.0f, false if not.</returns>
		public static bool IsNegativeOne(Vector2 v)
		{
			return v.x == -1.0f && v.y == -1.0f;
		}

		/// <summary>
		/// Checks if the supplied Vector3 is MathHelper.NaV.
		/// </summary>
		/// <param name="v">Vector3 to check.</param>
		/// <returns>True if v is NaV, false if not.</returns>
		public static bool IsNaV(Vector3 v)
		{
			return (float.IsNaN(v.x) && float.IsNaN(v.y) && float.IsNaN(v.z));
		}

		/// <summary>
		/// Checks if any elements of the supplied Vector3 are set to float.NaN.
		/// </summary>
		/// <param name="v">Vector3 to check.</param>
		/// <returns>True if any element of v is float.NaN, false if not.</returns>
		public static bool HasNaN(Vector3 v)
		{
			return (float.IsNaN(v.x) || float.IsNaN(v.y) || float.IsNaN(v.z));
		}

		/// <summary>
		/// Checks if the supplied Vector3 has all elements set to float.PositiveInfinity or float.NegativeInfinity.
		/// </summary>
		/// <param name="v">Vector3 to check.</param>
		/// <returns>True if all elements of v are float.PositiveInfinity or float.NegativeInfinity, false if not.</returns>
		public static bool IsInfinite(Vector3 v)
		{
			return (float.IsInfinity(v.x) && float.IsInfinity(v.y) && float.IsInfinity(v.z));
		}

		/// <summary>
		/// Checks if any elements of the supplied Vector3 are set to float.PositiveInfinity or float.NegativeInfinity.
		/// </summary>
		/// <param name="v">Vector3 to check.</param>
		/// <returns>True if any element of v is float.PositiveInfinity or float.NegativeInfinity, false if not.</returns>
		public static bool HasInfinite(Vector3 v)
		{
			return (float.IsInfinity(v.x) || float.IsInfinity(v.y) || float.IsInfinity(v.z));
		}

		/// <summary>
		/// Checks if the supplied Vector3 is valid by performing the IsNaV, IsInfinite and HasInfinite checks.
		/// </summary>
		/// <param name="v">Vector3 to check.</param>
		/// <returns>True if v fails all of the checks, false if any of them are true.</returns>
		public static bool IsValid(Vector3 v)
		{
			return !(IsNaV(v) || HasNaN(v) || IsInfinite(v) || HasInfinite(v));
		}

		/// <summary>
		/// Checks if the supplied Vector3 has all elements set to 0.0f.
		/// </summary>
		/// <param name="v">Vector3 to check.</param>
		/// <returns>True if all elements of v are 0.0f, false if not.</returns>
		public static bool IsZero(Vector3 v)
		{
			return v.x == 0.0f && v.y == 0.0f && v.z == 0.0f;
		}

		/// <summary>
		/// Checks if the supplied Vector3 has all elements set to 1.0f.
		/// </summary>
		/// <param name="v">Vector3 to check.</param>
		/// <returns>True if all elements of v are 1.0f, false if not.</returns>
		public static bool IsOne(Vector3 v)
		{
			return v.x == 1.0f && v.y == 1.0f && v.z == 1.0f;
		}

		/// <summary>
		/// Checks if the supplied Vector3 has all elements set to -1.0f.
		/// </summary>
		/// <param name="v">Vector3 to check.</param>
		/// <returns>True if all elements of v are -1.0f, false if not.</returns>
		public static bool IsNegativeOne(Vector3 v)
		{
			return v.x == -1.0f && v.y == -1.0f && v.z == -1.0f;
		}

		/// <summary>
		/// Converts an angle in radians into an angle in degrees.
		/// </summary>
		/// <param name="radians">The angle, in radians, be converted into degrees.</param>
		/// <returns>The converted angle, in degrees./returns>
		public static double RadiansToDegrees(double radians)
		{
			return radians * RadiansToDegreesMultiplier;
		}

		/// <summary>
		/// Converts an angle in degrees into an angle in radians.
		/// </summary>
		/// <param name="degrees">The angle, in degrees, be converted into radians.</param>
		/// <returns>The converted angle, in radians.</returns>
		public static double DegreesToRadians(double degrees)
		{
			return degrees * DegreesToRadiansMultiplier;
		}

		/// <summary>
		/// Modulates an angle in radians to be within the range: -2π to 2π.
		/// </summary>
		/// <param name="radians">Angle to modulate.</param>
		/// <returns>The modulated angle in radians.</returns>
		public static double ModulateAngleRadians(double radians)
		{
			return radians % (radians < 0.0 ? -TwoPi : TwoPi);
		}

		/// <summary>
		/// Modulates an angle in degrees to be within the range: -360 to 360.
		/// </summary>
		/// <param name="radians">Angle to modulate.</param>
		/// <returns>The modulated angle in degrees.</returns>
		public static double ModulateAngleDegrees(double degrees)
		{
			return degrees % (degrees < 0.0 ? -360.0 : 360.0);
		}

		/// <summary>
		/// Generates a random number between the specified range. The random number generator is seeded during the static constructor.
		/// </summary>
		/// <param name="min">Inclusive minimum int value to return.</param>
		/// <param name="max">Exclusive maximum int value to return.</param>
		/// <returns>A random int equal to or greater than min and less than max, or throws an ArgumentOutOfRangeException if max is less than min.</returns>
		public static int RandomRange(int min, int max)
		{
			return random.Next(min, max);
		}

		/// <summary>
		/// Generates a random number between the specified range. The random number generator is seeded during the static constructor.
		/// </summary>
		/// <param name="min">Inclusive minimum float value to return.</param>
		/// <param name="max">Exclusive maximum float value to return.</param>
		/// <returns>A random float equal to or greater than min and less than max, or float.NaN if max is less than min, min is not a valid number, and/or max is not a valid number.</returns>
		public static float RandomRange(float min, float max)
		{
			if(max < min || float.IsNaN(min) || float.IsNaN(max) || float.IsInfinity(min) || float.IsInfinity(max))
				return float.NaN;

			return min + ((float)random.NextDouble() * (max - min));
		}

		/// <summary>
		/// Generates a random number between the specified range. The random number generator is seeded during the static constructor.
		/// </summary>
		/// <param name="min">Inclusive minimum double value to return.</param>
		/// <param name="max">Exclusive maximum double value to return.</param>
		/// <returns>A random double equal to or greater than min and less than max, or double.NaN if max is less than min, min is not a valid number, and/or max is not a valid number.</returns>
		public static double RandomRange(double min, double max)
		{
			if(max < min || double.IsNaN(min) || double.IsNaN(max) || double.IsInfinity(min) || double.IsInfinity(max))
				return double.NaN;

			return min + (random.NextDouble() * (max - min));
		}

		/// <summary>
		/// Calculates the intersection point of the provided line segments.
		/// </summary>
		/// <returns>A Vector2 of the intersection point. If the segements do not intersect, a NaN Vector2 is returned.</returns>
		/// <param name="aX">X-Coord of the start point of the first line segment.</param>
		/// <param name="aY">Y-Coord of the start point of the first line segment.</param>
		/// <param name="bX">X-Coord of the end point of the first line segment.</param>
		/// <param name="bY">Y-Coord of the end point of the first line segment.</param>
		/// <param name="cX">X-Coord of the start point of the second line segment.</param>
		/// <param name="cY">Y-Coord of the start point of the second line segment.</param>
		/// <param name="dX">X-Coord of the end point of the second line segment.</param>
		/// <param name="dY">Y-Coord of the end point of the second line segment.</param>
		public static Vector2 IntersectionPoint(float aX, float aY, float bX, float bY, float cX, float cY, float dX, float dY)
		{
			return IntersectionPoint(new Vector2(aX, aY), new Vector2(bX, bY), new Vector2(cX, cY), new Vector2(dX, dY));
		}

		/// <summary>
		/// Calculates the intersection point of the provided line segments.
		/// </summary>
		/// <returns>A Vector2 of the intersection point. If the segements do not intersect, a NaN Vector2 is returned.</returns>
		/// <param name="a">Start point of the first line segment.</param>
		/// <param name="b">End point of the first line segment.</param>
		/// <param name="c">Start point of the second line segment.</param>
		/// <param name="d">End point of the second line segment.</param>
		public static Vector2 IntersectionPoint(Vector2 a, Vector2 b, Vector2 c, Vector2 d)
		{
			Vector2 ab = b - a;
			Vector2 cd = d - c;

			double rCrossS = TwoDCross(Vector2.zero, ab, cd);

			if(rCrossS == 0.0)
				return new Vector2(float.NaN, float.NaN);
			else
			{
				Vector2 ac = c - a;
				double u = TwoDCross(Vector2.zero, ac, ab) / rCrossS;
				double t = TwoDCross(Vector2.zero, ac, cd) / rCrossS;

				return (u >= 0.0 && u <= 1.0 && t >= 0.0 && t <= 1.0) ? c + ((float)u * cd) : new Vector2(float.NaN, float.NaN);
			}
		}

		/// <summary>
		/// 2D cross product of oa and ob vectors.
		/// </summary>
		/// <param name="o">Common point.</param>
		/// <param name="a">Vector from point o to point a.</param>
		/// <param name="b">Vector from point o to point b.</param>
		/// <returns>A double. Positive if oab makes a counter-clockwise turn, negative if clockwise turn, and zero if the points are collinear</returns>
		public static double TwoDCross(Vector2 o, Vector2 a, Vector2 b)
		{
			return ((a.x - o.x) * (b.y - o.y)) - ((a.y - o.y) * (b.x - o.x));
		}

		/// <summary>
		/// Calculates the angle between the specified vectors. The angle calculated is from Vector a to Vector b.
		/// </summary>
		/// <param name="a">First Vector2 to calculate from.</param>
		/// <param name="b">Second Vector2 to calculate from.</param>
		/// <param name="inRadians">Whether or not to return angle in radians. Defaults to true.</param>
		/// <param name="lowerBoundIsZero">Whether or not to return angle in range: 0 to 2π/360 or the range: -π/-180 to π/180. Defaults to false.</param>
		/// <returns>Angle between, as a double, or if either or both of the vectors are invalid, double.NaN.</returns>
		public static double AngleBetween(Vector2 a, Vector2 b, bool inRadians = true, bool lowerBoundIsZero = false)
		{
			if(!IsValid(a) || !IsValid(b))
				return double.NaN;

			double angle = Math.Atan2(a.y, a.x) - Math.Atan2(b.y, b.x);
			angle = Helper.ModulateAngleRadians(angle);
			if(lowerBoundIsZero && angle < 0)
				angle = Helper.TwoPi + angle;

			return inRadians ? angle : Helper.RadiansToDegrees(angle);
		}

		/// <summary>
		/// Calculates the angle between the specified vectors. They will be normalised if necessary.
		/// </summary>
		/// <param name="a">First Vector2 to calculate from.</param>
		/// <param name="b">Second Vector2 to calculate from.</param>
		/// <returns>Normalised Vector2 pointing halfway between a and b.</returns>
		public static Vector2 HalfwayBetween(Vector2 a, Vector2 b)
		{
			if(!IsValid(a) || !IsValid(b))
				return Helper.NaV2;

			if(a.sqrMagnitude != 1)
				a.Normalize();
			if(b.sqrMagnitude != 1)
				b.Normalize();

			return (a + b).normalized;
		}

		/// <summary>
		/// Calculates the angle between the specified vectors. They will be normalised if necessary.
		/// </summary>
		/// <param name="a">First Vector3 to calculate from.</param>
		/// <param name="b">Second Vector3 to calculate from.</param>
		/// <returns>Normalised Vector3 pointing halfway between a and b.</returns>
		public static Vector3 HalfwayBetween(Vector3 a, Vector3 b)
		{
			if(!IsValid(a) || !IsValid(b))
				return Helper.NaV3;

			if(a.sqrMagnitude != 1)
				a.Normalize();
			if(b.sqrMagnitude != 1)
				b.Normalize();

			return (a + b).normalized;
		}

		/// <summary>
		/// Computes the convex hull of the provided collection of points, using Andrew'cd Monotone Chain 2D Convex Hull Algorithm. (Implementation from here: http://en.wikibooks.org/wiki/Algorithm_Implementation/Geometry/Convex_hull/Monotone_chain).
		/// </summary>
		/// <param name="points">The collection points to get the convex hull of. Does not need to be sorted.</param>
		/// <returns>A List of Vector2s describing the convex hull of the provided points, or an empty list if there are no points.</returns>
		public static List<Vector2> ConvexHull(ICollection<Vector2> points)
		{
			if(points == null)
				return new List<Vector2>();

			int count = points.Count;
			if(count > 1)
			{
				List<Vector2> sortedPoints = new List<Vector2>(points);
				sortedPoints.Sort(new MonotoneChainConvexHullComparer());
				Vector2[] tempHull = new Vector2[count * 2];

				int k = 0;
				for(int i = 0; i < count; ++i)
				{
					while(k >= 2 && Helper.TwoDCross(tempHull[k - 2], tempHull[k - 1], sortedPoints[i]) <= 0)
						k--;

					tempHull[k++] = sortedPoints[i];
				}

				for(int i = count - 2, t = k + 1; i >= 0; i--)
				{
					while(k >= t && Helper.TwoDCross(tempHull[k - 2], tempHull[k - 1], sortedPoints[i]) <= 0)
						k--;

					tempHull[k++] = sortedPoints[i];
				}

				List<Vector2> hull = new List<Vector2>(tempHull);
				hull.RemoveRange(k, (count * 2) - k);
				return hull;
			}
			else if(count == 1)
				return new List<Vector2>(points);
			else
				return new List<Vector2>();
		}

		private static Vector2 Interpolate(Vector2[] p, float[] time, float t)
		{
			if(p == null || p.Length != 4 || time == null || time.Length != 4 || Helper.IsNaV(p[0]) || Helper.IsNaV(p[1]) || Helper.IsNaV(p[2]) || Helper.IsNaV(p[3]) || float.IsNaN(time[0]) || float.IsNaN(time[1]) || float.IsNaN(time[2]) || float.IsNaN(time[3]) || float.IsNaN(t))
				return Helper.NaV2;

			float t2MT = time[2] - t;
			float t3MT = time[3] - t;
			float tMT0 = t - time[0];
			float tMT1 = t - time[1];
			float t1MT0 = time[1] - time[0];
			float t2MT0 = time[2] - time[0];
			float t2MT1 = time[2] - time[1];
			float t3MT1 = time[3] - time[1];
			float t3MT2 = time[3] - time[2];

			Vector2 A01 = (p[0] * ((time[1] - t) / t1MT0)) + (p[1] * (tMT0 / t1MT0));
			Vector2 A12 = (p[1] * (t2MT / t2MT1)) + (p[2] * (tMT1 / t2MT1));
			Vector2 A23 = (p[2] * (t3MT / t3MT2)) + (p[3] * ((t - time[2]) / t3MT2));
			Vector2 B012 = (A01 * (t2MT / t2MT0)) + (A12 * (tMT0 / t2MT0));
			Vector2 B123 = (A12 * (t3MT / t3MT1)) + (A23 * (tMT1 / t3MT1));
			return (B012 * (t2MT / t2MT1)) + (B123 * (tMT1 / t2MT1));
		}

		private static List<Vector2> CalculateCatmullRomSpline(Vector2 p0, Vector2 p1, Vector2 p2, Vector2 p3, float delta, int pointsPerSegment)
		{
			List<Vector2> result = new List<Vector2>();
			Vector2[] p = new Vector2[4] { p0, p1, p2, p3 };
			float[] time = new float[4] { 0.0f, 1.0f, 2.0f, 3.0f };

			float power = 0.5f * delta;
			for(int i = 1; i < 4; i++)
				time[i] = time[i - 1] + Mathf.Pow((p[i] - p[i - 1]).sqrMagnitude, power);

			int segments = pointsPerSegment - 1;
			result.Add(p1);

			for(int i = 1; i < segments; i++)
				result.Add(Interpolate(p, time, time[1] + (i * (time[2] - time[1])) / segments));

			result.Add(p2);

			return result;
		}

		public static List<Vector2> CatmullRomSpline(IList<Vector2> points, float delta, int numberOfCurveSections)
		{
			if(delta < 0.0f || delta > 1.0f)
				return new List<Vector2>();

			if(numberOfCurveSections < 3 || points.Count < 3)
				return new List<Vector2>(points);

			Vector2 start = points[0] - (points[1] - points[0]);
			points.Insert(0, start);

			int n = points.Count - 1;
			Vector2 end = points[n] + (points[n] - points[n - 1]);
			points.Add(end);

			List<Vector2> result = new List<Vector2>();
			for(int i = 0; i < points.Count - 3; i++)
			{
				List<Vector2> splinePoints = CalculateCatmullRomSpline(points[i], points[i + 1], points[i + 2], points[i + 3], delta, numberOfCurveSections);

				if(result.Count > 0)
					splinePoints.RemoveAt(0);

				result.AddRange(splinePoints);
			}

			return result;
		}

		public static bool AreEqual(double actual, double expected, double threshold = 0.00001)
		{
			double diff = expected - actual;
			return Math.Abs(diff) < Math.Abs(threshold);
		}
	}
}
