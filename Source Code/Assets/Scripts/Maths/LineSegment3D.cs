﻿using System;

namespace CommonFramework.Maths
{
    /// <summary>
    /// LineSegment3D defines a line segment between two points in 3-Dimensional space.
    /// </summary>
    public class LineSegment3D
    {
        private Float3 a = null;
        private Float3 b = null;

        /// <summary>
        /// Creates a new LineSegment3D between the two specified points.
        /// </summary>
        /// <param name="ax">The line start point X-axis position.</param>
        /// <param name="ay">The line start point Y-axis position.</param>
        /// <param name="az">The line start point Z-axis position.</param>
        /// <param name="bx">The line end point X-axis position.</param>
        /// <param name="by">The line end point Y-axis position.</param>
        /// <param name="bz">The line end point Z-axis position.</param>
        public LineSegment3D(float ax, float ay, float az, float bx, float by, float bz)
            : this(new Float3(ax, ay, az), new Float3(bx, by, bz)) { }

        /// <summary>
        /// Creates a new LineSegment3D between the two specified points.
        /// </summary>
        /// <param name="a">The start point of the line.</param>
        /// <param name="b">The end point of the line.</param>
        public LineSegment3D(Float3 a, Float3 b)
        {
            this.a = a;
            this.b = b;
        }

        // ----- Properties ----- //

        /// <summary>
        /// Gets the start Vector3 point of the line.
        /// </summary>
        public Float3 A
        {
            get { return a; }
        }

        /// <summary>
        /// Gets the end Vector3 point of the line.
        /// </summary>
        public Float3 B
        {
            get { return b; }
        }
    }
}
