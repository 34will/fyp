﻿using System;

namespace CommonFramework.Maths
{
    /// <summary>
    /// Vector3 is spatial data structure representing a point in 3-Dimensions, implementing IPositionable3D and IPositionableND.
    /// </summary>
    public class Float3 : IPositionable3D, IPositionableND
    {
        private float x, y, z;

        /// <summary>
        /// Creates a new Vector3 with X, Y and Z values set to zero.
        /// </summary>
        public Float3()
        {
            this.x = 0.0f;
            this.y = 0.0f;
            this.z = 0.0f;
        }

        /// <summary>
        /// Creates a new Vector3 with the specified X, Y and Z values.
        /// </summary>
        /// <param name="x">The X position value of the Vector3.</param>
        /// <param name="y">The Y position value of the Vector3.</param>
        /// <param name="y">The Z position value of the Vector3.</param>
        public Float3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        /// <summary>
        /// Sets the spatial value for a specified dimension.
        /// </summary>
        /// <param name="dimension">The zero-based index of the dimension to set the spatial value for.</param>
        /// <param name="value">The spatial value to set on the specified dimension.</param>
        /// <remarks>This function will throw an exception if a dimension other than 0, 1 or 2 is specified.</remarks>
        public void SetDimension(int dimension, float value)
        {
            if (dimension == 0) x = value;
            else if (dimension == 1) y = value;
            else if (dimension == 2) z = value;
            else throw new Exception("Vector3 does not have " + dimension + " dimensions");
        }

        /// <summary>
        /// Gets the spatial value for a specified dimension.
        /// </summary>
        /// <param name="dimension">The zero-based index of the dimension to get the spatial value for.</param>
        /// <returns>The spatial value of the specified dimension.</returns>
        /// <remarks>This function will throw an exception if a dimension other than 0, 1 or 2 is specified.</remarks>
        public float GetDimension(int dimension)
        {
            if (dimension == 0) return x;
            else if (dimension == 1) return y;
            else if (dimension == 2) return z;
            else throw new Exception("Vector3 does not have " + dimension + " dimensions");
        }

        /// <summary>
        /// Reduces the length of the vectors components proportionally so that direction is maintained, but the length of the vector is unit length one.
        /// </summary>
        public void Normalise()
        {
            float inverseLength = 1.0f / Length;
            x *= inverseLength;
            y *= inverseLength;
            z *= inverseLength;
        }

        /// <summary>
        /// Calculates the dot product between the vector and another specified.
        /// The dot product is equal to the sum of the multiplication between the components of the two vectors.
        /// </summary>
        /// <param name="vector">The vector to dot product against.</param>
        /// <returns>The dot product float value.</returns>
        public float Dot(Float3 vector)
        {
            return (x * vector.x) + (y * vector.y) + (z * vector.z);
        }

        /// <summary>
        /// Calculates the dot product between two specified vectors.
        /// The dot product is equal to the sum of the multiplication between the components of the two vectors.
        /// </summary>
        /// <param name="a">The first vector to dot product against.</param>
        /// <param name="b">The second vector to dot product against.</param>
        /// <returns>The dot product float value.</returns>
        public static float Dot(Float3 a, Float3 b)
        {
            return (a.x * b.x) + (a.y * b.y) + (a.z * b.z);
        }

        // ----- Operators ----- //

        /// <summary>
        /// Produces a new vector as the sum of the two specified vectors.
        /// </summary>
        /// <param name="a">The left hand Float3 to add.</param>
        /// <param name="b">The right hand Float3 to add.</param>
        /// <returns>A new Float3 which is the sum of the two passed vectors.</returns>
        public static Float3 operator +(Float3 a, Float3 b)
        {
            return new Float3(a.x + b.x, a.y + b.y, a.z + b.z);
        }

        /// <summary>
        /// Produces a new vector as the subtraction of the right hand parameter B from the left hand parameter A.
        /// The parameter order for this operator matters.
        /// </summary>
        /// <param name="a">The vector to be subtracted from.</param>
        /// <param name="b">The vector that will be subtracted.</param>
        /// <returns>A new Float3 vector which is the subtraction of vector B from vector A.</returns>
        public static Float3 operator -(Float3 a, Float3 b)
        {
            return new Float3(a.x - b.x, a.y - b.y, a.z - b.z);
        }

        /// <summary>
        /// Multiplies a Float3 vector by a scalar value.
        /// </summary>
        /// <param name="vector">The vector to scale.</param>
        /// <param name="scalar">The scalar multiplication factor.</param>
        /// <returns>A new Float3 vector which is the specified vector multiplied by the scalar.</returns>
        public static Float3 operator *(Float3 vector, float scalar)
        {
            return new Float3(vector.x * scalar, vector.y * scalar, vector.z * scalar);
        }

        /// <summary>
        /// Multiplies a Float3 vector by a scalar value.
        /// </summary>
        /// <param name="vector">The vector to scale.</param>
        /// <param name="scalar">The scalar multiplication factor.</param>
        /// <returns>A new Float3 vector which is the specified vector multiplied by the scalar.</returns>
        public static Float3 operator *(float scalar, Float3 vector)
        {
            return new Float3(vector.x * scalar, vector.y * scalar, vector.z * scalar);
        }

        /// <summary>
        /// Adds a constant value to the components of a vector.
        /// </summary>
        /// <param name="vector">The vector to add the value to.</param>
        /// <param name="value">The value to add.</param>
        /// <returns>A new Float3 where the components are the sum of the input vector and the input value.</returns>
        public static Float3 operator +(Float3 vector, float value)
        {
            return new Float3(vector.x + value, vector.y + value, vector.z + value);
        }

        /// <summary>
        /// Adds a constant value to the components of a vector.
        /// </summary>
        /// <param name="vector">The vector to add the value to.</param>
        /// <param name="value">The value to add.</param>
        /// <returns>A new Float3 where the components are the sum of the input vector and the input value.</returns>
        public static Float3 operator +(float value, Float3 vector)
        {
            return new Float3(vector.x + value, vector.y + value, vector.z + value);
        }

        // ----- Properties ----- //

        /// <summary>
        /// Sets or gets the X spatial position of the Vector3.
        /// </summary>
        public float X
        {
            set { x = value; }
            get { return x; }
        }

        /// <summary>
        /// Sets or gets the Y spatial position of the Vector3.
        /// </summary>
        public float Y
        {
            set { y = value; }
            get { return y; }
        }

        /// <summary>
        /// Sets or gets the Z spatial position of the Vector3.
        /// </summary>
        public float Z
        {
            set { z = value; }
            get { return z; }
        }

        /// <summary>
        /// Gets the number of dimensions of the Vector3, which is always 3.
        /// </summary>
        public int Dimensions
        {
            get { return 3; }
        }

        /// <summary>
        /// Gets the squared length of the vector, which is the sum of all the components squared
        /// </summary>
        public float LengthSquared
        {
            get { return (x * x) + (y * y) + (z * z); }
        }

        /// <summary>
        /// Gets the length of the vector, which is equal to the square root of all the components squared
        /// </summary>
        public float Length
        {
            get { return (float)Math.Sqrt((x * x) + (y * y) + (z * z)); }
        }

        /// <summary>
        /// Sets or gets the spatial value position on a paticular dimension.
        /// </summary>
        /// <param name="dimension">The dimension to set or get the spatial value for.</param>
        /// <returns>The spatial value for a paticular dimension.</returns>
        /// <remarks>This function will throw an exception if a dimension other than 0, 1 or 2 is specified.</remarks>
        public float this[int dimension]
        {
            set
            {
                if (dimension == 0) x = value;
                else if (dimension == 1) y = value;
                else if (dimension == 2) z = value;
                else throw new Exception("Vector3 does not have " + dimension + " dimensions");
            }
            get
            {
                if (dimension == 0) return x;
                else if (dimension == 1) return y;
                else if (dimension == 2) return z;
                else throw new Exception("Vector3 does not have " + dimension + " dimensions");
            }
        }
    }
}
