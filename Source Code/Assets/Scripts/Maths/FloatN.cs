﻿using System;

namespace CommonFramework.Maths
{
    /// <summary>
    /// VectorN is spatial data structure representing a point in N-Dimensions that implements IPositionableND.
    /// </summary>
    public class FloatN : IPositionableND
    {
        private float[] values = null;

        /// <summary>
        /// Creates a new VectorN from an IPositionableND making a spatial structure with the same number of dimensions as the passed class.
        /// </summary>
        /// <param name="position">The IPositionableND to create a VectorN from.</param>
        public FloatN(IPositionableND position)
            : this(position.Dimensions)
        {
            for (int i = values.Length - 1; i >= 0; i--)
                values[i] = position.GetDimension(i);
        }

        /// <summary>
        /// Creates a new VectorN from an array of axes positions where the length of the array represents the number of spatial dimensions.
        /// </summary>
        /// <param name="values">The position values for each dimension.</param>
        public FloatN(float[] values)
            : this(values.Length)
        {
            for (int i = values.Length - 1; i >= 0; i--)
                this.values[i] = values[i];
        }

        /// <summary>
        /// Creates a new VectorN of the specified number of dimensions where the value for each dimension is set to zero.
        /// </summary>
        /// <param name="dimensions">The number of dimensions the VectorN will have.</param>
        public FloatN(int dimensions)
        {
            values = new float[dimensions];
        }

        /// <summary>
        /// Sets the spatial value for a specified dimension.
        /// </summary>
        /// <param name="dimension">The zero-based index of the dimension to set the spatial value for.</param>
        /// <param name="value">The spatial value to set on the specified dimension.</param>
        /// <remarks>This function will throw an exception if a dimension less than zero or above the number of dimensions is specified.</remarks>
        public void SetDimension(int dimension, float value)
        {
            if (dimension >= 0 && dimension < values.Length) values[dimension] = value;
            else throw new Exception("Dimension " + dimension + " does not exist in this NPosition instance.");
        }

        /// <summary>
        /// Gets the spatial value for a specified dimension.
        /// </summary>
        /// <param name="dimension">The zero-based index of the dimension to get the spatial value for.</param>
        /// <returns>The spatial value of the specified dimension.</returns>
        /// <remarks>This function will throw an exception if a dimension less than zero or above the number of dimensions is specified.</remarks>
        public float GetDimension(int dimension)
        {
            if (dimension >= 0 && dimension < values.Length) return values[dimension];
            else throw new Exception("Dimension " + dimension + " does not exist in this NPosition instance.");
        }

        /// <summary>
        /// Makes a cloned instance of the VectorN with a copy of all the spatial values of the original.
        /// </summary>
        /// <returns>A new instance of the VectorN instance.</returns>
        public FloatN Clone()
        {
            FloatN position = new FloatN(values.Length);
            for (int i = values.Length - 1; i >= 0; i--)
                position.values[i] = values[i];
            return position;
        }

        /// <summary>
        /// Reduces the length of the vectors components proportionally so that direction is maintained, but the length of the vector is unit length one.
        /// </summary>
        public void Normalise()
        {
            float inverseLength = 1.0f / Length;
            for (int i = values.Length - 1; i >= 0; i--)
                values[i] *= inverseLength;
        }

        /// <summary>
        /// Calculates the dot product between the vector and another specified.
        /// The dot product is equal to the sum of the multiplication between the components of the two vectors.
        /// </summary>
        /// <param name="vector">The vector to dot product against.</param>
        /// <returns>The dot product float value.</returns>
        public float Dot(FloatN vector)
        {
            if (vector.Dimensions == values.Length)
            {
                float dot = 0.0f;
                for (int i = values.Length - 1; i >= 0; i--)
                    dot += values[i] * vector.values[i];
                return dot;
            }
            else throw new Exception("Incompatible number of dimensions in dot product. Both vectors must have the same number of dimensions.");
        }

        /// <summary>
        /// Calculates the dot product between two specified vectors.
        /// The dot product is equal to the sum of the multiplication between the components of the two vectors.
        /// </summary>
        /// <param name="a">The first vector to dot product against.</param>
        /// <param name="b">The second vector to dot product against.</param>
        /// <returns>The dot product float value.</returns>
        public static float Dot(FloatN a, FloatN b)
        {
            if (a.values.Length == b.values.Length)
            {
                float dot = 0.0f;
                for (int i = a.Dimensions - 1; i >= 0; i--)
                    dot += a.values[i] * b.values[i];
                return dot;
            }
            else throw new Exception("Incompatible number of dimensions in dot product. Both vectors must have the same number of dimensions.");
        }

        // ----- Operators ----- //

        /// <summary>
        /// Produces a new vector as the sum of the two specified vectors.
        /// </summary>
        /// <param name="a">The left hand FloatN to add.</param>
        /// <param name="b">The right hand FloatN to add.</param>
        /// <returns>A new FloatN which is the sum of the two passed vectors.</returns>
        public static FloatN operator +(FloatN a, FloatN b)
        {
            if (a.Dimensions == b.Dimensions)
            {
                FloatN sum = new FloatN(a);
                for (int i = a.Dimensions - 1; i >= 0; i--)
                    sum.values[i] += b.values[i];
                return sum;
            }
            else throw new Exception("Incompatible number of dimensions. Both vectors must have the same number of dimensions.");
        }

        /// <summary>
        /// Produces a new vector as the subtraction of the right hand parameter B from the left hand parameter A.
        /// The parameter order for this operator matters.
        /// </summary>
        /// <param name="a">The vector to be subtracted from.</param>
        /// <param name="b">The vector that will be subtracted.</param>
        /// <returns>A new FloatN vector which is the subtraction of vector B from vector A.</returns>
        public static FloatN operator -(FloatN a, FloatN b)
        {
            if (a.Dimensions == b.Dimensions)
            {
                FloatN sum = new FloatN(a);
                for (int i = a.Dimensions - 1; i >= 0; i--)
                    sum.values[i] -= b.values[i];
                return sum;
            }
            else throw new Exception("Incompatible number of dimensions. Both vectors must have the same number of dimensions.");
        }

        /// <summary>
        /// Multiplies a FloatN vector by a scalar value.
        /// </summary>
        /// <param name="vector">The vector to scale.</param>
        /// <param name="scalar">The scalar multiplication factor.</param>
        /// <returns>A new FloatN vector which is the specified vector multiplied by the scalar.</returns>
        public static FloatN operator *(FloatN vector, float scalar)
        {
            FloatN sum = new FloatN(vector);
            for (int i = vector.Dimensions - 1; i >= 0; i--)
                sum.values[i] *= scalar;
            return sum;
        }

        /// <summary>
        /// Multiplies a FloatN vector by a scalar value.
        /// </summary>
        /// <param name="vector">The vector to scale.</param>
        /// <param name="scalar">The scalar multiplication factor.</param>
        /// <returns>A new Float3 vector which is the specified vector multiplied by the scalar.</returns>
        public static FloatN operator *(float scalar, FloatN vector)
        {
            FloatN sum = new FloatN(vector);
            for (int i = vector.Dimensions - 1; i >= 0; i--)
                sum.values[i] *= scalar;
            return sum;
        }

        /// <summary>
        /// Adds a constant value to the components of a vector.
        /// </summary>
        /// <param name="vector">The vector to add the value to.</param>
        /// <param name="value">The value to add.</param>
        /// <returns>A new FloatN where the components are the sum of the input vector and the input value.</returns>
        public static FloatN operator +(FloatN vector, float value)
        {
            FloatN sum = new FloatN(vector);
            for (int i = vector.Dimensions - 1; i >= 0; i--)
                sum.values[i] += value;
            return sum;
        }

        /// <summary>
        /// Adds a constant value to the components of a vector.
        /// </summary>
        /// <param name="vector">The vector to add the value to.</param>
        /// <param name="value">The value to add.</param>
        /// <returns>A new FloatN where the components are the sum of the input vector and the input value.</returns>
        public static FloatN operator +(float value, FloatN vector)
        {
            FloatN sum = new FloatN(vector);
            for (int i = vector.Dimensions - 1; i >= 0; i--)
                sum.values[i] += value;
            return sum;
        }

        // ----- Properties ----- //

        /// <summary>
        /// Gets the number of dimensions this VectorN stores spatial positions for.
        /// </summary>
        public int Dimensions
        {
            get { return values.Length; }
        }

        /// <summary>
        /// Sets or gets the spatial value for a specified dimension.
        /// </summary>
        /// <param name="dimension">The dimension to set or get the spatial value for.</param>
        /// <returns>The spatial value on the specified dimensions zero-based index.</returns>
        /// <remarks>This operator mirrors SetDimension and GetDimension. For more information see these functions.</remarks>
        public float this[int dimension]
        {
            set
            {
                if (dimension >= 0 && dimension < values.Length) values[dimension] = value;
                else throw new Exception("Dimension " + dimension + " does not exist in this NPosition instance.");
            }
            get
            {
                if (dimension >= 0 && dimension < values.Length) return values[dimension];
                else throw new Exception("Dimension " + dimension + " does not exist in this NPosition instance.");
            }
        }

        /// <summary>
        /// Gets the squared length of the vector, which is the sum of all the components squared
        /// </summary>
        public float LengthSquared
        {
            get
            {
                float lengthSquared = 0.0f;
                for (int i = values.Length - 1; i >= 0; i--)
                    lengthSquared += values[i] * values[i];
                return lengthSquared;
            }
        }

        /// <summary>
        /// Gets the length of the vector, which is equal to the square root of all the components squared
        /// </summary>
        public float Length
        {
            get { return (float)Math.Sqrt(LengthSquared); }
        }
    }
}
