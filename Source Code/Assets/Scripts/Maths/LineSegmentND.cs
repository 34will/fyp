﻿using System;

namespace CommonFramework.Maths
{
    /// <summary>
    /// LineSegmentND defines a line segment between two points in N-Dimensional space.
    /// </summary>
    public class LineSegmentND
    {
        private IPositionableND a = null;
        private IPositionableND b = null;

        /// <summary>
        /// Creates a new LineSegmentND between the two specified points in N-Dimensional space.
        /// </summary>
        /// <param name="a">The start position of line.</param>
        /// <param name="b">The end position of the line.</param>
        public LineSegmentND(IPositionableND a, IPositionableND b)
        {
            this.a = a;
            this.b = b;
        }

        // ----- Properties ----- //

        /// <summary>
        /// Gets the start IPositionableND of the line.
        /// </summary>
        public IPositionableND A
        {
            get { return a; }
        }

        /// <summary>
        /// Gets the end IPositionableND of the line.
        /// </summary>
        public IPositionableND B
        {
            get { return b; }
        }
    }
}
