﻿using System;

namespace CommonFramework.Maths
{
    /// <summary>
    /// Cuboid defines a 3-Dimensional structure with an X, Y, Z position and a Width, Height, Depth size.
    /// It has basic graphical operations including Intersects and Contains.
    /// </summary>
    public class Cuboid
    {
        float x, y, z, width, height, depth;

        /// <summary>
        /// Creates a new Cuboid with the specified location and size.
        /// </summary>
        /// <param name="x">The X position of the origin of the Cuboid.</param>
        /// <param name="y">The Y position of the origin of the Cuboid.</param>
        /// <param name="z">The Z position of the origin of the Cuboid.</param>
        /// <param name="width">The width of the Rectangle along the horizontal X axis.</param>
        /// <param name="height">The height of the Rectangle along the vertical Y axis.</param>
        /// <param name="depth">The depth of the Rectangle along the Z axis.</param>
        public Cuboid(float x, float y, float z, float width, float height, float depth)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.width = width;
            this.height = height;
            this.depth = depth;
        }

        /// <summary>
        /// Creates a new Cuboid with the specified location and size.
        /// </summary>
        /// <param name="position">The Vector3 representation of the Cuboids position.</param>
        /// <param name="size">The Vector3 representation of the the Cuboids size.</param>
        public Cuboid(Float3 position, Float3 size)
            : this(position.X, position.Y, position.Z, size.X, size.Y, size.Z) { }

        /// <summary>
        /// Determines if a point lies in or along the edge of the Cuboid.
        /// </summary>
        /// <param name="x">The X position of the point to check inclusion.</param>
        /// <param name="y">The Y position of the point to check inclusion.</param>
        /// <param name="z">The Z position of the point to check inclusion.</param>
        /// <returns>True if the point is inside the Cuboid, otherwise false.</returns>
        public bool Contains(float x, float y, float z)
        {
            return x >= this.x && x <= this.x + this.width
                && y >= this.y && y <= this.y + this.height
                && z >= this.z && z <= this.z + this.depth;
        }

        /// <summary>
        /// Determines if a point lies in or along the edge of the Cuboid.
        /// </summary>
        /// <param name="point">The Vector3 point to check if it is inside the Cuboid.</param>
        /// <returns>True if the point is inside the Cuboid, otherwise false.</returns>
        public bool Contains(Float3 point)
        {
            return Contains(point.X, point.Y, point.Z);
        }

        /// <summary>
        /// Determines if a point lies in or along the edge of the Cuboid.
        /// </summary>
        /// <param name="point">The IPositionable3D point to check if it is inside the Cuboid.</param>
        /// <returns>True if the point is inside the Cuboid, otherwise false.</returns>
        public bool Contains(IPositionable3D point)
        {
            return Contains(point.X, point.Y, point.Z);
        }

        /// <summary>
        /// Determines if another Cuboid intersects this one.
        /// </summary>
        /// <param name="cuboid">The cuboid to check intersection against.</param>
        /// <returns>True if the Cuboid intersects, otherwise false.</returns>
        public bool Intersects(Cuboid cuboid)
        {
            return x < cuboid.x + cuboid.width
                && x + width > cuboid.x
                && y < cuboid.y + cuboid.height
                && y + height > cuboid.y
                && z < cuboid.z + cuboid.depth
                && z + depth > cuboid.z;
        }

        // ----- Properties ----- //

        /// <summary>
        /// Gets the left X position of the Cuboid.
        /// </summary>
        /// <remarks>Left is equal to the X position of the Cuboid.</remarks>
        public float Left
        {
            get { return x; }
        }

        /// <summary>
        /// Gets the top Y position of the Cuboid.
        /// </summary>
        /// <remarks>Top is equal to the Y position of the Cuboid.</remarks>
        public float Top
        {
            get { return y; }
        }

        /// <summary>
        /// Gets the front Z position of the Cuboid.
        /// </summary>
        /// <remarks>Front is equal to the Z position of the Cuboid.</remarks>
        public float Front
        {
            get { return z; }
        }

        /// <summary>
        /// Gets the right X position of the Cuboid.
        /// </summary>
        /// <remarks>Right is equal to the X position of the Cuboid plus its width.</remarks>
        public float Right
        {
            get { return x + width; }
        }

        /// <summary>
        /// Gets the bottom Y position of the Cuboid.
        /// </summary>
        /// <remarks>Bottom is equal to the Y position of the Cuboid plus its height.</remarks>
        public float Bottom
        {
            get { return y + height; }
        }

        /// <summary>
        /// Gets the back Z position of the Cuboid.
        /// </summary>
        /// <remarks>Back is equal to the Z position of the Cuboid plus its depth.</remarks>
        public float Back
        {
            get { return z + depth; }
        }

        /// <summary>
        /// Sets or gets the X position of the Cuboid.
        /// </summary>
        public float X
        {
            set { x = value; }
            get { return x; }
        }

        /// <summary>
        /// Sets or gets the Y position of the Cuboid.
        /// </summary>
        public float Y
        {
            set { y = value; }
            get { return y; }
        }

        /// <summary>
        /// Sets or gets the Z position of the Cuboid.
        /// </summary>
        public float Z
        {
            set { z = value; }
            get { return z; }
        }

        /// <summary>
        /// Sets or gets the Width of the Cuboid.
        /// </summary>
        public float Width
        {
            set { width = value; }
            get { return width; }
        }

        /// <summary>
        /// Sets or gets the Height of the Cuboid.
        /// </summary>
        public float Height
        {
            set { height = value; }
            get { return height; }
        }

        /// <summary>
        /// Sets or gets the Depth of the Cuboid.
        /// </summary>
        public float Depth
        {
            set { depth = value; }
            get { return depth; }
        }
    }
}
