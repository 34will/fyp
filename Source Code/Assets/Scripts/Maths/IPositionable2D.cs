﻿using System;

namespace CommonFramework.Maths
{
    /// <summary>
    /// IPositionable2D defines the interface for a spatial point in 2-Dimensions.
    /// </summary>
    public interface IPositionable2D
    {
        /// <summary>
        /// Sets or gets the spatial position of the IPositionable2D in the X-axis.
        /// </summary>
        float X { set; get; }

        /// <summary>
        /// Sets or gets the spatial position of the IPositionable2D in the Y-axis.
        /// </summary>
        float Y { set; get; }
    }
}
