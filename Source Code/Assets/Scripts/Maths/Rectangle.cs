﻿using System;

namespace CommonFramework.Maths
{
    /// <summary>
    /// Rectangle defines a 2-Dimensional structure with an X, Y position and a Width and Height size.
    /// It has basic graphical operations including Intersects and Contains.
    /// </summary>
    public class Rectangle
    {
        float x, y, width, height;

        /// <summary>
        /// Creates a new Rectangle with the specified position and size.
        /// </summary>
        /// <param name="x">The X position of the origin of the Rectangle.</param>
        /// <param name="y">The Y position of the origin of the Rectangle.</param>
        /// <param name="width">The width of the Rectangle along the horizontal X axis.</param>
        /// <param name="height">The height of the Rectangle along the vertical Y axis.</param>
        public Rectangle(float x, float y, float width, float height)
        {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }

        /// <summary>
        /// Creates a new Rectangle with the specified position and size.
        /// </summary>
        /// <param name="position">The Vector2 representation of the position of the Rectangle.</param>
        /// <param name="size">The Vector2 representation of the size of the Rectangle.</param>
        public Rectangle(Float2 position, Float2 size)
            : this(position.X, position.Y, size.X, size.Y) { }

        /// <summary>
        /// Determines whether a point lies within the bounds or on the edge of the Rectangle.
        /// </summary>
        /// <param name="x">The X position of the point to check for inclusion.</param>
        /// <param name="y">The Y position of the point to chech for inclusion.</param>
        /// <returns>True if the point is within the Rectangle otherwise false.</returns>
        public bool Contains(float x, float y)
        {
            return x >= this.x && x <= this.x + this.width
                && y >= this.y && y <= this.y + this.height;
        }

        /// <summary>
        /// Determines whether a point lies within the bounds or on the edge of the Rectangle.
        /// </summary>
        /// <param name="point">The position of the point to check for inclusion.</param>
        /// <returns>True if the point is within the Rectangle otherwise false.</returns>
        public bool Contains(Float2 point)
        {
            return Contains(point.X, point.Y);
        }

        /// <summary>
        /// Determines if the Rectangle intersects with another Rectangle.
        /// </summary>
        /// <param name="rectangle">The Rectangle that should be checked with to see if an intersection occurs.</param>
        /// <returns>True if the Rectangles intersect, otherwise false.</returns>
        public bool Intersects(Rectangle rectangle)
        {
            return x < rectangle.x + rectangle.width
                && x + width > rectangle.x
                && y < rectangle.y + rectangle.height
                && y + height > rectangle.y;
        }

        // ----- Properties ----- //

        /// <summary>
        /// Gets the left X position of the Rectangle.
        /// </summary>
        /// <remarks>Left is equal to the X position of the Rectangle.</remarks>
        public float Left
        {
            get { return x; }
        }

        /// <summary>
        /// Gets the top Y position of the Rectangle.
        /// </summary>
        /// <remarks>Top is equal to the Y position of the Rectangle.</remarks>
        public float Top
        {
            get { return y; }
        }

        /// <summary>
        /// Gets the right X position of the Rectangle.
        /// </summary>
        /// <remarks>Right is equal to the X position of the Rectangle plus its width.</remarks>
        public float Right
        {
            get { return x + width; }
        }

        /// <summary>
        /// Gets the bottom Y position of the Rectangle.
        /// </summary>
        /// <remarks>Bottom is equal to the Y position of the Rectangle plus its height.</remarks>
        public float Bottom
        {
            get { return y + height; }
        }

        /// <summary>
        /// Sets or gets the X position of the Rectangle.
        /// </summary>
        public float X
        {
            set { x = value; }
            get { return x; }
        }

        /// <summary>
        /// Sets or gets the Y position of the Rectangle.
        /// </summary>
        public float Y
        {
            set { y = value; }
            get { return y; }
        }

        /// <summary>
        /// Sets or gets width of the Rectangle.
        /// </summary>
        public float Width
        {
            set { width = value; }
            get { return width; }
        }

        /// <summary>
        /// Sets or gets the height of the Rectangle.
        /// </summary>
        public float Height
        {
            set { height = value; }
            get { return height; }
        }

    }
}
