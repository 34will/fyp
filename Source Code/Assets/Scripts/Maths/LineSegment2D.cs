﻿using System;

namespace CommonFramework.Maths
{
    /// <summary>
    /// LineSegment2D defines a line segment between two points in 2-Dimensional space.
    /// </summary>
    public class LineSegment2D
    {
        private Float2 a = null;
        private Float2 b = null;

        /// <summary>
        /// Creates a new LineSegment2D between the two specified points.
        /// </summary>
        /// <param name="ax">The line start point X-axis position.</param>
        /// <param name="ay">The line start point Y-axis position.</param>
        /// <param name="bx">The line end point X-axis position.</param>
        /// <param name="by">The line end point Y-axis position.</param>
        public LineSegment2D(float ax, float ay, float bx, float by)
            : this(new Float2(ax, ay), new Float2(bx, by)) { }

        /// <summary>
        /// Creates a new LineSegment2D between the two specified points.
        /// </summary>
        /// <param name="a">The start point of the line.</param>
        /// <param name="b">The end point of the line.</param>
        public LineSegment2D(Float2 a, Float2 b)
        {
            this.a = a;
            this.b = b;
        }

        // ----- Properties ----- //

        /// <summary>
        /// Gets the start Vector2 point of the line.
        /// </summary>
        public Float2 A
        {
            get { return a; }
        }

        /// <summary>
        /// Gets the end Vector2 point of the line.
        /// </summary>
        public Float2 B
        {
            get { return b; }
        }
    }
}
