﻿using System;

namespace CommonFramework.Maths
{
    /// <summary>
    /// IPositionableND defines an interface for a spatial point in N-Dimensions.
    /// The contract of the interface provides methods to access information in a variant number of these dimensions.
    /// </summary>
    public interface IPositionableND
    {
        /// <summary>
        /// Sets the spatial value for a specified dimension.
        /// </summary>
        /// <param name="dimension">The zero-based index of the dimension to set the spatial value for.</param>
        /// <param name="value">The spatial value to set on the specified dimension.</param>
        void SetDimension(int dimension, float value);

        /// <summary>
        /// Gets the spatial value for a specified dimension.
        /// </summary>
        /// <param name="dimension">The zero-based index of the dimension to get the spatial value for.</param>
        /// <returns>The spatial value of the specified dimension.</returns>
        float GetDimension(int dimension);

        // ----- Properties ----- //

        /// <summary>
        /// Gets the number of spatial dimensions in the IPositionableND.
        /// </summary>
        int Dimensions { get; }

        /// <summary>
        /// Sets or gets the spatial value position on a paticular dimension.
        /// </summary>
        /// <param name="dimension">The dimension to set or get the spatial value for.</param>
        /// <returns>The spatial value for a paticular dimension.</returns>
        float this[int dimension] { set; get; }
    }
}
