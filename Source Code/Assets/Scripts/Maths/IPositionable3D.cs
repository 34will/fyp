﻿using System;

namespace CommonFramework.Maths
{
    /// <summary>
    /// IPositionable3D defines the interface for a spatial point in 3-Dimensions.
    /// </summary>
    public interface IPositionable3D
    {
        /// <summary>
        /// Sets or gets the spatial position of the IPositionable3D in the X-axis.
        /// </summary>
        float X { set; get; }

        /// <summary>
        /// Sets or gets the spatial position of the IPositionable3D in the Y-axis.
        /// </summary>
        float Y { set; get; }

        /// <summary>
        /// Sets or gets the spatial position of the IPositionable3D in the Z-axis.
        /// </summary>
        float Z { set; get; }
    }
}
