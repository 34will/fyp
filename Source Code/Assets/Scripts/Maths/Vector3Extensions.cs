﻿using System;

using UnityEngine;

using CommonFramework.Maths;

namespace CommonFramework.Maths
{
	/// <summary>
	/// Provides extension methods for the Vector3 struct.
	/// </summary>
	public static class Vector3Extensions
	{
		/// <summary>
		/// Returns a Vector2 with the X and Y components set to the X and Y components of this.
		/// </summary>
		/// <param name="value">The Vector3 to convert.</param>
		/// <returns>A Vector2 of { this.x, this.y }.</returns>
		public static Vector2 ToV2XY(this Vector3 value)
		{
			return new Vector2(value.x, value.y);
		}

		/// <summary>
		/// Returns a Vector2 with the X and Y components set to the Y and X components of this.
		/// </summary>
		/// <param name="value">The Vector3 to convert.</param>
		/// <returns>A Vector2 of { this.y, this.x }.</returns>
		public static Vector2 ToV2YX(this Vector3 value)
		{
			return new Vector2(value.y, value.x);
		}

		/// <summary>
		/// Returns a Vector2 with the X and Y components set to the X and Z components of this.
		/// </summary>
		/// <param name="value">The Vector3 to convert.</param>
		/// <returns>A Vector2 of { this.x, this.z }.</returns>
		public static Vector2 ToV2XZ(this Vector3 value)
		{
			return new Vector2(value.x, value.z);
		}

		/// <summary>
		/// Returns a Vector2 with the X and Y components set to the Z and X components of this.
		/// </summary>
		/// <param name="value">The Vector3 to convert.</param>
		/// <returns>A Vector2 of { this.z, this.x }.</returns>
		public static Vector2 ToV2ZX(this Vector3 value)
		{
			return new Vector2(value.z, value.x);
		}

		/// <summary>
		/// Returns a Vector2 with the X and Y components set to the Y and Z components of this.
		/// </summary>
		/// <param name="value">The Vector3 to convert.</param>
		/// <returns>A Vector2 of { this.y, this.z }.</returns>
		public static Vector2 ToV2YZ(this Vector3 value)
		{
			return new Vector2(value.y, value.z);
		}

		/// <summary>
		/// Returns a Vector2 with the X and Y components set to the Z and Y components of this.
		/// </summary>
		/// <param name="value">The Vector3 to convert.</param>
		/// <returns>A Vector2 of { this.z, this.y }.</returns>
		public static Vector2 ToV2ZY(this Vector3 value)
		{
			return new Vector2(value.z, value.y);
		}

        /// <summary>
        /// Returns a Float3 with the X, Y and Z components of the Vector3.
        /// </summary>
        /// <param name="value">The Vector3 to initialise the Float3 with.</param>
        /// <returns>A new Float3 initialised with values of the Vector3.</returns>
        public static Float3 ToFloat3(this Vector3 value)
        {
            return new Float3(value.x, value.y, value.z);
        }
	}
}
