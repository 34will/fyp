﻿using System;
using System.Collections.Generic;

using UnityEngine;

using CommonFramework.MessageSystem;

namespace CommonFramework.ObjectPlacement
{
    public class SpawnForestMessage : IMessage
    {
        public const string messageName = "SpawnForestMessage";
        private IMessageable source = null;
        private List<GameObject> forestPrefabs = new List<GameObject>();
        private GameObject forestParent = null;

        public SpawnForestMessage(List<GameObject> forestPrefabs, GameObject forestParent = null, IMessageable source = null)
        {
            this.forestParent = forestParent;
            this.forestPrefabs = forestPrefabs;
            this.source = source;
        }

        // ----- Properties ----- //

        public GameObject ForestParent
        {
            get { return forestParent; }
        }

        public List<GameObject> ForestPrefabs
        {
            get { return forestPrefabs; }
        }

        /// <summary>
        /// Gets the named identifier of the IMessage.
        /// </summary>
        public string MessageName
        {
            get { return messageName; }
        }

        /// <summary>
        /// Gets the source IMessageable object the message was sent from.
        /// </summary>
        public IMessageable Source
        {
            get { return source; }
            set { source = value; }
        }
    }
}
