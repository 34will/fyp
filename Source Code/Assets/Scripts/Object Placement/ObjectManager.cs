﻿using System;
using System.Collections.Generic;

using UnityEngine;

using CommonFramework.MessageSystem;

namespace CommonFramework.ObjectPlacement
{
    public class ObjectManager : MonoBehaviour
    {
        private GameObject shrubs = null;
        private GameObject forests = null;
        public List<GameObject> ShrubPrefabs = new List<GameObject>();
        public List<GameObject> TreePrefabs = new List<GameObject>();

        void OnGUI()
        {
            if(GUI.Button(new Rect(5, 350, 150, 21), "Toggle Shrubs"))
                ToggleShrubs();

            if(GUI.Button(new Rect(5, 380, 150, 21), "Toggle Forest"))
                ToggleForest();
        }

        private void SpawnShrubs()
        {
            MessageHandler.Instance.SendMessage(new SpawnShrubsMessage(ShrubPrefabs, shrubs));
        }

        private void SpawnForest()
        {
            MessageHandler.Instance.SendMessage(new SpawnForestMessage(TreePrefabs, forests));
        }

        private void ToggleShrubs()
        {
            if(shrubs != null)
                shrubs.SetActive(!shrubs.activeInHierarchy);
            else
            {
                shrubs = new GameObject();
                SpawnShrubs();
            }
        }

        private void ToggleForest()
        {
            if(forests != null)
                forests.SetActive(!forests.activeInHierarchy);
            else
            {
                forests = new GameObject();
                SpawnForest();
            }
        }
    }
}
