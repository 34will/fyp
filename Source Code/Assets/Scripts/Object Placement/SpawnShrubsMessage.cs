﻿using System;
using System.Collections.Generic;

using UnityEngine;

using CommonFramework.MessageSystem;

namespace CommonFramework.ObjectPlacement
{
    public class SpawnShrubsMessage : IMessage
    {
        internal const string messageName = "SpawnShrubsMessage";
        private IMessageable source = null;
        private List<GameObject> shrubPrefabs = new List<GameObject>();
        private GameObject shrubParent = null;

        public SpawnShrubsMessage(List<GameObject> shrubPrefabs, GameObject shrubParent = null, IMessageable source = null)
        {
            this.shrubParent = shrubParent;
            this.shrubPrefabs = shrubPrefabs;
            this.source = source;
        }

        // ----- Properties ----- //

        public GameObject ShrubParent
        {
            get { return shrubParent; }
        }

        public List<GameObject> ShrubPrefabs
        {
            get { return shrubPrefabs; }
        }

        /// <summary>
        /// Gets the named identifier of the IMessage.
        /// </summary>
        public string MessageName
        {
            get { return messageName; }
        }

        /// <summary>
        /// Gets the source IMessageable object the message was sent from.
        /// </summary>
        public IMessageable Source
        {
            get { return source; }
            set { source = value; }
        }
    }
}
