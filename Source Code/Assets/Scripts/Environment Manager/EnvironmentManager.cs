﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using UnityEngine;

using CommonFramework.MessageSystem;

namespace CommonFramework.Environment
{
    /// <summary>
    /// EnvironmentManager provides a way to add objects into the scene.
	/// </summary>
	[ExcludeFromCodeCoverage]
    public class EnvironmentManager : MonoBehaviour, IMessageable
    {
        internal const string messageableName = "EnvironmentManager";
        private Queue<IMessage> pendingMessages = new Queue<IMessage>();
        private Dictionary<string, GameObject> lookup = new Dictionary<string, GameObject>();
        private List<GameObject> assets = new List<GameObject>();
        private Dictionary<string, GameObject> identifiers = new Dictionary<string, GameObject>();

        /// <summary>
        /// A List of prefabs that can be spawned using an AssetMessage.
        /// </summary>
        public List<GameObject> prefabs = new List<GameObject>();

        /// <summary>
        /// Unity Awake() function: Awake this instance.
        /// </summary>
        void Awake()
        {
            // Copy the prefabs into a lookup for instancing
            foreach (GameObject gameObject in prefabs)
            {
                if(gameObject != null)
                    lookup.Add(gameObject.name, gameObject);
            }

            // Register the EnvironmentManager with the messaging system
            MessageHandler.Instance.RegisterMessageable(this);
        }

        /// <summary>
        /// Informs the EnvironmentManager that it needs to handle and process an IMessage.
        /// </summary>
        /// <param name="message">The IMessage that the EnvironmentManager needs to process.</param>
        public void ReceiveMessage(IMessage message)
        {
            if(message.MessageName == AssetMessage.messageName)
            {
                // Convert the IMessage to an AssetMessage
                AssetMessage assetMessage = message as AssetMessage;
                if(assetMessage == null) return;
                
                // Acquire the GameObject related to the name
                GameObject template = null;
                if (lookup.TryGetValue(assetMessage.Resource, out template))
                {
                    // Create the GameObject and add it to the assets collection
					GameObject gameObject = (GameObject)GameObject.Instantiate(template, assetMessage.Position, template.transform.rotation * assetMessage.Rotation);
                    gameObject.name = assetMessage.Identifier;
                    gameObject.transform.localScale = assetMessage.Scale;
                    assets.Add(gameObject);
                    assetMessage.PerformAssetInstanced(this, gameObject);
                    
                    // Add the identifier to identifiers collection
                    if (identifiers.ContainsKey(assetMessage.Identifier))
                        assetMessage.PerformAssetNameRegisterFailed(this, assetMessage.Identifier);
                    else
                    {
                        identifiers.Add(assetMessage.Identifier, gameObject);
                        assetMessage.PerformAssetNameRegistered(this, assetMessage.Identifier);
                    }
                }
            }
            else if(message.MessageName == GameObjectMessage.messageName)
            {
                // Convert the IMessage to a GameObjectMessage
                GameObjectMessage gameObjectMessage = message as GameObjectMessage;
                if (gameObjectMessage == null) return;

                assets.Add(gameObjectMessage.GameObject);
            }
        }

        // ----- Properties ----- //

        /// <summary>
        /// Gets the asset collection managed by the EnvironmentManager.
        /// </summary>
        public List<GameObject> Assets
        {
            get { return assets; }
        }

        /// <summary>
        /// Gets the pending outgoing IMessage queue that needs to be processed by the EnvironmentManager.
        /// </summary>
        public Queue<IMessage> PendingMessages
        {
            get { return pendingMessages; }
        }

        /// <summary>
        /// Gets the identifier name of the IMessageable component, EnvironmentManager.
        /// </summary>
        public string MessageableName
        {
            get { return messageableName; }
        }
    }
}