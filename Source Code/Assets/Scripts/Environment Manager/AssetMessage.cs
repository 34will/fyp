﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using UnityEngine;

using CommonFramework.MessageSystem;

namespace CommonFramework.Environment
{
    /// <summary>
    /// AssetMessage is an IMessage that is handled by the EnvironmentManager, used to inform it that an asset should be drawn.
	/// </summary>
    public class AssetMessage : IMessage
    {
        internal const string messageName = "AssetMessage";
        private IMessageable source = null;
        private string resource = "";
        private string identifier = "";
        private Vector3 position = Vector3.zero;
        private Quaternion rotation = Quaternion.identity;
        private Vector3 scale = Vector3.one;
        private GameObject instance = null;

        /// <summary>
        /// Creates a new AssetMessage using the specified: resource, position, rotation, scale and IMessageable source.
        /// </summary>
        /// <param name="resource">The external resource identifying the asset.</param>
        /// <param name="identifier">The internal name tag to uniquely identify the to be instatiated asset.</param>
        /// <param name="position">The position to place the asset.</param>
        /// <param name="rotation">The Quanternion matrix rotation of the asset.</param>
        /// <param name="scale">The scale transform of the asset.</param>
        public AssetMessage(string resource, string identifier, Vector3 position, Quaternion rotation, Vector3 scale)
            : this(resource, identifier, position, rotation, scale, null) { }

        /// <summary>
        /// Creates a new AssetMessage using the specified: resource, position, rotation, scale and IMessageable source.
        /// </summary>
        /// <param name="resource">The external resource identifying the asset.</param>
        /// <param name="identifier">The internal name tag to uniquely identify the to be instatiated asset.</param>
        /// <param name="position">The position to place the asset.</param>
        /// <param name="rotation">The Quanternion matrix rotation of the asset.</param>
        /// <param name="scale">The scale transform of the asset.</param>
        /// <param name="source">The source IMessageable component that sent the AssetMessage.</param>
        public AssetMessage(string resource, string identifier, Vector3 position, Quaternion rotation, Vector3 scale, IMessageable source)
        {
            this.resource = resource;
            this.identifier = identifier;
            this.position = position;
            this.rotation = rotation;
            this.scale = scale;
            this.source = source;
        }

        [ExcludeFromCodeCoverage]
        internal void PerformAssetNameRegistered(EnvironmentManager manager, string name)
        {
            if (AssetNameRegisteredEvent != null)
                AssetNameRegisteredEvent(manager, name);
        }

        [ExcludeFromCodeCoverage]
        internal void PerformAssetNameRegisterFailed(EnvironmentManager manager, string name)
        {
            if (AssetNameRegisterFailedEvent != null)
                AssetNameRegisterFailedEvent(manager, name);
        }

        [ExcludeFromCodeCoverage]
        internal void PerformAssetInstanced(EnvironmentManager manager, GameObject instance)
        {
            this.instance = instance;
            if (AssetInstancedEvent != null)
                AssetInstancedEvent(manager, instance);
        }

        // ----- Events ----- //

        /// <summary>
        /// Occurs when the receiving EnvironmentManager component registers the asset assosiated with the AssetMessage and its name is assigned.
        /// </summary>
        public event AssetNameCallback AssetNameRegisteredEvent;

        /// <summary>
        /// Occurs when the receiving EnvironmentManager component could not register the asset with the identifier specified in the AssetMessage.
        /// </summary>
        public event AssetNameCallback AssetNameRegisterFailedEvent;

        /// <summary>
        /// Occurs when the receiving EnvironmentManager component instantiates an instance of the asset assosiated with the AssetMessage.
        /// </summary>
        public event AssetGameObjectCallback AssetInstancedEvent;

        // ----- Properties ----- //

        /// <summary>
        /// Gets the external resource that this asset represents.
        /// </summary>
        public string Resource
        {
            get { return resource; }
        }

        /// <summary>
        /// Gets the identifier to be used with GameObject that will be instantiated for unique referencing.
        /// </summary>
        public string Identifier
        {
            get { return identifier; }
        }

        /// <summary>
        /// Gets the position of the asset.
        /// </summary>
        public Vector3 Position
        {
            get { return position; }
        }

        /// <summary>
        /// Gets the Quanternion matrix that represents the rotation transform of the asset.
        /// </summary>
        public Quaternion Rotation
        {
            get { return rotation; }
        }

        /// <summary>
        /// Gets the scale transform of the asset.
        /// </summary>
        public Vector3 Scale
        {
            get { return scale; }
        }

        /// <summary>
        /// Gets the name of the IMessage, AssetMessage.
        /// </summary>
        /// 
        [ExcludeFromCodeCoverage]
        public string MessageName
        {
            get { return messageName; }
        }

        /// <summary>
        /// Sets or gets the source IMessageable of the AssetMessage.
        /// </summary>
        public IMessageable Source
        {
            set { source = value; }
            get { return source; }
        }

        /// <summary>
        /// Gets the Game Object instance created by the Environment Manager
        /// </summary>
        [ExcludeFromCodeCoverage]
        public GameObject GameObjectInstance
        {
            get { return instance; }
        }
    }
}