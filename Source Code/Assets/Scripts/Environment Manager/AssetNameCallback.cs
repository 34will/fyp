﻿using UnityEngine;
using System.Collections;

namespace CommonFramework.Environment
{
    /// <summary>
    /// AssetNameCallback represents the method that will handle an identifier of a GameObject being sent.
    /// </summary>
    /// <param name="manager">The EnvironmentManager the identifier is relevant in.</param>
    /// <param name="name">The named identifier of the GameObject with respect to the EnvironmentManager.</param>
    public delegate void AssetNameCallback(EnvironmentManager manager, string name);
}