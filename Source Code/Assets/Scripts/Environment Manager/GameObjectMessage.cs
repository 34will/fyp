﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using UnityEngine;

using CommonFramework.MessageSystem;

namespace CommonFramework.Environment
{
    /// <summary>
    /// GameObjectMessage is an implementation of IMessage that encapsulates a Unity GameObject. The role of this message is to transfer information about
    /// GameObjects between components. The main usage of this, is to inform the EnvironmentManager that a GameObject needs to be drawn.
	/// </summary>
	[ExcludeFromCodeCoverage]
    class GameObjectMessage : IMessage
    {
        internal const string messageName = "GameObjectMessage";
        private IMessageable source = null;
        private GameObject gameObject = null;

        /// <summary>
        /// Creates a new GameObjectMessage with the specified GameObject and no specified source IMessageable
        /// </summary>
        /// <param name="gameObject">The GameObject which the message encapsulates</param>
        public GameObjectMessage(GameObject gameObject)
            : this(gameObject, null) { }

        /// <summary>
        /// Creates a new GameObjectMessage with the specified GameObject and source IMessageable
        /// </summary>
        /// <param name="gameObject">The GameObject which the message encapsulates</param>
        /// <param name="source">The optional source IMessageable component that sends this message instance</param>
        public GameObjectMessage(GameObject gameObject, IMessageable source)
        {
            this.gameObject = gameObject;
            this.source = source;
        }

        // ----- Properties ----- //

        /// <summary>
        /// Gets the GameObject instance encapsulated by the GameObjectMessage
        /// </summary>
        public GameObject GameObject
        {
            get { return gameObject; }
        }

        /// <summary>
        /// Gets the name of the IMessage, GameObjectMessage
        /// </summary>
        public string MessageName
        {
            get { return messageName; }
        }

        /// <summary>
        /// Sets or Gets the source IMessageable component of the GameObjectMessage
        /// </summary>
        public IMessageable Source
        {
            set { source = value; }
            get { return source; }
        }
    }
}
