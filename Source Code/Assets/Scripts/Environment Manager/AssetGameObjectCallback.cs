﻿using UnityEngine;
using System.Collections;

namespace CommonFramework.Environment
{
    /// <summary>
    /// AssetGameObjectCallback represents the method that will be called when an EnvironmentManager utilises a GameObject.
    /// </summary>
    /// <param name="manager">The EnvironmentManager that the makes the method call.</param>
    /// <param name="instance">The relevant GameObject instance.</param>
    public delegate void AssetGameObjectCallback(EnvironmentManager manager, GameObject instance);
}