﻿using System;
using UnityEngine;

using CommonFramework.Maths;

namespace CommonFramework.MapLoader
{
	/// <summary>
	/// Stores a Geographic coordinate and provides helper functions.
	/// </summary>
	public class GeoCoordinate
	{
		/// <summary>
		/// The mean radius of the Earth, used in some of the calculations.
		/// </summary>
		public static readonly double EarthMeanRadius = 6376500.0;
		private double latitude = 0, longitude = 0;

		/// <summary>
		/// Creates a new GeoCoordinate at the specified latitude and longitude.
		/// </summary>
		/// <param name="latitude">Latitude of position.</param>
		/// <param name="longitude">Longitude of position.</param>
		public GeoCoordinate(double latitude, double longitude)
		{
			this.latitude = latitude;
			this.longitude = longitude;
		}

		/// <summary>
		/// Calculates the distance between this GeoCoordinate and another.
		/// </summary>
		/// <returns>The calculated distance between the coordinates.</returns>
		/// <param name="other">Other GeoCoordinate.</param>
		public double DistanceBetween(GeoCoordinate other)
		{
			return DistanceBetween(this, other);
		}

		/// <summary>
		/// Calculates the distance between this GeoCoordinate and the provided latitude and longitude.
		/// </summary>
		/// <returns>The calculated distance between the coordinates.</returns>
		/// <param name="latitude">Latitude of other point.</param>
		/// <param name="longitude">Longitude of other point.</param>
		public double DistanceBetween(double latitude, double longitude)
		{
			return DistanceBetween(this, latitude, longitude);
		}

		/// <summary>
		/// Calculates the distance between the GeoCoordinates a and b.
		/// </summary>
		/// <returns>The calculated distance between the coordinates, or double.NaN if either GeoCoordinate is invalid.</returns>
		/// <param name="a">GeoCoordinate A.</param>
		/// <param name="b">GeoCoordinate B.</param>
		public static double DistanceBetween(GeoCoordinate a, GeoCoordinate b)
		{
			if(a == null || b == null)
				return double.NaN;

			if(a == b)
				return 0.0;

			if(double.IsNaN(a.Latitude) || double.IsNaN(a.Longitude) || double.IsNaN(b.Latitude) || double.IsNaN(b.Longitude) || double.IsInfinity(a.Latitude) || double.IsInfinity(a.Longitude) || double.IsInfinity(b.Latitude) || double.IsInfinity(b.Longitude))
				return double.NaN;

			double aLat = Helper.DegreesToRadians(a.Latitude);
			double aLong = Helper.DegreesToRadians(a.Longitude);
			double bLat = Helper.DegreesToRadians(b.Latitude);
			double bLong = Helper.DegreesToRadians(b.Longitude);

			double det = Math.Pow(Math.Sin((bLat - aLat) / 2.0), 2.0) + Math.Cos(aLat) * Math.Cos(bLat) * Math.Pow(Math.Sin((bLong - aLong) / 2.0), 2.0);
			return EarthMeanRadius * (2.0 * Math.Atan2(Math.Sqrt(det), Math.Sqrt(1.0 - det)));
		}

		/// <summary>
		/// Calculates the distance between the GeoCoordinates a and the provided latitude and longitude.
		/// </summary>
		/// <returns>The calculated distance between the coordinates.</returns>
		/// <param name="a">GeoCoordinate A.</param>
		/// <param name="latitude">Latitude of other point.</param>
		/// <param name="longitude">Longitude of other point.</param>
		public static double DistanceBetween(GeoCoordinate a, double latitude, double longitude)
		{
			return DistanceBetween(a, new GeoCoordinate(latitude, longitude));
		}

		/// <summary>
		/// Calculates the position of the specified GeoCordinate p relative to a specific origin.
		/// </summary>
		/// <returns>The calculated position of p, relative to origin.</returns>
		/// <param name="p">The point to calculate the position of.</param>
		/// <param name="origin">The point to calculate the position relative to.</param>
		public static Vector3 RelativePosition(GeoCoordinate p, GeoCoordinate origin)
		{
			return (p == null || origin == null) ? Helper.NaV3 : new Vector3((float)origin.DistanceBetween(new GeoCoordinate(origin.Latitude, p.Longitude)), 0, (float)origin.DistanceBetween(new GeoCoordinate(p.Latitude, origin.Longitude)));
		}

		/// <summary>
		/// Determines whether the specified <see cref="System.Object"/> is equal to the current <see cref="CommonFramework.MapLoader.GeoCoordinate"/>.
		/// </summary>
		/// <param name="other">The <see cref="System.Object"/> to compare with the current <see cref="CommonFramework.MapLoader.GeoCoordinate"/>.</param>
		/// <returns><c>true</c> if the specified <see cref="System.Object"/> is equal to the current <see cref="CommonFramework.MapLoader.GeoCoordinate"/>; otherwise, <c>false</c>.</returns>
		public override bool Equals(System.Object other)
		{
			if(other == null)
				return false;

			GeoCoordinate gc = other as GeoCoordinate;
			if(gc == null)
				return false;

			return (Latitude == gc.Latitude) && (Longitude == gc.Longitude);
		}

		/// <param name="a">GeoCoordinate a.</param>
		/// <param name="b">GeoCoordinate b.</param>
		public static bool operator ==(GeoCoordinate a, GeoCoordinate b)
		{
			if(System.Object.ReferenceEquals(a, b))
				return true;

			if(((object)a == null) || ((object)b == null))
				return false;

			return a.Equals(b);
		}

		/// <param name="a">GeoCoordinate a.</param>
		/// <param name="b">GeoCoordinate b.</param>
		public static bool operator !=(GeoCoordinate a, GeoCoordinate b)
		{
			return !(a == b);
		}

		/// <summary>
		/// Serves as a hash function for a <see cref="CommonFramework.MapLoader.GeoCoordinate"/> object.
		/// </summary>
		/// <returns>A hash code for this instance that is suitable for use in hashing algorithms and data structures such as a hash table.</returns>
		public override int GetHashCode()
		{
			unchecked
			{
				int hash = 345;
				hash = hash * 562 + latitude.GetHashCode();
				hash = hash * 234 + longitude.GetHashCode();
				return hash;
			}
		}

		// ----- Properties ----- //

		/// <summary>
		/// Gets the latitude of the GeoCoordinate.
		/// </summary>
		public double Latitude
		{
			get { return latitude; }
		}

		/// <summary>
		/// Gets the longitude of the GeoCoordinate
		/// </summary>
		public double Longitude
		{
			get { return longitude; }
		}
	}
}
