﻿using System;
using System.Runtime.CompilerServices;

using UnityEngine;
using CommonFramework.Maths;

[assembly: InternalsVisibleTo("CommonFrameworkUnitTests")]

namespace CommonFramework.MapLoader
{
	/// <summary>
	/// OSMNode is a cut-down representation of an &lt;node&gt; in an OpenStreetMap file.
	/// </summary>
	internal class OSMNode
	{
		private long id = 0;
		private GeoCoordinate geoCoord = new GeoCoordinate(0, 0);
		private Vector3 position = Helper.NaV3;

		/// <summary>
		/// Creates a new OSMNode with the specified Id, Latitude and Longitude.
		/// </summary>
		/// <param name="id">Identifier.</param>
		/// <param name="latitude">Latitude.</param>
		/// <param name="longitude">Longitude.</param>
		public OSMNode(long id, double latitude, double longitude)
		{
			this.id = id;
			geoCoord = new GeoCoordinate(latitude, longitude);
		}

		/// <summary>
		/// Calculates the position of the OSMNode, relative to the specified origin OSMNode.
		/// </summary>
		/// <param name="origin">The OSMNode to calculate the position relative to.</param>
		public void CalculateRelativePosition(OSMNode origin)
		{
			if(origin != null)
				CalculateRelativePosition(origin.geoCoord);
		}

		/// <summary>
		/// Calculates the position of the OSMNode, relative to the specified origin GeoCoordinate.
		/// </summary>
		/// <param name="origin">The GeoCoordinate to calculate the position relative to.</param>
		public void CalculateRelativePosition(GeoCoordinate origin)
		{
			position = GeoCoordinate.RelativePosition(geoCoord, origin);
		}

		// ----- Properties ----- //

		/// <summary>
		/// Gets the Id of the node.
		/// </summary>
		public long ID
		{
			get { return id; }
		}

		/// <summary>
		/// Gets the latitude of the node.
		/// </summary>
		public double Latitude
		{
			get { return geoCoord.Latitude; }
		}

		/// <summary>
		/// Gets the longitude of the node.
		/// </summary>
		public double Longitude
		{
			get { return geoCoord.Longitude; }
		}

		/// <summary>
		/// Gets the relative position of the node.
		/// </summary>
		public Vector3 RelativePosition
		{
			get { return position; }
		}
	}
}
