﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Diagnostics.CodeAnalysis;

using UnityEngine;

using CommonFramework;
using CommonFramework.Environment;
using CommonFramework.MessageSystem;
using CommonFramework.LineDrawer;
using CommonFramework.MovementControl;
using CommonFramework.Roads;
using CommonFramework.Logging;
using CommonFramework.Maths;

namespace CommonFramework.MapLoader
{
	/// <summary>
	/// OSMXmlParser parses the provided OpenStreetMap XML file into roads and buildings to be used by the system.
	/// </summary>
	public class OSMXmlParser
	{
		private static OSMXmlParser osmXmlParser = null;
		private string[] acceptedHighwayValues = { "motorway", "trunk", "primary", "secondary", "unclassified", "residential", "service", "motorway_link", "trunk_link", "primary_link", "secondary_link", "tertiary_link", "living_street", "pedestrian", "track", "raceway", "road" };

		private OSMXmlParser() { }

		/// <summary>
		/// If the singleton instance of XMLParser doesn't exist create it and return it, otherwise just return it.
		/// </summary>
		public static OSMXmlParser Instance
		{
			get { return osmXmlParser ?? (osmXmlParser = new OSMXmlParser()); }
		}

		/// <summary>
		/// Parses the OSM XML data file to obtain map data.
		/// </summary>
		/// <param name="osmFile">OSM file to be parsed.</param>
        [ExcludeFromCodeCoverage]
		public void ParseOSMFile(string osmFile)
		{
			parseOSMFile(osmFile);
		}

		/// <summary>
		/// Parses the OSM XML data file to obtain map data, and clips that data to the provided parameters.
		/// </summary>
		/// <param name="osmFile">OSM file to be parsed.</param>
		/// <param name="longMin">The minimum longitude of the area to load.</param>
		/// <param name="latMin">The minimum latitude of the area to load.</param>
		/// <param name="longMax">The maximum longitude of the area to load.</param>
        /// <param name="latMax">The maximum latitude of the area to load.</param>
        [ExcludeFromCodeCoverage]
		public void ParseClippedOSMFile(string osmFile, double longMin, double latMin, double longMax, double latMax)
		{
			parseOSMFile(osmFile, true, longMin, latMin, longMax, latMax);
		}

        [ExcludeFromCodeCoverage]
		private void parseOSMFile(string osmFile, bool clip = false, double longMin = double.NaN, double latMin = double.NaN, double longMax = double.NaN, double latMax = double.NaN)
        {
			XmlDocument osmDoc = ResourceHelper.LoadXMLResource(osmFile);
            if (osmDoc == null)
                return;

			Dictionary<long, OSMNode> nodes = new Dictionary<long, OSMNode>();
			Dictionary<string, OSMNode> buildings = new Dictionary<string, OSMNode>();
			double smallestLat = double.MaxValue, smallestLong = double.MaxValue, biggestLat = double.MinValue, biggestLong = double.MinValue;

			if(double.IsNaN(longMin) || double.IsNaN(latMin) || double.IsNaN(longMax) || double.IsNaN(latMax))
				clip = false;

			XmlNodeList xmlOSMNodes = osmDoc.DocumentElement.SelectNodes("/osm/node");
			foreach(XmlNode node in xmlOSMNodes)
			{
				string action = XMLHelper.ReadAttribute(node, "action");

				if(action != "delete")
				{
					double nodeLat = XMLHelper.ReadAttribute<double>(node, "lat");
					double nodeLong = XMLHelper.ReadAttribute<double>(node, "lon");
					long nodeID = XMLHelper.ReadAttribute<long>(node, "id");

					smallestLat = Math.Min(smallestLat, nodeLat);
					smallestLong = Math.Min(smallestLong, nodeLong);
					biggestLat = Math.Max(biggestLat, nodeLat);
					biggestLong = Math.Max(biggestLong, nodeLong);

					// If the node contains children, it is an object to spawn.
					if(node.HasChildNodes)
					{
						string k = "", v = "";
						foreach(XmlNode childNode in node.ChildNodes)
						{
							k = XMLHelper.ReadAttribute<string>(childNode, "k");

							if(k == "name")
							{
								v = XMLHelper.ReadAttribute<string>(childNode, "v");

								// TODO - Need a way to detect what resource to create, see tag with k="amenity".
								// Warning - If two nodes have the same ID, which technically shouldn't happen, the previous node will be overwritten by the later node.
								buildings[v] = new OSMNode(nodeID, nodeLat, nodeLong);
							}
						}
					}
					else
					{
						// Warning - If two nodes have the same ID, which technically shouldn't happen, the previous node will be overwritten by the later node.
						nodes[nodeID] = new OSMNode(nodeID, nodeLat, nodeLong);
					}
				}
			}

			GeoCoordinate origin = new GeoCoordinate(smallestLat, smallestLong);
			float originToClipX = 0.0f;
			float originToClipY = 0.0f;
			float clipWidth = (float)GeoCoordinate.DistanceBetween(origin, smallestLat, biggestLong);
			float clipHeight = (float)GeoCoordinate.DistanceBetween(origin, biggestLat, smallestLong);

			if(clip)
			{
				GeoCoordinate originClip = new GeoCoordinate(latMin, longMin);
				originToClipX = (float)GeoCoordinate.DistanceBetween(origin, origin.Latitude, longMin);
				originToClipY = (float)GeoCoordinate.DistanceBetween(origin, latMin, origin.Longitude);
				clipWidth = (float)GeoCoordinate.DistanceBetween(originClip, latMin, longMax);
				clipHeight = (float)GeoCoordinate.DistanceBetween(originClip, latMax, longMin);
			}

			/*Vector3 A = new Vector3(originToClipX, 0, originToClipY);
			Vector3 B = new Vector3(originToClipX, 0, originToClipY + clipHeight);
			Vector3 C = new Vector3(originToClipX + clipWidth, 0, originToClipY + clipHeight);
			Vector3 D = new Vector3(originToClipX + clipWidth, 0, originToClipY);

			MessageHandler.Instance.SendMessage(new DrawLineMessage(A, B));
			MessageHandler.Instance.SendMessage(new DrawLineMessage(B, C));
			MessageHandler.Instance.SendMessage(new DrawLineMessage(C, D));
			MessageHandler.Instance.SendMessage(new DrawLineMessage(D, A));*/

			MessageHandler.Instance.SendMessage(new MoveToMessage(new Vector3(originToClipX + (clipWidth / 2.0f), 0, originToClipY + (clipHeight / 2.0f))), "Camera");
			Rect clipBox = new Rect(originToClipX, originToClipY, clipWidth, clipHeight);

			foreach(OSMNode node in nodes.Values)
				node.CalculateRelativePosition(origin);

			foreach(KeyValuePair<string, OSMNode> kvp in buildings)
			{
				kvp.Value.CalculateRelativePosition(origin);

				/*if(!clip || clipBox.Contains(new Vector2(kvp.Value.RelativePosition.x, kvp.Value.RelativePosition.z)))
					MessageHandler.Instance.SendMessage(new AssetMessage("cube", kvp.Key, kvp.Value.RelativePosition, Quaternion.identity, new Vector3(10, 10, 10)));*/
			}

			XmlNodeList waypoints = osmDoc.DocumentElement.SelectNodes("/osm/way");
			foreach(XmlNode waypoint in waypoints)
			{
				if(waypoint.HasChildNodes)
				{
					bool isRoad = false;
					XmlNodeList tagNodes = waypoint.SelectNodes("./tag");
					foreach(XmlNode tagNode in tagNodes)
					{
						string kAttr = XMLHelper.ReadAttribute(tagNode, "k");
						if(kAttr == "highway")
						{
							string vAttr = XMLHelper.ReadAttribute(tagNode, "v");
							if(vAttr != "")
							{
								foreach(string valid in acceptedHighwayValues)
								{
									if(vAttr == valid)
									{
										isRoad = true;
										break;
									}
								}

								if(isRoad)
									break;
							}
						}
					}

					if(isRoad)
					{
						List<Vector3> nodePoints = new List<Vector3>();
						XmlNodeList waypointNodes = waypoint.SelectNodes("./nd");
						foreach(XmlNode waypointNode in waypointNodes)
						{
							long nodeRefID = XMLHelper.ReadAttribute<long>(waypointNode, "ref", int.MaxValue);
							OSMNode outNode = null;
							if(nodeRefID == int.MaxValue || !nodes.TryGetValue(nodeRefID, out outNode))
								continue;

							nodePoints.Add(outNode.RelativePosition);
						}

						if(nodePoints.Count <= 1)
							continue;

						bool continueCheck = false, comingFromOff = false;

						for(int i = nodePoints.Count - 1; i > 0; i--)
						{
							RoadSegmentType type = RoadSegmentType.Middle;
							if(i == nodePoints.Count - 1)
							{
								if(i == 1)
									type = RoadSegmentType.StartAndEnd;
								else
									type = RoadSegmentType.Start;
							}
							else if(i == 1)
								type = RoadSegmentType.End;

							if(!clip)
								MessageHandler.Instance.SendMessage(new RoadSegmentMessage(nodePoints[i], nodePoints[i - 1], type));
							else
							{
								Vector2 a = new Vector2(nodePoints[i].x, nodePoints[i].z), b = new Vector2(nodePoints[i - 1].x, nodePoints[i - 1].z);
								bool containsA = clipBox.Contains(a), containsB = clipBox.Contains(b);

								if(containsA && containsB)
								{
									MessageHandler.Instance.SendMessage(new RoadSegmentMessage(nodePoints[i], nodePoints[i - 1], type));
									comingFromOff = false;
								}
								else if(containsA || containsB)
								{
									if(continueCheck)
										break;

									bool found = false;
									Vector2 intersection = Vector2.zero;

									for(int j = 0; j < 4; j++)
									{
										if(j == 0)
											intersection = Helper.IntersectionPoint(a, b, new Vector2(clipBox.x, clipBox.y), new Vector2(clipBox.x, clipBox.yMax));
										else if(j == 1)
											intersection = Helper.IntersectionPoint(a, b, new Vector2(clipBox.x, clipBox.yMax), new Vector2(clipBox.xMax, clipBox.yMax));
										else if(j == 2)
											intersection = Helper.IntersectionPoint(a, b, new Vector2(clipBox.xMax, clipBox.yMax), new Vector2(clipBox.xMax, clipBox.y));
										else
											intersection = Helper.IntersectionPoint(a, b, new Vector2(clipBox.xMax, clipBox.y), new Vector2(clipBox.x, clipBox.y));

										if(!Helper.IsNaV(intersection))
										{
											found = true;
											break;
										}
									}

									if(found)
									{
										Vector3 inter = new Vector3(intersection.x, 0.0f, intersection.y);

										if(containsA)
											MessageHandler.Instance.SendMessage(new RoadSegmentMessage(nodePoints[i], inter, comingFromOff ? RoadSegmentType.Start : RoadSegmentType.End));
										else
											MessageHandler.Instance.SendMessage(new RoadSegmentMessage(inter, nodePoints[i - 1], comingFromOff ? RoadSegmentType.Start : RoadSegmentType.End));

										comingFromOff = false;
									}

									continueCheck = true;
								}
								else
								{
									if(continueCheck)
										continue;

									bool p1Set = false, found = false;
									Vector2 intersection = Vector2.zero;
									Vector3 p1 = Vector3.zero, p2 = Vector3.zero;

									for(int j = 0; j < 4; j++)
									{
										if(j == 0)
											intersection = Helper.IntersectionPoint(a, b, new Vector2(clipBox.x, clipBox.y), new Vector2(clipBox.x, clipBox.yMax));
										else if(j == 1)
											intersection = Helper.IntersectionPoint(a, b, new Vector2(clipBox.x, clipBox.yMax), new Vector2(clipBox.xMax, clipBox.yMax));
										else if(j == 2)
											intersection = Helper.IntersectionPoint(a, b, new Vector2(clipBox.xMax, clipBox.yMax), new Vector2(clipBox.xMax, clipBox.y));
										else
											intersection = Helper.IntersectionPoint(a, b, new Vector2(clipBox.xMax, clipBox.y), new Vector2(clipBox.x, clipBox.y));

										if(!Helper.IsNaV(intersection))
										{
											if(p1Set)
											{
												p2 = new Vector3(intersection.x, 0.0f, intersection.y);
												found = true;
												break;
											}
											else
											{
												p1 = new Vector3(intersection.x, 0.0f, intersection.y);
												p1Set = true;
											}
										}
									}

									if(found)
										MessageHandler.Instance.SendMessage(new RoadSegmentMessage(p1, p2, RoadSegmentType.StartAndEnd));
								}
							}

							// TODO - Perhaps use this data somehow?
							/*string k = "", v = "";
							XmlNodeList waypointTags = waypoint.SelectNodes("./tag");
							foreach(XmlNode waypointNode in waypointTags)
							{
								k = XMLHelper.ReadAttribute<string>(waypointNode, "k");
								v = XMLHelper.ReadAttribute<string>(waypointNode, "v");

								Debug.Log(string.Format("waypointtag k: {0}, v: {1}", k, v));
							}*/
						}
					}
				}
			}

			MessageHandler.Instance.SendMessage(new BuildRoadGraphMessage());
		}
	}
}
