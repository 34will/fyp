﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using CommonFramework.MessageSystem;

namespace CommonFramework.MapLoader
{
    /// <summary>
    /// MapAssetMessage is an IMessage implementation that the OSMMapLoader uses to indicate assets present in the visible scene.
    /// </summary>
    public class MapAssetMessage : IMessage
    {
        private const string messageName = "MapAssetMessage";
        private IMessageable source = null;
        private string structureType = "unknown";
        private List<Vector3> cartesianWaypoints = new List<Vector3>();

        /// <summary>
        /// Creates a new MapAssetMessage representing the specified structure on a set of waypoints.
        /// </summary>
        /// <param name="structureType">The type of structure the waypoints represent.</param>
        /// <param name="cartesianWaypoints">The waypoints defining the perimeter of the artefact.</param>
        public MapAssetMessage(string structureType, List<Vector3> cartesianWaypoints)
            : this(structureType, cartesianWaypoints, null) { }

        /// <summary>
        /// Creates a new MapAssetMessage representing the specified structure on a set of waypoints.
        /// </summary>
        /// <param name="structureType">The type of structure the waypoints represent.</param>
        /// <param name="cartesianWaypoints">The waypoints defining the perimeter of the artefact.</param>
        /// <param name="source">The source IMessageable that is sending this MapAssetMessage.</param>
        public MapAssetMessage(string structureType, List<Vector3> cartesianWaypoints, IMessageable source)
        {
            this.source = source;
            this.structureType = structureType;
            this.cartesianWaypoints = cartesianWaypoints;
        }

        // ----- Properties ----- //

        /// <summary>
        /// Gets the identifier name for this type of message, MapAssetMessage.
        /// </summary>
        [ExcludeFromCodeCoverage]
        public string MessageName
        {
            get { return messageName; }
        }

        /// <summary>
        /// Sets or gets the source IMessageable that sent the MapAssetMessage.
        /// </summary>
        public IMessageable Source
        {
            set { source = value; }
            get { return source; }
        }

        /// <summary>
        /// Gets the named type of the structure this MapAssetMessage represents.
        /// </summary>
        public string StructureType
        {
            get { return structureType; }
        }

        /// <summary>
        /// Gets the collection of Vector3 points representing the perimeter of the artefact.
        /// </summary>
        public List<Vector3> CartesianWaypoints
        {
            get { return cartesianWaypoints; }
        }
    }
}