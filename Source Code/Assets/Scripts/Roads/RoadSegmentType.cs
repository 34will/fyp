﻿using System;

namespace CommonFramework.Roads
{
	/// <summary>
	/// RoadSegmentType is an enum stating which part of the road a segment is.
	/// </summary>
	public enum RoadSegmentType
	{
		/// <summary>
		/// Any middle part of the road.
		/// </summary>
		Middle = 0,

		/// <summary>
		/// The start of the road.
		/// </summary>
		Start,

		/// <summary>
		/// The end of the road.
		/// </summary>
		End,

		/// <summary>
		/// The segment is both the start and the end of the road.
		/// </summary>
		StartAndEnd
	}
}
