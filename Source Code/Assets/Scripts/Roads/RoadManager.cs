﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using UnityEngine;

using CommonFramework.MessageSystem;
using CommonFramework.LineDrawer;
using CommonFramework.Roads.Graph;

namespace CommonFramework.Roads
{
	/// <summary>
	/// RoadManager provides a way to add objects into the scene.
	/// </summary>
	public class RoadManager : MonoBehaviour, IMessageable
	{
		private const string messageableName = "RoadManager";
		private Queue<IMessage> pendingMessages = new Queue<IMessage>();
		private RoadGraph graph = new RoadGraph();
		private bool drawLines = false, makeMesh = true;

		public float RoadWidth = 1.0f;
		public Material RoadMaterial = null;
		public float CatmullParameterisation = 0.5f;
		public int NumberOfSegments = 20;

		/// <summary>
		/// Unity Awake() function: Awake this instance.
		/// </summary>
		[ExcludeFromCodeCoverage]
		void Awake()
		{
			MessageHandler.Instance.RegisterMessageable(this);
		}


        /// <summary>
        /// Unity OnGUI() function: Perform GUI operations for this instance.
        /// </summary>
        [ExcludeFromCodeCoverage]
        void OnGUI()
        {
            GUI.Label(new Rect(5, 5, 85, 30), "Catmull Spline");
            float prevParamValue = CatmullParameterisation;
            CatmullParameterisation = GUIHelper.FloatSliderAndTextBox(new Vector2(95, 5), CatmullParameterisation, 0.0f, 1.0f);

            GUI.Label(new Rect(5, 35, 85, 40), "Catmull Spline Segments");
            int prevSegmentsValue = NumberOfSegments;
            NumberOfSegments = GUIHelper.IntSliderAndTextBox(new Vector2(95, 40), NumberOfSegments, 2, 40);

            GUI.Label(new Rect(5, 75, 85, 30), "Road Width");
            float prevWidthValue = RoadWidth;
            RoadWidth = GUIHelper.FloatSliderAndTextBox(new Vector2(95, 75), RoadWidth, 0.5f, 5.0f);

            bool prevDrawLines = drawLines;
            drawLines = GUI.Toggle(new Rect(5, 105, 100, 21), drawLines, " Draw Lines");

            bool prevMakeMesh = makeMesh;
            makeMesh = GUI.Toggle(new Rect(5, 135, 100, 21), makeMesh, " Create Mesh");

            if(prevParamValue != CatmullParameterisation || prevSegmentsValue != NumberOfSegments || prevWidthValue != RoadWidth || prevDrawLines != drawLines || prevMakeMesh != makeMesh)
            {
                MessageHandler.Instance.SendMessage(new ClearLinesMessage());
                BuildMeshFromGraph();
            }
        }

		/// <summary>
		/// Informs the RoadManager that it needs to handle and process an IMessage.
		/// </summary>
		/// <param name="message">The IMessage that the RoadManager needs to process.</param>
		public void ReceiveMessage(IMessage message)
		{
			if(message.MessageName == RoadSegmentMessage.messageName)
			{
				RoadSegmentMessage roadMessage = message as RoadSegmentMessage;
				if(roadMessage == null) return;

				graph.AddRoad(roadMessage.Start, roadMessage.End, roadMessage.Type);
            }
            else if(message.MessageName == BuildRoadGraphMessage.messageName)
            {
                BuildRoadGraphMessage buildGraphMessage = message as BuildRoadGraphMessage;
                if(buildGraphMessage == null) return;

                BuildMeshFromGraph();
            }
		}

		private void BuildMeshFromGraph()
		{
			graph.BuildRoadsMesh(RoadWidth * 0.5f, CatmullParameterisation, NumberOfSegments, RoadMaterial, drawLines: drawLines, makeMesh: makeMesh);
		}

		// ----- Properties ----- //

		/// <summary>
		/// Gets the pending outgoing IMessage queue that needs to be processed by the RoadManager.
		/// </summary>
		public Queue<IMessage> PendingMessages
		{
			get { return pendingMessages; }
		}

		/// <summary>
		/// Gets the identifier name of the IMessageable component, RoadManager.
		/// </summary>
		public string MessageableName
		{
			get { return messageableName; }
		}
	}
}
