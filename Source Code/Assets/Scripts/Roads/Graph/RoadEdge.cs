﻿using System;
using System.Collections.Generic;

using UnityEngine;

using CommonFramework.Maths;

namespace CommonFramework.Roads.Graph
{
	/// <summary>
	/// RoadEdge represents an edge in the RoadGraph, and a segment of a road in the road network. It stores the start and end points, as well as useful data like direction and length.
	/// </summary>
	public class RoadEdge
	{
		private RoadNode start = null, end = null;
		private Vector3 direction = Vector3.zero;
		private Vector3[] shoulders = new Vector3[4];
		private float length = 0.0f;
		private RoadSegmentType type = RoadSegmentType.Middle;

		/// <summary>
		/// Creates a RoadEdge, adds it to the specified start and end points and initiliases the data.
		/// </summary>
		/// <param name="start">The RoadNode at the start of the edge.</param>
		/// <param name="end">The RoadNode at the end of the edge.</param>
		public RoadEdge(RoadNode start, RoadNode end, RoadSegmentType type)
		{
			this.start = start;
			this.end = end;
			this.type = type;

			if(start.AddConnectedRoad(this) && end.AddConnectedRoad(this))
			{
				ReCalculate();
			}
			else
			{
				this.start = null;
				this.end = null;
			}
		}

		private void ReCalculate()
		{
			Vector3 startToEnd = (end.Point - start.Point);
			length = startToEnd.magnitude;
			direction = startToEnd / length;

			Vector3 perpendicular = new Vector3(-direction.z, 0.0f, direction.x);
			SetShoulder(start, perpendicular);
			SetShoulder(start, -perpendicular);
			SetShoulder(end, perpendicular);
			SetShoulder(end, -perpendicular);
		}

		/// <summary>
		/// Set the shoulder vector for the supplied node. Calculates which 'side' of the edge the vector points in and sets the appropriate shoulder accordingly.
		/// </summary>
		/// <param name="node">Start or End RoadNode of the edge.</param>
		/// <param name="shoulder">Vector of the shoulder for the supplied node.</param>
		public void SetShoulder(RoadNode node, Vector3 shoulder)
		{
			bool nodeIsStart = node == start, nodeIsEnd = node == end;

			if(!nodeIsStart && !nodeIsEnd)
				return;

			int position = (int)Mathf.Sign((direction.x * -shoulder.z) + (direction.z * shoulder.x));

			if(position > 0)
				shoulders[nodeIsStart ? 0 : 2] = shoulder;
			else if(position < 0)
				shoulders[nodeIsStart ? 1 : 3] = shoulder;
		}

		/// <summary>
		/// Gets the specified data between this and the specified edge.
		/// </summary>
		/// <param name="other">Other RoadEdge to get data from.</param>
		/// <param name="commonPoint">Outputted RoadNode which is present in both RoadEdges.</param>
		/// <param name="thisDirection">Outputted normalised Vector3 pointing from the common point along this RoadEdge.</param>
		/// <param name="otherDirection">Outputted normalised Vector3 pointing from the common point along other RoadEdge.</param>
		/// <returns>True if this and the supplied RoadEdge share a common point, and thus all returned data is valid. False if otherwise.</returns>
		public bool GetCorrectDataBetween(RoadEdge other, out RoadNode commonPoint, out Vector3 thisDirection, out Vector3 otherDirection)
		{
			return GetCorrectDataBetween(this, out commonPoint, out thisDirection, out otherDirection);
		}

		/// <summary>
		/// Gets the angle between this and the specified edge.
		/// </summary>
		/// <param name="other">Other RoadEdge to calculate against.</param>
		/// <param name="inRadians">Whether or not to return angle in radians. Defaults to true.</param>
		/// <param name="lowerBoundIsZero">Whether or not to return angle in range: 0 to 2π/360 or the range: -π/-180 to π/180. Defaults to true.</param>
		/// <returns>Angle between, as a double, if this and the supplied RoadEdge share a common point, otherwise double.NaN.</returns>
		public double AngleBetween(RoadEdge other, bool inRadians = true, bool lowerBoundIsZero = false)
		{
			return AngleBetween(this, other, inRadians, lowerBoundIsZero);
		}

		/// <summary>
		/// Gets the noramlised vector halfway between this and the specified edge, in the direction of the smallest closing angle.
		/// </summary>
		/// <param name="other">Other RoadEdge to calculate against.</param>
		/// <returns>Vector halfway between, if this and the supplied RoadEdge share a common point, otherwise MathHelper.NaV.</returns>
		public Vector3 HalfwayBetween(RoadEdge other)
		{
			return HalfwayBetween(this, other);
		}

		/// <summary>
		/// Gets the noramlised vector halfway between and angle between this and the specified edge, with the vector pointing in the direction of the smallest closing angle.
		/// </summary>
		/// <param name="other">Other RoadEdge to calculate against.</param>
		/// <param name="angle">Outputted angle between this and the supplied RoadEdge, if they share a common point, otherwise, double.NaN.</param>
		/// <returns>Vector halfway between, if this and the supplied RoadEdge share a common point, otherwise MathHelper.NaV.</returns>
		public Vector3 HalfwayBetween(RoadEdge other, out double angle)
		{
			return HalfwayBetween(this, other, out angle);
		}

		private bool isEqualTo(RoadEdge notNull)
		{
			return (start == notNull.start && end == notNull.end) || (end == notNull.end && start == notNull.start);
		}

		/// <summary>
		/// Checks whether this edge and the supplied System.Object are equal.
		/// </summary>
		/// <param name="obj">Object to check.</param>
		/// <returns>True if they are equal, false if not.</returns>
		public override bool Equals(System.Object obj)
		{
			if(obj == null)
				return false;

			RoadEdge e = obj as RoadEdge;
			if((System.Object)e == null)
				return false;

			return isEqualTo(e);
		}

		/// <summary>
		/// Checks whether this edge and the supplied node are equal.
		/// </summary>
		/// <param name="n">RoadEdge to check.</param>
		/// <returns>True if they are equal, false if not.</returns>
		public bool Equals(RoadEdge e)
		{
			if((object)e == null)
				return false;

			return isEqualTo(e);
		}

		/// <summary>
		/// Gets the hash of this edge. Made as the summed hash of the start and end points, so should provide the same hash as an edge with the start and end flipped.
		/// </summary>
		/// <returns>The hash of this edge.</returns>
		public override int GetHashCode()
		{
			return start.GetHashCode() + end.GetHashCode();
		}

		/// <summary>
		/// Checks whether the two supplied edges are equal.
		/// </summary>
		/// <param name="a">First RoadEdge to check.</param>
		/// <param name="b">Second RoadEdge to check.</param>
		/// <returns>True if they are equal, false if not.</returns>
		public static bool operator ==(RoadEdge a, RoadEdge b)
		{
			if(System.Object.ReferenceEquals(a, b))
				return true;

			if(((object)a == null) || ((object)b == null))
				return false;

			return a.isEqualTo(b);
		}

		/// <summary>
		/// Checks whether the two supplied edges are not equal.
		/// </summary>
		/// <param name="a">First RoadEdge to check.</param>
		/// <param name="b">Second RoadEdge to check.</param>
		/// <returns>True if they are not equal, false if they are.</returns>
		public static bool operator !=(RoadEdge a, RoadEdge b)
		{
			return !(a == b);
		}

		/// <summary>
		/// Gets the specified data between the two specified edges.
		/// </summary>
		/// <param name="a">First RoadEdge to calculate from.</param>
		/// <param name="b">Second RoadEdge to calculate from.</param>
		/// <param name="commonPoint">Outputted RoadNode which is present in both RoadEdges.</param>
		/// <param name="aDirection">Outputted normalised Vector3 pointing from the common point along RoadEdge a.</param>
		/// <param name="bDirection">Outputted normalised Vector3 pointing from the common point along RoadEdge b.</param>
		/// <returns>True if the supplied RoadEdges share a common point, and thus all returned data is valid. False if otherwise.</returns>
		public static bool GetCorrectDataBetween(RoadEdge a, RoadEdge b, out RoadNode commonPoint, out Vector3 aDirection, out Vector3 bDirection)
		{
			commonPoint = null;
			aDirection = Vector3.zero;
			bDirection = Vector3.zero;

			if(a.Start == b.Start || a.Start == b.End)
				commonPoint = a.Start;
			else if(a.End == b.Start || a.End == b.End)
				commonPoint = a.End;
			else
				return false;

			aDirection = a.Start == commonPoint ? a.Direction : -a.Direction;
			bDirection = b.Start == commonPoint ? b.Direction : -b.Direction;

			return true;
		}

		/// <summary>
		/// Gets the angle between the specified edges.
		/// </summary>
		/// <param name="a">First RoadEdge to calculate from.</param>
		/// <param name="b">Second RoadEdge to calculate from.</param>
		/// <param name="inRadians">Whether or not to return angle in radians. Defaults to true.</param>
		/// <param name="lowerBoundIsZero">Whether or not to return angle in range: 0 to 2π/360 or the range: -π/-180 to π/180. Defaults to true.</param>
		/// <returns>Angle between, as a double, if the supplied RoadEdges share a common point, otherwise double.NaN.</returns>
		public static double AngleBetween(RoadEdge a, RoadEdge b, bool inRadians = true, bool lowerBoundIsZero = false)
		{
			RoadNode commonPoint;
			Vector3 aDir;
			Vector3 bDir;
			if(!GetCorrectDataBetween(a, b, out commonPoint, out aDir, out bDir))
				return double.NaN;

			return Helper.AngleBetween(aDir.ToV2XZ(), bDir.ToV2XZ(), inRadians, lowerBoundIsZero);
		}

		/// <summary>
		/// Gets the noramlised vector halfway between the specified edges, in the direction of the smallest closing angle.
		/// </summary>
		/// <param name="a">First RoadEdge to calculate from.</param>
		/// <param name="b">Second RoadEdge to calculate from.</param>
		/// <returns>Vector halfway between, if the supplied RoadEdges share a common point, otherwise MathHelper.NaV.</returns>
		public static Vector3 HalfwayBetween(RoadEdge a, RoadEdge b)
		{
			RoadNode commonPoint;
			Vector3 aDir;
			Vector3 bDir;
			if(!GetCorrectDataBetween(a, b, out commonPoint, out aDir, out bDir))
				return Helper.NaV3;

			return Helper.HalfwayBetween(aDir, bDir);
		}

		/// <summary>
		/// Gets the noramlised vector halfway and angle between the specified edges, with the vector pointing in the direction of the smallest closing angle.
		/// </summary>
		/// <param name="a">First RoadEdge to calculate from.</param>
		/// <param name="b">Second RoadEdge to calculate from.</param>
		/// <param name="angle">Outputted angle between the supplied RoadEdges, if they share a common point, otherwise, double.NaN.</param>
		/// <returns>Vector halfway between, if the supplied RoadEdges share a common point, otherwise MathHelper.NaV.</returns>
		public static Vector3 HalfwayBetween(RoadEdge a, RoadEdge b, out double angle)
		{
			angle = double.NaN;
			RoadNode commonPoint;
			Vector3 aDir;
			Vector3 bDir;
			if(!GetCorrectDataBetween(a, b, out commonPoint, out aDir, out bDir))
				return Helper.NaV3;

			angle = Helper.AngleBetween(aDir.ToV2XZ(), bDir.ToV2XZ(), true, true);
			return Helper.HalfwayBetween(aDir, bDir);
		}

		// ----- Properties ----- //

		/// <summary>
		/// The RoadNode at the start of this edge.
		/// </summary>
		public RoadNode Start
		{
			internal set
			{
				start = value;
				start.ForceConnection(end, this);
				ReCalculate();
			}
			get { return start; }
		}

		/// <summary>
		/// The RoadNode at the end of this edge.
		/// </summary>
		public RoadNode End
		{
			internal set
			{
				end = value;
				end.ForceConnection(start, this);
				ReCalculate();
			}
			get { return end; }
		}

		/// <summary>
		/// The RoadSegmentType of this edge.
		/// </summary>
		public RoadSegmentType Type
		{
			internal set { type = value; }
			get { return type; }
		}

		/// <summary>
		/// shoulderPointsA vector pointing from the Start node to the End node.
		/// </summary>
		public Vector3 Direction
		{
			get { return direction; }
		}

		/// <summary>
		/// The distance between the Start and End node.
		/// </summary>
		public float Length
		{
			get { return length; }
		}

		/// <summary>
		/// The normalised vector pointing halfway between this and the edge to the 'left' of the Start point. 'Left', in this case, does not necessarily mean the left side of the edge, but it corresponds to the same side as the EndLeftShoudler, thus the line from the StartLeftShoulder to the EndLeftShoulder is parallel to the line from Start to End. The default value is a vector perpendicular to Direction, pointing to the 'left' of the edge.
		/// </summary>
		public Vector3 StartLeftShoulder
		{
			get { return shoulders[0]; }
		}

		/// <summary>
		/// The normalised vector pointing halfway between this and the edge to the 'right' of the Start point. 'Right', in this case, does not necessarily mean the right side of the edge, but it corresponds to the same side as the EndRightShoudler, thus the line from the StartRightShoulder to the EndRightShoulder is parallel to the line from Start to End. The default value is a vector perpendicular to Direction, pointing to the 'right' of the edge.
		/// </summary>
		public Vector3 StartRightShoulder
		{
			get { return shoulders[1]; }
		}

		/// <summary>
		/// The normalised vector pointing halfway between this and the edge to the 'left' of the End point. 'Left', in this case, does not necessarily mean the left side of the edge, but it corresponds to the same side as the StartLeftShoudler, thus the line from the StartLeftShoulder to the EndLeftShoulder is parallel to the line from Start to End. The default value is a vector perpendicular to Direction, pointing to the 'left' of the edge.
		/// </summary>
		public Vector3 EndLeftShoulder
		{
			get { return shoulders[2]; }
		}

		/// <summary>
		/// The normalised vector pointing halfway between this and the edge to the 'right' of the End point. 'Right', in this case, does not necessarily mean the right side of the edge, but it corresponds to the same side as the StartRightShoudler, thus the line from the StartRightShoulder to the EndRightShoulder is parallel to the line from Start to End. The default value is a vector perpendicular to Direction, pointing to the 'right' of the edge.
		/// </summary>
		public Vector3 EndRightShoulder
		{
			get { return shoulders[3]; }
		}
	}
}
