﻿using System;
using System.Collections.Generic;

using UnityEngine;

using CommonFramework.Maths;

namespace CommonFramework.Roads.Graph
{
	/// <summary>
	/// RoadNode represents a node in the RoadGraph, and a point on a road, in the road network. It is either a junction, change of direction in a road or the end of a road. It maintains a list of connected RoadNodes and their corresponding connecting RoadEdges.
	/// </summary>
	public class RoadNode
	{
		private Vector3 point = Vector3.zero;
		private Dictionary<RoadNode, RoadEdge> connections = new Dictionary<RoadNode, RoadEdge>();
		private bool shouldersCalculated = false;

		/// <summary>
		/// Creates a RoadNode for the specified point.
		/// </summary>
		/// <param name="point">Location of the RoadNode.</param>
		public RoadNode(Vector3 point)
		{
			this.point = point;
		}

		/// <summary>
		/// Adds a RoadEdge which should be connected to this node.
		/// </summary>
		/// <param name="road">RoadEdge to add.</param>
		/// <returns>True if the provided RoadEdge is connected to this node, false if it is not, or if the shoulders of the connected edges have been calculated.</returns>
		public bool AddConnectedRoad(RoadEdge road)
		{
			if(!shouldersCalculated)
			{
				RoadNode otherEnd = road.Start == this ? road.End : road.Start;
				if(connections.ContainsKey(otherEnd))
					return false;
				else
				{
					connections.Add(otherEnd, road);
					return true;
				}
			}
			else return false;
		}

		/// <summary>
		/// Connects a RoadNode to this one, making a new RoadEdge.
		/// </summary>
		/// <param name="node">RoadNode to connect to this one.</param>
		/// <param name="type">The RoadSegmentType of the new RoadEdge.</param>
		/// <returns>A RoadEdge, either the newly created one, or the one already present for the supplied RoadNode.</returns>
		public RoadEdge AddConnectedNode(RoadNode node, RoadSegmentType type)
		{
			if(!shouldersCalculated && type != RoadSegmentType.Middle && !connections.ContainsKey(node))
			{
				RoadEdge newEdge = new RoadEdge(this, node, type);
				return newEdge.Start != null ? newEdge : null;
			}
			else return null;
		}

		internal void ForceConnection(RoadNode node, RoadEdge edge)
		{
			if(node != null && edge != null && !connections.ContainsKey(node))
				connections.Add(node, edge);
		}

		internal bool RemoveConnectedRoad(RoadNode otherEnd)
		{
			return connections.Remove(otherEnd);
		}

		public void CalculateShoulders()
		{
			if(connections.Count > 1)
			{
				SortedDictionary<double, RoadEdge> sortedEdges = new SortedDictionary<double, RoadEdge>();
				List<Vector3> halfways = new List<Vector3>();
				RoadEdge firstEdge = null;

				foreach(RoadEdge currEdge in connections.Values)
				{
					if(firstEdge == null)
						firstEdge = currEdge;
					else
						sortedEdges.Add(firstEdge.AngleBetween(currEdge, true, true), currEdge);
				}

				sortedEdges.Add(Helper.TwoPi + 100.0, firstEdge);

				RoadEdge prevEdge = firstEdge;
				foreach(RoadEdge currEdge in sortedEdges.Values)
				{
					double outAngle = 0.0;
					Vector3 halfway = prevEdge.HalfwayBetween(currEdge, out outAngle);

					if(halfway == Vector3.zero && Helper.AreEqual(outAngle, Helper.Pi))
						halfway = new Vector3(-currEdge.Direction.z, 0.0f, currEdge.Direction.x);
					else if(outAngle > Helper.Pi)
						halfway = -halfway;

					halfways.Add(halfway);
					prevEdge.SetShoulder(this, halfway);
					currEdge.SetShoulder(this, halfway);
					prevEdge = currEdge;
				}
			}
		}

		/// <summary>
		/// Checks whether this node and the supplied node are connected.
		/// </summary>
		/// <param name="node">RoadNode to check.</param>
		/// <returns>True if the nodeLookup are connected, false if not.</returns>
		public bool IsConnectedTo(RoadNode node)
		{
			return connections.ContainsKey(node);
		}

		private bool isEqualTo(Vector3 notNull)
		{
			return point == notNull;
		}

		/// <summary>
		/// Checks whether this node and the supplied System.Object are equal.
		/// </summary>
		/// <param name="obj">Object to check.</param>
		/// <returns>True if they are equal, false if not.</returns>
		public override bool Equals(System.Object obj)
		{
			if(obj == null)
				return false;

			RoadNode n = obj as RoadNode;
			if((System.Object)n == null)
				return false;

			return isEqualTo(n.point);
		}

		/// <summary>
		/// Checks whether this node and the supplied node are equal.
		/// </summary>
		/// <param name="n">RoadNode to check.</param>
		/// <returns>True if they are equal, false if not.</returns>
		public bool Equals(RoadNode n)
		{
			if((object)n == null)
				return false;

			return isEqualTo(n.point);
		}

		/// <summary>
		/// Checks whether the point of this node and the supplied point are equal.
		/// </summary>
		/// <param name="n">Vector3 to check.</param>
		/// <returns>True if they are equal, false if not.</returns>
		public bool Equals(Vector3 n)
		{
			if((object)n == null)
				return false;

			return isEqualTo(n);
		}

		/// <summary>
		/// Gets the hash of the point of this node.
		/// </summary>
		/// <returns>The hash of the point of this node.</returns>
		public override int GetHashCode()
		{
			return point.GetHashCode();
		}

		/// <summary>
		/// Checks whether the two supplied nodeLookup are equal.
		/// </summary>
		/// <param name="a">First RoadNode to check.</param>
		/// <param name="b">Second RoadNode to check.</param>
		/// <returns>True if they are equal, false if not.</returns>
		public static bool operator ==(RoadNode a, RoadNode b)
		{
			if(System.Object.ReferenceEquals(a, b))
				return true;

			if(((object)a == null) || ((object)b == null))
				return false;

			return a.isEqualTo(b.point);
		}

		/// <summary>
		/// Checks whether the two supplied nodeLookup are not equal.
		/// </summary>
		/// <param name="a">First RoadNode to check.</param>
		/// <param name="b">Second RoadNode to check.</param>
		/// <returns>True if they are not equal, false if they are.</returns>
		public static bool operator !=(RoadNode a, RoadNode b)
		{
			return !(a == b);
		}

		/// <summary>
		/// Checks whether the supplied node and the supplied point are equal.
		/// </summary>
		/// <param name="a">RoadNode to check.</param>
		/// <param name="b">Vector3 to check.</param>
		/// <returns>True if they are equal, false if not.</returns>
		public static bool operator ==(RoadNode a, Vector3 b)
		{
			if(((object)a == null) || ((object)b == null))
				return false;

			return a.isEqualTo(b);
		}

		/// <summary>
		/// Checks whether the supplied node and the supplied point are not equal.
		/// </summary>
		/// <param name="a">RoadNode to check.</param>
		/// <param name="b">Vector3 to check.</param>
		/// <returns>True if they are not equal, false if they are.</returns>
		public static bool operator !=(RoadNode a, Vector3 b)
		{
			return !(a == b);
		}

		// ----- Properties ----- //

		/// <summary>
		/// The location of the RoadNode in world space.
		/// </summary>
		public Vector3 Point
		{
			internal set { point = value; }
			get { return point; }
		}

		/// <summary>
		/// The RoadEdges connecting this node.
		/// </summary>
		public IEnumerable<RoadEdge> Connections
		{
			get { return connections.Values; }
		}

		/// <summary>
		/// The RoadNodes connected to this node.
		/// </summary>
		public IEnumerable<RoadNode> ConnectedNodes
		{
			get { return connections.Keys; }
		}

		/// <summary>
		/// The number of RoadNodes (and as such, RoadEdges) connected to this node.
		/// </summary>
		public int NumberOfConnections
		{
			get { return connections.Count; }
		}
	}
}
