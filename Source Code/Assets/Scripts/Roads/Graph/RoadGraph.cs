﻿using System;
using System.Collections.Generic;

using UnityEngine;

using CommonFramework.MessageSystem;
using CommonFramework.LineDrawer;
using CommonFramework.Containers;
using CommonFramework.Maths;
using CommonFramework.Terrain;

namespace CommonFramework.Roads.Graph
{
    /// <summary>
    /// RoadGraph represents an entire road network, made of RoadNodes and RoadEdges.
    /// </summary>
    public class RoadGraph
    {
        private Dictionary<Vector3, RoadNode> nodeLookup = new Dictionary<Vector3, RoadNode>();
        private HashSet<RoadNode> nodes = new HashSet<RoadNode>();
        private HashSet<RoadEdge> edges = new HashSet<RoadEdge>();
        private List<List<RoadEdge>> separateRoads = new List<List<RoadEdge>>();
        private bool firstSegment = true;
        private RoadSegmentType prevSegment = RoadSegmentType.Start;
        private GameObject roadsParent = null;
        private List<GameObject> roadMeshGameObjects = new List<GameObject>();
        private float largestX = float.MinValue, largestZ = float.MinValue;

        /// <summary>
        /// Adds a road to the graph, creating or retrieving the two RoadNodes and adding a RoadEdge.
        /// </summary>
        /// <param name="start">Start point of the road.</param>
        /// <param name="end">End point of the road.</param>
        /// <param name="type">Type of the road.</param>
        /// <returns>True if a new RoadEdge was added, false if it already exists in the graph.</returns>
        public bool AddRoad(Vector3 start, Vector3 end, RoadSegmentType type)
        {
            if(!Helper.IsValid(start) || !Helper.IsValid(end) || type == RoadSegmentType.Middle && prevSegment == RoadSegmentType.StartAndEnd || type == RoadSegmentType.StartAndEnd && prevSegment == RoadSegmentType.Middle)
                return false;

            RoadNode startNode = null;
            if(!nodeLookup.TryGetValue(start, out startNode))
            {
                startNode = new RoadNode(start);
                nodeLookup.Add(start, startNode);
                nodes.Add(startNode);
            }

            RoadNode endNode = null;
            if(!nodeLookup.TryGetValue(end, out endNode))
            {
                endNode = new RoadNode(end);
                nodeLookup.Add(end, endNode);
                nodes.Add(endNode);
            }

            RoadEdge edge = new RoadEdge(startNode, endNode, type);

            if(edge.Start == null || edge.End == null)
            {
                return false;
            }

            if(edges.Add(edge))
            {
                if(firstSegment)
                {
                    separateRoads.Add(new List<RoadEdge>() { edge });
                    firstSegment = false;
                }
                else
                {
                    if(type == RoadSegmentType.StartAndEnd)
                        separateRoads.Add(new List<RoadEdge>() { edge });
                    else if(prevSegment == RoadSegmentType.Middle)
                        separateRoads[separateRoads.Count - 1].Add(edge);
                    else if(type == RoadSegmentType.Start || type == RoadSegmentType.End)
                        separateRoads.Add(new List<RoadEdge>() { edge });
                    else if(type == RoadSegmentType.Middle)
                        separateRoads[separateRoads.Count - 1].Add(edge);
                }

                prevSegment = type;

                return true;
            }
            else
                return false;
        }

        private List<RoadEdge> BuildCatmullRomSplines(float catmullParameterisation, int numberOfSegments)
        {
            List<RoadEdge> catmullEdges = new List<RoadEdge>();
            Dictionary<Vector3, RoadNode> catmullNodeLookup = new Dictionary<Vector3, RoadNode>();

            foreach(List<RoadEdge> road in separateRoads)
            {
                List<Vector2> roadPoints = new List<Vector2>(road.Count + 1);

                int i = 0;
                for(i = 0; i < road.Count; i++)
                    roadPoints.Add(road[i].Start.Point.ToV2XZ());

                roadPoints.Add(road[i - 1].End.Point.ToV2XZ());

                List<Vector2> catmullRomSplines = Helper.CatmullRomSpline(roadPoints, catmullParameterisation, numberOfSegments);
                int count = catmullRomSplines.Count - 1;
                for(i = 0; i < count; i++)
                {
                    Vector3 start = catmullRomSplines[i].ToV3XZ();
                    Vector3 end = catmullRomSplines[i + 1].ToV3XZ();
                    RoadNode startNode = null;
                    if(!catmullNodeLookup.TryGetValue(start, out startNode))
                    {
                        startNode = new RoadNode(start);
                        catmullNodeLookup.Add(start, startNode);
                    }

                    RoadNode endNode = null;
                    if(!catmullNodeLookup.TryGetValue(end, out endNode))
                    {
                        endNode = new RoadNode(end);
                        catmullNodeLookup.Add(end, endNode);
                    }

                    RoadSegmentType type = RoadSegmentType.Middle;
                    if(count == 1)
                        type = RoadSegmentType.StartAndEnd;
                    else if(i == 0)
                        type = RoadSegmentType.Start;
                    else if(i == count - 1)
                        type = RoadSegmentType.End;

                    catmullEdges.Add(new RoadEdge(startNode, endNode, type));
                }
            }

            foreach(RoadNode node in catmullNodeLookup.Values)
                node.CalculateShoulders();

            return catmullEdges;
        }

        private void CleanOSMRoads()
        {
            Dictionary<int, List<int>> multipleNodes = new Dictionary<int, List<int>>();
            for(int j = 0; j < separateRoads.Count; j++)
            {
                for(int i = 0; i < separateRoads[j].Count; i++)
                {
                    if(i == 0 || separateRoads[j][i].Start.NumberOfConnections >= 3)
                    {
                        List<int> outNums;
                        if(multipleNodes.TryGetValue(j, out outNums))
                            outNums.Add(i);
                        else
                            multipleNodes[j] = new List<int>() { i };
                    }
                    if(i == (separateRoads[j].Count - 1) || separateRoads[j][i].End.NumberOfConnections >= 3)
                    {
                        List<int> outNums;
                        if(multipleNodes.TryGetValue(j, out outNums))
                            outNums.Add(i);
                        else
                            multipleNodes[j] = new List<int>() { i };
                    }
                }
            }

            List<int> tempKeys = new List<int>(multipleNodes.Keys);
            foreach(int i in tempKeys)
            {
                if(multipleNodes[i].Count == 2)
                    multipleNodes.Remove(i);
            }

            List<List<RoadEdge>> splitRoads = new List<List<RoadEdge>>();
            foreach(KeyValuePair<int, List<int>> kvp in multipleNodes)
            {
                if(kvp.Value.Count % 2 == 1)
                    continue;

                List<RoadEdge> toSplit = new List<RoadEdge>(separateRoads[kvp.Key]);

                for(int i = 0; i < kvp.Value.Count - 1; i += 2)
                {
                    List<RoadEdge> toAdd = new List<RoadEdge>();

                    if(kvp.Value[i] == kvp.Value[i + 1])
                    {
                        toSplit[kvp.Value[i]].Type = RoadSegmentType.StartAndEnd;
                        toAdd.Add(toSplit[kvp.Value[i]]);
                    }
                    else
                    {
                        toSplit[kvp.Value[i]].Type = RoadSegmentType.Start;
                        toSplit[kvp.Value[i + 1]].Type = RoadSegmentType.End;
                        for(int k = kvp.Value[i]; k <= kvp.Value[i + 1]; k++)
                            toAdd.Add(toSplit[k]);
                    }

                    splitRoads.Add(toAdd);
                }
            }

            int mod = 0;
            foreach(int toRemove in multipleNodes.Keys)
            {
                separateRoads.RemoveAt(toRemove - mod);
                mod++;
            }

            separateRoads.AddRange(splitRoads);
        }

        private void AddJunctionOffsets(float halfRoadWidth)
        {
            List<RoadNode> tempNodes = new List<RoadNode>(nodes);

            foreach(RoadNode node in tempNodes)
            {
                node.CalculateShoulders();

                if(node.NumberOfConnections <= 2)
                    continue;

                Dictionary<RoadEdge, RoadNode> edgesToSplit = new Dictionary<RoadEdge, RoadNode>();
                foreach(RoadEdge edge in node.Connections)
                {
                    Vector2 lShoudler = edge.Start == node ? edge.StartLeftShoulder.ToV2XZ() : edge.EndLeftShoulder.ToV2XZ();
                    Vector2 rShoudler = edge.Start == node ? edge.StartRightShoulder.ToV2XZ() : edge.EndRightShoulder.ToV2XZ();

                    double theta = Helper.AngleBetween(edge.Direction.ToV2XZ(), lShoudler, true, true);
                    double leftShoulderSD = double.MinValue;
                    if(!(Helper.AreEqual(theta, 0.0) || Helper.AreEqual(theta, Helper.HalfPi) || Helper.AreEqual(theta, Helper.Pi) || Helper.AreEqual(theta, Helper.ThreeHalfsPi) || Helper.AreEqual(theta, Helper.TwoPi)))
                        leftShoulderSD = Math.Abs(halfRoadWidth / Math.Tan(theta));

                    theta = Helper.AngleBetween(edge.Direction.ToV2XZ(), rShoudler, true, true);
                    double rightShoulderSD = double.MinValue;
                    if(!(Helper.AreEqual(theta, 0.0) || Helper.AreEqual(theta, Helper.HalfPi) || Helper.AreEqual(theta, Helper.Pi) || Helper.AreEqual(theta, Helper.ThreeHalfsPi) || Helper.AreEqual(theta, Helper.TwoPi)))
                        rightShoulderSD = Math.Abs(halfRoadWidth / Math.Tan(theta));

                    double biggerSD = Math.Max(leftShoulderSD, rightShoulderSD);
                    if(biggerSD == double.MinValue)
                        continue;

                    Vector3 newPoint = node.Point + ((float)biggerSD * (edge.Start == node ? edge.Direction : -edge.Direction));

                    if(Helper.AreEqual(edge.Length, biggerSD) || edge.Length < biggerSD)
                    {
                        RoadNode otherEnd = edge.Start == node ? edge.End : edge.Start;
                        if(otherEnd.NumberOfConnections == 2)
                        {
                            nodeLookup.Remove(otherEnd.Point);
                            nodes.Remove(otherEnd);
                            otherEnd.Point = newPoint;
                            otherEnd.CalculateShoulders();
                            nodeLookup.Add(otherEnd.Point, otherEnd);
                            nodes.Add(otherEnd);

                            foreach(RoadEdge otherEdge in otherEnd.Connections)
                            {
                                if(otherEdge != edge)
                                {
                                    RoadSegmentType otherEdgeType = otherEdge.Type;
                                    if(otherEdgeType == RoadSegmentType.Middle)
                                        otherEdge.Type = edge.Type;
                                    else if((otherEdgeType == RoadSegmentType.Start && edge.Type == RoadSegmentType.End) || (otherEdgeType == RoadSegmentType.Start && edge.Type == RoadSegmentType.End))
                                        otherEdge.Type = RoadSegmentType.StartAndEnd;
                                }
                            }
                        }
                        edge.Type = RoadSegmentType.StartAndEnd;
                    }
                    else
                    {
                        edgesToSplit.Add(edge, new RoadNode(newPoint));
                    }
                }

                List<RoadEdge> keys = new List<RoadEdge>(edgesToSplit.Keys);
                foreach(RoadEdge edge in keys)
                {

                    if(edge.Type == RoadSegmentType.Start || edge.Type == RoadSegmentType.End)
                    {
                        RoadNode newNode = edgesToSplit[edge];
                        if(!nodeLookup.TryGetValue(newNode.Point, out newNode))
                        {
                            newNode = edgesToSplit[edge];
                            nodes.Add(newNode);
                            nodeLookup.Add(newNode.Point, newNode);
                        }

                        RoadNode otherEnd = edge.Start == node ? edge.End : edge.Start;
                        node.RemoveConnectedRoad(otherEnd);
                        otherEnd.RemoveConnectedRoad(node);

                        if(edge.Start == node)
                            edge.Start = newNode;
                        else
                            edge.End = newNode;

                        RoadEdge newEdge = node.AddConnectedNode(newNode, RoadSegmentType.StartAndEnd);
                        edges.Add(newEdge);
                        separateRoads.Add(new List<RoadEdge>() { newEdge });
                    }
                }
            }
        }

        /// <summary>
        /// Builds the mesh of the road graph.
        /// </summary>
        /// <param name="halfRoadWidth">Width of the generated roads.</param>
        public void BuildRoadsMesh(float halfRoadWidth, float catmullParameterisation, int numberOfSegments, Material roadMaterial, float scale = 1.0f, bool drawLines = false, bool makeMesh = true)
        {
            if(!drawLines && !makeMesh)
                return;

            scale = Mathf.Max(scale, 0.01f);
            //List<Vector2> points = new List<Vector2>(nodes.Count);

            foreach(GameObject roadMeshGameObject in roadMeshGameObjects)
                UnityEngine.Object.Destroy(roadMeshGameObject);

            roadMeshGameObjects.Clear();

            if(makeMesh && roadsParent == null)
                roadsParent = new GameObject("Roads");

            CleanOSMRoads();

            AddJunctionOffsets(halfRoadWidth);

            List<RoadEdge> catmullEdges = BuildCatmullRomSplines(catmullParameterisation, numberOfSegments);
            List<Vector3> vertices = new List<Vector3>();
            Dictionary<Vector3, int> vertexLookup = new Dictionary<Vector3, int>();
            List<int> indices = new List<int>();
            List<Vector2> uvs = new List<Vector2>();
            float roadLength = 0.0f;
            bool lookupStart = false, lookupEnd = false;

            foreach(RoadEdge edge in catmullEdges)
            {
                Vector3[] shoulderPoints = new Vector3[4];
                int[] shoulderIndices = new int[4];
                float length = edge.Length;

                if(edge.Type == RoadSegmentType.Start || edge.Type == RoadSegmentType.StartAndEnd)
                {
                    roadLength = 0.0f;

                    if(makeMesh)
                    {
                        vertices = new List<Vector3>();
                        vertexLookup = new Dictionary<Vector3, int>();
                        indices = new List<int>();
                        uvs = new List<Vector2>();
                    }
                }

                if(edge.Start.NumberOfConnections > 2)
                    lookupStart = false;
                if(edge.End.NumberOfConnections > 2)
                    lookupEnd = false;

                Vector2 direction = edge.Direction.ToV2XZ();
                Vector2 tangent = direction.Tangent();
                Vector2 oppositeTangent = (-direction).Tangent();

                for(int i = 0; i < 4; i++)
                {
                    Vector3 shoulder = new Vector3();

                    if(i == 0)
                        shoulder = edge.StartLeftShoulder;
                    else if(i == 1)
                        shoulder = edge.StartRightShoulder;
                    else if(i == 2)
                        shoulder = edge.EndLeftShoulder;
                    else
                        shoulder = edge.EndRightShoulder;

                    Vector2 shoulderV2 = shoulder.ToV2XZ();
                    double theta = Helper.AngleBetween(direction, shoulderV2, true, true);
                    bool invalidForSin = Helper.AreEqual(theta, 0.0) || Helper.AreEqual(theta, Helper.Pi) || Helper.AreEqual(theta, Helper.TwoPi);
                    double shoulderScale = invalidForSin ? halfRoadWidth : Math.Abs(halfRoadWidth / Math.Sin(theta));
                    shoulderPoints[i] = (i < 2 ? edge.Start.Point : edge.End.Point) + (shoulder * (float)shoulderScale * scale);

                    if(makeMesh)
                    {
                        if(!((lookupStart || lookupEnd) && vertexLookup.TryGetValue(shoulderPoints[i], out shoulderIndices[i])))
                        {
                            vertices.Add(shoulderPoints[i]);
                            shoulderIndices[i] = vertices.Count - 1;

                            if(lookupStart)
                                vertexLookup.Add(shoulderPoints[i], shoulderIndices[i]);

                            double alpha = Helper.AngleBetween(i < 2 ? tangent : oppositeTangent, shoulderV2, true, true);
                            if(alpha != Helper.HalfPi && alpha != Helper.ThreeHalfsPi)
                            {
                                if(alpha > Helper.Pi)
                                    alpha -= Helper.Pi;
                                else if(alpha > Helper.HalfPi)
                                    alpha = Helper.Pi - alpha;

                                uvs.Add(new Vector2(i % 2, roadLength + (i < 2 ? 0.0f : length) + (Mathf.Sign(Vector2.Dot(direction, shoulderV2)) * (float)(halfRoadWidth * Math.Tan(alpha)))));
                            }
                        }
                    }
                }

                if(edge.Type == RoadSegmentType.Middle)
                    roadLength += length;

                if(drawLines)
                {
                    Color c = Color.white;

                    if(edge.Type == RoadSegmentType.Start)
                        c = Color.green;
                    else if(edge.Type == RoadSegmentType.End)
                        c = Color.red;
                    else if(edge.Type == RoadSegmentType.StartAndEnd)
                        c = Color.blue;

                    MessageHandler.Instance.SendMessage(new DrawLineMessage(shoulderPoints[0], shoulderPoints[1], c));
                    MessageHandler.Instance.SendMessage(new DrawLineMessage(shoulderPoints[0], shoulderPoints[2], c));
                    MessageHandler.Instance.SendMessage(new DrawLineMessage(shoulderPoints[2], shoulderPoints[1], c));
                    MessageHandler.Instance.SendMessage(new DrawLineMessage(shoulderPoints[3], shoulderPoints[1], c));
                    MessageHandler.Instance.SendMessage(new DrawLineMessage(shoulderPoints[3], shoulderPoints[2], c));
                }

                if(makeMesh)
                {
                    indices.Add(shoulderIndices[0]);
                    indices.Add(shoulderIndices[1]);
                    indices.Add(shoulderIndices[2]);
                    indices.Add(shoulderIndices[2]);
                    indices.Add(shoulderIndices[1]);
                    indices.Add(shoulderIndices[3]);

                    if(edge.Start.NumberOfConnections >= 3)
                    {
                        vertices.Add(edge.Start.Point);
                        uvs.Add(new Vector2(0.5f, 0.5f));

                        vertices.Add(shoulderPoints[1]);
                        uvs.Add(new Vector2(0.0f, 1.0f));

                        vertices.Add(shoulderPoints[0]);
                        uvs.Add(new Vector2(1.0f, 1.0f));

                        indices.Add(vertices.Count - 3);
                        indices.Add(vertices.Count - 2);
                        indices.Add(vertices.Count - 1);
                    }
                    if(edge.End.NumberOfConnections >= 3)
                    {
                        vertices.Add(edge.End.Point);
                        uvs.Add(new Vector2(0.5f, 0.5f));

                        vertices.Add(shoulderPoints[2]);
                        uvs.Add(new Vector2(0.0f, 1.0f));

                        vertices.Add(shoulderPoints[3]);
                        uvs.Add(new Vector2(1.0f, 1.0f));

                        indices.Add(vertices.Count - 3);
                        indices.Add(vertices.Count - 2);
                        indices.Add(vertices.Count - 1);
                    }

                    if(edge.Type == RoadSegmentType.End || edge.Type == RoadSegmentType.StartAndEnd)
                    {
                        GameObject tempRoadMeshGameObject = new GameObject("Road Mesh Game Object " + (roadMeshGameObjects.Count + 1));
                        MeshRenderer renderer = tempRoadMeshGameObject.AddComponent<MeshRenderer>();
                        MeshFilter filter = tempRoadMeshGameObject.AddComponent<MeshFilter>();
                        Mesh roadMesh = new Mesh();
                        roadMesh.name = "Road Mesh";

                        Vector3[] tempVertices = new Vector3[vertices.Count];
                        vertices.CopyTo(tempVertices);
                        roadMesh.vertices = tempVertices;

                        for(int i = tempVertices.Length - 1; i >= 0; i--)
                        {
                            largestX = Math.Max(largestX, tempVertices[i].x);
                            largestZ = Math.Max(largestZ, tempVertices[i].z);
                        }

                        int[] tempIndices = new int[indices.Count];
                        indices.CopyTo(tempIndices);
                        roadMesh.triangles = tempIndices;

                        Vector2[] tempUVs = new Vector2[uvs.Count];
                        uvs.CopyTo(tempUVs);
                        roadMesh.uv = tempUVs;

                        roadMesh.RecalculateNormals();

                        filter.mesh = roadMesh;
                        renderer.material = roadMaterial;

                        tempRoadMeshGameObject.transform.parent = roadsParent.transform;

                        roadMeshGameObjects.Add(tempRoadMeshGameObject);
                    }
                }

                /*points.Add(shoulderPoints[0].ToV2XZ());
                points.Add(shoulderPoints[1].ToV2XZ());
                points.Add(shoulderPoints[2].ToV2XZ());
                points.Add(shoulderPoints[3].ToV2XZ());*/
            }

            /*List<Vector2> convex = Helper.ConvexHull(points);
            for(int i = convex.Count - 1; i > 0; i--)
                MessageHandler.Instance.SendMessage(new DrawLineMessage(convex[i].ToV3XZ(1.0f), convex[i - 1].ToV3XZ(1.0f), Color.red));*/

            if(makeMesh)
            {
                MessageHandler.Instance.SendMessage(new PrepareTerrainMessage(largestX, largestZ));

                foreach(GameObject go in roadMeshGameObjects)
                {
                    MeshFilter mf = go.GetComponent<MeshFilter>();
                    if(mf == null)
                        continue;

                    Mesh mesh = mf.mesh;
                    if(mesh == null)
                        continue;

                    int[] inds = mesh.triangles;
                    for(int i = 0; i < inds.Length; i += 3)
                        MessageHandler.Instance.SendMessage(new BlockTriangleMessage(mesh.vertices[inds[i]], mesh.vertices[inds[i + 1]], mesh.vertices[inds[i + 2]]));
                }
            }
        }

        // ----- Properties ----- //

        /// <summary>
        /// A collection of all the RoadEdges in the graph.
        /// </summary>
        public IEnumerable<RoadEdge> Edges
        {
            get { return edges; }
        }

        /// <summary>
        /// A collection of all the RoadNodes in the graph.
        /// </summary>
        public IEnumerable<RoadNode> Nodes
        {
            get { return nodes; }
        }
    }
}
