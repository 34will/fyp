using System;

using UnityEngine;

using CommonFramework.MessageSystem;

namespace CommonFramework.Roads
{
    /// <summary>
    /// RoadSegmentMessage is an IMessage implementation that the RoadManager uses to add roads to the scene.
    /// </summary>
    public class RoadSegmentMessage : IMessage
    {
        internal const string messageName = "RoadSegmentMessage";
        private Vector3 start = Vector3.zero, end = Vector3.zero;
		private IMessageable source = null;
		private RoadSegmentType roadSegmentType = RoadSegmentType.Middle;

		/// <summary>
		/// Creates a new RoadSegmentMessage representing a road in the scene.
		/// </summary>
		/// <param name="start">Start point of the road.</param>
		/// <param name="end">End point of the road.</param>
		/// <param name="roadSegementType">The type of the road segment.</param>
		public RoadSegmentMessage(Vector3 start, Vector3 end, RoadSegmentType roadSegmentType)
		{
			this.start = start;
			this.end = end;
			this.roadSegmentType = roadSegmentType;
		}
        
        // ----- Properties ----- //
        
        /// <summary>
        /// Get the start point of the line segment.
        /// </summary>
        public Vector3 Start
        {
            get { return start; }
        }
		
		/// <summary>
		/// Get the type of the road segment.
		/// </summary>
		public RoadSegmentType Type
		{
			get { return roadSegmentType; }
		}

		/// <summary>
		/// Get the end point of the line segment.
		/// </summary>
		public Vector3 End
		{
			get { return end; }
		}

        /// <summary>
        /// Gets the named identifier of the IMessage.
        /// </summary>
        public string MessageName
        {
            get { return messageName; }
        }

        /// <summary>
        /// Gets the source IMessageable object the message was sent from.
        /// </summary>
        public IMessageable Source
        {
            get { return source; }
            set { source = value; }
        }
    }
}

