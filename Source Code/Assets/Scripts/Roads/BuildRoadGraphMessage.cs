﻿using System;
using UnityEngine;

using CommonFramework.MessageSystem;

namespace CommonFramework.Roads
{
	/// <summary>
	/// BuildRoadGraphMessage is an IMessage implementation that the RoadManager uses to build the road graph.
	/// </summary>
	public class BuildRoadGraphMessage : IMessage
	{
		internal const string messageName = "BuildRoadGraphMessage";
		private IMessageable source = null;

		/// <summary>
		/// Creates a new BuildRoadGraphMessage to signal starting the road graph build process.
        /// <param name="source">Optional source IMessageable for this message.</param>
		/// </summary>
		public BuildRoadGraphMessage(IMessageable source = null) 
        {
            this.source = source;
        }

		// ----- Properties ----- //

		/// <summary>
		/// Gets the named identifier of the IMessage.
		/// </summary>
		public string MessageName
		{
			get { return messageName; }
		}

		/// <summary>
		/// Gets the source IMessageable object the message was sent from.
		/// </summary>
		public IMessageable Source
		{
			get { return source; }
			set { source = value; }
		}
	}
}
