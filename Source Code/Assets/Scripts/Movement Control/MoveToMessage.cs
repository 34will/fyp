﻿using UnityEngine;
using System.Collections;

using CommonFramework.MessageSystem;

namespace CommonFramework.MovementControl
{
    /// <summary>
    /// MoveToMessage is an IMessage that is handled by the Mover, used to inform it that it should move the camera to a location.
    /// </summary>
	public class MoveToMessage : IMessage 
	{
        internal const string messageName = "MoveToMessage";
        private Vector3 point = new Vector3(0, 0, 0);
        private IMessageable source = null;
        
        /// <summary>
        /// Creates a MoveToMessage, specifiying the point to move the camera to.
        /// </summary>
        /// <param name="point">Point to move the camera to.</param>
        public MoveToMessage(Vector3 point)
        {
            this.point = point;
        }

        /// <summary>
        /// Creates a MoveToMessage, specifiying the point to move the camera to.
        /// </summary>
        /// <param name="x">X-Coord of point to move the camera to.</param>
        /// <param name="y">Y-Coord of point to move the camera to.</param>
        /// <param name="z">Z-Coord of point to move the camera to.</param>
        public MoveToMessage(float x, float y, float z) : this(new Vector3(x, y, z)) { }

        // ----- Properties ----- //
        
        /// <summary>
        /// Point to move the camera to.
        /// </summary>
        public Vector3 Point
        {
            get { return point; }
        }

        /// <summary>
        /// Gets the name of the IMessage, AssetMessage.
        /// </summary>
        public string MessageName
        {
            get { return messageName; }
        }

        /// <summary>
        /// Sets or gets the source IMessageable of the AssetMessage.
        /// </summary>
        public IMessageable Source
        {
            set { source = value; }
            get { return source; }
        }
	}
}
