﻿using UnityEngine;
using System.Collections;

using CommonFramework.MessageSystem;

namespace CommonFramework.MovementControl
{
    /// <summary>
    /// MoveMessage is an IMessage that is handled by the Mover, used to inform it that it should move the camera by an amount.
    /// </summary>
	public class MoveMessage : IMessage 
	{
        internal const string messageName = "MoveMessage";
        private Vector3 translation = new Vector3(0, 0, 0);
        private IMessageable source = null;

        /// <summary>
        /// Creates a MoveMessage, specifiying the offset to move the camera by.
        /// </summary>
        /// <param name="translation">Offset to move the camera by.</param>
        public MoveMessage(Vector3 translation)
        {
            this.translation = translation;
        }

        /// <summary>
        /// Creates a MoveMessage, specifiying the offset to move the camera by.
        /// </summary>
        /// <param name="x">Amount to offset the camera by in the X-direction.</param>
        /// <param name="y">Amount to offset the camera by in the Y-direction.</param>
        /// <param name="z">Amount to offset the camera by in the Z-direction.</param>
        public MoveMessage(float x, float y, float z) : this(new Vector3(x, y, z)) { }

        // ----- Properties ----- //
        
        /// <summary>
        /// Vector3 specifying the translation to apply to the camera.
        /// </summary>
        public Vector3 Translation
        {
            get { return translation; }
        }

        /// <summary>
        /// Gets the name of the IMessage, AssetMessage.
        /// </summary>
        public string MessageName
        {
            get { return messageName; }
        }

        /// <summary>
        /// Sets or gets the source IMessageable of the AssetMessage.
        /// </summary>
        public IMessageable Source
        {
            set { source = value; }
            get { return source; }
        }
	}
}
