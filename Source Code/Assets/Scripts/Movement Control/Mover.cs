﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using UnityEngine;

using CommonFramework.MessageSystem;

namespace CommonFramework.MovementControl
{
    /// <summary>
    /// Mover provides a way to move the attached camera via the message system.
	/// </summary>
	[ExcludeFromCodeCoverage]
    public class Mover : MonoBehaviour, IMessageable
    {
        private string messageableName = "";
        private Queue<IMessage> pendingMessages = new Queue<IMessage>();

        /// <summary>
        /// The camera to apply the movement to.
        /// </summary>
        public Transform Moveable = null;

        /// <summary>
        /// Unity Awake() function: Awake this instance.
        /// </summary>
        void Awake()
        {
            if (Moveable != null)
                Moveable = transform;

            messageableName = Moveable.name;

            MessageHandler.Instance.RegisterMessageable(this);
        }

        /// <summary>
        /// Informs the Mover that it needs to handle and process an IMessage.
        /// </summary>
        /// <param name="message">The IMessage that the Mover needs to process.</param>
        public void ReceiveMessage(IMessage message)
        {
            if (Moveable != null)
            {
                if(message.MessageName == MoveMessage.messageName)
                {
                    MoveMessage cameraMoveMessage = message as MoveMessage;
                    if(cameraMoveMessage == null)
                        return;

                    Moveable.Translate(cameraMoveMessage.Translation);
                }
                else if(message.MessageName == MoveToMessage.messageName)
                {
                    MoveToMessage cameraMoveToMessage = message as MoveToMessage;
                    if(cameraMoveToMessage == null)
                        return;

                    Moveable.position = cameraMoveToMessage.Point;
                }
            }
        }

        // ----- Properties ----- //

        /// <summary>
        /// Gets the pending outgoing IMessage queue that needs to be processed by the Mover.
        /// </summary>
        public Queue<IMessage> PendingMessages
        {
            get { return pendingMessages; }
        }

        /// <summary>
        /// Gets the identifier name of the IMessageable component, Mover.
        /// </summary>
        public string MessageableName
        {
            get { return messageableName; }
        }
    }
}
