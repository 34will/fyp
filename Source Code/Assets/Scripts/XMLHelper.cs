﻿using UnityEngine;
using System.Collections;
using System.ComponentModel;
using System.Xml;

namespace CommonFramework
{
	/// <summary>
	/// XMLHelper provides useful utility functions for working with XmlDocuments.
	/// </summary>
	public static class XMLHelper
	{
		/// <summary>
		/// Reads the attribute of a specified XmlNode using its identifier.
		/// </summary>
		/// <param name="xmlNode">The XmlNode to read the attribute from.</param>
		/// <param name="attribute">The string named identifier of the attribute.</param>
		/// <param name="defaultValue">The optional default value to return if no attribute of the specified identifier exists, defaults to "".</param>
		/// <returns>The string contents of the attribute if it exists, otherwise the specified default value.</returns>
		public static string ReadAttribute(XmlNode xmlNode, string attribute, string defaultValue = "")
		{
			if(xmlNode == null)
				return defaultValue;
			XmlAttribute value = xmlNode.Attributes[attribute];
			return value == null ? defaultValue : value.Value;
		}

		/// <summary>
		/// Reads the attribute of a specified XmlNode using its identifier as a given type.
		/// </summary>
		/// <typeparam name="T">The type of the contents of the attribute.</typeparam>
		/// <param name="xmlNode">The XmlNode to read the attribute from.</param>
		/// <param name="attribute">The string named identifier of the attribute.</param>
		/// <returns>The contents of the attribute as the specified type, otherwise the default value of the type.</returns>
		public static T ReadAttribute<T>(XmlNode xmlNode, string attribute)
		{
			return ReadAttribute<T>(xmlNode, attribute, default(T));
		}

		/// <summary>
		/// Reads the attribute of a specified XmlNode using its identifier as a given type.
		/// </summary>
		/// <typeparam name="T">The type of the contents of the attribute.</typeparam>
		/// <param name="xmlNode">The XmlNode to read the attribute from.</param>
		/// <param name="attribute">The string named identifier of the attribute.</param>
		/// <param name="defaultValue">The value to return if the attribute cannot be read.</param>
		/// <returns>The contents of the attribute as the specified type, otherwise the specified default value.</returns>
		public static T ReadAttribute<T>(XmlNode xmlNode, string attribute, T defaultValue)
		{
			attribute = ReadAttribute(xmlNode, attribute);
			if(attribute.Length == 0) return defaultValue;
			TypeConverter attributeConverter = TypeDescriptor.GetConverter(typeof(T));
			return !attributeConverter.CanConvertFrom(typeof(string)) ? defaultValue : (T)attributeConverter.ConvertFromString(attribute);
		}
	}
}