﻿using System.Collections;
using System.Diagnostics.CodeAnalysis;

using UnityEngine;

using CommonFramework.MessageSystem;

namespace CommonFramework.CameraControl
{
	/// <summary>
	/// Attach3rdPersonToObjectMessage is an IMessage implementation that a ThirdPersonCamera uses to specify an object to attach itself to.
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class Attach3rdPersonToObjectMessage : IMessage
	{
		internal const string messageName = "AssetMessage";
		private IMessageable source = null;
		private GameObject target = null;
		private bool resetCamera = false;

		/// <summary>
		/// Creates a new Attach3rdPersonToObjectMessage representing an object to attach a camera to.
		/// </summary>
		/// <param name="targetName">Name of the target GameObject.</param>
		/// <param name="resetCamera">A flag to specify whether or not to reset the camera's zoom and rotation.</param>
		public Attach3rdPersonToObjectMessage(string targetName, bool resetCamera)
		{
			target = GameObject.Find(targetName);
			this.resetCamera = resetCamera;
		}

        /// <summary>
        /// Creates a new Attach3rdPersonToObjectMessage representing an object to attach a camera to.
        /// </summary>
        /// <param name="targetName">The target GameObject.</param>
        /// <param name="resetCamera">A flag to specify whether or not to reset the camera's zoom and rotation.</param>
        public Attach3rdPersonToObjectMessage(GameObject target, bool resetCamera)
        {
            this.target = target;
            this.resetCamera = resetCamera;
        }

		// ----- Properties ----- //

		/// <summary>
		/// The target GameObject to attach the camera to.
		/// </summary>
		public GameObject Target
		{
			get { return target; }
		}

		/// <summary>
		/// A flag to specify whether or not to reset the camera's zoom and rotation.
		/// </summary>
		public bool ResetCamera
		{
			get { return resetCamera; }
		}

		/// <summary>
		/// Gets the named identifier of the IMessage.
		/// </summary>
		public string MessageName
		{
			get { return messageName; }
		}

		/// <summary>
		/// Gets the source IMessageable object the message was sent from.
		/// </summary>
		public IMessageable Source
		{
			get { return source; }
			set { source = value; }
		}
	}
}
