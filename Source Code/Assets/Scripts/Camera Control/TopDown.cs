﻿using System.Collections;
using System.Diagnostics.CodeAnalysis;

using UnityEngine;

namespace CommonFramework.CameraControl
{
    /// <summary>
    /// Allows control of the attached camera object as if from a top down view.
    /// </summary>
	[ExcludeFromCodeCoverage]
    public class TopDown : MonoBehaviour
    {
        private bool orthographic = false;

        /// <summary>
        /// The camera to apply the movement to.
        /// </summary>
        public Camera TheCamera = null;

        /// <summary>
        /// The scroll speed.
        /// </summary>
        public float ScrollSpeed = 10000.0f;

        /// <summary>
        /// The move speed.
        /// </summary>
        public float MoveSpeed = 1000.0f;

        /// <summary>
        /// Unity Awake() function: Awake this instance.
        /// </summary>
        void Awake()
        {
            if (TheCamera == null)
                TheCamera = GetComponent<Camera>();

            if (TheCamera != null)
            {
                orthographic = TheCamera.orthographic;
            }
        }
    
        /// <summary>
        /// Unity Update() function: Update this instance.
        /// </summary>
        void Update()
        {
            Vector3 movement = new Vector3();

            if(Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
                movement.x = -MoveSpeed * Time.deltaTime;
            if(Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
                movement.x = MoveSpeed * Time.deltaTime;
            if(Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
                movement.y = MoveSpeed * Time.deltaTime;
            if(Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
                movement.y = -MoveSpeed * Time.deltaTime;

            if (TheCamera != null)
            {
                float scroll = Input.GetAxis("Mouse ScrollWheel");
                if(scroll != 0)
                {
                    if(orthographic)
                        TheCamera.orthographicSize -= scroll * ScrollSpeed * Time.deltaTime;
                    else
                        movement.z = scroll * ScrollSpeed * Time.deltaTime;
                }
            }

            transform.Translate(movement);
        }
    }
}
