﻿using System.Collections;
using System.Diagnostics.CodeAnalysis;

using UnityEngine;

namespace CommonFramework.CameraControl
{
	/// <summary>
	/// FirstPersonCamera allows control of a camera in a first person manner, giving forward movement (W or Up), backwards movement (S or Down), left strafe (Q), right strafe (E), vertical up movement (Space), vertical down movement (X), left turn (A or left), right turn (D or right) and changing the look direction (left click + move mouse).
	/// </summary>
	[ExcludeFromCodeCoverage]
    public class FirstPersonCamera : MonoBehaviour
    {
        private float rotationX = 0.0f, rotationY = 0.0f;

		/// <summary>
		/// Speed at which the camera strafes and moves forwards, backwards, up and down.
		/// </summary>
        public float MoveSpeed = 1000.0f;

		/// <summary>
		/// Speed at which the camera turns left and right.
		/// </summary>
        public float RotateSpeed = 300.0f;

		/// <summary>
		/// Speed at which the camera turns when being dragged with the mouse.
		/// </summary>
        public float LookSpeed = 300.0f;

        void Start()
        {
            transform.localRotation = Quaternion.AngleAxis(0.0f, Vector3.up);
            transform.localRotation *= Quaternion.AngleAxis(0.0f, Vector3.left);
        }

        void Update()
        {
            bool doRotate = false;

            if (Input.GetMouseButton(0))
            {
                rotationX += Input.GetAxis("Mouse X") * LookSpeed * Time.deltaTime;
                rotationY += LookSpeed * Input.GetAxis("Mouse Y") * Time.deltaTime;

                rotationY = Mathf.Clamp(rotationY, -90, 90);
                doRotate = true;
            }

            if (Input.GetKey(KeyCode.Q))
                transform.position -= transform.right * MoveSpeed * Time.deltaTime;
            if (Input.GetKey(KeyCode.E))
                transform.position += transform.right * MoveSpeed * Time.deltaTime;
            if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
                transform.position += transform.forward * MoveSpeed * Time.deltaTime;
            if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
				transform.position -= transform.forward * MoveSpeed * Time.deltaTime;
			if(Input.GetKey(KeyCode.Space))
				transform.position += transform.up * MoveSpeed * Time.deltaTime;
			if(Input.GetKey(KeyCode.X))
				transform.position -= transform.up * MoveSpeed * Time.deltaTime;
            if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
            {
                rotationX -= RotateSpeed * Time.deltaTime;
                doRotate = true;
            }
            if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
            {
                rotationX += RotateSpeed * Time.deltaTime;
                doRotate = true;
            }

            if(doRotate)
                transform.localRotation = Quaternion.AngleAxis(rotationX, Vector3.up) * Quaternion.AngleAxis(rotationY, Vector3.left);
        }
    }
}
