﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using UnityEngine;

using CommonFramework.MessageSystem;
using CommonFramework.MovementControl;

namespace CommonFramework.CameraControl
{
	/// <summary>
	/// ThirdPersonCamera allows control of a camera in a third person manner, giving forward movement (W or Up), backwards movement (S or Down), left strafe (Q), right strafe (E), vertical up movement (Space), vertical down movement (X), left turn (A or left), right turn (shoulderPointsD or right), zooming in and out from the target (scroll wheel) and orbiting (middle click + move mouse). All of these changes can be scaled down by holding the shift key. It can also be attached to a GameObject.
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class ThirdPersonCamera : MonoBehaviour, IMessageable
	{
		private string messageableName = "";
		private Queue<IMessage> pendingMessages = new Queue<IMessage>();
		private GameObject ballJoint = null;
		private float x = 0, y = 35.0f;
		private bool createdTarget = false;
		private Mover m = null;

        /// <summary>
        /// The distance of the camera from the focal point.
        /// </summary>
        public float Distance = 5.0f;

		/// <summary>
		/// Speed at which the camera strafes and moves forwards, backwards, up and down.
		/// </summary>
		public float MoveSpeed = 1000.0f;

		/// <summary>
		/// Speed at which the camera turns left and right.
		/// </summary>
		public float RotateSpeed = 300.0f;

		/// <summary>
		/// Speed at which the camera orbits when being dragged with the mouse.
		/// </summary>
		public float DragSpeed = 300.0f;

		/// <summary>
		/// Speed at which the camera zooms in and out from the target.
		/// </summary>
		public float ZoomSpeed = 300.0f;

		/// <summary>
		/// The GameObject
		/// </summary>
		public GameObject Target = null;

		private void reset(bool resetPositionAndRotation)
		{
			transform.parent = null;

			if(createdTarget && Target != null)
			{
				DestroyImmediate(Target);
				createdTarget = false;
			}

			if(ballJoint != null)
				DestroyImmediate(ballJoint);

			if(Target == null)
			{
				Target = new GameObject(name + " 3rd Person Camera anchor");
				Target.transform.position = transform.position;
				createdTarget = true;
			}

			ballJoint = new GameObject(name + " 3rd Person Camera ball joint");
			messageableName = Target.name;

			ballJoint.transform.parent = Target.transform;
			transform.parent = ballJoint.transform;

			if(resetPositionAndRotation)
			{
				ballJoint.transform.localPosition = new Vector3(0, 0, 0);
				ballJoint.transform.localRotation = Quaternion.Euler(y, x, 0);

				transform.localPosition = new Vector3(0, 0, -Distance);
				transform.LookAt(Target.transform);
			}

			if(m != null)
				m.Moveable = Target.transform;
		}

		void Awake()
		{
			m = GetComponent<Mover>();
			reset(true);

			MessageHandler.Instance.RegisterMessageable(this);
		}

		void Update()
		{
			float mod = Input.GetKey(KeyCode.LeftShift) ? 0.01f : 1.0f;
			float scroll = Input.GetAxis("Mouse ScrollWheel");
			if(scroll != 0)
			{
				Distance = Mathf.Max(Distance - (scroll * ZoomSpeed * Time.deltaTime * mod), 0.01f);
				transform.localPosition = new Vector3(0, 0, -Distance);
			}

			if(Input.GetMouseButton(2))
			{
				x += Input.GetAxis("Mouse X") * DragSpeed * Time.deltaTime;
				y = Mathf.Clamp(y - (Input.GetAxis("Mouse Y") * DragSpeed * Time.deltaTime * mod), -89, 89);

				ballJoint.transform.localRotation = Quaternion.Euler(y, x, 0);
			}

			if(Input.GetKey(KeyCode.R))
			{
				x = 0;
				y = 35.0f;

				ballJoint.transform.localRotation = Quaternion.Euler(y, x, 0);
			}

			if(Input.GetKey(KeyCode.Q))
				Target.transform.position -= Target.transform.right * MoveSpeed * Time.deltaTime * mod;
			if(Input.GetKey(KeyCode.E))
				Target.transform.position += Target.transform.right * MoveSpeed * Time.deltaTime * mod;
			if(Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
				Target.transform.position += Target.transform.forward * MoveSpeed * Time.deltaTime * mod;
			if(Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
				Target.transform.position -= Target.transform.forward * MoveSpeed * Time.deltaTime * mod;
			if(Input.GetKey(KeyCode.Space))
				Target.transform.position += Vector3.up * MoveSpeed * Time.deltaTime * mod;
			if(Input.GetKey(KeyCode.X))
				Target.transform.position -= Vector3.up * MoveSpeed * Time.deltaTime * mod;
			if(Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
				Target.transform.Rotate(0, -RotateSpeed * Time.deltaTime * mod, 0);
			if(Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
				Target.transform.Rotate(0, RotateSpeed * Time.deltaTime * mod, 0);

		}

		/// <summary>
		/// Informs the ThirdPersonCamera that it needs to handle and process an IMessage.
		/// </summary>
		/// <param name="message">The IMessage that the ThirdPersonCamera needs to process.</param>
		public void ReceiveMessage(IMessage message)
		{
			if(message.MessageName == Attach3rdPersonToObjectMessage.messageName)
			{
				Attach3rdPersonToObjectMessage attachToMessage = message as Attach3rdPersonToObjectMessage;
				if(attachToMessage == null)
					return;

				transform.parent = null;

				if(createdTarget && Target != null)
				{
					DestroyImmediate(Target);
					createdTarget = false;
				}

				Target = attachToMessage.Target;

				reset(attachToMessage.ResetCamera);
			}
		}

		// ----- Properties ----- //

		/// <summary>
		/// Gets the pending outgoing IMessage queue that needs to be processed by the ThirdPersonCamera.
		/// </summary>
		public Queue<IMessage> PendingMessages
		{
			get { return pendingMessages; }
		}

		/// <summary>
		/// Gets the identifier name of the IMessageable component, ThirdPersonCamera.
		/// </summary>
		public string MessageableName
		{
			get { return messageableName; }
		}
	}
}
