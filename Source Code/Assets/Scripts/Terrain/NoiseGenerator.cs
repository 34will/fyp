﻿using System;

using UnityEngine;

namespace CommonFramework.Terrain
{
    public static class NoiseGenerator
    {
        public static float PerlinNoise(float x, float y, float scale = 1.0f)
        {
            return Mathf.Clamp01(Mathf.PerlinNoise(x * scale, y * scale));
        }

        // Code snippet from http://catlikecoding.com/unity/tutorials/noise/
        public static float SumPerlinNoise(float x, float y, float scale, int octaves, float lacunarity = 2.0f, float persistence = 0.5f)
        {
            float sum = PerlinNoise(x, y, scale);
            float amplitude = 1.0f;
            float range = 1.0f;

            for(int octave = 1; octave < octaves; octave++)
            {
                scale *= lacunarity;
                amplitude *= persistence;
                range += amplitude;
                sum += PerlinNoise(x, y, scale) * amplitude;
            }

            return sum / range;
        }
    }
}
