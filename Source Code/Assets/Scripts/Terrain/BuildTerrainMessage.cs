﻿using System;
using UnityEngine;

using CommonFramework.MessageSystem;

namespace CommonFramework.Terrain
{
    public class BuildTerrainMessage : IMessage
    {
        internal const string messageName = "BuildTerrainMessage";
        private IMessageable source = null;

        public BuildTerrainMessage(IMessageable source = null)
        {
            this.source = source;
        }

        // ----- Properties ----- //

        /// <summary>
        /// Gets the named identifier of the IMessage.
        /// </summary>
        public string MessageName
        {
            get { return messageName; }
        }

        /// <summary>
        /// Gets the source IMessageable object the message was sent from.
        /// </summary>
        public IMessageable Source
        {
            get { return source; }
            set { source = value; }
        }
    }
}
