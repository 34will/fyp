﻿using System;
using UnityEngine;

using CommonFramework.MessageSystem;

namespace CommonFramework.Terrain
{
    public class PrepareTerrainMessage : IMessage
    {
        internal const string messageName = "PrepareTerrainMessage";
        private IMessageable source = null;
        private float minX = 0.0f, minZ = 0.0f, maxX = float.NaN, maxZ = float.NaN;

        public PrepareTerrainMessage(float maxX, float maxZ, float minX = 0.0f, float minZ = 0.0f, IMessageable source = null)
        {
            this.maxX = maxX;
            this.maxZ = maxZ;
            this.minX = minX;
            this.minZ = minZ;
            this.source = source;
        }

        // ----- Properties ----- //

        public float MinimumX
        {
            get { return minX; }
        }

        public float MinimumZ
        {
            get { return minZ; }
        }

        public float MaximumX
        {
            get { return maxX; }
        }

        public float MaximumZ
        {
            get { return maxZ; }
        }

        /// <summary>
        /// Gets the named identifier of the IMessage.
        /// </summary>
        public string MessageName
        {
            get { return messageName; }
        }

        /// <summary>
        /// Gets the source IMessageable object the message was sent from.
        /// </summary>
        public IMessageable Source
        {
            get { return source; }
            set { source = value; }
        }
    }
}
