﻿using System;
using UnityEngine;

using CommonFramework.MessageSystem;

namespace CommonFramework.Terrain
{
    public class BlockTriangleMessage : IMessage
    {
        internal const string messageName = "BlockTriangleMessage";
        private IMessageable source = null;
        private Vector3 p1, p2, p3;

        public BlockTriangleMessage(Vector3 p1, Vector3 p2, Vector3 p3, IMessageable source = null)
        {
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
            this.source = source;
        }

        // ----- Properties ----- //

        public Vector3 Point1
        {
            get { return p1; }
        }

        public Vector3 Point2
        {
            get { return p2; }
        }

        public Vector3 Point3
        {
            get { return p3; }
        }

        /// <summary>
        /// Gets the named identifier of the IMessage.
        /// </summary>
        public string MessageName
        {
            get { return messageName; }
        }

        /// <summary>
        /// Gets the source IMessageable object the message was sent from.
        /// </summary>
        public IMessageable Source
        {
            get { return source; }
            set { source = value; }
        }
    }
}
