﻿using System.Collections.Generic;

using UnityEngine;

using CommonFramework.MessageSystem;

using SRandom = System.Random;

namespace CommonFramework.Terrain
{
    public class MultiTerrainManager : MonoBehaviour, IMessageable
    {
        private const string messageableName = "TerrainManager";
        private Queue<IMessage> pendingMessages = new Queue<IMessage>();
        private SRandom offsetGenerator = null;
        private GameObject[,] terrains = null;
        private GameObject terrainParent = null;
        private float perlinScale = 1.0f;

        private Vector3[] defaultVerts = null;
        private int[] defaultInds = null;
        private Vector2[] defaultUVs = null;

        public int TerrainSize = 256;

        public Material TerrainMaterial = null;

        public int Seed = 0;

        public float HeightScale = 10.0f;

        void Awake()
        {
            MessageHandler.Instance.RegisterMessageable(this);

            int numDivs = (TerrainSize / 2);
            float d = TerrainSize / numDivs, halfSize = (0.5f * TerrainSize);
            int p = (numDivs + 1), numVerts = p * p, numInds = numDivs * numDivs * 6, k = 0;

            defaultVerts = new Vector3[numVerts];
            defaultInds = new int[numInds];
            defaultUVs = new Vector2[numVerts];

            for(int j = 0; j < p; j++)
            {
                for(int i = 0; i < p; i++)
                {
                    defaultVerts[k] = new Vector3((j * d) - halfSize, 0.0f, (i * d) - halfSize);
                    defaultUVs[k] = new Vector2((float)i / (float)numDivs, (float)j / (float)numDivs);
                    k++;
                }
            }

            k = 0;
            for(int j = 0; j < numDivs; j++)
            {
                for(int i = 0; i < numDivs; i++)
                {
                    defaultInds[k] = (i * p) + j;
                    defaultInds[k + 1] = (i * p) + j + 1;
                    defaultInds[k + 2] = ((i + 1) * p) + j;
                    defaultInds[k + 3] = ((i + 1) * p) + j;
                    defaultInds[k + 4] = (i * p) + j + 1;
                    defaultInds[k + 5] = ((i + 1) * p) + j + 1;

                    k += 6;
                }
            }
        }

        public void ReceiveMessage(IMessage message)
        {
            if(message.MessageName == PrepareTerrainMessage.messageName)
            {
                PrepareTerrainMessage btMessage = message as PrepareTerrainMessage;
                if(btMessage == null) return;

                BuildTerrain(btMessage.MinimumX, btMessage.MinimumZ, btMessage.MaximumX, btMessage.MaximumZ);
            }
        }

        private void BuildTerrain(float minX, float minZ, float maxX, float maxZ)
        {
            float xLength = maxX - minX;
            float zLength = maxZ - minZ;
            offsetGenerator = new SRandom(Seed);

            if(xLength <= 0 || zLength <= 0)
                return;

            int numTerrainsX = ((int)xLength / TerrainSize) + (xLength % TerrainSize == 0 ? 0 : 1);
            int numTerrainsZ = ((int)zLength / TerrainSize) + (zLength % TerrainSize == 0 ? 0 : 1);

            if(terrains != null)
            {
                for(int j = 0; j < numTerrainsZ; j++)
                {
                    for(int i = 0; i < numTerrainsX; i++)
                        UnityEngine.Object.Destroy(terrains[i, j]);
                }
            }

            terrains = new GameObject[numTerrainsX, numTerrainsZ];

            if(terrainParent == null)
                terrainParent = new GameObject("Terrains");

            float offset = TerrainSize * 0.5f;
            int randPerlinOffsetX = offsetGenerator.Next(0, 1000);
            int randPerlinOffsetZ = offsetGenerator.Next(0, 1000);

            for(int j = 0; j < numTerrainsZ; j++)
            {
                for(int i = 0; i < numTerrainsX; i++)
                {
                    Color32[] colours = new Color32[TerrainSize * TerrainSize];
                    Texture2D perlinTexture = new Texture2D(TerrainSize, TerrainSize, TextureFormat.RGBA32, true);
                    perlinTexture.filterMode = FilterMode.Trilinear;
                    perlinTexture.wrapMode = TextureWrapMode.Clamp;
                    perlinTexture.anisoLevel = 16;
                    float tSizeM = TerrainSize - 1;

                    int k = 0;
                    for(int z = 0; z < TerrainSize; z++)
                    {
                        float zCoord = randPerlinOffsetZ + (i + ((float)z / tSizeM));
                        for(int x = 0; x < TerrainSize; x++)
                        {
                            float xCoord = randPerlinOffsetX + (j + ((float)x / tSizeM));
                            float perlinF = NoiseGenerator.PerlinNoise(xCoord, zCoord, perlinScale);

                            byte perlin = (byte)(perlinF * 255.0f);
                            colours[k] = new Color32(perlin, perlin, perlin, 255);
                            k++;
                        }
                    }

                    perlinTexture.SetPixels32(colours);
                    perlinTexture.Apply();

                    terrains[i, j] = new GameObject("Terrain Game Object " + i + ", " + j);
                    MeshRenderer renderer = terrains[i, j].AddComponent<MeshRenderer>();
                    MeshFilter filter = terrains[i, j].AddComponent<MeshFilter>();
                    Mesh terrainMesh = new Mesh();
                    terrainMesh.name = "Terrain Mesh " + i + ", " + j;

                    Vector3[] heightVertices = new Vector3[defaultVerts.Length];
                    defaultVerts.CopyTo(heightVertices, 0);

                    int numDivs = (TerrainSize / 2);
                    int p = (numDivs + 1);
                    k = 0;
                    for(int z = 0; z < p; z++)
                    {
                        int zIndex = (int)(((float)z / (float)p) * (float)TerrainSize) * TerrainSize;
                        for(int x = 0; x < p; x++)
                        {
                            int xIndex = (int)(((float)x / (float)p) * (float)TerrainSize);
                            float height = (colours[zIndex + xIndex].r / 255.0f) * HeightScale;

                            heightVertices[k] = new Vector3(heightVertices[k].x, height, heightVertices[k].z);
                            k++;
                        }
                    }

                    terrainMesh.vertices = heightVertices;
                    terrainMesh.triangles = defaultInds;
                    terrainMesh.uv = defaultUVs;

                    terrainMesh.RecalculateNormals();
                    filter.mesh = terrainMesh;

                    renderer.material = new Material(TerrainMaterial);
                    renderer.material.mainTexture = perlinTexture;

                    terrains[i, j].transform.parent = terrainParent.transform;
                    terrains[i, j].transform.localPosition = new Vector3(offset + (i * TerrainSize), HeightScale * 0.5f, offset + (j * TerrainSize));
                }
            }
        }

        void OnGUI()
        {
            perlinScale = GUIHelper.FloatSliderAndTextBox(new Vector2(5, 155), perlinScale, 1.0f, 32.0f);

            HeightScale = GUIHelper.FloatSliderAndTextBox(new Vector2(5, 185), HeightScale, 1.0f, 32.0f);

            string stringSeed = GUI.TextField(new Rect(5, 215, 150, 21), Seed.ToString());
            int.TryParse(stringSeed, out Seed);

            if(GUI.Button(new Rect(5, 275, 150, 21), "Random Seed"))
            {
                Seed = Random.Range(0, 1000000);
                BuildTerrain(0.0f, 0.0f, 1611.0f, 1200.0f);
            }

            if(GUI.Button(new Rect(5, 305, 150, 21), "Generate Terrain"))
                BuildTerrain(0.0f, 0.0f, 1611.0f, 1200.0f);
        }

        // ----- Properties ----- //

        /// <summary>
        /// Gets the pending outgoing IMessage queue that needs to be processed by the TerrainManager.
        /// </summary>
        public Queue<IMessage> PendingMessages
        {
            get { return pendingMessages; }
        }

        /// <summary>
        /// Gets the identifier name of the IMessageable component, TerrainManager.
        /// </summary>
        public string MessageableName
        {
            get { return messageableName; }
        }
    }
}