﻿using System.Collections.Generic;

using UnityEngine;

using CommonFramework.MessageSystem;
using CommonFramework.Maths;
using CommonFramework.ObjectPlacement;

using SRandom = System.Random;

namespace CommonFramework.Terrain
{
	public class TerrainManager : MonoBehaviour, IMessageable
	{
		private const string messageableName = "TerrainManager";
		private Queue<IMessage> pendingMessages = new Queue<IMessage>();
		private SRandom offsetGenerator = null;
		private GameObject terrain = null;
		private float xLength = 0.0f;
		private float zLength = 0.0f;
		private int textureWidth = 0;
		private int textureHeight = 0;
		private bool[] occupiedSpaces = null;
		private float[] heights = null;
		private Vector3 triangleOffset = Vector3.zero;

		public Material TerrainMaterial = null;
		public Material RoadsMaterial = null;

		public int Seed = 0;

		public int PerlinOctaves = 4;

		public float HeightScale = 10.0f;

        public int TerrainResolution = 256;

        public float PerlinScale = 1.0f;

		void Awake()
		{
			MessageHandler.Instance.RegisterMessageable(this);
		}

		public void ReceiveMessage(IMessage message)
		{
			if(message.MessageName == PrepareTerrainMessage.messageName)
			{
				PrepareTerrainMessage ptMessage = message as PrepareTerrainMessage;
				if(ptMessage == null) return;

				PrepareTerrain(ptMessage.MinimumX, ptMessage.MinimumZ, ptMessage.MaximumX, ptMessage.MaximumZ);
			}
			else if(message.MessageName == BlockTriangleMessage.messageName)
			{
				BlockTriangleMessage btMessage = message as BlockTriangleMessage;
				if(btMessage == null) return;

				FillTriangle(btMessage.Point1, btMessage.Point2, btMessage.Point3);
			}
			else if(message.MessageName == BuildTerrainMessage.messageName)
			{
				BuildTerrainMessage btMessage = message as BuildTerrainMessage;
				if(btMessage == null) return;

				BuildTerrain();
            }
            else if(message.MessageName == SpawnForestMessage.messageName)
            {
                SpawnForestMessage sfm = message as SpawnForestMessage;
                if(sfm == null) return;

                AddForest(sfm.ForestPrefabs, sfm.ForestParent);
            }
		}

		private void PrepareTerrain(float minX, float minZ, float maxX, float maxZ)
		{
			xLength = maxX - minX;
			zLength = maxZ - minZ;

			if(xLength <= 0 || zLength <= 0)
				return;

			textureWidth = (((int)xLength / TerrainResolution) + (xLength % TerrainResolution == 0 ? 0 : 1)) * TerrainResolution;
			textureHeight = (((int)zLength / TerrainResolution) + (zLength % TerrainResolution == 0 ? 0 : 1)) * TerrainResolution;

			triangleOffset = new Vector3((textureWidth - xLength) * 0.5f, 0.0f, (textureHeight - zLength) * 0.5f);

			Color32 red = new Color32(255, 0, 0, 255);
			long coloursSize = textureWidth * textureHeight;
			occupiedSpaces = new bool[coloursSize];
			for(long i = 0; i < coloursSize; i++)
				occupiedSpaces[i] = false;
		}

		private void BuildTerrain()
		{
			if(xLength <= 0 || zLength <= 0)
				return;

			offsetGenerator = new SRandom(Seed);

			long coloursSize = textureWidth * textureHeight;
			Color32[] perlinBlendColours = new Color32[coloursSize];
			heights = new float[coloursSize];

			float widthM = textureWidth - 1;
			float depthM = textureHeight - 1;
			int randHeightPerlinX = offsetGenerator.Next(0, 1000);
			int randHeightPerlinZ = offsetGenerator.Next(0, 1000);
			int randBlendPerlinX = offsetGenerator.Next(2000, 3000);

			int k = 0;
			for(int z = 0; z < textureHeight; z++)
			{
				float zCoord = randHeightPerlinZ + ((float)z / depthM);
				for(int x = 0; x < textureWidth; x++)
				{
					float xCoord = (float)x / widthM;
					heights[k] = NoiseGenerator.SumPerlinNoise(randHeightPerlinX + xCoord, zCoord, PerlinScale, PerlinOctaves);

					if(heights[k] < 0.5f)
						heights[k] = (heights[k] * 0.2f) + 0.4f;

					float perlinF = NoiseGenerator.SumPerlinNoise(randBlendPerlinX + xCoord, zCoord, PerlinScale, PerlinOctaves);
					byte perlin = (byte)(perlinF * 255.0f);
					perlinBlendColours[k] = new Color32(perlin, perlin, perlin, 255);
					k++;
				}
			}

			Texture2D perlinTexture = new Texture2D(textureWidth, textureHeight, TextureFormat.RGBA32, true);
			perlinTexture.filterMode = FilterMode.Trilinear;
			perlinTexture.wrapMode = TextureWrapMode.Repeat;
			perlinTexture.anisoLevel = 16;
			perlinTexture.SetPixels32(perlinBlendColours);
			perlinTexture.Apply();

			int numCols = textureWidth / 8, numRows = textureHeight / 8;
			float dX = textureWidth / numCols, dZ = textureHeight / numRows, halfWidth = (0.5f * textureWidth), halfDepth = (0.5f * textureHeight);
			int m = (numCols + 1), n = (numRows + 1), numVerts = (m * n), numInds = (numCols * numRows * 6);

			Vector3[] vertices = new Vector3[numVerts];
			Vector2[] uvs = new Vector2[numVerts];
			Vector2[] uv2s = new Vector2[numVerts];
			int[] indices = new int[numInds];
			float blendedTextureScale = 16.0f;

			k = 0;
			for(int j = 0; j < n; j++)
			{
				int zIndex = (int)(((float)j / (float)n) * textureHeight);
				for(int i = 0; i < m; i++)
				{
					int xIndex = (int)(((float)i / (float)m) * textureWidth);
					int heightIndex = (zIndex * textureWidth) + xIndex;
					
					if(IsOccupied(xIndex, zIndex, 15))
						heights[heightIndex] = 0.499f;
					float height = heights[heightIndex] * HeightScale;

					float iDX = (i * dX);
					float jDZ = (j * dZ);
					vertices[k] = new Vector3(iDX - halfWidth, height, jDZ - halfDepth);
					uvs[k] = new Vector2(iDX / blendedTextureScale, jDZ / blendedTextureScale);
					uv2s[k] = new Vector2(((float)i / (float)numCols), ((float)j / (float)numRows));
					k++;
				}
			}

			k = 0;
			for(int j = 0; j < numRows; j++)
			{
				for(int i = 0; i < numCols; i++)
				{
					indices[k] = (j * m) + i;
					indices[k + 1] = ((j + 1) * m) + i;
					indices[k + 2] = (j * m) + i + 1;
					indices[k + 3] = ((j + 1) * m) + i;
					indices[k + 4] = ((j + 1) * m) + i + 1;
					indices[k + 5] = (j * m) + i + 1;

					k += 6;
				}
			}

			if(terrain != null)
				UnityEngine.Object.Destroy(terrain);

			terrain = new GameObject("Terrain Game Object");
			MeshRenderer renderer = terrain.AddComponent<MeshRenderer>();
			MeshFilter filter = terrain.AddComponent<MeshFilter>();
			Mesh terrainMesh = new Mesh();
			terrainMesh.name = "Terrain Mesh";

			terrainMesh.vertices = vertices;
			terrainMesh.triangles = indices;
			terrainMesh.uv = uvs;
			terrainMesh.uv2 = uv2s;

			terrainMesh.RecalculateNormals();
			filter.mesh = terrainMesh;

			renderer.material = new Material(TerrainMaterial);
			renderer.material.SetTexture("_BlendTex", perlinTexture);

			terrain.transform.localPosition = new Vector3(xLength * 0.5f, HeightScale * -0.5f, zLength * 0.5f);
		}

		private bool IsOccupied(int x, int y, int radius = 0)
		{
			if(x < 0 || y < 0 || x > textureWidth || y > textureHeight || radius < 0)
				return false;

			if(radius == 0)
				return occupiedSpaces[(y * textureWidth) + x];
			else
			{
				int minX = Mathf.Max(0, x - radius), maxX = Mathf.Min(textureWidth, x + radius), minY = Mathf.Max(0, y - radius), maxY = Mathf.Min(textureHeight, y + radius);
				for(y = minY; y < maxY; y++)
				{
					int yPos = y * textureWidth;
					for(x = minX; x < maxX; x++)
					{
						if(occupiedSpaces[yPos + x])
							return true;
					}
				}
				return false;
			}
		}

		private void FillTriangle(Vector3 p1, Vector3 p2, Vector3 p3)
		{
			if(xLength <= 0 || zLength <= 0)
				return;

			p1 += triangleOffset;
			p2 += triangleOffset;
			p3 += triangleOffset;

			int x0 = (int)p1.x, z0 = (int)p1.z, x1 = (int)p2.x, z1 = (int)p2.z, x2 = (int)p3.x, z2 = (int)p3.z;
			FillTriangle(occupiedSpaces, textureWidth, textureHeight, x0, z0, x1, z1, x2, z2, true);
		}

		// Code snippet modified from http://www.codeproject.com/Tips/86354/draw-triangle-algorithm-D
		private static void FillTriangle<T>(T[] values, int width, int height, int x0, int y0, int x1, int y1, int x2, int y2, T value)
		{
			if(y1 > y2)
			{
				int temp = x1;
				x1 = x2;
				x2 = temp;

				temp = y1;
				y1 = y2;
				y2 = temp;
			}
			if(y0 > y1)
			{
				int temp = x0;
				x0 = x1;
				x1 = temp;

				temp = y0;
				y0 = y1;
				y1 = temp;
			}
			if(y1 > y2)
			{
				int temp = x1;
				x1 = x2;
				x2 = temp;

				temp = y1;
				y1 = y2;
				y2 = temp;
			}

			double dx_far = (double)(x2 - x0) / (double)(y2 - y0 + 1);
			double dx_upper = (double)(x1 - x0) / (double)(y1 - y0 + 1);
			double dx_low = (double)(x2 - x1) / (double)(y2 - y1 + 1);
			double xf = x0;
			double xt = x0 + dx_upper;

			for(int y = y0; y <= (y2 > height - 1 ? height - 1 : y2); y++)
			{
				if(y >= 0)
				{
					for(int x = (xf > 0 ? (int)xf : 0); x <= (xt < width ? xt : width - 1); x++)
						values[(int)(x + (y * width))] = value;
					for(int x = (xf < width ? (int)xf : width - 1); x >= (xt > 0 ? xt : 0); x--)
						values[(int)(x + (y * width))] = value;
				}

				xf += dx_far;
				if(y < y1)
					xt += dx_upper;
				else
					xt += dx_low;
			}
		}

		// Code snippet modified from http://notes.ericwillis.com/2009/10/blur-an-image-with-csharp/
		private Texture2D Blur(Texture2D image, int blurSize)
		{
			int width = image.width;
			int height = image.height;
			Color32[] pixels = image.GetPixels32();
			Color32[] blurredPixels = new Color32[width * height];

			for(int xx = 0; xx < width; xx++)
			{
				for(int yy = 0; yy < height; yy++)
				{
					float avgR = 0, avgG = 0, avgB = 0, avgA = 0;
					int blurPixelCount = 0;

					int xxBS = xx + blurSize;
					int yyBS = yy + blurSize;

					for(int x = xx; (x < xxBS && x < width); x++)
					{
						for(int y = yy; (y < yyBS && y < height); y++)
						{
							Color32 pixel = pixels[x + (y * width)];

							avgR += pixel.r;
							avgG += pixel.g;
							avgB += pixel.b;
							avgA += pixel.a;

							blurPixelCount++;
						}
					}

					avgR /= blurPixelCount;
					avgG /= blurPixelCount;
					avgB /= blurPixelCount;
					avgA /= blurPixelCount;

					Color32 avgColour = new Color32((byte)avgR, (byte)avgG, (byte)avgB, (byte)avgA);
					for(int x = xx; x < xxBS && x < width; x++)
					{
						for(int y = yy; y < yyBS && y < height; y++)
							blurredPixels[x + (y * width)] = avgColour;
					}
				}
			}

			Texture2D blurred = new Texture2D(width, height);
			blurred.SetPixels32(blurredPixels);
			blurred.Apply();
			return blurred;
		}


		// Code snippet modified from http://www.gamasutra.com/view/feature/130071/random_scattering_creating_.php?page=2
		private void AddForest(List<GameObject> treePrefabs, GameObject treeParent)
		{
            if(treePrefabs == null || treePrefabs.Count < 1 || xLength <= 0 || zLength <= 0)
				return;

			int randTreePositionX = offsetGenerator.Next(0, textureWidth);
			int randTreePositionZ = offsetGenerator.Next(0, textureHeight);

			int whileBreaker = 0;
			while(!IsOccupied(randTreePositionX, randTreePositionZ, 5))
			{
				if(whileBreaker > 100)
					return;

				randTreePositionX = offsetGenerator.Next(0, textureWidth);
				randTreePositionZ = offsetGenerator.Next(0, textureHeight);

				whileBreaker++;
			}

            if(treeParent == null)
                treeParent = new GameObject("Forest Parent");

			float xd = 100.0f / 20.0f;
			float yd = 100.0f / 20.0f;

			for(int i = 0; i < 21; i++)
			{
				for(int j = 0; j < 21; j++)
				{
					float x = randTreePositionX + (xd * i) + offsetGenerator.Next(-5, 5);
					int iX = (int)x;
					float z = randTreePositionZ + (yd * j) + offsetGenerator.Next(-5, 5);
					int iZ = (int)z;

					if(!IsOccupied(iX, iZ, 3))
					{
						float height = heights[(iZ * textureWidth) + iX] * HeightScale;
						GameObject tree = (GameObject)Instantiate(treePrefabs[Random.Range(0, treePrefabs.Count)], new Vector3(x - triangleOffset.x, height, z - triangleOffset.z), Quaternion.Euler(0.0f, (float)offsetGenerator.NextDouble() * 360.0f, 0.0f));

                        tree.transform.parent = treeParent.transform;
					}
				}
			}

            treeParent.transform.localPosition = new Vector3(0.0f, HeightScale * -0.5f, 0.0f);
		}

		void OnGUI()
        {
            GUI.Label(new Rect(5, 160, 85, 30), "Perlin Scale");
			PerlinScale = GUIHelper.FloatSliderAndTextBox(new Vector2(95, 160), PerlinScale, 1.0f, 32.0f);

            GUI.Label(new Rect(5, 190, 85, 30), "Height Scale");
			HeightScale = GUIHelper.FloatSliderAndTextBox(new Vector2(95, 190), HeightScale, 1.0f, 40.0f);

            GUI.Label(new Rect(5, 220, 85, 40), "Perlin Octaves");
			string stringPerlinOctaves = GUI.TextField(new Rect(95, 225, 150, 21), PerlinOctaves.ToString());
			int outOctaves;
			if(int.TryParse(stringPerlinOctaves, out outOctaves))
				PerlinOctaves = outOctaves;

            GUI.Label(new Rect(5, 260, 85, 30), "Random Seed");
			string stringSeed = GUI.TextField(new Rect(95, 260, 150, 21), Seed.ToString());
			int outSeed;
			if(int.TryParse(stringSeed, out outSeed) && outSeed > 0)
				Seed = outSeed;

            if(GUI.Button(new Rect(5, 290, 150, 21), "Random Seed"))
            {
                Seed = Random.Range(0, 1000000);
                BuildTerrain();
            }

            if(GUI.Button(new Rect(5, 320, 150, 21), "Generate Terrain"))
                BuildTerrain();
		}

		// ----- Properties ----- //

		/// <summary>
		/// Gets the pending outgoing IMessage queue that needs to be processed by the TerrainManager.
		/// </summary>
		public Queue<IMessage> PendingMessages
		{
			get { return pendingMessages; }
		}

		/// <summary>
		/// Gets the identifier name of the IMessageable component, TerrainManager.
		/// </summary>
		public string MessageableName
		{
			get { return messageableName; }
		}
	}
}