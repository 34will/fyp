﻿using System.Collections;
using System.Diagnostics.CodeAnalysis;

using UnityEngine;

namespace CommonFramework.DataStream
{
    /// <summary>
    /// PacketTranscriberPump wraps an instance of PacketTranscriber providing access to it through
    /// update method call an instance.
    /// </summary>
	[ExcludeFromCodeCoverage]
    public class PacketTranscriberPump : MonoBehaviour
    {
        private IListener listener = null;

        /// <summary>
        /// Starts the PacketTranscriberPump preparing it for proccessing.
        /// </summary>
        public void Start()
        {
            if (listener != null)
            {
                PacketTranscriber.Instance.SetListener(listener);
                listener.Start();
            }
        }

        /// <summary>
        /// Update is called once per frame calling Update on the encapsulated PacketTranscriber instance.
        /// </summary>
        public void Update()
        {
            PacketTranscriber.Instance.Update();
        }

        // ----- Properties ----- //

        /// <summary>
        /// Sets or gets the IListener used by the PacketTranscriberPump.
        /// (Warning, the set only works before the Start call is made on this PacketTranscriberPump instance)
        /// </summary>
        public IListener Listener
        {
            set { listener = value; }
            get { return PacketTranscriber.Instance.Listener; }
        }
    }
}