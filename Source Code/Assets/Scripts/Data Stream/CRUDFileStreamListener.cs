﻿using UnityEngine;
using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Net;

using CommonFramework.MessageSystem;
using CommonFramework.Logging;

namespace CommonFramework.DataStream
{
    /// <summary>
    /// FileStreamListener is used to simulate the normal stream read in through the program
    /// however using a file as the input. Customised Readable UDP Data (CRUD) listener.
    /// </summary>
    public class CRUDFileStreamListener : IListener
    {
        private XmlDocument document = null;
        private XmlNodeList baseElements = null;
        private XmlNodeList packetElements = null;
        private int current = 0;

        private string title, info;
        private long fileStartTime = 0, fileEndTime = 0, clockStartTime = 0, deltaTime = 0;

        private MemoryStream stream = new MemoryStream(12);

        /// <summary>
        /// Creates a new CRUDFileStreamListener using the specified filename of the XML document.
        /// </summary>
        /// <param name="filename">The filename of the XML resource.</param>
        public CRUDFileStreamListener(string filename)
        {
            document = new XmlDocument();
            document.Load(filename);
        }

        /// <summary>
        /// Creates a new CRUDFileStreamListener using the passed XDocument object.
        /// </summary>
        /// <param name="document">The XmlDocument to use in the CRUDFileStreamListener.</param>
        public CRUDFileStreamListener(XmlDocument document)
        {
            this.document = document;
        }

        /// <summary>
        /// Opens the file bound with the CRUDFileStreamListener and moves the read cursor to the start.
        /// </summary>
        public void Start()
        {
            // Read in the document information ready for processing
            XmlNode root = document.FirstChild;
            if (root == null) return;
            baseElements = root.ChildNodes;

            foreach (XmlNode element in baseElements)
            {
                switch (element.Name.ToString())
                {
                    case "fileinfo":
                        ReadFileInfo(element);
                        break;
                    case "packets":
                        packetElements = element.ChildNodes;
                        break;
                }
            }

            // Store the current time stamp so that future timestamps can be converted
            // to the time relative to the document timestamps
            clockStartTime = DateTime.Now.Ticks;
        }

        /// <summary>
        /// Stops the file bound with the CRUDFileStreamListener and closes the file.
        /// </summary>
        public void Stop()
        {
            current = packetElements.Count;
        }

        /// <summary>
        /// Reads the next block of Packets up to the currently elapsed timestamp since the file read was initiated.
        /// </summary>
        /// <returns>An enumeration of the Packets in the time block since the last ReceivePackets call.</returns>
        public IEnumerable<Packet> ReceivePackets()
        {
            // Set the current clock time
            deltaTime = DateTime.Now.Ticks - clockStartTime;

            // Read in the packets up to the current clock time
            List<Packet> packets = new List<Packet>();
            Packet nextPacket = ReadNextPacket();
            while (nextPacket != null)
            {
                packets.Add(nextPacket);
                nextPacket = ReadNextPacket();
            }

            return packets;
        }

        private void ReadFileInfo(XmlNode infoElement)
        {
            foreach (XmlNode element in infoElement.ChildNodes)
            {
                switch (element.Name.ToString())
                {
                    case "title": title = element.InnerText.Trim(); break;
                    case "info": info = element.InnerText; break;
                    case "starttimestamp": fileStartTime = ReadTimestamp(element); break;
                    case "endtimestamp": fileEndTime = ReadTimestamp(element); break;
                }
            }
        }

        private long ReadTimestamp(XmlNode element)
        {
            // Determine the format of the timestamp
            string mode = XMLHelper.ReadAttribute(element, "mode");
            if (mode.Length == 0) mode = "epoch";
            long timestamp = 0;

            switch (mode)
            {
                case "epoch": timestamp = XMLHelper.ReadAttribute<long>(element, "long"); break;

                case "datetime":
                    int day = XMLHelper.ReadAttribute<int>(element, "day");
                    int month = XMLHelper.ReadAttribute<int>(element, "month");
                    int year = XMLHelper.ReadAttribute<int>(element, "year");
                    int hour = XMLHelper.ReadAttribute<int>(element, "hour");
                    int minute = XMLHelper.ReadAttribute<int>(element, "minute");
                    int second = XMLHelper.ReadAttribute<int>(element, "second");
                    DateTime datetime = new DateTime(year, month, day, hour, minute, second, DateTimeKind.Local);
                    timestamp = datetime.Ticks;
                    break;
            }

            return timestamp;
        }

        private Packet ReadNextPacket()
        {
            if (current < packetElements.Count)
            {
                // Acquire the active packets XElement
                XmlNode packetElement = packetElements[current];

                // Determine the timestamp of the packet
                XmlNodeList timestampNodes = packetElement.SelectNodes("timestamp");
                long timestamp = 0;
                foreach (XmlNode timestampNode in timestampNodes)
                {
                    timestamp = ReadTimestamp(timestampNode);
                    break;
                }

                // Check the timestamp is within range (can this packet be read yet?)
                if (timestamp - fileStartTime > deltaTime) return null;

                current++;
                return ReadPacket(packetElement);
            }
            else return null;
        }

        private Packet ReadPacket(XmlNode packetElement)
        {
            long timestamp = 0;
            float positionX = 0.0f, positionY = 0.0f, positionZ = 0.0f;

            foreach (XmlNode element in packetElement.ChildNodes)
            {
                string elementName = element.Name.ToString();
                switch (elementName)
                {
                    case "timestamp": timestamp = ReadTimestamp(element); break;
                    case "position":
                        positionX = XMLHelper.ReadAttribute<float>(element, "x");
                        positionY = XMLHelper.ReadAttribute<float>(element, "y");
                        positionZ = XMLHelper.ReadAttribute<float>(element, "z");
                        break;
                }
            }

            stream.Position = 0;
            stream.Write(BitConverter.GetBytes(positionX), 0, 4);
            stream.Write(BitConverter.GetBytes(positionY), 0, 4);
            stream.Write(BitConverter.GetBytes(positionZ), 0, 4);
            byte[] bytes = new byte[12];
            stream.GetBuffer().CopyTo(bytes, 0);

            string message = String.Format("\n--== Packet {0} ==--\nTimestamp: {1} \nPosition: ({2}, {3}, {4})", current, timestamp, positionX, positionY, positionZ);
            MessageHandler.Instance.SendMessage(new LogMessage(message));

            return new Packet(bytes, new IPEndPoint(IPAddress.Parse("127.0.0.1"), 4000));
        }

        // ----- Properties ----- //

        /// <summary>
        /// Gets the title field of the CRUD document.
        /// </summary>
        public string Title
        {
            get { return title; }
        }

        /// <summary>
        /// Gets the information about the CRUD document.
        /// </summary>
        public string Info
        {
            get { return info; }
        }
    }
}