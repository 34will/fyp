﻿using UnityEngine;
using System.Collections;

using CommonFramework.MessageSystem;

namespace CommonFramework.DataStream
{
    /// <summary>
    /// PacketMessage defines an IMessage that encapsulates a Packet that has been received from a data source.
    /// </summary>
    public class PacketMessage : IMessage
    {
        private const string messageName = "PacketMessage";
        private IMessageable source = null;
        private Packet packet;

        /// <summary>
        /// Creates a new PacketMessage with the specified Packet and no specified source IMessageable.
        /// </summary>
        /// <param name="packet">The data Packet to be sent in the message.</param>
        public PacketMessage(Packet packet)
            : this(packet, null) { }

        /// <summary>
        /// Creates a new PacketMessage with the specified Packet and source IMessageable.
        /// </summary>
        /// <param name="packet">The data Packet to be sent in the message.</param>
        /// <param name="source">The source IMessageable the PacketMessage is being sent from.</param>
        public PacketMessage(Packet packet, IMessageable source)
        {
            this.packet = packet;
            this.source = source;
        }

        // ----- Properties ----- //

        /// <summary>
        /// Gets the Packet that this message is encapsulating.
        /// </summary>
        public Packet Packet
        {
            get { return packet; }
        }

        /// <summary>
        /// Gets the name of the message type identifier, PacketMessage.
        /// </summary>
        public string MessageName
        {
            get { return messageName; }
        }

        /// <summary>
        /// Sets or gets the source IMessageable componenent.
        /// </summary>
        public IMessageable Source
        {
            set { source = value; }
            get { return source; }
        }
    }
}