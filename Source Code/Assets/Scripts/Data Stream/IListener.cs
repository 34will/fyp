﻿using UnityEngine;
using System.Collections.Generic;

namespace CommonFramework.DataStream
{
    /// <summary>
    /// IListener defines the abstraction for listeners that can receive data packets.
    /// </summary>
    public interface IListener
    {
        /// <summary>
        /// Instructs the IListener to start listening to the data stream.
        /// </summary>
        void Start();

        /// <summary>
        /// Instructs the IListener to stop listening to the data stream.
        /// </summary>
        void Stop();

        /// <summary>
        /// Receives a data packet from the data stream.
        /// </summary>
        /// <returns>The data packets read from the data stream.</returns>
        IEnumerable<Packet> ReceivePackets();
    }
}