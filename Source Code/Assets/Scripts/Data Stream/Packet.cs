﻿using UnityEngine;
using System.Collections;
using System.Net;

namespace CommonFramework.DataStream
{
    /// <summary>
    /// Packet represents byte data that has been received from a data stream.
    /// </summary>
    public class Packet
    {
        private byte[] data;
        private IPEndPoint sender;

        /// <summary>
        /// Creates a new Packet with the specified byte data.
        /// </summary>
        /// <param name="data">The byte array data to store in the Packet.</param>
        /// <param name="sender">The source IPAddress of the byte data.</param>
        public Packet(byte[] data, IPEndPoint sender)
        {
            this.data = data;
            this.sender = sender;
        }

        // ----- Properties ----- //

        /// <summary>
        /// Gets the IPAddress sender of the byte data that is tagged with the data Packet.
        /// </summary>
        public IPEndPoint Sender
        {
            get { return sender; }
        }

        /// <summary>
        /// Gets the byte array data stored in the Packet.
        /// </summary>
        public byte[] Data
        {
            get { return data; }
        }
    }
}