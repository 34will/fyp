﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace CommonFramework.DataStream
{
    /// <summary>
    /// UDPListener implements the IListener interface reading data packets from UDP streams.
    /// </summary>
    public class UDPListener : IListener
    {
        private bool stopped = false;
        private UdpClient client = null;
        private IPEndPoint endpoint = null;
        private List<Packet> packets = new List<Packet>();

        /// <summary>
        /// Creates a new UDPListener that listens for incoming data on a certain port.
        /// </summary>
        /// <param name="port">The port to listen for packet data.</param>
        public UDPListener(int port)
        {
            this.endpoint = new IPEndPoint(IPAddress.Any, port);
        }

        /// <summary>
        /// Instructs the UDPListener to start listening to the data stream.
        /// </summary>
        public void Start()
        {
            try
            {
                client = new UdpClient(endpoint);
                client.BeginReceive(new AsyncCallback(PacketReceived), this);
                stopped = false;
            }
            catch (Exception ex)
            {
                stopped = true;
                File.AppendAllText("udperror.log", ex.ToString() + System.Environment.NewLine);
            }
        }

        /// <summary>
        /// Instructs the UDPListener to stop listening to the data stream.
        /// </summary>
        public void Stop()
        {
            stopped = true;
        }

        /// <summary>
        /// Reads the next block of Packets up to the end of the currently received packets buffer.
        /// </summary>
        /// <returns>An enumeration of the Packets in the time block since the last ReceivePackets call.</returns>
        public IEnumerable<Packet> ReceivePackets()
        {
            List<Packet> receivedPackets = packets;
            packets = new List<Packet>();
            return receivedPackets;
        }

        private void PacketReceived(IAsyncResult result)
        {
            if(!stopped)
            {
                byte[] data = client.EndReceive(result, ref endpoint);
                packets.Add(new Packet(data, endpoint));
                client.BeginReceive(new AsyncCallback(PacketReceived), this);
            }
        }

        // ----- Properties ----- //

        /// <summary>
        /// Gets the port the UDPListener is listening on.
        /// </summary>
        public int Port
        {
            get { return endpoint.Port; }
        }

        /// <summary>
        /// Gets if the UDPListener is currently listening.
        /// </summary>
        public bool Running
        {
            get { return client != null && !stopped; }
        }
    }
}