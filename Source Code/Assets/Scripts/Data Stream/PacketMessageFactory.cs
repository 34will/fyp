﻿using UnityEngine;
using System.Collections;

using CommonFramework.MessageSystem;

namespace CommonFramework.DataStream
{
    /// <summary>
    /// PacketMessageFactory is an IPacketMessageFactory that produces a simple PacketMessage IMessage.
    /// </summary>
    public class PacketMessageFactory : IPacketMessageFactory
    {
        /// <summary>
        /// Create message creates a new instance of PacketMessage encapsulating the passed Packet.
        /// </summary>
        /// <param name="packet">The Packet to encapsulate in the PacketMessage.</param>
        /// <returns>A new PacketMessage instance.</returns>
        public IMessage CreateMessage(Packet packet)
        {
            return new PacketMessage(packet, null);
        }
    }
}