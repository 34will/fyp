﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net;

using CommonFramework.MessageSystem;

namespace CommonFramework.DataStream
{
    /// <summary>
    /// PacketTranscriber maps data packet patterns to IPacketMessageFactories. The PacketTranscriber
    /// converts incoming data into IMessages using registered factories to build the IMessage.
    /// </summary>
    public class PacketTranscriber : IEnumerable
    {
        private static PacketTranscriber instance = new PacketTranscriber();
        private List<IPacketMessageFactory> factories = new List<IPacketMessageFactory>();
        private IListener listener = null;

        private PacketTranscriber() { }

        /// <summary>
        /// Sets the PacketTranscriber with the specified IListener object to receive data with.
        /// </summary>
        /// <param name="listener">The IListener object that will be used as a data stream.</param>
        public void SetListener(IListener listener)
        {
            this.listener = listener;
        }

        /// <summary>
        /// Updates the data stream converting any received data. Converted data packets are pushed
        /// as messages into the messaging system.
        /// </summary>
        public void Update()
        {
            // Receive packets from the listener
            if (listener == null) return;
            IEnumerable<Packet> packets = listener.ReceivePackets();

            // Ask all the factories to build a message from the data for all the packets
            foreach (Packet packet in packets)
            {
                foreach (IPacketMessageFactory factory in factories)
                {
                    IMessage message = factory.CreateMessage(packet);
                    if (message != null) MessageHandler.Instance.SendMessage(message);
                }
            }
        }

        /// <summary>
        /// Registers an IPacketMessageFactory with the PacketTranscriber for data conversion.
        /// </summary>
        /// <param name="factory">The IPacketMessageFactory to add to the PacketTranscriber.</param>
        public void Register(IPacketMessageFactory factory)
        {
            if (!factories.Contains(factory)) factories.Add(factory);
        }

        /// <summary>
        /// Removes an IPacketMessageFactory from the PacketTranscriber so it will not be considered for data conversion.
        /// </summary>
        /// <param name="factory">The IPacketMessageFactory to remove from the PacketTranscriber.</param>
        public void Remove(IPacketMessageFactory factory)
        {
            factories.Remove(factory);
        }

        /// <summary>
        /// Gets the underlying IEnumerator for the internal IPacketMessageFactory collection.
        /// </summary>
        /// <returns>The IEnumerator for the internal IPacketMessageFactory collection.</returns>
        public IEnumerator GetEnumerator()
        {
            return factories.GetEnumerator();
        }

        // ----- Properties ----- //

        /// <summary>
        /// Gets the singleton instance of the PacketTranscriber
        /// </summary>
        public static PacketTranscriber Instance
        {
            get { return instance; }
        }

        /// <summary>
        /// Gets the underlying IListener that the PacketTranscriber uses to receive byte data.
        /// </summary>
        public IListener Listener
        {
            get { return listener; }
        }

        /// <summary>
        /// Gets the count of the internal IPacketMessageFactory collection.
        /// </summary>
        public int MessageFactoryCount
        {
            get { return factories.Count; }
        }
    }
}