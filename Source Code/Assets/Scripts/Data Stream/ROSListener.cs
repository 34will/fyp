﻿using UnityEngine;
using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

using CommonFramework.MessageSystem;
using CommonFramework.Logging;

namespace CommonFramework.DataStream
{
    public class ROSListener : IListener
    {
        
        public void Start()
        {
        }

        public void Stop()
        {
            
        }

        public IEnumerable<Packet> ReceivePackets()
        {
            yield return new Packet(new byte[0] { }, new IPEndPoint(0, 0));
        }

    }
}