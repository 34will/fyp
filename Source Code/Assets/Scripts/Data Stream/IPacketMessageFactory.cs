﻿using UnityEngine;
using System.Collections;

using CommonFramework.MessageSystem;

namespace CommonFramework.DataStream
{
    /// <summary>
    /// IPacketMessageFactory defines the contract for a factory that creates an IMessage from
    /// received byte array data.
    /// </summary>
    public interface IPacketMessageFactory
    {

        /// <summary>
        /// Informs the IPacketMessageFactory instance that it should create an IMessage from the Packet.
        /// </summary>
        /// <param name="packet">The packet received from a data stream with needed data encoded within.</param>
        /// <returns>The IMessage representation of the byte data, or null when the data is not supported.</returns>
        IMessage CreateMessage(Packet packet);
    }
}