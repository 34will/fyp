﻿using System;
using System.Collections.Generic;

namespace CommonFramework.Containers
{
	internal class SortedSetNode<T>
	{
		private SortedSetNode<T> left = null, right = null;
		private T data;

		public SortedSetNode(T data)
		{
			this.data = data;
		}

		public IEnumerable<T> Depth()
		{
			if(left != null)
			{
				foreach(T item in left.Depth())
					yield return item;
			}

			yield return this.Data;

			if(right != null)
			{
				foreach(T item in right.Depth())
					yield return item;
			}
		}

		// ----- Properties ----- //

		public T Data
		{
			set { data = value; }
			get { return data; }
		}

		public SortedSetNode<T> Left
		{
			set { left = value; }
			get { return left; }
		}

		public SortedSetNode<T> Right
		{
			set { right = value; }
			get { return right; }
		}

		internal T SmallestValue
		{
			get
			{
				if(left == null)
					return data;
				else
					return left.SmallestValue;
			}
		}
	}
}
