﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace CommonFramework.Containers
{
	/// <summary>
	/// Collection used to store a sorted set of objects of type T.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class SortedSet<T> : ICollection<T>, IEnumerable<T>
	{
		private IComparer<T> comparer = Comparer<T>.Default;
		private SortedSetNode<T> root = null, min = null, max = null;
		private HashSet<int> hashes = new HashSet<int>();

		/// <summary>
		/// Default constructor.
		/// </summary>
		public SortedSet() { }

		/// <summary>
		/// Creates a SortedSet with a custom IComparer&lt;T&gt; to use in sorting.
		/// </summary>
		/// <param name="comparer">Custom IComparer&lt;T&gt;.</param>
		public SortedSet(IComparer<T> comparer)
		{
			this.comparer = comparer;
		}

		/// <summary>
		/// Creates a SortedSet and populates it with the supplied IEnumerable&lt;T&gt;.
		/// </summary>
		/// <param name="data">Container of elements to populate set with.</param>
		public SortedSet(IEnumerable<T> data)
		{
			foreach(T item in data)
				Add(item);
		}

		/// <summary>
		/// Creates a SortedSet with a custom IComparer&lt;T&gt; to use in sorting and populates it with the supplied IEnumerable&lt;T&gt;.
		/// </summary>
		/// <param name="comparer">Custom IComparer&lt;T&gt;.</param>
		/// <param name="data">Container of elements to populate set with.</param>
		public SortedSet(IComparer<T> comparer, IEnumerable<T> data)
		{
			this.comparer = comparer;

			foreach(T item in data)
				Add(item);
		}

		/// <summary>
		/// Clears all items from the set.
		/// </summary>
		public void Clear()
		{
			hashes.Clear();
			root = null;
			min = null;
			max = null;
		}

		private void Insert(SortedSetNode<T> node, T data)
		{
			if(comparer.Compare(node.Data, data) > 0)
			{
				if(node.Left == null)
				{
					node.Left = new SortedSetNode<T>(data);
					if(node == min)
						min = node.Left;
				}
				else
					Insert(node.Left, data);
			}
			else
			{
				if(node.Right == null)
				{
					node.Right = new SortedSetNode<T>(data);
					if(node == max)
						max = node.Right;
				}
				else
					Insert(node.Right, data);
			}
		}

		/// <summary>
		/// Sorts a new element in the set.
		/// </summary>
		/// <param name="item">Item to add.</param>
		/// <returns>True if the item is successfully added, false if it already exists in the set.</returns>
		public bool Add(T item)
		{
			if(!hashes.Add(item.GetHashCode()))
				return false;

			if(root == null)
			{
				root = new SortedSetNode<T>(item);
				min = root;
				max = root;
			}
			else
				Insert(root, item);

			return true;
		}

		void ICollection<T>.Add(T item)
		{
			Add(item);
		}

		private void Remove(SortedSetNode<T> parent, SortedSetNode<T> node, T data)
		{
			int comparison = comparer.Compare(data, node.Data);
			if(comparison < 0)
				Remove(node, node.Left, data);
			else if(comparison > 0)
				Remove(node, node.Right, data);
			else
			{
				bool leftNotNull = node.Left != null;
				if(leftNotNull && node.Right != null)
				{
					node.Data = node.Right.SmallestValue;
					Remove(node, node.Right, node.Data);
				}
				else if(parent.Left == node)
					parent.Left = leftNotNull ? node.Left : node.Right;
				else if(parent.Right == node)
					parent.Right = leftNotNull ? node.Left : node.Right;
			}
		}

		/// <summary>
		/// Removes an item from the set.
		/// </summary>
		/// <param name="item">Item to remove.</param>
		/// <returns>True if the element is successfully removed, false if it is not present in the set.</returns>
		public bool Remove(T item)
		{
			if(!hashes.Remove(item.GetHashCode()))
				return false;

			SortedSetNode<T> temp = new SortedSetNode<T>(default(T));
			temp.Left = root;
			Remove(temp, root, item);
			root = temp.Left;

			return true;
		}

		/// <summary>
		/// Copies the set to a provided array.
		/// </summary>
		/// <param name="array">Array to copy set into.</param>
		public void CopyTo(T[] array)
		{
			CopyTo(array, 0, Count);
		}

		/// <summary>
		/// Copies the set to a provided array, at the provided offset.
		/// </summary>
		/// <param name="array">Array to copy set into.</param>
		/// <param name="arrayStart">Offset in array to copy to.</param>
		public void CopyTo(T[] array, int arrayStart)
		{
			CopyTo(array, arrayStart, Count);
		}

		/// <summary>
		/// Copies a number of elements from the set to a provided array, at the provided offset.
		/// </summary>
		/// <param name="array">Array to copy set into.</param>
		/// <param name="arrayStart">Offset in array to copy to.</param>
		/// <param name="count">Number of elements in the set, from the first element, inclusive, to copy.</param>
		public void CopyTo(T[] array, int arrayStart, int count)
		{
			if(array == null || arrayStart + count > array.Length)
				return;

			int i = arrayStart;
			foreach(T data in this)
			{
				if(i >= (arrayStart + count))
					break;

				array[i] = data;
				i++;
			}
		}

		/// <summary>
		/// Checks whether a provided item is present in the set.
		/// </summary>
		/// <param name="item">Item to check if exists.</param>
		/// <returns>True if the item exists, false if it does not.</returns>
		public bool Contains(T item)
		{
			return hashes.Contains(item.GetHashCode());
		}

		/// <summary>
		/// Used in enumeration of the set.
		/// </summary>
		/// <returns>IEnumerator used to iterate over the set.</returns>
		public IEnumerator GetEnumerator()
		{
			return ((IEnumerable<T>)this).GetEnumerator();
		}

		IEnumerator<T> IEnumerable<T>.GetEnumerator()
		{
			foreach(T item in root.Depth())
				yield return item;
		}

		// ----- Properties ----- //

		/// <summary>
		/// Comparer used to determine placement in the set. Defaults to Comparer&lt;T&gt;.Default.
		/// </summary>
		public IComparer<T> Comparer
		{
			get { return comparer; }
		}

		/// <summary>
		/// Number of elements in the set.
		/// </summary>
		public int Count
		{
			get { return hashes.Count; }
		}

		/// <summary>
		/// The 'minimum', as defined by the comparer, value in the set.
		/// </summary>
		public T Minimum
		{
			get { return min.Data; }
		}

		/// <summary>
		/// The 'Maximum', as defined by the comparer, value in the set.
		/// </summary>
		public T Maximum
		{
			get { return max.Data; }
		}

		/// <summary>
		/// Flag to say whether the container can be written to.
		/// </summary>
		public bool IsReadOnly
		{
			get { return false; }
		}
	}
}
