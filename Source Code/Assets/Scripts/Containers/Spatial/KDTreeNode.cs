﻿using System;
using System.Collections.Generic;

using CommonFramework.Maths;

namespace CommonFramework.Containers.Spatial
{
    internal class KDTreeNode<T>
        where T : class, IPositionableND
    {
        // Node information
        private int splitDimension = 0;
        private int maxItemCount = 0;
        private List<T> items = null;

        // Nodes position in n-dimensions
        private FloatN position = null;

        // Nodes
        private KDTreeNode<T> parent = null;
        private KDTreeNode<T> left = null;
        private KDTreeNode<T> right = null;

        internal KDTreeNode(int dimensions, int splitDimension, int maxItemCount, KDTreeNode<T> parent = null)
        {
            this.parent = parent;
            this.maxItemCount = maxItemCount;
            this.splitDimension = splitDimension;
            this.position = new FloatN(dimensions);
            this.items = new List<T>(maxItemCount);
        }

        public bool Insert(T item)
        {
            if (left == null && right == null)
            {
                // Check if the items 
                for (int i = items.Count - 1; i >= 0; i--)
                {
                    if (Equal(items[i], item))
                        return false;
                }
                
                // Insert to this layer
                items.Add(item);

                // Create and push to children?
                if (items.Count > maxItemCount)
                {
                    // Create the next nodes
                    int nextSplit = (splitDimension + 1) % position.Dimensions;
                    left = new KDTreeNode<T>(position.Dimensions, nextSplit, maxItemCount, this);
                    right = new KDTreeNode<T>(position.Dimensions, nextSplit, maxItemCount, this);
                    position[splitDimension] = CalculateSplitAverage(items, splitDimension);
                    left.position = new FloatN(position);
                    right.position = new FloatN(position);

                    // Push items to children
                    foreach (T subItem in items)
                    {
                        if (subItem[splitDimension] < position[splitDimension]) left.Insert(subItem);
                        else right.Insert(subItem);
                    }
                    items.Clear();
                }
                return true;
            }
            else
            {
                // What side should the item be pushed?
                if (item[splitDimension] < position[splitDimension]) return left.Insert(item);
                else return right.Insert(item);
            }
        }

        private float CalculateSplitAverage(List<T> items, int dimension)
        {
            float average = 0.0f;
            for (int i = 0; i < items.Count; i++)
                average += items[i][dimension];
            return average / items.Count;
        }

        private bool Equal(T a, T b)
        {
            for(int i = a.Dimensions - 1; i >= 0; i--)
            {
                if (a[i] != b[i])
                    return false;
            }
            return true;
        }

        // ----- Properties ----- //

        public int SplitDimension
        {
            get { return splitDimension; }
        }

        public int ItemCount
        {
            get
            {
                int itemCount = items.Count;
                if (left != null) itemCount += left.ItemCount;
                if (right != null) itemCount += right.ItemCount;
                return itemCount;
            }
        }

        public FloatN Position
        {
            internal set { position = value; }
            get { return position; }
        }

        public KDTreeNode<T> Left
        {
            get { return left; }
        }

        public KDTreeNode<T> Right
        {
            get { return right; }
        }

        public List<T> Items
        {
            get { return items; }
        }

        public bool IsLeaf
        {
            get { return left == null && right == null; }
        }

        public IEnumerable<T> SubTreeItems
        {
            get
            {
                foreach (T item in items)
                    yield return item;

                if (left != null)
                {
                    foreach (T item in left.SubTreeItems)
                        yield return item;
                }
                if (right != null)
                {
                    foreach (T item in right.SubTreeItems)
                        yield return item;
                }
            }
        }

        public IEnumerable<LineSegmentND> SubTreeAxes
        {
            get
            {
                FloatN a = new FloatN(position);
                FloatN b = new FloatN(position);

                int splitDrawAxis = splitDimension - 1;
                if (splitDrawAxis < 0) splitDrawAxis = position.Dimensions - 1;
                if (parent == null)
                {
                    a[splitDrawAxis] = 0.0f;
                    b[splitDrawAxis] = 8000.0f;
                }
                else
                {
                    a[splitDrawAxis] = parent.position[splitDrawAxis];
                    KDTreeNode<T> next = parent.parent;
                    while (next != null)
                    {
                        if (next.splitDimension == splitDrawAxis)
                            break;
                        next = next.parent;
                    }
                    if (next != null)
                        b[splitDrawAxis] = next.position[splitDrawAxis];
                    else if (parent.right == this) b[splitDrawAxis] = 8000.0f;
                    else b[splitDrawAxis] = 0.0f;
                }

                yield return new LineSegmentND(a, b);

                if (left != null)
                {
                    foreach (LineSegmentND segment in left.SubTreeAxes)
                        yield return segment;
                }
                if (right != null)
                {
                    foreach (LineSegmentND segment in right.SubTreeAxes)
                        yield return segment;
                }
            }
        }
    }
}
