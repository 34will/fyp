﻿using System;
using System.Collections;
using System.Collections.Generic;

using CommonFramework.Maths;

namespace CommonFramework.Containers.Spatial
{
    internal class QuadTreeNode<T> where T : class, IPositionable2D
    {
        private QuadTree<T> tree = null;
        private List<QuadTreeNode<T>> nodes = new List<QuadTreeNode<T>>(4);
        private List<T> items = new List<T>();
        private Rectangle bounds = null;

        internal QuadTreeNode(QuadTree<T> tree, Rectangle bounds)
        {
            this.tree = tree;
            this.bounds = bounds;
        }

        public bool Insert(T item)
        {
            // Make sure the item lies within this node
            if (!bounds.Contains(item.X, item.Y)) return false;

            // Add node to this layer (maybe temporary)
            items.Add(item);
            if (nodes.Count == 0 && items.Count == 1) return true;

            // Make nodes
            if (nodes.Count == 0)
            {
                float halfWidth = bounds.Width * 0.5f;
                float halfHeight = bounds.Height * 0.5f;

                if (halfWidth * halfHeight > tree.MinimumQuadArea)
                {
                    nodes.Add(new QuadTreeNode<T>(tree, new Rectangle(bounds.X, bounds.Y, halfWidth, halfHeight)));
                    nodes.Add(new QuadTreeNode<T>(tree, new Rectangle(bounds.X + halfWidth, bounds.Y, halfWidth, halfHeight)));
                    nodes.Add(new QuadTreeNode<T>(tree, new Rectangle(bounds.X + halfWidth, bounds.Y + halfHeight, halfWidth, halfHeight)));
                    nodes.Add(new QuadTreeNode<T>(tree, new Rectangle(bounds.X, bounds.Y + halfHeight, halfWidth, halfHeight)));
                }
            }

            // Determine if no more layers have been created
            if (nodes.Count == 0) return true;

            // Try to push all items onto children
            for (int i = items.Count - 1; i >= 0; i--)
            {
                foreach (QuadTreeNode<T> node in nodes)
                {
                    if (node.Insert(items[i]))
                    {
                        items.RemoveAt(i);
                        break;
                    }
                }
            }
            return true;
        }

        public IEnumerable<T> Search(Rectangle searchBounds)
        {
            // Before proceeding, check if this node will contain any intersecting points
            if (searchBounds.Intersects(bounds))
            {
                // Determine if items on this node intersect
                foreach (T item in items)
                {
                    if (searchBounds.Contains(item.X, item.Y))
                        yield return item;
                }

                // Determine if items on sub nodes intersect
                foreach (QuadTreeNode<T> node in nodes)
                {
                    foreach (T item in node.Search(searchBounds))
                        yield return item;
                }
            }
        }

        // ----- Properties ----- //

        public IEnumerable<T> SubTreeItems
        {
            get
            {
                foreach (T item in items)
                    yield return item;
                foreach (QuadTreeNode<T> node in nodes)
                {
                    foreach (T item in node.SubTreeItems)
                        yield return item;
                }
            }
        }

        public IEnumerable<Rectangle> SubTreeQuads
        {
            get
            {
                foreach (QuadTreeNode<T> node in nodes)
                {
                    yield return node.Bounds;
                    foreach (Rectangle rectangle in node.SubTreeQuads)
                        yield return rectangle;
                }
            }
        }

        public int ItemCount
        {
            get
            {
                int itemCount = items.Count;

                for (int i = nodes.Count - 1; i >= 0; i--)
                    itemCount += nodes[i].ItemCount;

                return itemCount;
            }
        }

        public Rectangle Bounds
        {
            get { return bounds; }
        }
    }
}
