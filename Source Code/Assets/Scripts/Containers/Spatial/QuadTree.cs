﻿using System;
using System.Collections;
using System.Collections.Generic;

using CommonFramework.Maths;

namespace CommonFramework.Containers.Spatial
{
    /// <summary>
    /// QuadTree is a spatial data structure in which every node has exactly four children, recursively dividing as more data is added.
    /// </summary>
    /// <typeparam name="T">The IPositiable2D type of the data that is stored in the QuadTree structure.</typeparam>
    /// <remarks>The type of the items being stored in the OcTree must implement IPositionable2D.</remarks>
    public class QuadTree<T> : ITree<T>
        where T : class, IPositionable2D
    {
        private QuadTreeNode<T> root = null;
        private float minimumArea = 10.0f;

        /// <summary>
        /// Creates a new QuadTree with the specified bounds.
        /// </summary>
        /// <param name="x">The X position of the QuadTree.</param>
        /// <param name="y">The Y position of the QuadTree.</param>
        /// <param name="width">The width of the QuadTree.</param>
        /// <param name="height">The height of the QuadTree.</param>
        public QuadTree(float x, float y, float width, float height)
            : this(new Rectangle(x, y, width, height)) { }

        /// <summary>
        /// Creates a new QuadTree with the specified bounds.
        /// </summary>
        /// <param name="bounds">The Rectangle representing the bounds of the Quad Tree.</param>
        public QuadTree(Rectangle bounds)
        {
            this.root = new QuadTreeNode<T>(this, bounds);
            this.minimumArea = (float)Math.Sqrt((bounds.Width * bounds.Width) + (bounds.Height * bounds.Height)) * 0.1f;
        }

        /// <summary>
        /// Inserts a new item into the QuadTree structure.
        /// </summary>
        /// <param name="item">The item to add to the QuadTree structure.</param>
        /// <returns>True if the item was inserted, otherwise false.</returns>
        public bool Insert(T item)
        {
            return root.Insert(item);
        }

        /// <summary>
        /// Get an IEnumerator for the QuadTree for iterating over all the contained items.
        /// </summary>
        /// <returns>The IEnumerator represeting all the data in the structure.</returns>
        public IEnumerator GetEnumerator()
        {
            return root.SubTreeItems.GetEnumerator();
        }

        /// <summary>
        /// Searchs for items in the structure in the specified search bounds.
        /// </summary>
        /// <param name="x">The X position of the search bounds.</param>
        /// <param name="y">The Y position of the search bounds.</param>
        /// <param name="width">The width of the search bounds.</param>
        /// <param name="height">The height of the search bounds.</param>
        /// <returns>The IEnumerable collection of items found in the search bounds.</returns>
        public IEnumerable<T> Search(float x, float y, float width, float height)
        {
            return root.Search(new Rectangle(x, y, width, height));
        }

        /// <summary>
        /// Searchs for items in the structure in the specified search bounds.
        /// </summary>
        /// <param name="searchBounds">The Rectangle bounds to search for items in.</param>
        /// <returns>The IEnumerable collection of the items in the search bounds.</returns>
        public IEnumerable<T> Search(Rectangle searchBounds)
        {
            return root.Search(searchBounds);
        }

        // ----- Properties ----- //

        /// <summary>
        /// Gets an IEnumerable for all the items in the QuadTree and all sub-trees.
        /// </summary>
        public IEnumerable<T> SubTreeItems
        {
            get { return root.SubTreeItems; }
        }

        /// <summary>
        /// Gets an IEnumerable for all the nodes bounds in the QuadTree and sub-trees.
        /// </summary>
        public IEnumerable<Rectangle> SubTreeQuads
        {
            get { return root.SubTreeQuads; }
        }

        /// <summary>
        /// Gets the total item count for every item in the QuadTree.
        /// </summary>
        public int ItemCount
        {
            get { return root.ItemCount; }
        }

        /// <summary>
        /// Sets or gets the minimum area for a cell in the QuadTree structure.
        /// </summary>
        /// <remarks>
        /// This prevents cells below a certain size being generated.
        /// Once a cell can't be sub-divided any more the items are stored in the final bucket.
        /// </remarks>
        public float MinimumQuadArea
        {
            set { minimumArea = value; }
            get { return minimumArea; }
        }

        /// <summary>
        /// Gets the boundary Rectangle encapsulating the tree and all items in it.
        /// </summary>
        public Rectangle Bounds
        {
            get
            {
                Rectangle bounds = root.Bounds;
                return new Rectangle(bounds.X, bounds.Y, bounds.Width, bounds.Height);
            }
        }
    }
}
