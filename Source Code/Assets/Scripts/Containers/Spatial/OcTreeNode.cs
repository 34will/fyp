﻿using System;
using System.Collections.Generic;

using CommonFramework.Maths;

namespace CommonFramework.Containers.Spatial
{
    internal class OcTreeNode<T> where T : class, IPositionable3D
    {
        private OcTree<T> tree = null;
        private List<OcTreeNode<T>> nodes = new List<OcTreeNode<T>>(8);
        private List<T> items = new List<T>();
        private Cuboid bounds = null;
        private OcTreeNode<T> parent = null;

        internal OcTreeNode(OcTree<T> tree, OcTreeNode<T> parent, Cuboid bounds)
        {
            this.tree = tree;
            this.parent = parent;
            this.bounds = bounds;
        }

        public bool Insert(T item)
        {
            // Make sure the item lies within this node
            if (!bounds.Contains(item.X, item.Y, item.Z)) return false;

            // Add node to this layer (maybe temporary)
            items.Add(item);
            if (nodes.Count == 0 && items.Count == 1) return true;

            // Make nodes
            if (nodes.Count == 0)
            {
                float halfWidth = bounds.Width * 0.5f;
                float halfHeight = bounds.Height * 0.5f;
                float halfDepth = bounds.Depth * 0.5f;

                if (halfWidth * halfHeight > tree.MinimumCellVolume)
                {
                    nodes.Add(new OcTreeNode<T>(tree, this, new Cuboid(bounds.X, bounds.Y, bounds.Z, halfWidth, halfHeight, halfDepth)));
                    nodes.Add(new OcTreeNode<T>(tree, this, new Cuboid(bounds.X + halfWidth, bounds.Y, bounds.Z, halfWidth, halfHeight, halfDepth)));
                    nodes.Add(new OcTreeNode<T>(tree, this, new Cuboid(bounds.X + halfWidth, bounds.Y + halfHeight, bounds.Z, halfWidth, halfHeight, halfDepth)));
                    nodes.Add(new OcTreeNode<T>(tree, this, new Cuboid(bounds.X, bounds.Y + halfHeight, bounds.Z, halfWidth, halfHeight, halfDepth)));
                    nodes.Add(new OcTreeNode<T>(tree, this, new Cuboid(bounds.X, bounds.Y, bounds.Z + halfDepth, halfWidth, halfHeight, halfDepth)));
                    nodes.Add(new OcTreeNode<T>(tree, this, new Cuboid(bounds.X + halfWidth, bounds.Y, bounds.Z + halfDepth, halfWidth, halfHeight, halfDepth)));
                    nodes.Add(new OcTreeNode<T>(tree, this, new Cuboid(bounds.X + halfWidth, bounds.Y + halfHeight, bounds.Z + halfDepth, halfWidth, halfHeight, halfDepth)));
                    nodes.Add(new OcTreeNode<T>(tree, this, new Cuboid(bounds.X, bounds.Y + halfHeight, bounds.Z + halfDepth, halfWidth, halfHeight, halfDepth)));
                }
            }

            // Determine if no more layers have been created
            if (nodes.Count == 0) return true;

            // Try to push all items onto children
            for (int i = items.Count - 1; i >= 0; i--)
            {
                foreach (OcTreeNode<T> node in nodes)
                {
                    if (node.Insert(items[i]))
                    {
                        items.RemoveAt(i);
                        break;
                    }
                }
            }
            return true;
        }

        public IEnumerable<T> Search(Cuboid searchBounds)
        {
            // Before proceeding, check if this node will contain any intersecting points
            if (searchBounds.Intersects(bounds))
            {
                // Determine if items on this node intersect
                foreach (T item in items)
                {
                    if (searchBounds.Contains(item.X, item.Y, item.Z))
                        yield return item;
                }

                // Determine if items on sub nodes intersect
                foreach (OcTreeNode<T> node in nodes)
                {
                    foreach (T item in node.Search(searchBounds))
                        yield return item;
                }
            }
        }

        public T NearestNeighbour(T item)
        {
            // Check if children contain the item
            // This is also the end recursion condition when there are no more bounds
            foreach (OcTreeNode<T> node in nodes)
            {
                if (node.bounds.Contains(item))
                    return node.NearestNeighbour(item);
            }

            // Step up the parents to find the first instance where children exist
            List<T> subItems = null;
            OcTreeNode<T> next = parent;
            if (next != null)
            {
                // TODO - Obvious optimisation here. Don't use SubTreeItems. Manually create it for speed.
                subItems = new List<T>(next.SubTreeItems);
                subItems.Remove(item);
            }

            if (subItems == null) return null;
            else
            {
                // Find the surrounding nodes
                T foundItem = subItems[0];
                float closest = CalculateDistanceSquared(item, foundItem);
                for (int i = subItems.Count - 1; i > 0; i--)
                {
                    float distance = CalculateDistanceSquared(item, subItems[i]);
                    if (distance < closest)
                    {
                        closest = distance;
                        foundItem = subItems[i];
                    }
                }
                return foundItem;
            }
        }

        private float CalculateDistanceSquared(IPositionable3D a, IPositionable3D b)
        {
            float x = b.X - a.X;
            float y = b.Y - a.Y;
            float z = b.Z - a.Z;
            return (x * x) + (y * y) + (z * z);
        }

        // ----- Properties ----- //

        public IEnumerable<T> SubTreeItems
        {
            get
            {
                foreach (T item in items)
                    yield return item;
                foreach (OcTreeNode<T> node in nodes)
                {
                    foreach (T item in node.SubTreeItems)
                        yield return item;
                }
            }
        }

        public IEnumerable<Cuboid> SubTreeCubes
        {
            get
            {
                foreach (OcTreeNode<T> node in nodes)
                {
                    yield return node.Bounds;
                    foreach (Cuboid rectangle in node.SubTreeCubes)
                        yield return rectangle;
                }
            }
        }

        public int ItemCount
        {
            get
            {
                int itemCount = items.Count;

                for (int i = nodes.Count - 1; i >= 0; i--)
                    itemCount += nodes[i].ItemCount;

                return itemCount;
            }
        }

        public Cuboid Bounds
        {
            get { return bounds; }
        }
    }
}
