﻿using System;
using System.Collections;
using System.Collections.Generic;

using CommonFramework.Maths;

namespace CommonFramework.Containers.Spatial
{
    /// <summary>
    /// OcTree is a spatial tree in which every node has exactaly eight children, recursively dividing into smaller regions as data values are added.
    /// </summary>
    /// <typeparam name="T">The IPositionable3D type of the data to be stored in the OcTree.</typeparam>
    /// <remarks>The type of the items being stored in the OcTree must implement IPositionable3D.</remarks>
    public class OcTree<T> : ITree<T>
        where T : class, IPositionable3D
    {
        private OcTreeNode<T> root = null;
        private float minimumVolume = 10.0f;

        /// <summary>
        /// Creates a new OcTree with the specified volume bounds.
        /// </summary>
        /// <param name="x">The X origin of the OcTree volume.</param>
        /// <param name="y">The Y origin of the OcTree volume.</param>
        /// <param name="z">The Z origin of the OcTree volume.</param>
        /// <param name="width">The width of the OcTree volume.</param>
        /// <param name="height">The height of the OcTree volume.</param>
        /// <param name="depth">The depth of the OcTree volume.</param>
        public OcTree(float x, float y, float z, float width, float height, float depth)
            : this(new Cuboid(x, y, z, width, height, depth)) { }

        /// <summary>
        /// Creates a new OcTree with the specified volume bounds.
        /// </summary>
        /// <param name="bounds">The Cuboid bounds of the OcTree volume.</param>
        public OcTree(Cuboid bounds)
        {
            this.root = new OcTreeNode<T>(this, null, bounds);
            this.minimumVolume = (float)Math.Sqrt((bounds.Width * bounds.Width) + (bounds.Height * bounds.Height) + (bounds.Depth * bounds.Depth)) * 0.1f;
        }

        /// <summary>
        /// Inserts a new item into the OcTree structure.
        /// </summary>
        /// <param name="item">The item to add to the OcTree structure.</param>
        /// <returns>True if the item was inserted, otherwise false.</returns>
        public bool Insert(T item)
        {
            return root.Insert(item);
        }

        /// <summary>
        /// Get an IEnumerator for the OcTree for iterating over all the contained items.
        /// </summary>
        /// <returns>The IEnumerator represeting all the data in the structure.</returns>
        public IEnumerator GetEnumerator()
        {
            return root.SubTreeItems.GetEnumerator();
        }

        /// <summary>
        /// Searchs for items in the structure in the specified search bounds.
        /// </summary>
        /// <param name="x">The X position of the search bounds.</param>
        /// <param name="y">The Y position of the search bounds.</param>
        /// <param name="z">The Z position of the search bounds.</param>
        /// <param name="width">The width of the search bounds.</param>
        /// <param name="height">The height of the search bounds.</param>
        /// <param name="depth">The depth of the search bounds.</param>
        /// <returns>The IEnumerable collection of items found in the search bounds.</returns>
        public IEnumerable<T> Search(float x, float y, float z, float width, float height, float depth)
        {
            return root.Search(new Cuboid(x, y, z, width, height, depth));
        }

        /// <summary>
        /// Searchs for items in the structure in the specified search bounds.
        /// </summary>
        /// <param name="searchBounds">The bounds to limit the item search to.</param>
        /// <returns>The IEnumerable collection of items found in the search bounds.</returns>
        public IEnumerable<T> Search(Cuboid searchBounds)
        {
            return root.Search(searchBounds);
        }

        /// <summary>
        /// Searchs for the tree for the closest item in the structure that is not itself.
        /// Please note there are a few edge cases in which this method does not work at the gain of an increased search speed.
        /// </summary>
        /// <param name="item">The item instance in the tree to locate and search surrounding items.</param>
        /// <returns>The closest item to the found T item instance, otherwise null.</returns>
        /// <remarks>In some cases, at the edge of a sub-node boundary the algorithm may find an item in the same branch
        /// (that is slightly further) than the one closer in another branch.</remarks>
        public T NearestNeighbour(T item)
        {
            return root.NearestNeighbour(item);
        }

        // ----- Properties ----- //

        /// <summary>
        /// Gets an IEnumerable for all the items in the OcTree and all sub-trees.
        /// </summary>
        public IEnumerable<T> SubTreeItems
        {
            get { return root.SubTreeItems; }
        }

        /// <summary>
        /// Gets an IEnumerable for all the nodes bounds in the OcTree and sub-trees.
        /// </summary>
        public IEnumerable<Cuboid> SubTreeCubes
        {
            get { return root.SubTreeCubes; }
        }

        /// <summary>
        /// Gets the total item count for every item in the OcTree.
        /// </summary>
        public int ItemCount
        {
            get { return root.ItemCount; }
        }

        /// <summary>
        /// Sets or gets the minimum volume for a cell in the OcTree structure.
        /// </summary>
        /// <remarks>
        /// This prevents cells below a certain size being generated.
        /// Once a cell can't be sub-divided any more the items are stored in the final bucket.
        /// </remarks>
        public float MinimumCellVolume
        {
            set { minimumVolume = value; }
            get { return minimumVolume; }
        }

        /// <summary>
        /// Gets the boundary Cuboid encapsulating the tree and all items in it.
        /// </summary>
        public Cuboid Bounds
        {
            get
            {
                Cuboid bounds = root.Bounds;
                return new Cuboid(bounds.X, bounds.Y, bounds.Z, bounds.Width, bounds.Height, bounds.Depth);
            }
        }
    }
}
