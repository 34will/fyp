﻿using System;
using System.Collections;
using System.Collections.Generic;

using CommonFramework.Maths;

namespace CommonFramework.Containers.Spatial
{
    /// <summary>
    /// KDTree is a spatial binary tree in which every node is point in N-Dimensions.
    /// </summary>
    /// <typeparam name="T">The IPositionableND type of the data to be stored in the KDTree.</typeparam>
    /// <remarks>The type of the items being stored in the KDTree must implement IPositionableND.</remarks>
    public class KDTree<T> : ITree<T>
        where T : class, IPositionableND
    {
        private int dimensions = 0;
        private int maxNodeItems = 10;
        private KDTreeNode<T> root = null;

        /// <summary>
        /// Creates a new KDTree that operates in the specified number of dimensions.
        /// </summary>
        /// <param name="dimensions">The number of dimensions the KDTree operates in.</param>
        /// <param name="maxNodeItems">The maximum number of items that can be stored at each leaf node before being pushed to child nodes.</param>
        public KDTree(int dimensions = 3, int maxNodeItems = 10)
        {
            this.dimensions = dimensions;
            this.maxNodeItems = maxNodeItems;
            this.root = new KDTreeNode<T>(dimensions, 0, maxNodeItems);
            this.root.Position = new FloatN(dimensions);
        }

        /// <summary>
        /// Inserts a new item into the KDTree structure.
        /// </summary>
        /// <param name="item">The item to add to the KDTree structure.</param>
        /// <returns>True if the item was inserted, otherwise false.</returns>
        public bool Insert(T item)
        {
            return root.Insert(item);
        }

        /// <summary>
        /// Finds the nearest set of items spatially to the specified IPositionableND.
        /// </summary>
        /// <param name="point">The IPositionableND to find the nearest points around.</param>
        /// <param name="count">The number of nearest points to find.</param>
        /// <returns>The nearest points collection.</returns>
        public SortedList<float, IPositionableND> NearestNeighbours(IPositionableND point, int count = 5)
        {
            if (point.Dimensions == dimensions)
            {
                SortedList<float, IPositionableND> items = new SortedList<float, IPositionableND>(count + 1);
                NearestNeighbours(point, count, items, root);
                return items;
            }
            else throw new Exception("The number of dimensions of the search point is " + point.Dimensions + " and is not compatible for search with the KDTree which is " + dimensions + " dimensions");
        }

        private void NearestNeighbours(IPositionableND point, int count, SortedList<float, IPositionableND> items, KDTreeNode<T> node)
        {
            // Recursively step through the hyperplanes
            // determine at each node, which side of the hyperplane lies in the hyper sphere
            // The hyper sphere's radius squared is defined by ItemsMax

            if (node.IsLeaf)
            {
                for (int t = node.Items.Count - 1; t >= 0; t--)
                {
                    float dist = CalculateDistanceSquared(point, node.Items[t]);
                    items[dist] = node.Items[t];
                    if (items.Count > count) items.RemoveAt(count);
                }
            }
            else
            {
                int dimension = node.SplitDimension;
                float dist = point[dimension] - node.Position[dimension];
                dist *= dist;

                if (point[dimension] < node.Position[dimension])
                {
                    // Point would've been natrually inserted down the left
                    NearestNeighbours(point, count, items, node.Left);

                    // Even though we went down the left, did the enclosing hypersphere overlap in the right
                    float max = ItemsMax(items, count);
                    if (dist < max) NearestNeighbours(point, count, items, node.Right);
                }
                else
                {
                    // Point would've been natrually inserted down the right
                    NearestNeighbours(point, count, items, node.Right);

                    // Even though we went down the right, did the enclosing hypersphere overlap in the left
                    float max = ItemsMax(items, count);
                    if (dist < max) NearestNeighbours(point, count, items, node.Left);
                }
            }
        }

        private float ItemsMax(SortedList<float, IPositionableND> items, int count)
        {
            if (items.Count < count) return float.MaxValue;
            else return items.Keys[count - 1];
        }

        private float CalculateDistanceSquared(IPositionableND a, IPositionableND b)
        {
            float distanceSquared = 0.0f;
            for (int i = a.Dimensions - 1; i >= 0; i--)
            {
                float diff = a[i] - b[i];
                distanceSquared += diff * diff;
            }
            return distanceSquared;
        }

        /// <summary>
        /// Get an IEnumerator for the KDTree for iterating over all the contained items.
        /// </summary>
        /// <returns>The IEnumerator represeting all the data in the structure.</returns>
        public IEnumerator GetEnumerator()
        {
            return root.SubTreeItems.GetEnumerator();
        }

        // ----- Properties ----- //

        /// <summary>
        /// Gets the number of dimensions this KDTree operates in.
        /// </summary>
        public int Dimensions
        {
            get { return dimensions; }
        }

        /// <summary>
        /// Gets the total item count for every item in the KDTree.
        /// </summary>
        public int ItemCount
        {
            get { return root.ItemCount; }
        }

        /// <summary>
        /// Gets an IEnumerable for all the items in the KDTree and all sub-trees.
        /// </summary>
        public IEnumerable<T> SubTreeItems
        {
            get { return root.SubTreeItems; }
        }

        /// <summary>
        /// Gets the line segments for the axes of the subtree
        /// </summary>
        public IEnumerable<LineSegmentND> SubTreeAxes
        {
            get { return root.SubTreeAxes; }
        }
    }
}
