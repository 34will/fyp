using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace CommonFramework.Containers.Spatial
{
    /// <summary>
    /// ITree defines the interface for tree based data structures.
    /// </summary>
    /// <typeparam name="T">The type of the data structure the tree stores.</typeparam>
    public interface ITree<T> : IEnumerable
    {
        /// <summary>
        /// Insert defines the method for inserting data into the tree structure.
        /// </summary>
        /// <param name="item">The item to insert into the data structure tree.</param>
        /// <returns>Returns true when the item is added to the tree structure, otherwise false.</returns>
        bool Insert(T item);

        // ----- Properties ----- //

        /// <summary>
        /// Gets an IEnumerable typed collection of the data values of all the sub trees.
        /// </summary>
        IEnumerable<T> SubTreeItems { get; }

        /// <summary>
        /// Gets the number of items stored in the entire tree structure.
        /// </summary>
        int ItemCount { get; }
    }
}