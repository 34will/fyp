﻿using UnityEngine;

using CommonFramework.MessageSystem;

namespace CommonFramework.LineDrawer
{
    /// <summary>
    /// DrawLineStatusMessage implements IMessage and is handled by the LineDrawer. Use this message to indicate if the LineDrawer should draw lines to the screen.
    /// </summary>
    public class DrawLineStatusMessage : IMessage
    {
        public const string DrawLineStatusMessageName = "DrawLineStatusMessage";
        private IMessageable source = null;
        private bool drawLines = false;

        /// <summary>
        /// Creates a new DrawLineStatusMessage which is handled by the LineDrawer to determine if lines should be drawn.
        /// </summary>
        /// <param name="drawLines">A flag that indicates if lines should be drawn.</param>
        /// <param name="source">The source IMessageable component that sent the IMessage.</param>
        public DrawLineStatusMessage(bool drawLines, IMessageable source = null)
        {
            this.drawLines = drawLines;
            this.source = source;
        }

        // ----- Properties ----- //

        /// <summary>
        /// Gets the flag indicating if lines should be drawn.
        /// </summary>
        public bool DrawLines
        {
            get { return drawLines; }
        }

        /// <summary>
        /// Gets the name of the IMessage, DrawLineStatusMessage.
        /// </summary>
        public string MessageName
        {
            get { return DrawLineStatusMessageName; }
        }

        /// <summary>
        /// Sets or gets the source IMessageable component that sent the DrawLineStatusMessage.
        /// </summary>
        public IMessageable Source
        {
            set { source = value; }
            get { return source; }
        }
    }
}
