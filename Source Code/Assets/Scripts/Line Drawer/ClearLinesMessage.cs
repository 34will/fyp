﻿using UnityEngine;
using System.Collections;

using CommonFramework.MessageSystem;

namespace CommonFramework.LineDrawer
{
	/// <summary>
	/// ClearLinesMessage is an IMessage implementation that signals the LineDrawer to clear all stored lines.
	/// </summary>
    public class ClearLinesMessage : IMessage
    {
		internal const string messageName = "ClearLinesMessage";
        private IMessageable source = null;
    
        /// <summary>
        /// Creates a new ClearLinesMessage with the optional source IMessageable component.
        /// </summary>
        /// <param name="source">The source IMessageable component that sent the ClearLinesMessage.</param>
        public ClearLinesMessage(IMessageable source = null)
        {
            this.source = source;
        }

        // ----- Properties ----- //

        /// <summary>
		/// Gets the identifier name for this type of message, ClearLinesMessage.
        /// </summary>
        public string MessageName
        {
            get { return messageName; }
        }
    
        /// <summary>
		/// Sets or gets the source IMessageable that sent the ClearLinesMessage.
        /// </summary>
        public IMessageable Source
        {
            set { source = value; }
            get { return source; }
        }
    }
}

