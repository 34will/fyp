﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using UnityEngine;

using CommonFramework.MessageSystem;

namespace CommonFramework.LineDrawer
{
    /// <summary>
    /// LineDrawer provides a method of drawing coloured lines in the scene.
    /// </summary>
    [RequireComponent(typeof(Camera))]
	[ExcludeFromCodeCoverage]
    public class LineDrawer : MonoBehaviour, IMessageable
    {
        private const string messageableName = "LineDrawer";
        private List<KeyValuePair<Vector3, Color>> lines = new List<KeyValuePair<Vector3, Color>>();
        private Queue<IMessage> pendingMessages = new Queue<IMessage>();
        private bool drawLines = true;

        /// <summary>
        /// The material used in the OpenGL post render process.
        /// </summary>
        public Material Material;

        /// <summary>
        /// Unity Awake() function: Awake this instance.
        /// </summary>
        void Awake()
        {
            MessageHandler.Instance.RegisterMessageable(this);
        }

        /// <summary>
        /// Unity OnPostRender() function: Raises the post render event.
        /// </summary>
        void OnPostRender()
        {
            if (drawLines)
            {
                GL.PushMatrix();
                Material.SetPass(0);
                GL.Begin(GL.LINES);

                foreach (KeyValuePair<Vector3, Color> kvp in lines)
                {
                    GL.Color(kvp.Value);
                    GL.Vertex3(kvp.Key.x, kvp.Key.y, kvp.Key.z);
                }

                GL.End();
                GL.PopMatrix();
            }
        }
        
        /// <summary>
        /// Adds the line.
        /// </summary>
        /// <param name="start">Start.</param>
        /// <param name="startColour">Start colour.</param>
        /// <param name="end">End.</param>
        /// <param name="endColour">End colour.</param>
        public void AddLine(Vector3 start, Color startColour, Vector3 end, Color endColour)
        {
            lines.Add(new KeyValuePair<Vector3, Color>(start, startColour));
            lines.Add(new KeyValuePair<Vector3, Color>(end, endColour));
        }
        
        /// <summary>
        /// Adds the line.
        /// </summary>
        /// <param name="start">Start.</param>
        /// <param name="end">End.</param>
        /// <param name="lineColour">Line colour.</param>
        public void AddLine(Vector3 start, Vector3 end, Color lineColour)
        {
            AddLine(start, lineColour, end, lineColour);
        }
        
        /// <summary>
        /// Adds the line.
        /// </summary>
        /// <param name="start">Start.</param>
        /// <param name="end">End.</param>
        public void AddLine(Vector3 start, Vector3 end)
        {
            AddLine(start, end, Color.white);
        }

        /// <summary>
        /// Informs the LineDrawer that it should handle and process the passed IMessage.
        /// </summary>
        /// <param name="message">The IMessage that the LineDrawer should handle.</param>
        public void ReceiveMessage(IMessage message)
        {
            if(message.MessageName == DrawLineMessage.messageName)
            {
                DrawLineMessage drawLineMessage = message as DrawLineMessage;
                if(drawLineMessage == null)
                    return;

                AddLine(drawLineMessage.Start, drawLineMessage.StartColour, drawLineMessage.End, drawLineMessage.EndColour);
			}
			else if(message.MessageName == ClearLinesMessage.messageName)
			{
				ClearLinesMessage clearLinesMessage = message as ClearLinesMessage;
				if(clearLinesMessage == null)
					return;

				lines.Clear();
			}
            else if(message.MessageName == DrawLineStatusMessage.DrawLineStatusMessageName)
            {
                drawLines = ((DrawLineStatusMessage)message).DrawLines;
            }
        }

        // ----- Properties ----- //

        /// <summary>
        /// Gets a Queue of the IMessages that this LineDrawer wants to dispatch.
        /// </summary>
        public Queue<IMessage> PendingMessages
        {
            get { return pendingMessages; }
        }

        /// <summary>
        /// Gets the identifier to use when addressing the LineDrawer.
        /// </summary>
        public string MessageableName
        {
            get { return messageableName; }
        }
    }
}
