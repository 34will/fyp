﻿using UnityEngine;
using System.Collections;

using CommonFramework.MessageSystem;

namespace CommonFramework.LineDrawer
{
    /// <summary>
    /// DrawLineMessage is an IMessage implementation that the LineDrawer uses to render OpenGL line segments in the scene.
    /// </summary>
    public class DrawLineMessage : IMessage
    {
        internal const string messageName = "DrawLineMessage";
        private Vector3 start = Vector3.zero, end = Vector3.zero;
        private Color startColour = Color.white, endColour = Color.white;
        private IMessageable source = null;
    
        /// <summary>
        /// Creates a new DrawLineMessage representing a coloured line segment, where the colour LERPs from the start colour to the end colour.
        /// </summary>
        /// <param name="start">Start point of the line segment.</param>
        /// <param name="startColour">Colour of the start of the line segment.</param>
        /// <param name="end">End point of the line segment.</param>
        /// <param name="endColour">Colour of the end of the line segment.</param>
        public DrawLineMessage(Vector3 start, Color startColour, Vector3 end, Color endColour)
        {
            this.start = start;
            this.startColour = startColour;
            this.end = end;
            this.endColour = endColour;
        }
    
        /// <summary>
        /// Creates a new DrawLineMessage representing a coloured line segment.
        /// </summary>
        /// <param name="start">Start point of the line segment.</param>
        /// <param name="end">End point of the line segment.</param>
        /// <param name="lineColour">Colour of the line segment.</param>
        public DrawLineMessage(Vector3 start, Vector3 end, Color lineColour) : this(start, lineColour, end, lineColour) { }
    
        /// <summary>
        /// Creates a new DrawLineMessage representing a white line segment.
        /// </summary>
        /// <param name="start">Start point of the line segment.</param>
        /// <param name="end">End point of the line segment.</param>
        public DrawLineMessage(Vector3 start, Vector3 end) : this(start, end, Color.white) { }
    
        // ----- Properties ----- //
    
        /// <summary>
        /// Get the start point of the line segment.
        /// </summary>
        public Vector3 Start
        {
            get { return start; }
        }
    
        /// <summary>
        /// Get the end point of the line segment.
        /// </summary>
        public Vector3 End
        {
            get { return end; }
        }
    
        /// <summary>
        /// Get the start colour of the line segment.
        /// </summary>
        public Color StartColour
        {
            get { return startColour; }
        }
    
        /// <summary>
        /// Get the end colour of the line segment.
        /// </summary>
        public Color EndColour
        {
            get { return endColour; }
        }
    
        /// <summary>
        /// Gets the identifier name for this type of message, DrawLineMessage.
        /// </summary>
        public string MessageName
        {
            get { return messageName; }
        }
    
        /// <summary>
        /// Sets or gets the source IMessageable that sent the DrawLineMessage.
        /// </summary>
        public IMessageable Source
        {
            set { source = value; }
            get { return source; }
        }
    }
}
