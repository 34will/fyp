﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace CommonFramework.SoftBodyPhysics
{
    public interface ISoftbodyPhysicsGameObject
    {
        // Properties

        /// <summary>
        /// Get the SoftbodyPhysicsObject copy of the appended ISoftbodyPhysicsObject.
        /// </summary>
        GameObject SoftbodyPhysicsObject { get; }

        /// <summary>
        /// Get the Accel of the appended ISoftbodyPhysicsObject.
        /// </summary>
        Vector3 Accel { get; }

        /// <summary>
        /// Get the Velocity of the appended ISoftbodyPhysicsObject.
        /// </summary>
        Vector3 Velocity { get; set; }

        /// <summary>
        /// Get the Position of the appended ISoftbodyPhysicsObject.
        /// </summary>
        Vector3 Position { get; set; }

        /// <summary>
        /// Get the Mass of the appended ISoftbodyPhysicsObject.
        /// </summary>
        float Mass { get; set; }

        /// <summary>
        /// Get the Type of the appended ISoftbodyPhysicsObject.
        /// </summary>
        string Type { get; }
    }
}
