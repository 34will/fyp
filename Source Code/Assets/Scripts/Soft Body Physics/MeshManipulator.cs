﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using CommonFramework.CameraControl;
using CommonFramework.Environment;
using CommonFramework.Maths;
using UnityEngine;
using CommonFramework.Logging;
using CommonFramework.MessageSystem;
using Random = System.Random;

namespace CommonFramework.SoftBodyPhysics
{
    /// <summary>
    /// MeshManipulator enables the manipulation of the mesh data of objects.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class MeshManipulator : MonoBehaviour
    {
        //IMessageable concrete implementation containers
        private string messageableName = "MeshManipulator";
        private Queue<IMessage> pendingMessages = new Queue<IMessage>();

        //Locally copied mesh data
        private Mesh softbodyMesh = null;
        private Vector3[] vertices = null;
        private Vector3[] normals = null;
        private Vector2[] texUVs = null;
        private int[] triangles = null;

        //Containers for soft-body attributes
        public MassPoint[] massPoints = null;
        public ISpring[] massSprings = null;

        /// <summary>
        /// Prints information regarding the MeshManipulator mesh
        /// </summary>
        public bool PrintInfo = false;

        /// <summary>
        /// Copies the necessary data of a GameObject locally for the MeshManipulator.
        /// Registers the MeshManipulator as Messageable.
        /// Assigns and populates the MassPoints and MassSpring data structures.
        /// </summary>
        void Start() //Start performed here to ensure the objects are all assigned their masses before manipulation
        {
            CopyMeshDataLocally(); //gameObject doesn't even need to be passed as a parameter, but kept it just incase in the future other classes need to access it
            GiveMeshAMassSpringSystem();
        }

        /// <summary>
        /// Performs an internal update of the Springs and Masspoints of the GameObject.
        /// </summary>
        void Update()
        {
            //foreach (var v in massPoints) v.Velocity += NextVector()*0.25f;
            InternalUpdate();
            //foreach (var s in massPoints) s.ResetForces();
            //SoftbodyCollisionHelper.ResizeGameObjectMeshCollider(gameObjectCopy, false);
        }

        /// <summary>
        /// Update the mesh data held within the MeshManipulator component of an object.
        /// </summary>
        public void InternalUpdate()
        {
            Vector3 forceTerm1 = Vector3.zero;
            Vector3 forceTerm2 = Vector3.zero;
            Vector3 forceTerm3 = Vector3.zero;
            Vector3 forceTerm4 = Vector3.zero;
            Vector3 forceTerm5 = Vector3.zero;
            Vector3 forceTerm6 = Vector3.zero;
            Vector3 saveDivBy0 = new Vector3(1.0f, 1.0f, 1.0f);

            for (int i = 0; i < massSprings.Length; i++)
            {
                massSprings[i].UpdateSpring(Time.fixedDeltaTime);
            }

            //Update mesh based on MassPoints and Springs
            /*for (int i = 1; i < vertices.Length-2; i++)
            {
                Vector3 constant1 = new Vector3(MassStructuralSprings[i - 1].Stiffness, MassStructuralSprings[i].Stiffness, MassStructuralSprings[i].Stiffness);
                Vector3 common1 = new Vector3(massPoints[i - 1].RestPosition.x, massPoints[i].RestPosition.y, massPoints[i].RestPosition.z) - massPoints[i].RestPosition;
                if (common1 == Vector3.zero) common1 = saveDivBy0;
                float mag1 = Vector3.Magnitude(common1);
                Vector3 restLength1 = new Vector3(MassStructuralSprings[i - 1].RestLength, MassStructuralSprings[i].RestLength, MassStructuralSprings[i].RestLength);
                Vector3 magVector1 = new Vector3(mag1, mag1, mag1);
                forceTerm1 = Vector3.Scale(Vector3.Scale(constant1, (magVector1 - restLength1)), (common1 / mag1));

                Vector3 constant2 = new Vector3(MassStructuralSprings[i + 1].Stiffness, MassStructuralSprings[i].Stiffness, MassStructuralSprings[i].Stiffness);
                Vector3 common2 = new Vector3(massPoints[i + 1].RestPosition.x, massPoints[i].RestPosition.y, massPoints[i].RestPosition.z) - massPoints[i].RestPosition;
                if (common2 == Vector3.zero) common2 = saveDivBy0;
                float mag2 = Vector3.Magnitude(common2);
                Vector3 restLength2 = new Vector3(MassStructuralSprings[i + 1].RestLength, MassStructuralSprings[i].RestLength, MassStructuralSprings[i].RestLength);
                Vector3 magVector2 = new Vector3(mag2, mag2, mag2);
                forceTerm2 = Vector3.Scale(Vector3.Scale(constant2, (magVector2 - restLength2)), (common2 / mag2));

                Vector3 constant3 = new Vector3(MassStructuralSprings[i].Stiffness, MassStructuralSprings[i - 1].Stiffness, MassStructuralSprings[i].Stiffness);
                Vector3 common3 = new Vector3(massPoints[i].RestPosition.x, massPoints[i - 1].RestPosition.y, massPoints[i].RestPosition.z) - massPoints[i].RestPosition;
                if (common3 == Vector3.zero) common3 = saveDivBy0;
                float mag3 = Vector3.Magnitude(common3);
                Vector3 restLength3 = new Vector3(MassStructuralSprings[i].RestLength, MassStructuralSprings[i - 1].RestLength, MassStructuralSprings[i].RestLength);
                Vector3 magVector3 = new Vector3(mag3, mag3, mag3);
                forceTerm3 = Vector3.Scale(Vector3.Scale(constant3, (magVector3 - restLength3)), (common3 / mag3));

                Vector3 constant4 = new Vector3(MassStructuralSprings[i].Stiffness, MassStructuralSprings[i + 1].Stiffness, MassStructuralSprings[i].Stiffness);
                Vector3 common4 = new Vector3(massPoints[i].RestPosition.x, massPoints[i + 1].RestPosition.y, massPoints[i].RestPosition.z) - massPoints[i].RestPosition;
                if (common4 == Vector3.zero) common4 = saveDivBy0;
                float mag4 = Vector3.Magnitude(common4);
                Vector3 restLength4 = new Vector3(MassStructuralSprings[i].RestLength, MassStructuralSprings[i + 1].RestLength, MassStructuralSprings[i].RestLength);
                Vector3 magVector4 = new Vector3(mag4, mag4, mag4);
                forceTerm4 = Vector3.Scale(Vector3.Scale(constant4, (magVector4 - restLength4)), (common4 / mag4));

                Vector3 constant5 = new Vector3(MassStructuralSprings[i].Stiffness, MassStructuralSprings[i].Stiffness, MassStructuralSprings[i - 1].Stiffness);
                Vector3 common5 = new Vector3(massPoints[i].RestPosition.x, massPoints[i].RestPosition.y, massPoints[i - 1].RestPosition.z) - massPoints[i].RestPosition;
                if (common5 == Vector3.zero) common5 = saveDivBy0;
                float mag5 = Vector3.Magnitude(common5);
                Vector3 restLength5 = new Vector3(MassStructuralSprings[i].RestLength, MassStructuralSprings[i].RestLength, MassStructuralSprings[i - 1].RestLength);
                Vector3 magVector5 = new Vector3(mag5, mag5, mag5);
                forceTerm5 = Vector3.Scale(Vector3.Scale(constant5, (magVector5 - restLength5)), (common5 / mag5));

                Vector3 constant6 = new Vector3(MassStructuralSprings[i].Stiffness, MassStructuralSprings[i].Stiffness, MassStructuralSprings[i + 1].Stiffness);
                Vector3 common6 = new Vector3(massPoints[i].RestPosition.x, massPoints[i].RestPosition.y, massPoints[i + 1].RestPosition.z) - massPoints[i].RestPosition;
                if (common6 == Vector3.zero) common6 = saveDivBy0;
                float mag6 = Vector3.Magnitude(common6);
                Vector3 restLength6 = new Vector3(MassStructuralSprings[i].RestLength, MassStructuralSprings[i].RestLength, MassStructuralSprings[i + 1].RestLength);
                Vector3 magVector6 = new Vector3(mag6, mag6, mag6);
                forceTerm6 = Vector3.Scale(Vector3.Scale(constant6, (magVector6 - restLength6)), (common6 / mag6));

                Vector3 sumOfAdjacentMassPoints = forceTerm1 + forceTerm2 + forceTerm3 + forceTerm4 + forceTerm5 + forceTerm6;

                massPoints[i].Force = sumOfAdjacentMassPoints + massPoints[i].Force;

                massPoints[i].Update(Time.fixedDeltaTime);
                vertices[i] = massPoints[i].RestPosition;
            }*/

            for (int i = 0; i < massSprings.Length; i++)
            {
                vertices[i] = massSprings[i].UpdateSpringMassPoint(Time.fixedDeltaTime);
                if (i == massSprings.Length - 1)
                {
                    vertices[i] = massSprings[i].EndMassPoint.RestPosition;
                }
            }


            //the clear will resize the box of the mesh collider stuffs
            //softbodyMesh.Clear();
            softbodyMesh = gameObject.GetComponent<MeshFilter>().mesh;
            texUVs = softbodyMesh.uv;
            triangles = softbodyMesh.triangles;
            softbodyMesh.vertices = vertices;
            //softbodyMesh.uv = texUVs;
            //softbodyMesh.triangles = triangles;
            softbodyMesh.RecalculateNormals();
            softbodyMesh.RecalculateBounds();
            //gameObjectCopy.GetComponent<MeshFilter>().mesh = softbodyMesh;
        }

        /// <summary>
        /// Populates the MassPoints and Springs of a soft-body GameObject.
        /// </summary>
        private void GiveMeshAMassSpringSystem()
        {
            massPoints = new MassPoint[vertices.Length];
            massSprings = new StructuralSpring[vertices.Length - 1];

            MessageHandler.Instance.SendMessage(new LogMessage(string.Format("Number of triangles for {0}: {1}.", gameObject.name, triangles.Length/3)));
            if(PrintInfo)
            {
                for (int i=0; i < triangles.Length; i+=3)
                {
                    MessageHandler.Instance.SendMessage(new LogMessage(string.Format("Triangle: {0}, {1}, {2}.", triangles[i], triangles[i+1], triangles[i+2])));
                }
            }

            ISoftbodyPhysicsGameObject sb = transform.root.GetComponent<ISoftbodyPhysicsGameObject>();
            //Iterate the vertices and add a new MassPoint for each one
            for(int i = 0; i < massPoints.Length; i++)
            {
                massPoints[i] = new MassPoint(vertices[i], sb.Mass/massPoints.Length);
                if(PrintInfo) MessageHandler.Instance.SendMessage(new LogMessage(string.Format("New MassPoint at {0}", vertices[i])));
            }
            MessageHandler.Instance.SendMessage(new LogMessage(string.Format("Mass of {0} is: {1}, type: {2}.", sb.SoftbodyPhysicsObject.name, sb.Mass, sb.Type)));
            //Add the Springs for the mesh of the GameObject
            for(int i = 0; i < massSprings.Length; i++)
            {
                massSprings[i] = new StructuralSpring(massPoints[i], massPoints[i+1], 300.0f, 800.0f, 0.2f);
                //Draw debug lines for Spring connections between masspoints
                Debug.DrawLine(massSprings[i].StartMassPoint.RestPosition, massSprings[i].EndMassPoint.RestPosition, Color.yellow, 200.0f);
                if (PrintInfo) MessageHandler.Instance.SendMessage(new LogMessage(string.Format("New spring between {0} and {1}", massPoints.ElementAt(i).RestPosition, massPoints.ElementAt(i + 1).RestPosition)));
            }
            MessageHandler.Instance.SendMessage(new LogMessage(string.Format("GameObject {0} has {1} MassPoints and {2} Springs attached to it.", gameObject.name, massPoints.Length, massSprings.Length)));
        }

        /// <summary>
        /// Provides a means of extracting relevant mesh data from a GameObject.
        /// </summary>
        /// <param name="softbodyObject">The GameObject from which mesh data is extracted.</param>
        private void CopyMeshDataLocally()
        {
            softbodyMesh = gameObject.GetComponent<MeshFilter>().mesh;
            vertices = softbodyMesh.vertices;
            normals = softbodyMesh.normals;
            texUVs = softbodyMesh.uv;
            triangles = softbodyMesh.triangles;
        }

        static Vector3 NextVector()
        {
            Random random = new Random();
            double mantissa = (random.NextDouble() * 2.0) - 1.0;
            double exponent = Math.Pow(2.0, random.Next(-3, 3));
            float n = (float)(mantissa * exponent);
            return new Vector3(n, n, n);
        }

        static float NextFloat()
        {
            Random random = new Random();
            double mantissa = (random.NextDouble() * 2.0) - 1.0;
            double exponent = Math.Pow(2.0, random.Next(-8, 8));
            return (float)(mantissa * exponent);
        }


        /// <summary>
        /// Get the local copy of the Softbody Physics GameObject version of a Mesh.
        /// </summary>
        public Mesh SoftbodyMesh
        {
            get { return softbodyMesh; }
        }
    }
}
