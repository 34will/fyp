﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonFramework.Logging;
using CommonFramework.MessageSystem;
using UnityEngine;

namespace CommonFramework.SoftBodyPhysics
{
    public class MassPoint
    {
        private Vector3 restPosition = Vector3.zero;
        private Vector3 velocity = Vector3.zero;
        private Vector3 accel = Vector3.zero;
        private Vector3 force = Vector3.zero;
        private float mass = 0.0f;

        /// <summary>
        /// Instantiate a MassPoint with a rest position and a zeroed mass.
        /// </summary>
        /// <param name="restPosition">The rest position of the MassPoint.</param>
        public MassPoint(Vector3 restPosition)
        {
            this.restPosition = restPosition;
        }

        /// <summary>
        /// Instantiate a MassPoint with a position and a mass.
        /// </summary>
        /// 
        /// <param name="restPosition">The rest position of the MassPoint.</param>
        /// <param name="mass">The mass of the MassPoint</param>
        public MassPoint(Vector3 restPosition, float mass)
        {
            this.restPosition = restPosition;
            this.mass = mass;
        }

        /// <summary>
        /// Update the Accel, Velocity and RestPositions of a MassPoint.
        /// </summary>
        /// <param name="time">The timestep of the update.</param>
        public void Update(float time)
        {
            accel = force / mass;
            velocity += accel * time;
            restPosition += velocity * time;
            //turn force reset off to simulate more of a jellylike substance with springiness
            //force = Vector3.zero;
        }

        public void ResetForces()
        {
            accel = Vector3.zero;
            velocity = Vector3.zero;
            force = Vector3.zero;
        }

        //Properties
        /// <summary>
        /// Get the rest position of the MassPoint.
        /// </summary>
        public Vector3 RestPosition
        {
            get { return restPosition; }
            set { restPosition = value; }
        }

        /// <summary>
        /// Get the Velocity of a MassPoint.
        /// </summary>
        public Vector3 Velocity
        {
            get { return velocity; }
            set { velocity = value; }
        }

        /// <summary>
        /// Get the Accel of a MassPoint.
        /// </summary>
        public Vector3 Accel
        {
            get { return accel; }
            set { accel = value; }
        }

        /// <summary>
        /// Get the Mass of a MassPoint.
        /// </summary>
        public float Mass
        {
            get { return mass; }
            set { mass = value; }
        }

        /// <summary>
        /// Get the Force acting on a MassPoint.
        /// </summary>
        public Vector3 Force
        {
            get { return force; }
            set { force = value; }
        }

    }
}
