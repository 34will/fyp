﻿
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using UnityEngine;
using CommonFramework.CameraControl;
using CommonFramework.MessageSystem;
using CommonFramework.Logging;
using CommonFramework.Environment;

using SoftbodyHelper = CommonFramework.SoftBodyPhysics.SoftbodyGameObjectHelper;

namespace CommonFramework.SoftBodyPhysics
{
    [ExcludeFromCodeCoverage]
    public class SoftbodyPhysicsManager : MonoBehaviour, IMessageable
    {
        internal const string messageableName = "SoftbodyPhysicsManager";
        private Queue<IMessage> pendingMessages = new Queue<IMessage>();
        public List<GameObject> softbodyGameObjects = new List<GameObject>();
        IPhysicsGameObjectFactory sbFactory = new SoftBodyGameObjectFactory();
        private GameObject fakeMouseObject = null;

        /// <summary>
        /// Register the SoftbodyPhysicsManager as Messageable.
        /// </summary>
        void Awake()
        {
            MessageHandler.Instance.RegisterMessageable(this);
        }

        /// <summary>
        /// Called at Instantiation as part of the UnityEngine's MonoBehaviour interface.
        /// </summary>
        void Start()
        {
            /*AssetMessage bmwi3 = new AssetMessage("hd_cube", "Rigid Body Cube", new Vector3(1098, 50, 611), Quaternion.Euler(0.0f, 0.0f, 0.0f), new Vector3(0.2f, 0.2f, 0.2f));
            MessageHandler.Instance.SendMessage(bmwi3, EnvironmentManager.messageableName);
            GameObject car = bmwi3.GameObjectInstance;
            sbFactory.CreatePhysicsGameObject(car, RigidbodySoftbodyGameObject.objectType, 500.0f);

            //Objects like monkey, bmw_i3 and Sphere do not have masspoints connected to more than 1 spring so disappear, so sort out the spring connections! PRIO!!
            AssetMessage softBodyBarrelAssetMessage = new AssetMessage("simple_car", "Soft Body Door", new Vector3(1098, 50, 811), Quaternion.Euler(0.0f, 0.0f, 0.0f), new Vector3(15, 15, 15));
            MessageHandler.Instance.SendMessage(softBodyBarrelAssetMessage, EnvironmentManager.messageableName);
            GameObject softBodyBarrel = softBodyBarrelAssetMessage.GameObjectInstance;
            sbFactory.CreatePhysicsGameObject(softBodyBarrel, SoftbodyGameObject.objectType, 1500.0f);

            AssetMessage softBodyMessage = new AssetMessage("simple_car", "Soft Body Simple Car", new Vector3(1498, 50, 811), Quaternion.Euler(0.0f, 0.0f, 0.0f), new Vector3(15, 15, 15));
            MessageHandler.Instance.SendMessage(softBodyMessage, EnvironmentManager.messageableName);
            GameObject softBody = softBodyMessage.GameObjectInstance;
            sbFactory.CreatePhysicsGameObject(softBody, RigidbodySoftbodyGameObject.objectType, 10500.0f);

            //SoftbodyHelper.AssignAllGameObjectsToUndergoMeshManipulation();
            //Fill soft body object list
            softbodyGameObjects.Add(car);
            softbodyGameObjects.Add(softBodyBarrel);
            softbodyGameObjects.Add(softBody);

            MessageHandler.Instance.SendMessage(new Attach3rdPersonToObjectMessage(car.name, true));*/

            //SoftbodyHelper.AssignAllGameObjectsToUndergoMeshManipulationExcludingSome(softbodyGameObjects, false, car, softBodyBarrel);

            //Fake mouse game object initialisation
            GameObjectMessage mouseObjectMsg = new GameObjectMessage(new GameObject("Mouse object"));
            MessageHandler.Instance.SendMessage(mouseObjectMsg, EnvironmentManager.messageableName);
            fakeMouseObject = mouseObjectMsg.GameObject;
            SoftbodyHelper.AssignObjectRigidBody(fakeMouseObject);
        }

        /// <summary>
        /// Update method called once per frame inherited from UnityEngine's MonoBehaviour interface and is used to update the MeshManipulators.
        /// </summary>
        void Update()
        {
            //Handle mouse collision with false update on position of closest vertex of click ray on mesh
            if (Input.GetMouseButtonDown(0)) //left mouse to give oscillation through velocity manipulation
            {
                Camera c = GameObject.FindObjectOfType<Camera>();
                if (c.GetComponent<ThirdPersonCamera>() != null)
                {
                    Ray ray = c.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;
                    if (Physics.Raycast(ray, out hit))
                    {
                        MessageHandler.Instance.SendMessage(new LogMessage(string.Format("Mouse collision with  {0}, Position hit: {1}", hit.transform.name, hit.point)));
                        int index = SoftbodyCollisionHelper.DetectClosestVertexOnMeshGivenCollisionPoint(hit.transform.gameObject, hit.point);
                        if (index >= 0)
                        {
                            Vector3 forceExertedByMouse = NextVector();
                            hit.transform.gameObject.GetComponent<MeshManipulator>().massPoints.ElementAt(index).Velocity += forceExertedByMouse.normalized*150;
                            hit.transform.gameObject.GetComponent<MeshManipulator>().massPoints.ElementAt(index).Update(Time.fixedDeltaTime);
                            //hit.transform.gameObject.GetComponent<MeshManipulator>().massPoints.ElementAt(index).ResetForces();
                            if (hit.transform.gameObject.GetComponent<Rigidbody>() == null) SoftbodyCollisionHelper.ResizeGameObjectCollider(hit.collider.transform.gameObject, false);
                        }
                    }
                }
            }
            else if (Input.GetMouseButtonDown(1)) //right mouse to add force
            {
                Camera c = GameObject.FindObjectOfType<Camera>();
                if (c.GetComponent<ThirdPersonCamera>() != null)
                {
                    Ray ray = c.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;
                    if (Physics.Raycast(ray, out hit))
                    {
                        MessageHandler.Instance.SendMessage(new LogMessage(string.Format("Mouse collision with  {0}, Position hit: {1}", hit.transform.name, hit.point)));
                        int index = SoftbodyCollisionHelper.DetectClosestVertexOnMeshGivenCollisionPoint(hit.transform.gameObject, hit.point);
                        if (index >= 0)
                        {
                            Vector3 forceExertedByMouse = NextVector();
                            hit.transform.gameObject.GetComponent<MeshManipulator>().massPoints.ElementAt(index).Force += forceExertedByMouse;
                            hit.transform.gameObject.GetComponent<MeshManipulator>().massPoints.ElementAt(index).Update(Time.fixedDeltaTime);
                            //hit.transform.gameObject.GetComponent<MeshManipulator>().massPoints.ElementAt(index).ResetForces();
                            if (hit.transform.gameObject.GetComponent<Rigidbody>() == null) SoftbodyCollisionHelper.ResizeGameObjectCollider(hit.collider.transform.gameObject, false);
                        }
                    }
                }
            }

            //remove the code below
            //softbodyGameObjects.ElementAt(softbodyGameObjects.Count-2).transform.position += new Vector3(0.4f, 0, 0);
            //softbodyGameObjects.ElementAt(softbodyGameObjects.Count-1).transform.position += new Vector3(-0.4f, 0, 0);
        }

        /// <summary>
        /// Get a 'random' Vector3.
        /// </summary>
        /// <returns> The 'random' Vector3.</returns>
        static Vector3 NextVector()
        {
            System.Random random = new System.Random();
            double mantissa = (random.NextDouble() * 2.0) - 1.0;
            double exponent = Math.Pow(2.0, random.Next(-8, 8));
            float n = (float)(mantissa * exponent);
            return new Vector3(n, n, n);
        }
        /// <summary>
        /// Informs the IMessagable that it should handle and process the passed IMessage.
        /// </summary>
        /// <param name="message">The IMessage that the IMessagable should handle.</param>
        void IMessageable.ReceiveMessage(IMessage message)
        {
            //Asset messages read in from the map loading are automatically made soft body objects
            if (message.MessageName == AssetMessage.messageName)
            {
                // Convert the IMessage to an AssetMessage
                AssetMessage assetMessage = message as AssetMessage;
                if (assetMessage == null) return;
                softbodyGameObjects.Add(assetMessage.GameObjectInstance);
            }
            else if (message.MessageName == SoftbodyPhysicsMessage.messageName)
            {
                // Convert the IMessage to an AssetMessage
                SoftbodyPhysicsMessage sbpMessage = message as SoftbodyPhysicsMessage;
                if (sbpMessage == null) return;
                AssetMessage assetMessage = new AssetMessage(sbpMessage.Resource, sbpMessage.Identifier, sbpMessage.Position, sbpMessage.Rotation, sbpMessage.Scale);
                MessageHandler.Instance.SendMessage(assetMessage, EnvironmentManager.messageableName);
                sbFactory.CreatePhysicsGameObject(assetMessage.GameObjectInstance, sbpMessage.Type, sbpMessage.Mass);
                softbodyGameObjects.Add(assetMessage.GameObjectInstance);
                if (softbodyGameObjects.Count == 1) MessageHandler.Instance.SendMessage(new Attach3rdPersonToObjectMessage(assetMessage.GameObjectInstance.name, true));
            }
        }

        // ----- Properties ----- //

        /// <summary>
        /// Gets a Queue of the IMessages that this IMessagable wants to dispatch.
        /// </summary>
        Queue<IMessage> IMessageable.PendingMessages 
        {
            get { return pendingMessages; } 
        }

        /// <summary>
        /// Gets the identifier to use when addressing the IMessageable.
        /// </summary>
        public string MessageableName 
        { 
            get { return messageableName; }
        }
    }
}
