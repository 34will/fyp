﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace CommonFramework.SoftBodyPhysics
{
    public interface ISpring
    {
        /// <summary>
        /// Update the attributes contained within the concrete Spring implemention of ISpring.
        /// </summary>
        void UpdateSpring(float time);

        /// <summary>
        /// Update the positions of the StartMassPoint of the ISpring.
        /// </summary>
        /// <param name="time">The new position of the StartMassPoint.</param>
        /// <returns></returns>
        Vector3 UpdateSpringMassPoint(float time);

        //Properties

        /// <summary>
        /// Get the first MassPoint the ISpring is connected to.
        /// </summary>
        MassPoint StartMassPoint { get; }

        /// <summary>
        /// Get the MassPoint at the latter end of the ISpring.
        /// </summary>
        MassPoint EndMassPoint { get; }

        /// <summary>
        /// Get the RestLength of the ISpring.
        /// </summary>
        float RestLength { get; }

        /// <summary>
        /// Get the Stiffness of the ISpring.
        /// </summary>
        float Stiffness { get; }

        /// <summary>
        /// Get the force exerted by the ISpring.
        /// </summary>
        Vector3 ExertedSpringForce { get; }
    }
}
