﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using CommonFramework.SoftBodyPhysics;
using UnityEngine;
using Object = System.Object;
using CommonFramework.MessageSystem;
using CommonFramework.Logging;

namespace CommonFramework.SoftBodyPhysics
{
    [ExcludeFromCodeCoverage]
    public class SoftbodyCollision : MonoBehaviour
    {
        private Vector3 lastPointOfContact = Vector3.zero;
        private Vector3 lastNormalOfContact = Vector3.zero;
        private const int debugRayMultiplier = 10;
        private const float minDrag = 99999.0f;
        private const float debugRayDuration = 20.0f;
        private const bool printCollisionInfo = false;

        /// <summary>
        /// Reset the Position, Rotation, Velocity and AngularVelocity of an object.
        /// </summary>
        void Awake()
        {
            FreezePosition(gameObject);
            FreezeRotation(gameObject);
            FreezeVelocity(gameObject);
            FreezeAngularVelocity(gameObject);
        }

        /// <summary>
        /// When first colliding with another object, detect the closest point on the mesh from where the Colliders collided (might not be MeshCollider).
        /// </summary>
        /// <param name="c"></param>
        void OnCollisionEnter(Collision c)
        {
            int index = 0;
            int index2 = 0;
            foreach (ContactPoint contactPoint in c.contacts)
            {
                index = SoftbodyCollisionHelper.DetectClosestVertexOnMeshGivenCollisionPoint(c.gameObject, contactPoint.point);
                index2 = SoftbodyCollisionHelper.DetectClosestVertexOnMeshGivenCollisionPoint(gameObject, contactPoint.point);
                if (c.gameObject.GetComponent<Rigidbody>() != null)
                {
                    c.gameObject.GetComponent<MeshManipulator>().massPoints.ElementAt(index).Force = Vector3.zero;
                    gameObject.GetComponent<MeshManipulator>().massPoints.ElementAt(index2).Force = Vector3.zero;
                    c.gameObject.GetComponent<MeshManipulator>().massPoints.ElementAt(index).Force += (gameObject.GetComponent<MeshManipulator>().massPoints.ElementAt(index2).RestPosition / gameObject.GetComponent<MeshManipulator>().massPoints.ElementAt(index2).RestPosition.magnitude) * 300;
                    //c.gameObject.GetComponent<MeshManipulator>().massPoints.ElementAt(index).Velocity += gameObject.GetComponent<MeshManipulator>().massPoints.ElementAt(index2).Velocity;
                    c.gameObject.GetComponent<MeshManipulator>().massPoints.ElementAt(index).Update(Time.fixedDeltaTime);
                    gameObject.GetComponent<MeshManipulator>().massPoints.ElementAt(index2).Force += -c.gameObject.GetComponent<MeshManipulator>().massPoints.ElementAt(index).Force;
                    //gameObject.GetComponent<MeshManipulator>().massPoints.ElementAt(index2).Velocity += -c.gameObject.GetComponent<MeshManipulator>().massPoints.ElementAt(index).Velocity;
                    gameObject.GetComponent<MeshManipulator>().massPoints.ElementAt(index2).Update(Time.fixedDeltaTime);
                }
            }
            //if (c.gameObject.GetComponent<Rigidbody>() == null) c.gameObject.GetComponent<MeshManipulator>().InternalUpdate();
            if (c.gameObject.GetComponent<Rigidbody>() == null) SoftbodyCollisionHelper.ResizeGameObjectCollider(c.gameObject, false);
            FreezePosition(c.gameObject);
            FreezeRotation(c.gameObject);
            FreezeVelocity(c.gameObject);
            FreezeAngularVelocity(c.gameObject);
        }

        void OnCollisionStay(Collision c)
        {
            if (printCollisionInfo)
            {
                foreach(ContactPoint contactPoint in c.contacts)
                {
                    Debug.DrawRay(contactPoint.point, contactPoint.normal * debugRayMultiplier, Color.magenta, debugRayDuration, false);
                }
                lastPointOfContact = c.gameObject.transform.position;
                lastNormalOfContact = c.gameObject.transform.up;
            }
        }

        void OnCollisionExit(Collision c)
        {
            if (printCollisionInfo) Debug.DrawRay(lastPointOfContact, lastNormalOfContact * debugRayMultiplier, Color.red, debugRayDuration, false);
            //if (c.gameObject.GetComponent<Rigidbody>() == null) c.gameObject.GetComponent<MeshManipulator>().InternalUpdate();
            //if (c.gameObject.GetComponent<Rigidbody>() == null) GameObjectHelper.ResizeGameObjectMeshCollider(c.gameObject, false);
        }

        /// <summary>
        /// Freezes the Rotation of a given Gameobject providing it is a soft-body.
        /// </summary>
        /// <param name="gameobject">The GameObject to have it's Rotation frozen.</param>
        private void FreezeRotation(GameObject gameobject)
        {
            if (gameobject.GetComponent<Rigidbody>() != null)
            {
                gameobject.GetComponent<Rigidbody>().freezeRotation = true;
            }
        }

        /// <summary>
        /// Freezes the Position of a given GameObject providing it is a soft-body.
        /// </summary>
        /// <param name="gameobject">The GameObject to have it's Position frozen.</param>
        private void FreezePosition(GameObject gameobject)
        {
            if (gameobject.GetComponent<Rigidbody>() != null)
            {
                gameobject.GetComponent<Rigidbody>().MovePosition(gameobject.transform.position);
            }
        }

        /// <summary>
        /// Freezes the Velocity of a given GameObject providing it is a soft-body.
        /// </summary>
        /// <param name="gameobject">The GameObject to have it's Velocity frozen.</param>
        private void FreezeVelocity(GameObject gameobject)
        {
            if (gameobject.GetComponent<Rigidbody>() != null)
            {
                gameobject.GetComponent<Rigidbody>().drag = minDrag;
                gameobject.GetComponent<Rigidbody>().velocity = Vector3.zero;
            }
        }

        /// <summary>
        /// Freezes the Angular Velocity of a given GameObject providing it is a soft-body.
        /// </summary>
        /// <param name="gameobject">The GameObject to have it's Angular Velocity frozen.</param>
        private void FreezeAngularVelocity(GameObject gameobject)
        {
            if (gameobject.GetComponent<Rigidbody>() != null)
            {
                gameobject.GetComponent<Rigidbody>().angularDrag = minDrag;
                gameobject.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            }
        }
    }
}