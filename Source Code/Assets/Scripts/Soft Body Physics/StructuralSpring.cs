﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Schema;
using UnityEngine;

namespace CommonFramework.SoftBodyPhysics
{
    public class StructuralSpring : ISpring
    {
        private MassPoint startMassPoint = null;
        private MassPoint endMassPoint = null;
        private float restLength = 0.0f;
        private float stiffness = 0.0f;
        private float damping = 0.0f;
        private float threshold = 0.0f;
        private Vector3 springForce = Vector3.zero;

        /// <summary>
        /// Instantiation of a Structural Spring parameterised by two MassPoint structures and the stiffness of spring expressed as a float.
        /// </summary>
        /// <param name="startMassPoint"></param>
        /// <param name="endMassPoint"></param>
        public StructuralSpring(MassPoint startMassPoint, MassPoint endMassPoint, float stiffness, float damping, float threshold)
        {
            this.startMassPoint = startMassPoint;
            this.endMassPoint = endMassPoint;
            this.stiffness = stiffness;
            this.damping = damping;
            this.threshold = threshold;
            restLength = Vector3.Magnitude(endMassPoint.RestPosition - startMassPoint.RestPosition);
        }

        /// <summary>
        /// Update the forces exerted by the Structural Spring.
        /// </summary>
        /// <param name="time">The timestep to apply the update.</param>
        public void UpdateSpring(float time)
        {
            Vector3 posDiff = endMassPoint.RestPosition - startMassPoint.RestPosition;
            float currLength = Vector3.Magnitude(posDiff);
            if (currLength - restLength > threshold)
            {
                posDiff = Vector3.zero;
                //if currentlength is zero, results in missing masspoints
                startMassPoint.Velocity *= 0.8f;
                endMassPoint.Velocity *= 0.8f;
            }
            //Oscillate object
            //Harmonic oscillation calculation
            springForce = stiffness * (currLength - restLength) * (posDiff / currLength);
            //dampen force
            Vector3 velDiff = endMassPoint.Velocity - startMassPoint.Velocity;
            springForce += damping * velDiff;

            //Update the MassPoints attached to the spring
            //UpdateSpringMassPoint(time);
        }

        /// <summary>
        /// Update the Forces acting on each MassPoint attached at either end of the Structural Spring.
        /// Update the MassPoints internally.
        /// </summary>
        /// <param name="time">The timestep to apply the update.</param>
        /// <returns>The new position of the StartMassPoint.</returns>
        public Vector3 UpdateSpringMassPoint(float time)
        {
            startMassPoint.Force += springForce;
            endMassPoint.Force += -springForce;

            //startMassPoint.Update(time);
            //endMassPoint.Update(time);

            //restLength = Vector3.Magnitude(endMassPoint.RestPosition - startMassPoint.RestPosition);

            return startMassPoint.RestPosition;
        }

        //Properties

        /// <summary>
        /// Get the first MassPoint the Structural Spring is connected to.
        /// </summary>
        public MassPoint StartMassPoint
        {
            get { return startMassPoint; }
        }

        /// <summary>
        /// Get the MassPoint at the latter end of the Structural Spring.
        /// </summary>
        public MassPoint EndMassPoint
        {
            get { return endMassPoint; }
        }

        /// <summary>
        /// Get the RestLength of the Structural Spring.
        /// </summary>
        public float RestLength
        {
            get { return restLength; }
        }

        /// <summary>
        /// Get the Stiffness of the Structural Spring.
        /// </summary>
        public float Stiffness
        {
            get { return stiffness; }
        }

        /// <summary>
        /// Get the force exerted by the Structural Spring.
        /// </summary>
        public Vector3 ExertedSpringForce
        {
            get { return springForce; }
        }
    }
}
