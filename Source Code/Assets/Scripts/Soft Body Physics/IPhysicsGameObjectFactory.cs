﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace CommonFramework.SoftBodyPhysics
{
    public interface IPhysicsGameObjectFactory
    {
        /// <summary>
        /// Defines the factory method concrete physics implementations should create.
        /// </summary>
        /// <param name="gameObject">The GameObject to be attributed with a physics object type.</param>
        /// <returns>The GameObject post-allocation of a physics object type.</returns>
        void CreatePhysicsGameObject(GameObject gameObject, string softbodyObjectType, float mass);
    }
}
