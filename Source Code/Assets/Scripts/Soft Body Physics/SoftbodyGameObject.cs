﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using SoftbodyHelper = CommonFramework.SoftBodyPhysics.SoftbodyGameObjectHelper;

namespace CommonFramework.SoftBodyPhysics
{
    public class SoftbodyGameObject : MonoBehaviour, ISoftbodyPhysicsGameObject
    {
        private GameObject softbodyGameObject = null;
        private Vector3 accel = Vector3.zero;
        private Vector3 velocity = Vector3.zero;
        private Vector3 position = Vector3.zero;
        public float mass = 22.0f;
        internal const string objectType = "softbodyGameObject";

        /// <summary>
        /// Construct a Softbody Physics GameObject by adding the Softbody requirements to a GameObject.
        /// </summary>
        /// <param name="gameObject">The GameObject to turn into a Softbody Physics GameObject.</param>
        /// <param name="mass">The Mass of the GameObject.</param>
        void Awake()
        {
            softbodyGameObject = gameObject;

            //Add what defines the SoftbodyGameObject
            List<GameObject> softBodyChildObjects = GameObjectHelper.GetAllChildObjectsFromObjectTree(softbodyGameObject);
            SoftbodyHelper.AddListOfGameObjectsToMeshManipulator(softBodyChildObjects);
        }

        // Properties

        /// <summary>
        /// Get the SoftbodyPhysicsObject copy of the appended GameObject.
        /// </summary>
        public GameObject SoftbodyPhysicsObject
        {
            get { return softbodyGameObject; }
        }

        /// <summary>
        /// Get the Accel of the appended GameObject.
        /// </summary>
        public Vector3 Accel
        {
            get { return accel; }
        }

        /// <summary>
        /// Get the Velocity of the appended GameObject.
        /// </summary>
        public Vector3 Velocity
        {
            get { return velocity; }
            set { velocity = value; }
        }

        /// <summary>
        /// Get the Position of the appended GameObject.
        /// </summary>
        public Vector3 Position
        {
            get { return position; }
            set { position = value; }
        }

        /// <summary>
        /// Get the Mass of the appended GameObject.
        /// </summary>
        public float Mass
        {
            get { return mass; }
            set { mass = value; }
        }

        /// <summary>
        /// Get the Type of the appended GameObject.
        /// </summary>
        public string Type
        {
            get { return objectType; }
        }
    }
}
