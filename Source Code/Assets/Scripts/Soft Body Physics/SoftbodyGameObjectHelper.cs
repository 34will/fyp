﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using CommonFramework.Logging;
using CommonFramework.MessageSystem;
using UnityEngine;

namespace CommonFramework.SoftBodyPhysics
{
    public static class SoftbodyGameObjectHelper
    {

        /// <summary>
        /// Adds a list of GameObjects to the MeshManipulator structure so the meshes of the objects can experience deformation.
        /// </summary>
        /// <param name="gameObjects">The list of GameObjects to be added to the MeshManipulator structure.</param>
        public static void AddListOfGameObjectsToMeshManipulator(List<GameObject> gameObjects)
        {
            foreach (GameObject softbodyGameObject in gameObjects)
            {
                //Check a Camera isn't add to meshmanipulator since it will throw an error
                if (softbodyGameObject.GetType() != typeof(Camera))
                {
                    EnsureGameObjectHasAMeshRenderer(softbodyGameObject);
                    EnsureGameObjectHasAMeshFilter(softbodyGameObject);
                    EnsureGameObjectHasAppropriateCollider(softbodyGameObject);
                    softbodyGameObject.AddComponent<MeshManipulator>();
                }
            }
        }

        /// <summary>
        /// Assigns a single GameObject to have it's mesh manipulated by the MeshManipulator.
        /// </summary>
        /// <param name="softbodyGameObject">The GameObject to undergo mesh manipulation.</param>
        public static void AssignAGameObjectToUndergoMeshManipulation(GameObject softbodyGameObject)
        {
            EnsureGameObjectHasAMeshFilter(softbodyGameObject);
            EnsureGameObjectHasAMeshRenderer(softbodyGameObject);
            EnsureGameObjectHasAppropriateCollider(softbodyGameObject);
            softbodyGameObject.AddComponent<MeshManipulator>();
        }

        /// <summary>
        /// All GameObjects are assigned to have their meshes manipulated by the MeshManipulator.
        /// </summary>
        public static void AssignAllGameObjectsToUndergoMeshManipulation()
        {
            foreach (GameObject softbodyGameObject in GameObject.FindObjectsOfType(typeof(GameObject)))
            {
                EnsureGameObjectHasAMeshFilter(softbodyGameObject);
                EnsureGameObjectHasAMeshRenderer(softbodyGameObject);
                EnsureGameObjectHasAppropriateCollider(softbodyGameObject);
                softbodyGameObject.AddComponent<MeshManipulator>();
            }
        }
        
        /// <summary>
        /// Assign a list of GameObjects to undergo mesh manipulation but exclude the parameterised GameObjects.
        /// </summary>
        /// <param name="assets">the list of GameObjects to undergo mesh manipulation.</param>
        /// <param name="addRigidBodyComponent">Whether or not to give the GameObjects a Rigidbody component.</param>
        /// <param name="args">The GameObjects to exclude from mesh manipulation.</param>
        public static void AssignAllGameObjectsToUndergoMeshManipulationExcludingSome(List<GameObject> assets, bool addRigidBodyComponent = false, params GameObject[] args)
        {
            bool objectCanBeAdded = true;

            foreach (GameObject softbodyGameObject in assets)
            {
                foreach (GameObject arg in args)
                {
                    if (softbodyGameObject == arg)
                    {
                        objectCanBeAdded = false;
                        break;
                    }
                }

                if (objectCanBeAdded)
                {
                    if (addRigidBodyComponent) AssignObjectRigidBody(softbodyGameObject);
                    EnsureGameObjectHasAMeshFilter(softbodyGameObject);
                    EnsureGameObjectHasAMeshRenderer(softbodyGameObject);
                    EnsureGameObjectHasAppropriateCollider(softbodyGameObject);
                    softbodyGameObject.AddComponent<MeshManipulator>();
                }
                objectCanBeAdded = true;
            }
        }

        /// <summary>
        /// Assign a GameObject Softbody Physics compatible Rigidbody status.
        /// </summary>
        /// <param name="gameObject">The GameObject that needs assigning a compatible Softbody Physics Rigidbody components.</param>
        public static void AssignObjectRigidBody(GameObject gameObject)
        {
            gameObject.AddComponent<Rigidbody>();
            gameObject.GetComponent<Rigidbody>().useGravity = false;
            gameObject.GetComponent<Rigidbody>().isKinematic = false;
        }

        /// <summary>
        /// Assign each element in a list of GameObjects a Softbody Physics compatible Rigidbody status.
        /// </summary>
        /// <param name="gameObjects">The list of GameObjects, each of which needs assigning a compatible Softbody Physics Rigidbody components.</param>
        public static void AssignObjectsRigidBody(List<GameObject> gameObjects)
        {
            foreach(GameObject gameObject in gameObjects)
            {
                //Ensure the object isn't a Camera type GameObject (saves errors)
                if(gameObject.GetType() != typeof(Camera))
                {
                    gameObject.AddComponent<Rigidbody>();
                    gameObject.GetComponent<Rigidbody>().useGravity = false;
                    gameObject.GetComponent<Rigidbody>().isKinematic = false;                }
            }
        }

        /// <summary>
        /// If a GameObject doesn't contain a MeshFilter component it is given one.
        /// </summary>
        /// <param name="gameObject">The GameObject to be checked for a MeshFilter component.</param>
        private static void EnsureGameObjectHasAMeshFilter(GameObject gameObject)
        {
            if (gameObject.GetComponent<MeshFilter>() == null)
                gameObject.AddComponent<MeshFilter>();
        }

        /// <summary>
        /// If a GameObject doesn't contain a MeshRenderer component it is given one.
        /// </summary>
        /// <param name="gameObject">The GameObject to be checked for a MeshRenderer component.</param>
        private static void EnsureGameObjectHasAMeshRenderer(GameObject gameObject)
        {
            if (gameObject.GetComponent<MeshRenderer>() == null)
                gameObject.AddComponent<MeshRenderer>();
        }

        /// <summary>
        /// If a GameObject doesn't contain a MeshCollider component it is given one.
        /// </summary>
        /// <param name="gameObject">The GameObject to be checked for a MeshCollider component.</param>
        private static void EnsureGameObjectHasAppropriateCollider(GameObject gameObject)
        {
            //TODO Change the type of collider based on the number of polygons in a shape
            if (gameObject.GetComponent<MeshFilter>() != null)
            {
                if (gameObject.GetComponent<MeshFilter>().mesh.triangles.Length * 0.33333334 <= 255)
                {
                    gameObject.AddComponent<MeshCollider>();
                    gameObject.GetComponent<MeshCollider>().convex = true;
                    MessageHandler.Instance.SendMessage(new LogMessage(string.Format("Created MeshCollider for {0}", gameObject.name)));
                }
                else
                {
                    gameObject.AddComponent<BoxCollider>();
                    MessageHandler.Instance.SendMessage(new LogMessage(string.Format("Created BoxCollider for {0}", gameObject.name)));
                }
                gameObject.AddComponent<SoftbodyCollision>();
            }
        }
    }
}
