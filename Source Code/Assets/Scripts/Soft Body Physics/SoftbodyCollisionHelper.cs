﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonFramework.Logging;
using CommonFramework.MessageSystem;

using UnityEngine;

namespace CommonFramework.SoftBodyPhysics
{
    public static class SoftbodyCollisionHelper
    {
        /// <summary>
        /// Detects the closest point on a mesh relative to a collision/contact point and returns the index of the vertex.
        /// </summary>
        /// <param name="gameobject">GameObject to use to detect the point of a mesh.</param>
        /// <param name="contactPoint">The point of contact between two colliding objects.</param>
        /// <returns>return the index of the vertex closest to the collision point.</returns>
        public static int DetectClosestVertexOnMeshGivenCollisionPoint(GameObject gameobject, Vector3 contactPoint)
        {
            float closestPoint = float.MaxValue;
            float comparisonPoint = float.MaxValue;
            Vector3 diff = Vector3.zero;
            int index = 0;
            Vector3[] vertexList = gameobject.GetComponent<MeshFilter>().mesh.vertices;

            for (int i = 0; i < vertexList.Length; i++)
            {
                comparisonPoint = (contactPoint - gameobject.transform.TransformPoint(vertexList[i])).magnitude;
                if (comparisonPoint < closestPoint)
                {
                    closestPoint = comparisonPoint;
                    index = i;
                }
            }

            return index;
        }

        /// <summary>
        /// Resize the Collider of a GameObject.
        /// </summary>
        /// <param name="gameobject">The GameObject whose Collider needs resizing.</param>
        /// <param name="printCollisionInfo">To print or not to print information about the bound calculations of a GameObject pre and post resizing.</param>
        public static void ResizeGameObjectCollider(GameObject gameobject, bool printCollisionInfo)
        {
            //Destroy the BoxCollider if the GameObject contains one and reassign it.
            if (gameobject.GetComponent<BoxCollider>() != null)
            {
                if (printCollisionInfo) MessageHandler.Instance.SendMessage(new LogMessage(string.Format("BoxCollider Before bounds: {0}", gameobject.GetComponent<BoxCollider>().bounds.size)));
                UnityEngine.Object.Destroy(gameobject.GetComponent<BoxCollider>());
                gameobject.AddComponent<BoxCollider>();
                gameobject.GetComponent<BoxCollider>().size = gameobject.GetComponent<MeshRenderer>().bounds.size;

                //Add new collider
                if (gameobject.GetComponent<MeshFilter>() != null)
                {
                    if (printCollisionInfo) MessageHandler.Instance.SendMessage(new LogMessage(string.Format("BoxCollider After bounds: {0}", gameobject.GetComponent<BoxCollider>().bounds.size)));
                }
            }
            else if (gameobject.GetComponent<MeshCollider>() != null && gameobject.GetComponent<Rigidbody>() == null)
            {
                if (printCollisionInfo) MessageHandler.Instance.SendMessage(new LogMessage(string.Format("MeshCollider Before bounds: {0}", gameobject.GetComponent<MeshCollider>().bounds.size)));
                MeshCollider meshCollider = gameobject.GetComponent<MeshCollider>();
                //The meshcollider resets now, but it looked like it wasn't working because the InternalUpdate() method turned the object concave chucking errors!
                if (meshCollider == null) gameobject.AddComponent<MeshCollider>();
                else
                {
                    meshCollider.sharedMesh = null;
                    meshCollider.sharedMesh = gameobject.GetComponent<MeshFilter>().sharedMesh;
                }

                //Add new collider
                if (gameobject.GetComponent<MeshFilter>() != null)
                {
                    if (printCollisionInfo) MessageHandler.Instance.SendMessage(new LogMessage(string.Format("MeshCollider After bounds: {0}", gameobject.GetComponent<MeshCollider>().bounds.size)));
                }
            }
        }
    }
}
