﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonFramework.Logging;
using CommonFramework.MessageSystem;
using UnityEngine;

namespace CommonFramework.SoftBodyPhysics
{
    public static class GameObjectHelper
    {
        /// <summary>
        /// Obtains and returns a list of all child objects contained within a parent game object.
        /// </summary>
        /// <param name="rootGameObject">The parent game object used to find it's child game objects.</param>
        /// <returns>The list of all children of a root GameObject.</returns>
        public static List<GameObject> GetAllChildObjectsFromObjectTree(GameObject rootGameObject)
        {
            List<GameObject> childGameObjects = new List<GameObject>();
            Transform t = rootGameObject.transform;
            Transform childt = t;
            //if the object has no children just return it as a list
            if (t.childCount == 0)
            {
                childGameObjects.Add(rootGameObject);
                return childGameObjects;
            }
            //else process the hierarchy and return it as a list
            else
            {
                while (childt.childCount > 0 && childt != null)
                {
                    for (int i = 0; i < t.childCount; i++)
                    {
                        childt = t.GetChild(i);
                        MessageHandler.Instance.SendMessage(new LogMessage(string.Format("New GameObject found: \"{0}\"! has {1} children!!", childt.name, childt.childCount)));
                        childt.name += rootGameObject.name;
                        childGameObjects.Add(childt.gameObject);
                    }
                    t = childt;
                }
            }

            return childGameObjects;
        }
    }
}
