﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;

using UnityEngine;
using CommonFramework.MessageSystem;

namespace CommonFramework.SoftBodyPhysics
{
    [ExcludeFromCodeCoverage]
    /// <summary>
    /// SoftbodyPhysicsMessage defines the IMessage type containing relevant Soft-body Physics information.
    /// </summary>
    public class SoftbodyPhysicsMessage : IMessage
    {
        //Softbody message properties
        private IMessageable source = null;
        internal const string messageName = "SoftbodyPhysicsMessage";
        private GameObject softbodyObject = null;
        private float mass = 0.0f;
        private string type = "";

        //AssetMessageproperties
        private string resource = "";
        private string identifier = "";
        private Vector3 position = Vector3.zero;
        private Quaternion rotation = Quaternion.identity;
        private Vector3 scale = Vector3.one;


        /// <summary>
        /// Instantiates a SoftbodyPhysicsMessage.
        /// </summary>
        /// <param name="softbodyObject">The Soft-body GameObject.</param>
        public SoftbodyPhysicsMessage(string resource, string identifier, Vector3 position, Quaternion rotation, Vector3 scale, float mass, string type)
        {
            this.resource = resource;
            this.identifier = identifier;
            this.position = position;
            this.rotation = rotation;
            this.scale = scale;

            this.mass = mass;
            this.type = type;
        }

        /// <summary>
        /// Gets the IMessage type component, SoftbodyPhysicsMessage.
        /// </summary>
        public string MessageName
        {
            get { return messageName; }
        }

        /// <summary>
        /// Gets or Sets the IMessageable source/sender of a message.
        /// </summary>
        public IMessageable Source
        {
            get { return source; }
            set { source = value; }
        }

        /// <summary>
        /// Gets the Soft-body GameObject.
        /// </summary>
        public GameObject SoftBodyObject
        {
            get { return softbodyObject; }
        }

        /// <summary>
        /// Gets the external resource the softbody asset represents.
        /// </summary>
        public string Resource
        {
            get { return resource; }
        }

        /// <summary>
        /// Gets the identifier to be used with GameObject that will be instantiated for unique referencing.
        /// </summary>
        public string Identifier
        {
            get { return identifier; }
        }

        /// <summary>
        /// Gets the position of the softbody asset.
        /// </summary>
        public Vector3 Position
        {
            get { return position; }
        }

        /// <summary>
        /// Gets the Quanternion matrix that represents the rotation transform of the softbody asset.
        /// </summary>
        public Quaternion Rotation
        {
            get { return rotation; }
        }

        /// <summary>
        /// Gets the scale transform of the softbody asset.
        /// </summary>
        public Vector3 Scale
        {
            get { return scale; }
        }

        public string Type
        {
            get { return type; }
        }

        public float Mass
        {
            get { return mass; }
        }
    }
}
