﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace CommonFramework.SoftBodyPhysics
{
    public class SoftBodyGameObjectFactory : IPhysicsGameObjectFactory
    {
        /// <summary>
        /// Create a new instance of a type of Softbody Physics GameObject.
        /// </summary>
        /// <param name="gameObject">The GameObject that needs customising to a Softbody Physics object type.</param>
        /// <param name="softbodyObjectType">The type of the desired GameObject.</param>
        /// <param name="mass">The Mass of the GameObject.</param>
        /// <returns>The newly created instance of the desired type of Softbody Physics GameObject.</returns>
        public void CreatePhysicsGameObject(GameObject gameObject, string softbodyObjectType, float mass)
        {
            switch(softbodyObjectType)
            {
                case SoftbodyGameObject.objectType:
                    gameObject.AddComponent<SoftbodyGameObject>();
                    gameObject.GetComponent<SoftbodyGameObject>().Mass = mass;
                    break;
                case RigidbodySoftbodyGameObject.objectType:
                    gameObject.AddComponent<RigidbodySoftbodyGameObject>();
                    gameObject.GetComponent<RigidbodySoftbodyGameObject>().Mass = mass;
                    break;
                default:
                    break;
            }
        }
    }
}
