﻿using System;
using System.Collections;
using System.Xml;
using System.Xml.Linq;
using System.Diagnostics.CodeAnalysis;
using System.IO;

using UnityEngine;

using CommonFramework;
using CommonFramework.DataStream;
using CommonFramework.Environment;
using CommonFramework.MessageSystem;
using CommonFramework.CameraControl;
using CommonFramework.MapLoader;
using CommonFramework.SoftBodyPhysics;
using CommonFramework.Logging;

[ExcludeFromCodeCoverage]
public class ConfigurationLoader : MonoBehaviour
{
	private PacketTranscriberPump transcriberPump = null;
	private XmlDocument document = new XmlDocument();
	private XmlNode root = null;

    /// <summary>
    /// The configuration file that will be loaded into the application
    /// </summary>
    public string ConfigurationFilename = "configuration";

	void Start()
	{
		// Acquire the transcriber pump
		GameObject tpObject = GameObject.Find("PacketTranscriberPumpObject");
		if(tpObject != null)
			transcriberPump = tpObject.GetComponent<PacketTranscriberPump>();

        document = ResourceHelper.LoadXMLResource(ConfigurationFilename);
        if (document == null)
            return;

		root = document.FirstChild;
		if(root == null)
			return;

		// Determine the setting to load
		foreach(XmlNode node in root.ChildNodes)
		{
			switch(node.Name)
			{
				case "datasource":
					PrepareDataSource(node);
                    break;
                case "mapsource":
                    PrepareMapSource(node);
                    break;
                case "softbodysource":
                    PrepareSoftbodySource(node);
                    break;
			}
		}
	}

	private void PrepareDataSource(XmlNode node)
	{
		// Prepare the IListener for pump
		IListener listener = null;
		string mode = XMLHelper.ReadAttribute(node, "mode", "crud");
		switch(mode)
		{
			case "crud":
				string filename = XMLHelper.ReadAttribute(node, "filename");
				listener = new CRUDFileStreamListener(filename);
				break;
			case "udp":
				int port = XMLHelper.ReadAttribute<int>(node, "port");
				listener = new UDPListener(port);
				break;
            case "ros":
                listener = new ROSListener();
                break;
		}

		// Assign the listener to the transcriber pump
		if(transcriberPump != null)
			transcriberPump.Listener = listener;
	}

    private void PrepareMapSource(XmlNode node)
    {
        string mode = XMLHelper.ReadAttribute(node, "mode", "osm");
        switch (mode)
        {
            case "osm":
                string filename = XMLHelper.ReadAttribute(node, "filename");
                bool clip = XMLHelper.ReadAttribute<bool>(node, "clip", false);

                if (clip)
                {
                    float longmin = XMLHelper.ReadAttribute<float>(node, "lonmin");
                    float latmin = XMLHelper.ReadAttribute<float>(node, "latmin");
                    float longmax = XMLHelper.ReadAttribute<float>(node, "lonmax");
                    float latmax = XMLHelper.ReadAttribute<float>(node, "latmax");
                    OSMXmlParser.Instance.ParseClippedOSMFile(filename, longmin, latmin, longmax, latmax);
                }
                else
                    OSMXmlParser.Instance.ParseOSMFile(filename);

                break;
        }
    }

    private void PrepareSoftbodySource(XmlNode node)
    {
        string modelname = XMLHelper.ReadAttribute(node, "model");
        string modelid = XMLHelper.ReadAttribute(node, "modelid");
        string type = XMLHelper.ReadAttribute(node, "type");
        float mass = XMLHelper.ReadAttribute<float>(node, "mass");
        Vector3 position = Vector3.zero;
        Quaternion rotation = Quaternion.identity;
        Vector3 scale = Vector3.zero;
        float x = 0.0f;
        float y = 0.0f;
        float z = 0.0f;

        foreach(XmlNode childnode in node.ChildNodes)
        {
            switch (childnode.Name)
            {
                case "position":
                    x = XMLHelper.ReadAttribute<float>(childnode, "x");
                    y = XMLHelper.ReadAttribute<float>(childnode, "y");
                    z = XMLHelper.ReadAttribute<float>(childnode, "z");
                    position = new Vector3(x, y, z);
                    break;
                case "rotation":
                    x = XMLHelper.ReadAttribute<float>(childnode, "x");
                    y = XMLHelper.ReadAttribute<float>(childnode, "y");
                    z = XMLHelper.ReadAttribute<float>(childnode, "z");
                    rotation = Quaternion.Euler(x, y, z);
                    break;
                case "scale":
                    x = XMLHelper.ReadAttribute<float>(childnode, "x");
                    y = XMLHelper.ReadAttribute<float>(childnode, "y");
                    z = XMLHelper.ReadAttribute<float>(childnode, "z");
                    scale = new Vector3(x, y, z);
                    break;
            }
        }


        MessageHandler.Instance.SendMessage(new SoftbodyPhysicsMessage(modelname, modelid, position, rotation, scale, mass, type), SoftbodyPhysicsManager.messageableName);
    }

}
