﻿Shader "Custom/PointCloudShader"
{
	Properties
	{
      _SplatTex ("Texture", 2D) = "white" { }
	  _Fluff ("Fluff", Float) = 0.9
    }

	SubShader
	{
		Pass
		{
			Tags { "RenderType" = "Transparent" }
			
			Blend One One
			BlendOp Max
			ZWrite Off

			CGPROGRAM
			#pragma vertex VS
			#pragma geometry GS
			#pragma fragment PS
			#pragma target 3.0

			#include "UnityCG.cginc"

			// ----- Structures ----- //

			struct VS_Input
			{
				float4 vertex		: POSITION0;
				float4 tangent		: TANGENT0;
				float3 normal		: NORMAL0;
				float3 colour		: COLOR0;
			};

			struct GS_Input
			{
				float4 positionSV	: SV_POSITION0;
				float4 positionW	: POSITION1;
				float3 normal		: NORMAL0;
				float4 tangent		: TANGENT0;
				float3 colour		: COLOR0;
			};

			struct PS_Input
			{
				float4 positionSV	: SV_POSITION0;
				float3 positionW	: POSITION1;
				float3 normal		: NORMAL0;
				float2 texcoord		: TEXCOORD0;
				float3 colour		: COLOR0;
			};

			// ------ Variables ----- //

			sampler2D _SplatTex;
			float _Fluff;

			uniform float4 _LightColor0;

			// ----- Shaders ----- //

			GS_Input VS(VS_Input i)
			{
				GS_Input o;
				o.positionW = mul(_Object2World, i.vertex);
				o.positionSV = mul(UNITY_MATRIX_MVP, i.vertex);
				o.normal = i.normal;
				o.tangent = i.tangent.xyzw;
				o.colour = i.colour;
				return o;
			}

			[maxvertexcount(6)]
			void GS(point GS_Input vertex[1], inout TriangleStream<PS_Input> output)
			{
				GS_Input input = vertex[0];

				// Calculate spacers
				float radius = input.tangent.w;
				float3 tangent = input.tangent.xyz;		// Normalised offline
				float3 biNormal = cross(tangent, input.normal);

				// Set up vertices for quad output
				PS_Input a = (PS_Input)0;
				a.positionSV = input.positionSV;
				a.positionW = input.positionW.xyz;
				a.normal = normalize(input.normal);
				a.texcoord = input.tangent.xy;
				a.colour = input.colour;
				PS_Input b = a;
				PS_Input c = a;
				PS_Input d = a;

				// Set splat texture coordinates
				a.texcoord = float2(0.0, 0.0);
				b.texcoord = float2(0.0, 1.0);
				c.texcoord = float2(1.0, 1.0);
				d.texcoord = float2(1.0, 0.0);
				
				// Set the positions
				a.positionW += radius * (biNormal - tangent);
				b.positionW += radius * (biNormal + tangent);
				c.positionW += radius * ( tangent - biNormal);
				d.positionW += radius * (-tangent - biNormal);

				a.positionSV = mul(UNITY_MATRIX_VP, float4(a.positionW, 1.0));
				b.positionSV = mul(UNITY_MATRIX_VP, float4(b.positionW, 1.0));
				c.positionSV = mul(UNITY_MATRIX_VP, float4(c.positionW, 1.0));
				d.positionSV = mul(UNITY_MATRIX_VP, float4(d.positionW, 1.0));

				output.Append(a);
				output.Append(b);
				output.Append(c);

				output.Append(c);
				output.Append(a);
				output.Append(d);
			}

			fixed4 PS(PS_Input i) : SV_Target
			{
				// Calculate light values
				float3 lightPosition = _WorldSpaceLightPos0;
				float3 lightColour = _LightColor0.xyz;

				// Calculate light direction (0.0 is directional)
				float3 pixelToLight;
				if (unity_LightPosition[0].w == 0.0) pixelToLight = normalize(lightPosition);
				else pixelToLight = normalize(lightPosition - i.positionW.xyz);
				
				// Calculate diffuse
				float3 sample = tex2D(_SplatTex, i.texcoord).rgb * i.colour;
				float3 ambient = float3(0.6, 0.6, 0.6);
				float3 diffuse = ambient + saturate(lightColour * max(dot(i.normal, pixelToLight), 0));
				return fixed4(sample * saturate(diffuse), sample.r * _Fluff);
			}

			ENDCG
		}
	}
}