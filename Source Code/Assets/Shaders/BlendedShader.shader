﻿Shader "Custom/BlendedShader" {
	Properties {
		_TexOne ("Texture One (RGB)", 2D) = "white" {}
		_TexTwo ("Texture Two (RGB)", 2D) = "white" {}
		_BlendTex ("Blend Texture (RGB)", 2D) = "white" {}
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows

		#pragma target 3.0

		sampler2D _TexOne;
		sampler2D _TexTwo;
		sampler2D _BlendTex;

		struct Input {
			float2 uv_TexOne;
			float2 uv_TexTwo;
			float2 uv2_BlendTex;
		};

		void surf (Input IN, inout SurfaceOutputStandard o) {
			fixed4 cOne = tex2D (_TexOne, IN.uv_TexOne);
			fixed4 cTwo = tex2D (_TexTwo, IN.uv_TexTwo);
			fixed4 Blend = tex2D (_BlendTex, IN.uv2_BlendTex);

			o.Albedo = lerp(cOne.rgb, cTwo.rgb, Blend.r);
			o.Alpha = cOne.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
