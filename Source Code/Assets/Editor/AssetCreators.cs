﻿using UnityEngine;
using UnityEditor;
using System.IO;

public class AssetCreators
{
	[MenuItem("Assets/Create/C# Interface")]
	static void CreateCSharpInterface()
	{
		string path = (Selection.activeObject) ? AssetDatabase.GetAssetPath(Selection.activeObject).Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "") : "Assets/";
		path += "NewInterface.cs";
		Debug.Log("Creating interface: " + path);

		if(File.Exists(path) == false)
		{
			using(StreamWriter outfile = new StreamWriter(path))
			{
				outfile.WriteLine("using UnityEngine;");
				outfile.WriteLine("using System.Collections;");
				outfile.WriteLine("");
				outfile.WriteLine("public interface NewInterface");
				outfile.WriteLine("{");
				outfile.WriteLine("}");
			}
		}

		AssetDatabase.Refresh();
	}

	[MenuItem("Assets/Create/C# ScriptableObject")]
	static void CreateCSharpScriptableObject()
	{
		string path = (Selection.activeObject) ? AssetDatabase.GetAssetPath(Selection.activeObject).Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "") : "Assets/";
		path += "NewScriptableObject.cs";
		Debug.Log("Creating interface: " + path);

		if(File.Exists(path) == false)
		{
			using(StreamWriter outfile = new StreamWriter(path))
			{
				outfile.WriteLine("using UnityEngine;");
				outfile.WriteLine("using System.Collections;");
				outfile.WriteLine("");
				outfile.WriteLine("public class NewScriptableObject : ScriptableObject");
				outfile.WriteLine("{");
				outfile.WriteLine("}");
			}
		}

		AssetDatabase.Refresh();
	}
}
