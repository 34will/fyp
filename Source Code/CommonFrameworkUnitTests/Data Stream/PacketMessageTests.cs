﻿using System;
using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CommonFramework.DataStream;
using CommonFramework.MessageSystem;

namespace CommonFrameworkUnitTests
{
    [TestClass]
    public class PacketMessageTests
    {
        [TestMethod]
        public void Constructor()
        {
            // Prepare a PacketMessage
            TestMessageable messageable = new TestMessageable();
            Packet packet = new Packet(new byte[] { 1, 2, 3}, new IPEndPoint(IPAddress.Parse("127.0.0.1"), 5000));
            PacketMessage message = new PacketMessage(packet, messageable);
            
            // Ensure properties present correct references
            Assert.ReferenceEquals(packet, message.Packet);
            Assert.ReferenceEquals(messageable, packet.Sender);
        }

        [TestMethod]
        public void Name()
        {
            PacketMessage message = new PacketMessage(null);
            Assert.AreEqual("PacketMessage", message.MessageName);
        }

        [TestMethod]
        public void PropertySource()
        {
            // Prepare a PacketMessage
            TestMessageable messageable = new TestMessageable();
            Packet packet = new Packet(new byte[] { 1, 2, 3 }, new IPEndPoint(IPAddress.Parse("127.0.0.1"), 5000));
            PacketMessage message = new PacketMessage(packet, null);

            // Ensure properties present correct references
            Assert.ReferenceEquals(null, message.Source);
            message.Source = messageable;
            Assert.ReferenceEquals(messageable, message.Source);
        }
    }
}
