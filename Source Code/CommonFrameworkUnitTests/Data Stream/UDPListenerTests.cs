﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CommonFramework.DataStream;

namespace CommonFrameworkUnitTests
{
    [TestClass]
    public class UDPListenerTests
    {
        private static int port = 25000;

        [TestMethod]
        public void ReceivePackets()
        {
            // Prepare the UDPListener to test
            port++;
            IListener listener = new UDPListener(port);
            listener.Start();

            // Send some data to the UDPListener
            TestDataSender sender = new TestDataSender(port + 1);
            IPEndPoint target = new IPEndPoint(IPAddress.Parse("127.0.0.1"), port);
            byte[] data = Encoding.ASCII.GetBytes("Hello world");
            sender.Send(data, target);
            Thread.Sleep(100);
            sender.Stop();

            // Ask the listener if it received the packets
            List<Packet> packets = new List<Packet>(listener.ReceivePackets());
            Assert.AreEqual(1, packets.Count, "The listener should have received 1 packet. If there is more than one packet, ensure the port " + port + " is free");
            
            // Ensure the packet is the same as that sent
            foreach(Packet packet in packets)
            {
                Assert.AreEqual(data.Length, packet.Data.Length, "The received data length should match that what has been sent");
                if (data.Length == packet.Data.Length)
                {
                    for (int i = 0; i < data.Length; i++)
                    {
                        Assert.AreEqual(data[i], packet.Data[i], "Character at position should be '" + data[i] + "'. '" + packet.Data[i] + "' found");
                    }
                }
            }
        }

        [TestMethod]
        public void Stop()
        {
            // Prepare the UDPListener to test
            port++;
            UDPListener listener = new UDPListener(port);
            listener.Start();
            listener.Stop();

            // Send some data to the UDPListener
            TestDataSender sender = new TestDataSender(port + 1);
            IPEndPoint target = new IPEndPoint(IPAddress.Parse("127.0.0.1"), port);
            byte[] data = Encoding.ASCII.GetBytes("Hello world");
            sender.Send(data, target);
            Thread.Sleep(100);
            sender.Stop();

            // Ask the listener if it received the packets
            List<Packet> packets = new List<Packet>(listener.ReceivePackets());
            Assert.AreEqual(0, packets.Count, "The listener should have received no packets now that stop has been called");
        }

        [TestMethod]
        public void Running()
        {
            // Prepare the UDPListener to test
            port++;
            UDPListener listener = new UDPListener(port);
            Assert.AreEqual(false, listener.Running, "UDPListener should not be running before start is called");

            // Should be running once start is called
            listener.Start();
            Assert.IsTrue(listener.Running, "The UDPListener should flag as running after start is called");

            // Should no longer be running once stop is called
            listener.Stop();
            Assert.AreEqual(false, listener.Running, "UDPListener should no longer be running now stop has been called");
        }

        [TestMethod]
        public void Port()
        {
            UDPListener listener = new UDPListener(port);
            Assert.AreEqual(port, listener.Port, "The port assigned to the listener does not match the one returned by its property");
        }

        [TestMethod]
        public void StartException()
        {
            // Prepare the UDPListener to test
            port++;
            UDPListener listener = new UDPListener(port);
            listener.Start();
            listener.Start();
        }
    }
}
