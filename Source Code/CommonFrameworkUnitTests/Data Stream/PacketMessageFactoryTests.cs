﻿using System;
using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CommonFramework.DataStream;
using CommonFramework.MessageSystem;

namespace CommonFrameworkUnitTests
{
    [TestClass]
    public class PacketMessageFactoryTests
    {
        [TestMethod]
        public void FactoryCreateMessage()
        {
            // Create test packet and factory for test
            Packet packet = new Packet(new byte[] { 1, 2, 3 }, new IPEndPoint(IPAddress.Parse("127.0.0.1"), 42));
            PacketMessageFactory factory = new PacketMessageFactory();

            // Ask the factory to create a message
            IMessage message = factory.CreateMessage(packet);
            PacketMessage packetMessage = message as PacketMessage;

            // Check the test produced the correct results
            Assert.IsNotNull(message, "The IMessage created should not be null");
            Assert.IsNotNull(packetMessage, "The IMessage created should be of type PacketMessage");
            Assert.IsNull(message.Source);
            Assert.ReferenceEquals(packetMessage.Packet.Data, packet.Data);
            Assert.ReferenceEquals(packetMessage.Packet.Sender, packet.Sender);
        }
    }
}