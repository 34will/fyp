﻿using System;
using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CommonFramework.DataStream;

namespace CommonFrameworkUnitTests
{
    [TestClass]
    public class PacketTests
    {
        [TestMethod]
        public void Constructor()
        {
            Packet packet = new Packet(new byte[] { 1, 2, 3 }, new IPEndPoint(IPAddress.Parse("127.0.0.1"), 42));
            Assert.AreEqual(3, packet.Data.Length);
            Assert.AreEqual(1, packet.Data[0]);
            Assert.AreEqual(2, packet.Data[1]);
            Assert.AreEqual(3, packet.Data[2]);
            Assert.AreEqual("127.0.0.1", packet.Sender.Address.ToString());
            Assert.AreEqual(42, packet.Sender.Port);
        }
    }
}
