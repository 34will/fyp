﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CommonFramework.DataStream;

namespace CommonFrameworkUnitTests
{
    [TestClass]
    public class CRUDFileStreamListenerTests
    {
        [TestMethod]
        public void ConstructorXmlDocument()
        {
            XmlDocument document = new XmlDocument();
            document.LoadXml(File.ReadAllText("Test Assets/singlepacket.crud"));
            CRUDFileStreamListener listener = new CRUDFileStreamListener(document);
            listener.Start();
        }

        [TestMethod]
        public void Parse()
        {
            // Open the test CRUD file
            CRUDFileStreamListener listener = new CRUDFileStreamListener("Test Assets/singlepacket.crud");
            listener.Start();

            // Ensure the information has been read from the file correctly
            Assert.AreEqual("Single Packet CRUD Document", listener.Title);
            Assert.AreEqual("Information about the CRUD document", listener.Info);
        }

        [TestMethod]
        public void ParseNoDocument()
        {
            XmlDocument document = new XmlDocument();
            CRUDFileStreamListener listener = new CRUDFileStreamListener(document);
            listener.Start();
        }

        [TestMethod]
        public void ReceivePackets()
        {
            // Create a PacketTranscriber with a certain listener
            IListener listener = new CRUDFileStreamListener("Test Assets/sample.crud");
            listener.Start();

            // Wait until the packet should arrive
            Thread.Sleep(1000);
            List<Packet> packets = new List<Packet>(listener.ReceivePackets());

            // The file contains 2 packets
            Assert.AreEqual(2, packets.Count, "2 packets should be read from the file over the one second period");
        }

        [TestMethod]
        public void Stop()
        {
            // Create a PacketTranscriber with a certain listener
            IListener listener = new CRUDFileStreamListener("Test Assets/sample.crud");
            listener.Start();
            listener.Stop();

            List<Packet> packets = new List<Packet>(listener.ReceivePackets());

            // Ensure there was no oppertunity to read in any packets
            Assert.AreEqual(0, packets.Count, "Once stop is called no more packets should be read");
        }

        [TestMethod]
        public void ReadEpoch()
        {
            // Create a PacketTranscriber with a certain listener
            IListener listener = new CRUDFileStreamListener("Test Assets/epoch.crud");
            listener.Start();

            // Wait until the packet should arrive
            Thread.Sleep(1000);
            List<Packet> packets = new List<Packet>(listener.ReceivePackets());

            // The file contains 2 packets
            Assert.AreEqual(1, packets.Count, "2 packets should be read from the file over the one second period");
        }

        [TestMethod]
        public void ReadWait()
        {
            // Create a PacketTranscriber with a certain listener
            IListener listener = new CRUDFileStreamListener("Test Assets/readwait.crud");
            listener.Start();

            // Attempt to read the packet
            List<Packet> packets = new List<Packet>(listener.ReceivePackets());

            // The file contains 2 packets
            Assert.AreEqual(0, packets.Count, "2 packets should be read from the file over the one second period");
        }
    }
}
