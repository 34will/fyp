﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CommonFramework.DataStream;
using CommonFramework.MessageSystem;

namespace CommonFrameworkUnitTests
{
    [TestClass]
    public class PacketTranscriberTests
    {
        [TestMethod]
        public void Constructor()
        {
            // Create a PacketTranscriber with a certain listener
            IListener listener = new CRUDFileStreamListener("Test Assets/sample.crud");
            PacketTranscriber.Instance.SetListener(listener);

            // Assert that listener is referenced in the transcriber
            Assert.ReferenceEquals(listener, PacketTranscriber.Instance.Listener);
        }

        [TestMethod]
        public void Register()
        {
            // Create a PacketTranscriber to operate tests with
            IListener listener = new CRUDFileStreamListener("Test Assets/sample.crud");
            PacketTranscriber.Instance.SetListener(listener);

            // Firstly check no factories are registered
            Assert.AreEqual(0, PacketTranscriber.Instance.MessageFactoryCount, "Before registering a message factory the packet transcriber should be empty");
            PacketMessageFactory factory = new PacketMessageFactory();
            PacketTranscriber.Instance.Register(factory);

            // Secondly check register adds the factory to the collection
            Assert.AreEqual(1, PacketTranscriber.Instance.MessageFactoryCount, "After registering a message factory the packet transcriber should have 1 element");

            //Remove PacketMessageFactory to give PacketTranscriber prior singleton state
            PacketTranscriber.Instance.Remove(factory);
        }

        [TestMethod]
        public void Remove()
        {
            // Create a PacketTranscriber to operate tests with
            IListener listener = new CRUDFileStreamListener("Test Assets/sample.crud");
            PacketTranscriber.Instance.SetListener(listener);
            PacketMessageFactory factory = new PacketMessageFactory();
            PacketTranscriber.Instance.Register(factory);

            // Firstly check the one factory exists
            Assert.AreEqual(1, PacketTranscriber.Instance.MessageFactoryCount, "Before removing a message factory the packet transcriber should have 1 element");

            PacketTranscriber.Instance.Remove(factory);

            // Secondly check the factory is removed
            Assert.AreEqual(0, PacketTranscriber.Instance.MessageFactoryCount, "After removing a message factory the packet transcriber should be empty");
        }

        [TestMethod]
        public void UpdateNullListener()
        {
            try
            {
                PacketTranscriber.Instance.SetListener(null);
                PacketTranscriber.Instance.Update();
            }
            catch
            {
                Assert.Fail("The transcriber threw an Exception when it should have returned on null listener");
            }
        }

        [TestMethod]
        public void Update()
        {
            // Create a PacketTranscriber to operate tests with
            IListener listener = new CRUDFileStreamListener("Test Assets/epoch.crud");
            PacketTranscriber.Instance.SetListener(listener);
            PacketMessageFactory factory = new PacketMessageFactory();
            PacketTranscriber.Instance.Register(factory);

            listener.Start();
            Thread.Sleep(100);

            PacketTranscriber.Instance.Update();

            //Remove PacketMessageFactory to give PacketTranscriber prior singleton state
            PacketTranscriber.Instance.Remove(factory);
        }

        [TestMethod]
        public void FactoriesEnumerator()
        {
            // Create a PacketTranscriber to operate tests with
            PacketTranscriber.Instance.SetListener(new CRUDFileStreamListener("Test Assets/epoch.crud"));
            PacketTranscriber.Instance.Register(new PacketMessageFactory());

            // Get the factories back from the transcriber
            int count = 0;
            foreach (IPacketMessageFactory factory in PacketTranscriber.Instance)
                count++;

            Assert.AreEqual(1, count, "The number of items in the enumerator does not match the number of factories added to the transcriber");
        }
    }
}
