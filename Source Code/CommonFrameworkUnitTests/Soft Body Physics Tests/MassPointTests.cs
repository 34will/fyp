﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CommonFramework.SoftBodyPhysics;
using UnityEngine;

namespace CommonFrameworkUnitTests
{
    [TestClass]
    public class MassPointTests
    {
        private static Vector3 restPosition = new Vector3(4.2f, 3.0f, -1.0f);
        private const float mass = 51.7f;

        [TestMethod]
        public void MassPointRestPositionConstructor()
        {
            MassPoint massPoint = new MassPoint(restPosition);
            Assert.AreEqual(restPosition, massPoint.RestPosition);
            Assert.AreEqual(0.0f, massPoint.Mass);
            Assert.AreEqual(Vector3.zero, massPoint.Velocity);
        }

        [TestMethod]
        public void MassPointRestPositionAndMassConstructor()
        {
            MassPoint massPoint = new MassPoint(restPosition, mass);
            Assert.AreEqual(restPosition, massPoint.RestPosition);
            Assert.AreEqual(mass, massPoint.Mass);
            Assert.AreEqual(Vector3.zero, massPoint.Velocity);
        }

        [TestMethod]
        public void MassPointGetSetMassComparison()
        {
            MassPoint dummyMassPoint = new MassPoint(restPosition, mass);
            float newMass = 10.0f;
            dummyMassPoint.Mass = newMass;
            Assert.AreEqual(newMass, dummyMassPoint.Mass);
            Assert.AreNotEqual(mass, dummyMassPoint.Mass);
        }

        [TestMethod]
        public void MassPointGetSetVelocityComparison()
        {
            MassPoint dummyMassPoint = new MassPoint(restPosition, mass);
            Vector3 oldVelocity = dummyMassPoint.Velocity;
            Vector3 newVelocity = new Vector3(3.1f, 0.4f, -7.1f);
            dummyMassPoint.Velocity = newVelocity;
            Assert.AreEqual(newVelocity, dummyMassPoint.Velocity);
            Assert.AreNotEqual(oldVelocity, dummyMassPoint.Velocity);
        }

        [TestMethod]
        public void MassPointGetSetRestPositionComparison()
        {
            MassPoint dummyMassPoint = new MassPoint(restPosition, mass);
            Vector3 oldRestPosition = dummyMassPoint.RestPosition;
            Vector3 newRestPosition = new Vector3(3.1f, 0.4f, -7.1f);
            dummyMassPoint.RestPosition = newRestPosition;
            Assert.AreEqual(newRestPosition, dummyMassPoint.RestPosition);
            Assert.AreNotEqual(oldRestPosition, dummyMassPoint.RestPosition);
        }

        [TestMethod]
        public void MassPointGetSetForceComparison()
        {
            MassPoint dummyMassPoint = new MassPoint(restPosition, mass);
            Vector3 oldForce = dummyMassPoint.Force;
            Vector3 newForce = new Vector3(3.7f, 0.1f, -2.4f);
            dummyMassPoint.Force = newForce;
            Assert.AreEqual(newForce, dummyMassPoint.Force);
            Assert.AreNotEqual(oldForce, dummyMassPoint.Force);
        }

        [TestMethod]
        public void MassPointUpdate()
        {
            MassPoint dummyMassPoint = new MassPoint(restPosition, mass);
            dummyMassPoint.Force = new Vector3(1.0f, 2.0f, -0.5f);
            Vector3 startPos = dummyMassPoint.RestPosition;
            Vector3 startVelocity = dummyMassPoint.Velocity;
            Vector3 startAccel = dummyMassPoint.Accel;
            dummyMassPoint.Update(0.02f);
            dummyMassPoint.Update(0.03f);
            Assert.AreNotEqual(startPos, dummyMassPoint.RestPosition, "Rest Position");
            Assert.AreNotEqual(startVelocity, dummyMassPoint.Velocity, "Velocity");

            dummyMassPoint.Force += new Vector3(2.0f, 1.0f, -0.5f);
            dummyMassPoint.Update(0.02f);
            Assert.AreNotEqual(startAccel, dummyMassPoint.Accel, "Accel");
        }
    }
}
