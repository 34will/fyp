﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CommonFramework.SoftBodyPhysics;
using UnityEngine;

namespace CommonFrameworkUnitTests
{
    /*
    [TestClass]
    public class SpringTests
    {
        private MassPoint dummyMassPointStart = new MassPoint(Vector3.up);
        private MassPoint dummyMassPointEnd = new MassPoint(Vector3.zero);
        private float stiffness = 4.189f;

        [TestMethod]
        public void SpringConstructorStartAndEndMassPoint()
        {
            Spring spring = new Spring(dummyMassPointStart, dummyMassPointEnd);
            Assert.AreEqual(dummyMassPointStart, spring.StartMassPoint);
            Assert.AreEqual(dummyMassPointEnd, spring.EndMassPoint);
        }

        [TestMethod]
        public void SpringConstructorStartAndEndMassPointWithStiffness()
        {
            Spring spring = new Spring(dummyMassPointStart, dummyMassPointEnd, stiffness);
            Assert.AreEqual(dummyMassPointStart, spring.StartMassPoint);
            Assert.AreEqual(dummyMassPointEnd, spring.EndMassPoint);
            Assert.AreEqual(stiffness, spring.Stiffness);
        }

        [TestMethod]
        public void SpringSetGetRestLength()
        {
            Spring spring = new Spring(dummyMassPointStart, dummyMassPointEnd);
            Vector3 newRestLength = Vector3.forward;
            spring.RestLength = newRestLength;
            Assert.AreEqual(newRestLength, spring.RestLength);
        }

        [TestMethod]
        public void SpringGetConstant()
        {
            Spring spring = new Spring(dummyMassPointStart, dummyMassPointEnd);
            Assert.AreEqual(Vector3.zero, spring.SpringConstant);
        }

        [TestMethod]
        public void SpringUpdate()
        {
            Spring spring = new Spring(dummyMassPointStart, dummyMassPointEnd);
            throw new NotImplementedException();
        }
    }
    */
}
