﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using UnityEngine;

using CommonFramework.MovementControl;
using CommonFramework.MessageSystem;

namespace CommonFrameworkUnitTests
{
	[TestClass]
	public class MoveToMessageTests
	{
		[TestMethod]
		public void Vector3Constructor()
		{
			MoveToMessage mm = new MoveToMessage(new Vector3(10.0f, -6.5f, 100000.2f));

			Assert.AreEqual(new Vector3(10.0f, -6.5f, 100000.2f), mm.Point, "The Point should be { 10.0f, -6.5f, 100000.2f }.");
		}

		[TestMethod]
		public void ThreeFloatConstructor()
		{
			MoveToMessage mm = new MoveToMessage(10.0f, -6.5f, 100000.2f);

			Assert.AreEqual(new Vector3(10.0f, -6.5f, 100000.2f), mm.Point, "The Point should be { 10.0f, -6.5f, 100000.2f }.");
		}

		[TestMethod]
		public void SourceSetAndSet()
		{
			IMessageable m = new TestMessageable();
			MoveToMessage mm = new MoveToMessage(new Vector3(10.0f, -6.5f, 100000.2f));
			mm.Source = m;

			Assert.AreEqual(m, mm.Source, "The Source should be the provided TestMessageable.");
		}

		[TestMethod]
		public void MessageNameGet()
		{
			MoveToMessage mm = new MoveToMessage(new Vector3(10.0f, -6.5f, 100000.2f));

			Assert.AreEqual("MoveToMessage", mm.MessageName, "The MessageName should be \"MoveToMessage\".");
		}
	}
}
