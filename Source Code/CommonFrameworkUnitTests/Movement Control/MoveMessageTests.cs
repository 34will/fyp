﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using UnityEngine;

using CommonFramework.MovementControl;
using CommonFramework.MessageSystem;

namespace CommonFrameworkUnitTests
{
	[TestClass]
	public class MoveMessageTests
	{
		[TestMethod]
		public void Vector3Constructor()
		{
			MoveMessage mm = new MoveMessage(new Vector3(10.0f, -6.5f, 100000.2f));

			Assert.AreEqual(new Vector3(10.0f, -6.5f, 100000.2f), mm.Translation, "The Translation should be { 10.0f, -6.5f, 100000.2f }.");
		}

		[TestMethod]
		public void ThreeFloatConstructor()
		{
			MoveMessage mm = new MoveMessage(10.0f, -6.5f, 100000.2f);

			Assert.AreEqual(new Vector3(10.0f, -6.5f, 100000.2f), mm.Translation, "The Translation should be { 10.0f, -6.5f, 100000.2f }.");
		}

		[TestMethod]
		public void SourceSetAndSet()
		{
			IMessageable m = new TestMessageable();
			MoveMessage mm = new MoveMessage(new Vector3(10.0f, -6.5f, 100000.2f));
			mm.Source = m;

			Assert.AreEqual(m, mm.Source, "The Source should be the provided TestMessageable.");
		}

		[TestMethod]
		public void MessageNameGet()
		{
			MoveMessage mm = new MoveMessage(new Vector3(10.0f, -6.5f, 100000.2f));

			Assert.AreEqual("MoveMessage", mm.MessageName, "The MessageName should be \"MoveMessage\".");
		}
	}
}
