﻿using System.IO;
using System;
using System.Xml;
using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using UnityEngine;

using CommonFramework;

namespace CommonFrameworkUnitTests
{
	[TestClass]
	public class XMLHelperTests
	{
		[TestMethod]
		public void ReadValidStringAttribute()
		{
			XmlDocument document = new XmlDocument();
			document.LoadXml(File.ReadAllText("Test Assets/xmlhelpertestfile.xml"));
			XmlNodeList root = document.FirstChild.NextSibling.ChildNodes;
			foreach(XmlNode node in root)
			{
				if(node.Name == "StringAttribute")
				{
					string value = XMLHelper.ReadAttribute(node, "valid");
					Assert.AreEqual("yes", value, "The value of the \'valid\' attribute from the test file should be \"yes\".");
					break;
				}
			}
		}

		[TestMethod]
		public void ReadInvalidStringAttribute()
		{
			XmlDocument document = new XmlDocument();
			document.LoadXml(File.ReadAllText("Test Assets/xmlhelpertestfile.xml"));
			XmlNodeList root = document.FirstChild.NextSibling.ChildNodes;
			foreach(XmlNode node in root)
			{
				if(node.Name == "StringAttribute")
				{
					string value = XMLHelper.ReadAttribute(node, "notvalid");
					Assert.AreEqual("", value, "The value returned for the non-existant attribute from the test file should be \"\".");
					break;
				}
			}
		}

		[TestMethod]
		public void ReadInvalidStringAttributeAlternativeDefault()
		{
			XmlDocument document = new XmlDocument();
			document.LoadXml(File.ReadAllText("Test Assets/xmlhelpertestfile.xml"));
			XmlNodeList root = document.FirstChild.NextSibling.ChildNodes;
			foreach(XmlNode node in root)
			{
				if(node.Name == "StringAttribute")
				{
					string value = XMLHelper.ReadAttribute(node, "notvalid", "default");
					Assert.AreEqual("default", value, "The value returned for the non-existant attribute from the test file should be \"default\".");
					break;
				}
			}
		}

		[TestMethod]
		public void ReadStringAttributeFromNullXMLNode()
		{
			string value = XMLHelper.ReadAttribute(null, "notvalid");
			Assert.AreEqual("", value, "The value returned for the null XmlNode should be \"\".");
		}

		[TestMethod]
		public void ReadValidBoolAttribute()
		{
			XmlDocument document = new XmlDocument();
			document.LoadXml(File.ReadAllText("Test Assets/xmlhelpertestfile.xml"));
			XmlNodeList root = document.FirstChild.NextSibling.ChildNodes;
			foreach(XmlNode node in root)
			{
				if(node.Name == "BoolAttribute")
				{
					bool value = XMLHelper.ReadAttribute<bool>(node, "valid");
					Assert.AreEqual(true, value, "The value of the \'valid\' attribute from the test file should be true.");
					break;
				}
			}
		}

		[TestMethod]
		public void ReadInvalidBoolAttribute()
		{
			XmlDocument document = new XmlDocument();
			document.LoadXml(File.ReadAllText("Test Assets/xmlhelpertestfile.xml"));
			XmlNodeList root = document.FirstChild.NextSibling.ChildNodes;
			foreach(XmlNode node in root)
			{
				if(node.Name == "BoolAttribute")
				{
					bool value = XMLHelper.ReadAttribute<bool>(node, "notvalid");
					Assert.AreEqual(default(bool), value, "The value returned for the non-existant attribute from the test file should be default(bool).");
					break;
				}
			}
		}

		[TestMethod]
		public void ReadInvalidBoolAttributeAlternativeDefault()
		{
			XmlDocument document = new XmlDocument();
			document.LoadXml(File.ReadAllText("Test Assets/xmlhelpertestfile.xml"));
			XmlNodeList root = document.FirstChild.NextSibling.ChildNodes;
			foreach(XmlNode node in root)
			{
				if(node.Name == "BoolAttribute")
				{
					bool value = XMLHelper.ReadAttribute<bool>(node, "notvalid", true);
					Assert.IsTrue(value, "The value returned for the non-existant attribute from the test file should be true.");
					break;
				}
			}
		}

		[TestMethod]
		public void ReadBoolAttributeFromNullXMLNode()
		{
			bool value = XMLHelper.ReadAttribute<bool>(null, "notvalid");
			Assert.AreEqual(default(bool), value, "The value returned for the null XmlNode should be default(bool).");
		}

		[TestMethod]
		public void ReadValidIntAttribute()
		{
			XmlDocument document = new XmlDocument();
			document.LoadXml(File.ReadAllText("Test Assets/xmlhelpertestfile.xml"));
			XmlNodeList root = document.FirstChild.NextSibling.ChildNodes;
			foreach(XmlNode node in root)
			{
				if(node.Name == "IntAttribute")
				{
					int value = XMLHelper.ReadAttribute<int>(node, "valid");
					Assert.AreEqual(10, value, "The value of the \'valid\' attribute from the test file should be 10.");
					break;
				}
			}
		}

		[TestMethod]
		public void ReadInvalidIntAttribute()
		{
			XmlDocument document = new XmlDocument();
			document.LoadXml(File.ReadAllText("Test Assets/xmlhelpertestfile.xml"));
			XmlNodeList root = document.FirstChild.NextSibling.ChildNodes;
			foreach(XmlNode node in root)
			{
				if(node.Name == "IntAttribute")
				{
					int value = XMLHelper.ReadAttribute<int>(node, "notvalid");
					Assert.AreEqual(default(int), value, "The value returned for the non-existant attribute from the test file should be default(int).");
					break;
				}
			}
		}

		[TestMethod]
		public void ReadInvalidIntAttributeAlternativeDefault()
		{
			XmlDocument document = new XmlDocument();
			document.LoadXml(File.ReadAllText("Test Assets/xmlhelpertestfile.xml"));
			XmlNodeList root = document.FirstChild.NextSibling.ChildNodes;
			foreach(XmlNode node in root)
			{
				if(node.Name == "IntAttribute")
				{
					int value = XMLHelper.ReadAttribute<int>(node, "notvalid", 30);
					Assert.AreEqual(30, value, "The value returned for the non-existant attribute from the test file should be 30.");
					break;
				}
			}
		}

		[TestMethod]
		public void ReadIntAttributeFromNullXMLNode()
		{
			int value = XMLHelper.ReadAttribute<int>(null, "notvalid");
			Assert.AreEqual(default(int), value, "The value returned for the null XmlNode should be default(int).");
		}

		[TestMethod]
		public void ReadValidFloatAttribute()
		{
			XmlDocument document = new XmlDocument();
			document.LoadXml(File.ReadAllText("Test Assets/xmlhelpertestfile.xml"));
			XmlNodeList root = document.FirstChild.NextSibling.ChildNodes;
			foreach(XmlNode node in root)
			{
				if(node.Name == "FloatAttribute")
				{
					float value = XMLHelper.ReadAttribute<float>(node, "valid");
					Assert.AreEqual(14.65f, value, "The value of the \'valid\' attribute from the test file should be 14.65f.");
					break;
				}
			}
		}

		[TestMethod]
		public void ReadInvalidFloatAttribute()
		{
			XmlDocument document = new XmlDocument();
			document.LoadXml(File.ReadAllText("Test Assets/xmlhelpertestfile.xml"));
			XmlNodeList root = document.FirstChild.NextSibling.ChildNodes;
			foreach(XmlNode node in root)
			{
				if(node.Name == "FloatAttribute")
				{
					float value = XMLHelper.ReadAttribute<float>(node, "notvalid");
					Assert.AreEqual(default(float), value, "The value returned for the non-existant attribute from the test file should be default(float).");
					break;
				}
			}
		}

		[TestMethod]
		public void ReadInvalidFloatAttributeAlternativeDefault()
		{
			XmlDocument document = new XmlDocument();
			document.LoadXml(File.ReadAllText("Test Assets/xmlhelpertestfile.xml"));
			XmlNodeList root = document.FirstChild.NextSibling.ChildNodes;
			foreach(XmlNode node in root)
			{
				if(node.Name == "FloatAttribute")
				{
					float value = XMLHelper.ReadAttribute<float>(node, "notvalid", 30.0f);
					Assert.AreEqual(30.0f, value, "The value returned for the non-existant attribute from the test file should be 30.0f.");
					break;
				}
			}
		}

		[TestMethod]
		public void ReadFloatAttributeFromNullXMLNode()
		{
			float value = XMLHelper.ReadAttribute<float>(null, "notvalid");
			Assert.AreEqual(default(float), value, "The value returned for the null XmlNode should be default(float).");
		}

		[TestMethod]
		public void ReadValidDoubleAttribute()
		{
			XmlDocument document = new XmlDocument();
			document.LoadXml(File.ReadAllText("Test Assets/xmlhelpertestfile.xml"));
			XmlNodeList root = document.FirstChild.NextSibling.ChildNodes;
			foreach(XmlNode node in root)
			{
				if(node.Name == "DoubleAttribute")
				{
					double value = XMLHelper.ReadAttribute<double>(node, "valid");
					Assert.AreEqual(-17.03, value, "The value of the \'valid\' attribute from the test file should be -17.03.");
					break;
				}
			}
		}

		[TestMethod]
		public void ReadInvalidDoubleAttribute()
		{
			XmlDocument document = new XmlDocument();
			document.LoadXml(File.ReadAllText("Test Assets/xmlhelpertestfile.xml"));
			XmlNodeList root = document.FirstChild.NextSibling.ChildNodes;
			foreach(XmlNode node in root)
			{
				if(node.Name == "DoubleAttribute")
				{
					double value = XMLHelper.ReadAttribute<double>(node, "notvalid");
					Assert.AreEqual(default(double), value, "The value returned for the non-existant attribute from the test file should be default(double).");
					break;
				}
			}
		}

		[TestMethod]
		public void ReadInvalidDoubleAttributeAlternativeDefault()
		{
			XmlDocument document = new XmlDocument();
			document.LoadXml(File.ReadAllText("Test Assets/xmlhelpertestfile.xml"));
			XmlNodeList root = document.FirstChild.NextSibling.ChildNodes;
			foreach(XmlNode node in root)
			{
				if(node.Name == "DoubleAttribute")
				{
					double value = XMLHelper.ReadAttribute<double>(node, "notvalid", 30.0);
					Assert.AreEqual(30.0, value, "The value returned for the non-existant attribute from the test file should be 30.0.");
					break;
				}
			}
		}

		[TestMethod]
		public void ReadDoubleAttributeFromNullXMLNode()
		{
			double value = XMLHelper.ReadAttribute<double>(null, "notvalid");
			Assert.AreEqual(default(double), value, "The value returned for the null XmlNode should be default(double).");
		}

		[TestMethod]
		public void ReadInvalidTypeAttribute()
		{
			XmlDocument document = new XmlDocument();
			document.LoadXml(File.ReadAllText("Test Assets/xmlhelpertestfile.xml"));
			XmlNodeList root = document.FirstChild.NextSibling.ChildNodes;
			foreach(XmlNode node in root)
			{
				if(node.Name == "Vector3Attribute")
				{
					Vector3 value = XMLHelper.ReadAttribute<Vector3>(node, "valid");
					Assert.AreEqual(default(Vector3), value, "The value returned for the invalid type attribute from the test file should be default(Vector3).");
					break;
				}
			}
		}

		[TestMethod]
		public void ReadInvalidTypeAttributeAlternativeDefault()
		{
			XmlDocument document = new XmlDocument();
			document.LoadXml(File.ReadAllText("Test Assets/xmlhelpertestfile.xml"));
			XmlNodeList root = document.FirstChild.NextSibling.ChildNodes;
			foreach(XmlNode node in root)
			{
				if(node.Name == "Vector3Attribute")
				{
					Vector3 value = XMLHelper.ReadAttribute<Vector3>(node, "valid", new Vector3(7.0f, -50.0f, 900000.0f));
					Assert.AreEqual(new Vector3(7.0f, -50.0f, 900000.0f), value, "The value returned for the invalid type attribute from the test file should be { 7.0f, -50.0f, 900000.0f }.");
					break;
				}
			}
		}
	}
}
