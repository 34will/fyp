﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CommonFramework.Environment;
using CommonFramework.MessageSystem;
using UnityEngine;

namespace CommonFrameworkUnitTests
{
    [TestClass]
    public class AssetMessageTests
    {
        string resource = "BMW_i3";
        string id = "Test Item";
        Vector3 position = Vector3.zero;
        Quaternion rotation = Quaternion.identity;
        Vector3 scale = Vector3.up;

        /// <summary>
        /// Test AssetMessage constructor that takes a resource as a string, an ID as a string, a position, rotation and scale
        /// where the IMessageable source is set to null.
        /// Check the resource of the AssetMessage is equal to the parameterized resource.
        /// Check the id of the AssetMessage is equal to the parameterized id.
        /// Check the position of the AssetMessage is equal to the parameterized position.
        /// Check the rotation of the AssetMessage is equal to the parameterized rotation.
        /// Check the scale of the AssetMessage is equal to the parameterized scale.
        /// Check the IMessageable source of the AssetMessage is defaulted to null.
        /// </summary>
        [TestMethod]
        public void AssetMessageNullSourceConstructor()
        {
            AssetMessage assetMessage = new AssetMessage(resource, id, position, rotation, scale);

            Assert.AreEqual(resource, assetMessage.Resource, string.Format("Expected: {0}, Actual: {1}.", resource, assetMessage.Resource));
            Assert.AreEqual(id, assetMessage.Identifier, string.Format("Expected: {0}, Actual: {1}.", id, assetMessage.Identifier));
            Assert.AreEqual(position, assetMessage.Position, string.Format("Expected: {0}, Actual: {1}.", position, assetMessage.Position));
            Assert.AreEqual(rotation, assetMessage.Rotation, string.Format("Expected: {0}, Actual: {1}.", rotation, assetMessage.Rotation));
            Assert.AreEqual(scale, assetMessage.Scale, string.Format("Expected: {0}, Actual: {1}.", scale, assetMessage.Scale));
            Assert.IsNull(assetMessage.Source, string.Format("The IMessageable source of the AssetMessage is expected to be null."));
        }

        /// <summary>
        /// Test AssetMessage constructor that takes a resource as a string, an ID as a string, a position, rotation, 
        /// scale and IMessageable source.
        /// Check the resource of the AssetMessage is equal to the parameterized resource.
        /// Check the id of the AssetMessage is equal to the parameterized id.
        /// Check the position of the AssetMessage is equal to the parameterized position.
        /// Check the rotation of the AssetMessage is equal to the parameterized rotation.
        /// Check the scale of the asset message is equal to the parameterized scale.
        /// Check the IMessageable source of the AssetMessage is not null.
        /// Check the IMessageable source of the AssetMessage is equal to the parameterized IMessageable.
        /// </summary>
        [TestMethod]
        public void AssetMessageNotNullSourceConstructor()
        {
            TestMessageable testMessageable = new TestMessageable();
            AssetMessage assetMessage = new AssetMessage(resource, id, position, rotation, scale, testMessageable);

            Assert.AreEqual(resource, assetMessage.Resource, string.Format("Expected: {0}, Actual: {1}.", resource, assetMessage.Resource));
            Assert.AreEqual(id, assetMessage.Identifier, string.Format("Expected: {0}, Actual: {1}.", id, assetMessage.Identifier));
            Assert.AreEqual(position, assetMessage.Position, string.Format("Expected: {0}, Actual: {1}.", position, assetMessage.Position));
            Assert.AreEqual(rotation, assetMessage.Rotation, string.Format("Expected: {0}, Actual: {1}.", rotation, assetMessage.Rotation));
            Assert.AreEqual(scale, assetMessage.Scale, string.Format("Expected: {0}, Actual: {1}.", scale, assetMessage.Scale));
            Assert.IsNotNull(assetMessage.Source, string.Format("The IMessageable source of the AssetMessage is not expected to be null."));
            Assert.AreEqual(testMessageable, assetMessage.Source, string.Format("Expected: {0}, Actual: {1}.", testMessageable, assetMessage.Source));
        }


        /// <summary>
        /// Test AssetMessage constructor that takes a resource as a string, an ID as a string, a position, rotation and scale
        /// where the IMessageable source is set to null.
        /// Check the IMessageable source of the AssetMessage is null.
        /// Set IMessageable to be TargetMessageable.
        /// Check the IMessageable source of the AssetMessage is not null.
        /// Check the IMessageable source of the AssetMessage is equal to the set IMessageable - TargetMessageable.
        /// </summary>
        [TestMethod]
        public void AssetMessageSetGetSourceIMessageable()
        {
            TargetableMessageable targetMessageable = new TargetableMessageable();
            AssetMessage assetMessage = new AssetMessage(resource, id, position, rotation, scale);

            Assert.IsNull(assetMessage.Source, string.Format("The IMessageable source of the AssetMessage is expected to be null."));
            assetMessage.Source = targetMessageable;
            Assert.IsNotNull(assetMessage.Source, string.Format("The IMessageable source of the AssetMessage is not expected to be null."));
            Assert.AreEqual(targetMessageable, assetMessage.Source, string.Format("Expected: {0}, Actual: {1}.", targetMessageable, assetMessage.Source));
        }
    }
}
