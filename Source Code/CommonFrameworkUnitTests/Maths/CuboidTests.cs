﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CommonFramework.Maths;

namespace CommonFrameworkUnitTests
{
    [TestClass]
    public class CuboidTests
    {
        [TestMethod]
        public void ConstructorFloats()
        {
            Cuboid cuboid = new Cuboid(1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f);
            Assert.AreEqual(1.0f, cuboid.X);
            Assert.AreEqual(2.0f, cuboid.Y);
            Assert.AreEqual(3.0f, cuboid.Z);
            Assert.AreEqual(4.0f, cuboid.Width);
            Assert.AreEqual(5.0f, cuboid.Height);
            Assert.AreEqual(6.0f, cuboid.Depth);
        }

        [TestMethod]
        public void ConstructorVectors()
        {
            Cuboid cuboid = new Cuboid(new Float3(1.0f, 2.0f, 3.0f), new Float3(4.0f, 5.0f, 6.0f));
            Assert.AreEqual(1.0f, cuboid.X);
            Assert.AreEqual(2.0f, cuboid.Y);
            Assert.AreEqual(3.0f, cuboid.Z);
            Assert.AreEqual(4.0f, cuboid.Width);
            Assert.AreEqual(5.0f, cuboid.Height);
            Assert.AreEqual(6.0f, cuboid.Depth);
        }

        [TestMethod]
        public void ContainsFloats()
        {
            Cuboid cuboid = new Cuboid(2.0f, 2.0f, 2.0f, 4.0f, 4.0f, 4.0f);
            
            // Check one inside the cube
            Assert.IsTrue(cuboid.Contains(3.0f, 3.0f, 3.0f), "The inside cube assert has failed");

            // Check one on the edge of the cube
            Assert.IsTrue(cuboid.Contains(6.0f, 4.0f, 4.0f), "The edge of cube assert has failed");

            // Check one on the outside
            Assert.IsFalse(cuboid.Contains(7.0f, 4.0f, 4.0f), "The outside cube assert has failed");
        }

        [TestMethod]
        public void ContainsVector()
        {
            Cuboid cuboid = new Cuboid(2.0f, 2.0f, 2.0f, 4.0f, 4.0f, 4.0f);

            // Check one inside the cube
            Assert.IsTrue(cuboid.Contains(new Float3(3.0f, 3.0f, 3.0f)), "The inside cube assert has failed");

            // Check one on the edge of the cube
            Assert.IsTrue(cuboid.Contains(new Float3(6.0f, 4.0f, 4.0f)), "The edge of cube assert has failed");

            // Check one on the outside
            Assert.IsFalse(cuboid.Contains(new Float3(7.0f, 4.0f, 4.0f)), "The outside cube assert has failed");
        }

        [TestMethod]
        public void ContainsPositionable3D()
        {
            Cuboid cuboid = new Cuboid(2.0f, 2.0f, 2.0f, 4.0f, 4.0f, 4.0f);

            // Check one inside the cube
            Assert.IsTrue(cuboid.Contains((IPositionable3D)new Float3(3.0f, 3.0f, 3.0f)), "The inside cube assert has failed");

            // Check one on the edge of the cube
            Assert.IsTrue(cuboid.Contains((IPositionable3D)new Float3(6.0f, 4.0f, 4.0f)), "The edge of cube assert has failed");

            // Check one on the outside
            Assert.IsFalse(cuboid.Contains((IPositionable3D)new Float3(7.0f, 4.0f, 4.0f)), "The outside cube assert has failed");
        }

        [TestMethod]
        public void Intersects()
        {
            Cuboid a = new Cuboid(2.0f, 2.0f, 2.0f, 4.0f, 4.0f, 4.0f);
            Cuboid b = new Cuboid(2.0f, 2.0f, 2.0f, 4.0f, 4.0f, 4.0f);

            // Check perfect overlapping
            Assert.IsTrue(a.Intersects(b), "A intersects B, perfect overlap assert failed");
            Assert.IsTrue(b.Intersects(a), "B intersects A, perfect overlap assert failed");

            // Check partial overlap
            b.X = 4.0f;
            b.Y = 4.0f;
            b.Z = 4.0f;
            Assert.IsTrue(a.Intersects(b), "A intersects B, partial overlap assert failed");
            Assert.IsTrue(b.Intersects(a), "B intersects A, partial overlap assert failed");

            // Check no overlap
            b.X = 7.0f;
            b.Y = 4.0f;
            b.Z = 4.0f;
            Assert.IsFalse(a.Intersects(b), "A intersects B, no overlap assert failed");
            Assert.IsFalse(b.Intersects(a), "B intersects A, no overlap assert failed");
        }

		[TestMethod]
		public void Properties()
		{
			// Standard cuboid
			Cuboid cuboid = new Cuboid(1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f);

			// Normal properties handled by the constructor
			Assert.AreEqual(1.0f, cuboid.X);
			Assert.AreEqual(2.0f, cuboid.Y);
			Assert.AreEqual(3.0f, cuboid.Z);
			Assert.AreEqual(4.0f, cuboid.Width);
			Assert.AreEqual(5.0f, cuboid.Height);
			Assert.AreEqual(6.0f, cuboid.Depth);

			// Accumulated properties
			Assert.AreEqual(1.0f, cuboid.Left);
			Assert.AreEqual(2.0f, cuboid.Top);
			Assert.AreEqual(3.0f, cuboid.Front);
			Assert.AreEqual(5.0f, cuboid.Right);
			Assert.AreEqual(7.0f, cuboid.Bottom);
			Assert.AreEqual(9.0f, cuboid.Back);
		}

		[TestMethod]
		public void PropertiesSetAndGet()
		{
			// Standard cuboid
			Cuboid cuboid = new Cuboid(1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f);

			cuboid.X = 2.0f;
			cuboid.Y = 3.0f;
			cuboid.Z = 4.0f;
			cuboid.Width = 5.0f;
			cuboid.Height = 6.0f;
			cuboid.Depth = 7.0f;

			Assert.AreEqual(2.0f, cuboid.X);
			Assert.AreEqual(3.0f, cuboid.Y);
			Assert.AreEqual(4.0f, cuboid.Z);
			Assert.AreEqual(5.0f, cuboid.Width);
			Assert.AreEqual(6.0f, cuboid.Height);
			Assert.AreEqual(7.0f, cuboid.Depth);
		}

        [TestMethod]
        public void SizeSetProperties()
        {
            // Standard cuboid
            Cuboid cuboid = new Cuboid(1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f);

            // Normal properties handled by the constructor
            Assert.AreEqual(4.0f, cuboid.Width);
            Assert.AreEqual(5.0f, cuboid.Height);
            Assert.AreEqual(6.0f, cuboid.Depth);

            // Set new values
            cuboid.Width = 7.0f;
            cuboid.Height = 8.0f;
            cuboid.Depth = 9.0f;

            // Check values now setter used
            Assert.AreEqual(7.0f, cuboid.Width);
            Assert.AreEqual(8.0f, cuboid.Height);
            Assert.AreEqual(9.0f, cuboid.Depth);
        }
    }
}
