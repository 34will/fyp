﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using UnityEngine;

using CommonFramework;
using CommonFramework.Maths;

namespace CommonFrameworkUnitTests
{
	[TestClass]
	public class HelperTests
	{
		[TestMethod]
		public void RootTwoConstant()
		{
			Assert.AreEqual(1.41421356237, Helper.RootTwo, "The √2 double constant should be 1.41421356237.");
		}

		[TestMethod]
		public void RootTwoFConstant()
		{
			Assert.AreEqual(1.41421356237f, Helper.RootTwoF, "The √2 float constant should be 1.41421356237f.");
		}

		[TestMethod]
		public void OneOverRootTwoConstant()
		{
			Assert.AreEqual(0.70710678118, Helper.OneOverRootTwo, "The 1/√2 double constant should be 0.70710678118.");
		}

		[TestMethod]
		public void OneOverRootTwoFConstant()
		{
			Assert.AreEqual(0.70710678118f, Helper.OneOverRootTwoF, "The 1/√2 float constant should be 0.70710678118f.");
		}

		[TestMethod]
		public void PiConstant()
		{
			Assert.AreEqual(3.14159265359, Helper.Pi, "The Pi constant should be 3.14159265359.");
		}

		[TestMethod]
		public void TwoPiConstant()
		{
			Assert.AreEqual(6.28318530718, Helper.TwoPi, "The Two Pi constant should be 6.28318530718.");
		}

		[TestMethod]
		public void RadiansToDegreesMultiplierConstant()
		{
			Assert.AreEqual(180.0, (Helper.RadiansToDegreesMultiplier * Helper.Pi), "The degree value of Pi should be 180.");
		}

		[TestMethod]
		public void DegreesToRadiansMultiplierConstant()
		{
			Assert.AreEqual(Helper.Pi, (Helper.DegreesToRadiansMultiplier * 180.0), "The radians value of 180 should be Pi.");
		}

		[TestMethod]
		public void RandomRangeNaNMinMax()
		{
			float random = Helper.RandomRange(float.NaN, 1.0f);

			Assert.IsTrue(float.IsNaN(random), "The random number returned between the range [float.NaN, 1.0f] should be float.NaN.");

			random = Helper.RandomRange(1.0f, float.NaN);

			Assert.IsTrue(float.IsNaN(random), "The random number returned between the range [1.0f, float.NaN] should be float.NaN.");

			random = Helper.RandomRange(float.NaN, float.NaN);

			Assert.IsTrue(float.IsNaN(random), "The random number returned between the range [float.NaN, float.NaN] should be float.NaN.");
		}

		[TestMethod]
		public void RandomRangePositiveInfinityMinMax()
		{
			float random = Helper.RandomRange(float.PositiveInfinity, 1.0f);

			Assert.IsTrue(float.IsNaN(random), "The random number returned between the range [float.PositiveInfinity, 1.0f] should be float.NaN.");

			random = Helper.RandomRange(1.0f, float.PositiveInfinity);

			Assert.IsTrue(float.IsNaN(random), "The random number returned between the range [1.0f, float.PositiveInfinity] should be float.NaN.");

			random = Helper.RandomRange(float.PositiveInfinity, float.PositiveInfinity);

			Assert.IsTrue(float.IsNaN(random), "The random number returned between the range [float.PositiveInfinity, float.PositiveInfinity] should be float.NaN.");
		}

		[TestMethod]
		public void RandomRangeNegativeInfinityMinMax()
		{
			float random = Helper.RandomRange(float.NegativeInfinity, 1.0f);

			Assert.IsTrue(float.IsNaN(random), "The random number returned between the range [float.NegativeInfinity, 1.0f] should be float.NaN.");

			random = Helper.RandomRange(1.0f, float.NegativeInfinity);

			Assert.IsTrue(float.IsNaN(random), "The random number returned between the range [1.0f, float.NegativeInfinity] should be float.NaN.");

			random = Helper.RandomRange(float.NegativeInfinity, float.NegativeInfinity);

			Assert.IsTrue(float.IsNaN(random), "The random number returned between the range [float.NegativeInfinity, float.NegativeInfinity] should be float.NaN.");
		}

		[TestMethod]
		public void RandomRangeMinGreaterThanMax()
		{
			float random = Helper.RandomRange(10.0f, -1000.0f);

			Assert.IsTrue(float.IsNaN(random), "The random number returned should be float.NaN because the minimum was bigger than the maximum.");
		}

		[TestMethod]
		public void Vector2NotAVectorConstant()
		{
			Vector2 testVec = Helper.NaV3;

			Assert.IsTrue(float.IsNaN(testVec.x), "The x component of a Vector2 set to NaV should be float.NaN.");
			Assert.IsTrue(float.IsNaN(testVec.y), "The y component of a Vector2 set to NaV should be float.NaN.");
		}

		[TestMethod]
		public void Vector2IsNaVNormalRandomVector()
		{
			Vector2 testVec = new Vector2(Helper.RandomRange(float.MinValue, float.MaxValue), Helper.RandomRange(float.MinValue, float.MaxValue));

			Assert.IsFalse(Helper.IsNaV(testVec), "A Vector2 of any three real numbers, within the float bounds, should return false from the IsNaV check.");
		}

		[TestMethod]
		public void Vector2IsNaV()
		{
			Vector2 testVec = Helper.NaV3;

			Assert.IsTrue(Helper.IsNaV(testVec), "A Vector2 set to NaV should return true from the IsNaV check.");
		}

		[TestMethod]
		public void Vector2HasNaNNormalRandomVector()
		{
			Vector2 testVec = new Vector2(Helper.RandomRange(float.MinValue, float.MaxValue), Helper.RandomRange(float.MinValue, float.MaxValue));

			Assert.IsFalse(Helper.HasNaN(testVec), "A Vector2 of any three real numbers, within the float bounds, should return false from the HasNaN check.");
		}

		[TestMethod]
		public void Vector2HasNaNXYComponent()
		{
			Vector2 testVec = new Vector2(float.NaN, 0.0f);

			Assert.IsTrue(Helper.HasNaN(testVec), "A Vector2 with the x-component set to float.NaN should return true from the HasNaN check.");

			testVec = new Vector2(0.0f, float.NaN);

			Assert.IsTrue(Helper.HasNaN(testVec), "A Vector2 with the y-component set to float.NaN should return true from the HasNaN check.");
		}

		[TestMethod]
		public void Vector2IsInfiniteNormalRandomVector()
		{
			Vector2 testVec = new Vector2(Helper.RandomRange(float.MinValue, float.MaxValue), Helper.RandomRange(float.MinValue, float.MaxValue));

			Assert.IsFalse(Helper.IsInfinite(testVec), "A Vector2 of any three real numbers, within the float bounds, should return false from the IsInfinite check.");
		}

		[TestMethod]
		public void Vector2IsInfinitePositive()
		{
			Vector2 testVec = new Vector2(float.PositiveInfinity, float.PositiveInfinity);

			Assert.IsTrue(Helper.IsInfinite(testVec), "A Vector2 with all elements set to float.PositiveInfinity should return true from the IsInfinite check.");
		}

		[TestMethod]
		public void Vector2IsInfiniteNegative()
		{
			Vector2 testVec = new Vector2(float.NegativeInfinity, float.NegativeInfinity);

			Assert.IsTrue(Helper.IsInfinite(testVec), "A Vector2 with all elements set to float.NegativeInfinity should return true from the IsInfinite check.");
		}

		[TestMethod]
		public void Vector2HasInfiniteNormalRandomVector()
		{
			Vector2 testVec = new Vector2(Helper.RandomRange(float.MinValue, float.MaxValue), Helper.RandomRange(float.MinValue, float.MaxValue));

			Assert.IsFalse(Helper.HasInfinite(testVec), "A Vector2 of any three real numbers, within the float bounds, should return false from the HasInfinite check.");
		}

		[TestMethod]
		public void Vector2HasInfiniteXYPositive()
		{
			Vector2 testVec = new Vector2(float.PositiveInfinity, 0.0f);

			Assert.IsTrue(Helper.HasInfinite(testVec), "A Vector2 with the x-component set to float.PositiveInfinity should return true from the HasInfinite check.");

			testVec = new Vector2(0.0f, float.PositiveInfinity);

			Assert.IsTrue(Helper.HasInfinite(testVec), "A Vector2 with the y-component set to float.PositiveInfinity should return true from the HasInfinite check.");
		}

		[TestMethod]
		public void Vector2HasInfiniteXYNegative()
		{
			Vector2 testVec = new Vector2(float.NegativeInfinity, 0.0f);

			Assert.IsTrue(Helper.HasInfinite(testVec), "A Vector2 with the x-component set to float.NegativeInfinity should return true from the HasInfinite check.");

			testVec = new Vector2(0.0f, float.NegativeInfinity);

			Assert.IsTrue(Helper.HasInfinite(testVec), "A Vector2 with the y-component set to float.NegativeInfinity should return true from the HasInfinite check.");
		}

		[TestMethod]
		public void Vector2IsValidNormalRandomVector()
		{
			Vector2 testVec = new Vector2(Helper.RandomRange(float.MinValue, float.MaxValue), Helper.RandomRange(float.MinValue, float.MaxValue));

			Assert.IsTrue(Helper.IsValid(testVec), "A Vector2 of any three real numbers, within the float bounds, should return true from the IsValid check.");
		}

		[TestMethod]
		public void Vector2IsValidNaV()
		{
			Vector2 testVec = Helper.NaV2;

			Assert.IsFalse(Helper.IsValid(testVec), "A Vector2 set to NaV should return false from the IsValid check.");
		}

		[TestMethod]
		public void Vector2IsValidPositiveInfinityVector()
		{
			Vector2 testVec = new Vector2(float.PositiveInfinity, float.PositiveInfinity);

			Assert.IsFalse(Helper.IsValid(testVec), "A Vector2 with all elements set to float.PositiveInfinity should return false from the IsValid check.");
		}

		[TestMethod]
		public void Vector2IsValidNegativeInfinityVector()
		{
			Vector2 testVec = new Vector2(float.NegativeInfinity, float.NegativeInfinity);

			Assert.IsFalse(Helper.IsValid(testVec), "A Vector2 with all elements set to float.NegativeInfinity should return false from the IsValid check.");
		}

		[TestMethod]
		public void Vector2IsValidXYNaN()
		{
			Vector2 testVec = new Vector2(float.NaN, Helper.RandomRange(float.MinValue, float.MaxValue));

			Assert.IsFalse(Helper.IsValid(testVec), "A Vector2 with the x-component set to float.NaN should return false from the IsValid check.");

			testVec = new Vector2(Helper.RandomRange(float.MinValue, float.MaxValue), float.NaN);

			Assert.IsFalse(Helper.IsValid(testVec), "A Vector2 with the y-component set to float.NaN should return false from the IsValid check.");
		}

		[TestMethod]
		public void Vector2IsValidXYPositiveInfinity()
		{
			Vector2 testVec = new Vector2(float.PositiveInfinity, Helper.RandomRange(float.MinValue, float.MaxValue));

			Assert.IsFalse(Helper.IsValid(testVec), "A Vector2 with the x-component set to float.PositiveInfinity should return false from the IsValid check.");

			testVec = new Vector2(Helper.RandomRange(float.MinValue, float.MaxValue), float.PositiveInfinity);

			Assert.IsFalse(Helper.IsValid(testVec), "A Vector2 with the y-component set to float.PositiveInfinity should return false from the IsValid check.");
		}

		[TestMethod]
		public void Vector2IsValidXYNegativeInfinity()
		{
			Vector2 testVec = new Vector2(float.NegativeInfinity, Helper.RandomRange(float.MinValue, float.MaxValue));

			Assert.IsFalse(Helper.IsValid(testVec), "A Vector2 with the x-component set to float.NegativeInfinity should return false from the IsValid check.");

			testVec = new Vector2(Helper.RandomRange(float.MinValue, float.MaxValue), float.NegativeInfinity);

			Assert.IsFalse(Helper.IsValid(testVec), "A Vector2 with the y-component set to float.NegativeInfinity should return false from the IsValid check.");
		}

		[TestMethod]
		public void Vector2IsZeroZeroVector()
		{
			Vector2 testVec = new Vector2(0.0f, 0.0f);

			Assert.IsTrue(Helper.IsZero(testVec), "A Vector2 of { 0, 0 } should return true from the IsZero check.");
		}

		[TestMethod]
		public void Vector2IsZeroOneVector()
		{
			Vector2 testVec = new Vector2(1.0f, 1.0f);

			Assert.IsFalse(Helper.IsZero(testVec), "A Vector2 of { 1, 1 } should return false from the IsZero check.");
		}

		[TestMethod]
		public void Vector2IsZeroXYOne()
		{
			Vector2 testVec = new Vector2(1.0f, 0.0f);

			Assert.IsFalse(Helper.IsZero(testVec), "A Vector2 of { 1, 0 } should return false from the IsZero check.");

			testVec = new Vector2(0.0f, 1.0f);

			Assert.IsFalse(Helper.IsZero(testVec), "A Vector2 of { 0, 1 } should return false from the IsZero check.");
		}

		[TestMethod]
		public void Vector2IsOneZeroVector()
		{
			Vector2 testVec = new Vector2(0.0f, 0.0f);

			Assert.IsFalse(Helper.IsOne(testVec), "A Vector2 of { 0, 0 } should return false from the IsOne check.");
		}

		[TestMethod]
		public void Vector2IsOneOneVector()
		{
			Vector2 testVec = new Vector2(1.0f, 1.0f);

			Assert.IsTrue(Helper.IsOne(testVec), "A Vector2 of { 1, 1 } should return true from the IsOne check.");
		}

		[TestMethod]
		public void Vector2IsOneXYOne()
		{
			Vector2 testVec = new Vector2(1.0f, 0.0f);

			Assert.IsFalse(Helper.IsOne(testVec), "A Vector2 of { 1, 0 } should return false from the IsOne check.");

			testVec = new Vector2(0.0f, 1.0f);

			Assert.IsFalse(Helper.IsOne(testVec), "A Vector2 of { 0, 1 } should return false from the IsOne check.");
		}

		[TestMethod]
		public void Vector2IsNegativeOneZeroVector()
		{
			Vector2 testVec = new Vector2(-1.0f, -1.0f);

			Assert.IsTrue(Helper.IsNegativeOne(testVec), "A Vector2 of { -1, -1 } should return true from the IsNegativeOne check.");
		}

		[TestMethod]
		public void Vector2IsNegativeOneOneVector()
		{
			Vector2 testVec = new Vector2(1.0f, 1.0f);

			Assert.IsFalse(Helper.IsNegativeOne(testVec), "A Vector2 of { 1, 1 } should return false from the IsNegativeOne check.");
		}

		[TestMethod]
		public void Vector2IsNegativeOneXYZOne()
		{
			Vector2 testVec = new Vector2(-1.0f, 0.0f);

			Assert.IsFalse(Helper.IsNegativeOne(testVec), "A Vector2 of { -1, 0 } should return false from the IsNegativeOne check.");

			testVec = new Vector2(0.0f, -1.0f);

			Assert.IsFalse(Helper.IsNegativeOne(testVec), "A Vector2 of { 0, -1 } should return false from the IsNegativeOne check.");
		}

		[TestMethod]
		public void Vector3NotAVectorConstant()
		{
			Vector3 testVec = Helper.NaV3;

			Assert.IsTrue(float.IsNaN(testVec.x), "The x component of a Vector3 set to NaV should be float.NaN.");
			Assert.IsTrue(float.IsNaN(testVec.y), "The y component of a Vector3 set to NaV should be float.NaN.");
			Assert.IsTrue(float.IsNaN(testVec.z), "The z component of a Vector3 set to NaV should be float.NaN.");
		}

		[TestMethod]
		public void Vector3IsNaVNormalRandomVector()
		{
			Vector3 testVec = new Vector3(Helper.RandomRange(float.MinValue, float.MaxValue), Helper.RandomRange(float.MinValue, float.MaxValue), Helper.RandomRange(float.MinValue, float.MaxValue));

			Assert.IsFalse(Helper.IsNaV(testVec), "A Vector3 of any three real numbers, within the float bounds, should return false from the IsNaV check.");
		}

		[TestMethod]
		public void Vector3IsNaV()
		{
			Vector3 testVec = Helper.NaV3;

			Assert.IsTrue(Helper.IsNaV(testVec), "A Vector3 set to NaV should return true from the IsNaV check.");
		}

		[TestMethod]
		public void Vector3HasNaNNormalRandomVector()
		{
			Vector3 testVec = new Vector3(Helper.RandomRange(float.MinValue, float.MaxValue), Helper.RandomRange(float.MinValue, float.MaxValue), Helper.RandomRange(float.MinValue, float.MaxValue));

			Assert.IsFalse(Helper.HasNaN(testVec), "A Vector3 of any three real numbers, within the float bounds, should return false from the HasNaN check.");
		}

		[TestMethod]
		public void Vector3HasNaNXYZComponent()
		{
			Vector3 testVec = new Vector3(float.NaN, 0.0f, 0.0f);

			Assert.IsTrue(Helper.HasNaN(testVec), "A Vector3 with the x-component set to float.NaN should return true from the HasNaN check.");

			testVec = new Vector3(0.0f, float.NaN, 0.0f);

			Assert.IsTrue(Helper.HasNaN(testVec), "A Vector3 with the y-component set to float.NaN should return true from the HasNaN check.");

			testVec = new Vector3(0.0f, 0.0f, float.NaN);

			Assert.IsTrue(Helper.HasNaN(testVec), "A Vector3 with the z-component set to float.NaN should return true from the HasNaN check.");
		}

		[TestMethod]
		public void Vector3IsInfiniteNormalRandomVector()
		{
			Vector3 testVec = new Vector3(Helper.RandomRange(float.MinValue, float.MaxValue), Helper.RandomRange(float.MinValue, float.MaxValue), Helper.RandomRange(float.MinValue, float.MaxValue));

			Assert.IsFalse(Helper.IsInfinite(testVec), "A Vector3 of any three real numbers, within the float bounds, should return false from the IsInfinite check.");
		}

		[TestMethod]
		public void Vector3IsInfinitePositive()
		{
			Vector3 testVec = new Vector3(float.PositiveInfinity, float.PositiveInfinity, float.PositiveInfinity);

			Assert.IsTrue(Helper.IsInfinite(testVec), "A Vector3 with all elements set to float.PositiveInfinity should return true from the IsInfinite check.");
		}

		[TestMethod]
		public void Vector3IsInfiniteNegative()
		{
			Vector3 testVec = new Vector3(float.NegativeInfinity, float.NegativeInfinity, float.NegativeInfinity);

			Assert.IsTrue(Helper.IsInfinite(testVec), "A Vector3 with all elements set to float.NegativeInfinity should return true from the IsInfinite check.");
		}

		[TestMethod]
		public void Vector3HasInfiniteNormalRandomVector()
		{
			Vector3 testVec = new Vector3(Helper.RandomRange(float.MinValue, float.MaxValue), Helper.RandomRange(float.MinValue, float.MaxValue), Helper.RandomRange(float.MinValue, float.MaxValue));

			Assert.IsFalse(Helper.HasInfinite(testVec), "A Vector3 of any three real numbers, within the float bounds, should return false from the HasInfinite check.");
		}

		[TestMethod]
		public void Vector3HasInfiniteXYZPositive()
		{
			Vector3 testVec = new Vector3(float.PositiveInfinity, 0.0f, 0.0f);

			Assert.IsTrue(Helper.HasInfinite(testVec), "A Vector3 with the x-component set to float.PositiveInfinity should return true from the HasInfinite check.");

			testVec = new Vector3(0.0f, float.PositiveInfinity, 0.0f);

			Assert.IsTrue(Helper.HasInfinite(testVec), "A Vector3 with the y-component set to float.PositiveInfinity should return true from the HasInfinite check.");

			testVec = new Vector3(0.0f, 0.0f, float.PositiveInfinity);

			Assert.IsTrue(Helper.HasInfinite(testVec), "A Vector3 with the z-component set to float.PositiveInfinity should return true from the HasInfinite check.");
		}

		[TestMethod]
		public void Vector3HasInfiniteXYZNegative()
		{
			Vector3 testVec = new Vector3(float.NegativeInfinity, 0.0f, 0.0f);

			Assert.IsTrue(Helper.HasInfinite(testVec), "A Vector3 with the x-component set to float.NegativeInfinity should return true from the HasInfinite check.");

			testVec = new Vector3(0.0f, float.NegativeInfinity, 0.0f);

			Assert.IsTrue(Helper.HasInfinite(testVec), "A Vector3 with the y-component set to float.NegativeInfinity should return true from the HasInfinite check.");

			testVec = new Vector3(0.0f, 0.0f, float.NegativeInfinity);

			Assert.IsTrue(Helper.HasInfinite(testVec), "A Vector3 with the z-component set to float.NegativeInfinity should return true from the HasInfinite check.");
		}

		[TestMethod]
		public void Vector3IsValidNormalRandomVector()
		{
			Vector3 testVec = new Vector3(Helper.RandomRange(float.MinValue, float.MaxValue), Helper.RandomRange(float.MinValue, float.MaxValue), Helper.RandomRange(float.MinValue, float.MaxValue));

			Assert.IsTrue(Helper.IsValid(testVec), "A Vector3 of any three real numbers, within the float bounds, should return true from the IsValid check.");
		}

		[TestMethod]
		public void Vector3IsValidNaV()
		{
			Vector3 testVec = Helper.NaV3;

			Assert.IsFalse(Helper.IsValid(testVec), "A Vector3 set to NaV should return false from the IsValid check.");
		}

		[TestMethod]
		public void Vector3IsValidPositiveInfinityVector()
		{
			Vector3 testVec = new Vector3(float.PositiveInfinity, float.PositiveInfinity, float.PositiveInfinity);

			Assert.IsFalse(Helper.IsValid(testVec), "A Vector3 with all elements set to float.PositiveInfinity should return false from the IsValid check.");
		}

		[TestMethod]
		public void Vector3IsValidNegativeInfinityVector()
		{
			Vector3 testVec = new Vector3(float.NegativeInfinity, float.NegativeInfinity, float.NegativeInfinity);

			Assert.IsFalse(Helper.IsValid(testVec), "A Vector3 with all elements set to float.NegativeInfinity should return false from the IsValid check.");
		}

		[TestMethod]
		public void Vector3IsValidXYZNaN()
		{
			Vector3 testVec = new Vector3(float.NaN, Helper.RandomRange(float.MinValue, float.MaxValue), Helper.RandomRange(float.MinValue, float.MaxValue));

			Assert.IsFalse(Helper.IsValid(testVec), "A Vector3 with the x-component set to float.NaN should return false from the IsValid check.");

			testVec = new Vector3(Helper.RandomRange(float.MinValue, float.MaxValue), float.NaN, Helper.RandomRange(float.MinValue, float.MaxValue));

			Assert.IsFalse(Helper.IsValid(testVec), "A Vector3 with the y-component set to float.NaN should return false from the IsValid check.");

			testVec = new Vector3(Helper.RandomRange(float.MinValue, float.MaxValue), Helper.RandomRange(float.MinValue, float.MaxValue), float.NaN);

			Assert.IsFalse(Helper.IsValid(testVec), "A Vector3 with the z-component set to float.NaN should return false from the IsValid check.");
		}

		[TestMethod]
		public void Vector3IsValidXYZPositiveInfinity()
		{
			Vector3 testVec = new Vector3(float.PositiveInfinity, Helper.RandomRange(float.MinValue, float.MaxValue), Helper.RandomRange(float.MinValue, float.MaxValue));

			Assert.IsFalse(Helper.IsValid(testVec), "A Vector3 with the x-component set to float.PositiveInfinity should return false from the IsValid check.");

			testVec = new Vector3(Helper.RandomRange(float.MinValue, float.MaxValue), float.PositiveInfinity, Helper.RandomRange(float.MinValue, float.MaxValue));

			Assert.IsFalse(Helper.IsValid(testVec), "A Vector3 with the y-component set to float.PositiveInfinity should return false from the IsValid check.");

			testVec = new Vector3(Helper.RandomRange(float.MinValue, float.MaxValue), Helper.RandomRange(float.MinValue, float.MaxValue), float.PositiveInfinity);

			Assert.IsFalse(Helper.IsValid(testVec), "A Vector3 with the z-component set to float.PositiveInfinity should return false from the IsValid check.");
		}

		[TestMethod]
		public void Vector3IsValidXYZNegativeInfinity()
		{
			Vector3 testVec = new Vector3(float.NegativeInfinity, Helper.RandomRange(float.MinValue, float.MaxValue), Helper.RandomRange(float.MinValue, float.MaxValue));

			Assert.IsFalse(Helper.IsValid(testVec), "A Vector3 with the x-component set to float.NegativeInfinity should return false from the IsValid check.");

			testVec = new Vector3(Helper.RandomRange(float.MinValue, float.MaxValue), float.NegativeInfinity, Helper.RandomRange(float.MinValue, float.MaxValue));

			Assert.IsFalse(Helper.IsValid(testVec), "A Vector3 with the y-component set to float.NegativeInfinity should return false from the IsValid check.");

			testVec = new Vector3(Helper.RandomRange(float.MinValue, float.MaxValue), Helper.RandomRange(float.MinValue, float.MaxValue), float.NegativeInfinity);

			Assert.IsFalse(Helper.IsValid(testVec), "A Vector3 with the z-component set to float.NegativeInfinity should return false from the IsValid check.");
		}

		[TestMethod]
		public void Vector3IsZeroZeroVector()
		{
			Vector3 testVec = new Vector3(0.0f, 0.0f, 0.0f);

			Assert.IsTrue(Helper.IsZero(testVec), "A Vector3 of { 0, 0, 0 } should return true from the IsZero check.");
		}

		[TestMethod]
		public void Vector3IsZeroOneVector()
		{
			Vector3 testVec = new Vector3(1.0f, 1.0f, 1.0f);

			Assert.IsFalse(Helper.IsZero(testVec), "A Vector3 of { 1, 1, 1 } should return false from the IsZero check.");
		}

		[TestMethod]
		public void Vector3IsZeroXYZOne()
		{
			Vector3 testVec = new Vector3(1.0f, 0.0f, 0.0f);

			Assert.IsFalse(Helper.IsZero(testVec), "A Vector3 of { 1, 0, 0 } should return false from the IsZero check.");

			testVec = new Vector3(0.0f, 1.0f, 0.0f);

			Assert.IsFalse(Helper.IsZero(testVec), "A Vector3 of { 0, 1, 0 } should return false from the IsZero check.");

			testVec = new Vector3(0.0f, 0.0f, 1.0f);

			Assert.IsFalse(Helper.IsZero(testVec), "A Vector3 of { 0, 0, 1 } should return false from the IsZero check.");
		}

		[TestMethod]
		public void Vector3IsOneZeroVector()
		{
			Vector3 testVec = new Vector3(0.0f, 0.0f, 0.0f);

			Assert.IsFalse(Helper.IsOne(testVec), "A Vector3 of { 0, 0, 0 } should return false from the IsOne check.");
		}

		[TestMethod]
		public void Vector3IsOneOneVector()
		{
			Vector3 testVec = new Vector3(1.0f, 1.0f, 1.0f);

			Assert.IsTrue(Helper.IsOne(testVec), "A Vector3 of { 1, 1, 1 } should return true from the IsOne check.");
		}

		[TestMethod]
		public void Vector3IsOneXYZOne()
		{
			Vector3 testVec = new Vector3(1.0f, 0.0f, 0.0f);

			Assert.IsFalse(Helper.IsOne(testVec), "A Vector3 of { 1, 0, 0 } should return false from the IsOne check.");

			testVec = new Vector3(0.0f, 1.0f, 0.0f);

			Assert.IsFalse(Helper.IsOne(testVec), "A Vector3 of { 0, 1, 0 } should return false from the IsOne check.");

			testVec = new Vector3(0.0f, 0.0f, 1.0f);

			Assert.IsFalse(Helper.IsOne(testVec), "A Vector3 of { 0, 0, 1 } should return false from the IsOne check.");
		}

		[TestMethod]
		public void Vector3IsNegativeOneZeroVector()
		{
			Vector3 testVec = new Vector3(-1.0f, -1.0f, -1.0f);

			Assert.IsTrue(Helper.IsNegativeOne(testVec), "A Vector3 of { -1, -1, -1 } should return true from the IsNegativeOne check.");
		}

		[TestMethod]
		public void Vector3IsNegativeOneOneVector()
		{
			Vector3 testVec = new Vector3(1.0f, 1.0f, 1.0f);

			Assert.IsFalse(Helper.IsNegativeOne(testVec), "A Vector3 of { 1, 1, 1 } should return false from the IsNegativeOne check.");
		}

		[TestMethod]
		public void Vector3IsNegativeOneXYZOne()
		{
			Vector3 testVec = new Vector3(-1.0f, 0.0f, 0.0f);

			Assert.IsFalse(Helper.IsNegativeOne(testVec), "A Vector3 of { -1, 0, 0 } should return false from the IsNegativeOne check.");

			testVec = new Vector3(0.0f, -1.0f, 0.0f);

			Assert.IsFalse(Helper.IsNegativeOne(testVec), "A Vector3 of { 0, -1, 0 } should return false from the IsNegativeOne check.");

			testVec = new Vector3(0.0f, 0.0f, -1.0f);

			Assert.IsFalse(Helper.IsNegativeOne(testVec), "A Vector3 of { 0, 0, -1 } should return false from the IsNegativeOne check.");
		}

		[TestMethod]
		public void RadiansToDegrees()
		{
			double degrees = Helper.RadiansToDegrees(Helper.Pi);

			Assert.AreEqual(180.0, degrees, 0.001, "An angle of Pi radians should convert into an angle of 180 degrees.");
		}

		[TestMethod]
		public void DegreesToRadians()
		{
			double radians = Helper.DegreesToRadians(180.0);

			Assert.AreEqual(Helper.Pi, radians, 0.001, "An angle of 180 degrees should convert into an angle of Pi radians.");
		}

		[TestMethod]
		public void ModulateAngleRadians()
		{
			double radians = Helper.ModulateAngleRadians((4.5 * Helper.Pi));

			Assert.AreEqual(Helper.Pi / 2.0, radians, 0.001, "An angle of 4.5 Pi radians should modulate into an angle of Pi/2 radians.");
		}

		[TestMethod]
		public void ModulateAngleRadiansNegative()
		{
			double radians = Helper.ModulateAngleRadians((-4.5 * Helper.Pi));

			Assert.AreEqual(-Helper.Pi / 2.0, radians, 0.001, "An angle of -4.5 Pi radians should modulate into an angle of -Pi/2 radians.");
		}

		[TestMethod]
		public void ModulateAngleDegrees()
		{
			double degrees = Helper.ModulateAngleDegrees(765.0);

			Assert.AreEqual(45.0, degrees, "An angle of 765 degrees should modulate into an angle of 45 degrees.");
		}

		[TestMethod]
		public void ModulateAngleDegreesNegative()
		{
			double degrees = Helper.ModulateAngleDegrees(-765.0);

			Assert.AreEqual(-45.0, degrees, "An angle of -765 degrees should modulate into an angle of -45 degrees.");
		}

		[TestMethod]
		public void IntersectionPointSimple()
		{
			Vector2 intersectionPoint = Helper.IntersectionPoint(new Vector2(0.0f, 1.0f), new Vector2(0.0f, -1.0f), new Vector2(-1.0f, 0.0f), new Vector2(1.0f, 0.0f));

			Assert.AreEqual(new Vector2(0.0f, 0.0f), intersectionPoint, "The intersection point of the two line segments { { 0.0f, 1.0f } -> { 0.0f, -1.0f } } and { { -1.0f, 0.0f } -> { 1.0f, 0.0f } } should be { 0.0f, 0.0f }");
		}

		[TestMethod]
		public void IntersectionPointCross()
		{
			Vector2 intersectionPoint = Helper.IntersectionPoint(new Vector2(1.0f, 1.0f), new Vector2(-1.0f, -1.0f), new Vector2(-1.0f, 1.0f), new Vector2(1.0f, -1.0f));

			Assert.AreEqual(new Vector2(0.0f, 0.0f), intersectionPoint, "The intersection point of the two line segments { { 1.0f, 1.0f } -> { -1.0f, -1.0f } } and { { -1.0f, 1.0f } -> { 1.0f, -1.0f } } should be { 0.0f, 0.0f }");
		}

		[TestMethod]
		public void IntersectionPointParallelVertical()
		{
			Vector2 intersectionPoint = Helper.IntersectionPoint(new Vector2(1.0f, 1.0f), new Vector2(1.0f, -1.0f), new Vector2(-1.0f, 1.0f), new Vector2(-1.0f, -1.0f));

			Assert.IsTrue(Helper.IsNaV(intersectionPoint), "The two line segments { { 1.0f, 1.0f } -> { 1.0f, -1.0f } } and { { -1.0f, 1.0f } -> { -1.0f, -1.0f } } should not intersect.");
		}

		[TestMethod]
		public void IntersectionPointParallelHorizontal()
		{
			Vector2 intersectionPoint = Helper.IntersectionPoint(new Vector2(1.0f, 1.0f), new Vector2(-1.0f, 1.0f), new Vector2(-1.0f, -1.0f), new Vector2(1.0f, -1.0f));

			Assert.IsTrue(Helper.IsNaV(intersectionPoint), "The two line segments { { 1.0f, 1.0f } -> { -1.0f, 1.0f } } and { { -1.0f, -1.0f } -> { 1.0f, -1.0f } } should not intersect.");
		}

		[TestMethod]
		public void IntersectionPointOverlapParallelVertical()
		{
			Vector2 intersectionPoint = Helper.IntersectionPoint(new Vector2(0.0f, 0.5f), new Vector2(0.0f, -1.0f), new Vector2(0.0f, -0.5f), new Vector2(0.0f, 1.0f));

			Assert.IsTrue(Helper.IsNaV(intersectionPoint), "The two line segments { { 0.0f, 0.5f } -> { 0.0f, -1.0f } } and { { 0.0f, -0.5f } -> { 0.0f, 1.0f } } should not intersect.");
		}

		[TestMethod]
		public void IntersectionPointOverlapParallelHorizontal()
		{
			Vector2 intersectionPoint = Helper.IntersectionPoint(new Vector2(0.5f, 0.0f), new Vector2(-1.0f, 0.0f), new Vector2(-0.5f, 0.0f), new Vector2(1.0f, 0.0f));

			Assert.IsTrue(Helper.IsNaV(intersectionPoint), "The two line segments { { 0.5f, 0.0f } -> { -1.0f, 0.0f } } and { { -0.5f, 0.0f } -> { 1.0f, 0.0f } } should not intersect.");
		}

		[TestMethod]
		public void IntersectionPointLine1TooFarRight()
		{
			Vector2 intersectionPoint = Helper.IntersectionPoint(new Vector2(2.0f, -1.0f), new Vector2(2.0f, 1.0f), new Vector2(-0.1f, -1.0f), new Vector2(0.1f, 1.0f));

			Assert.IsTrue(Helper.IsNaV(intersectionPoint), "The two line segments { { 5.0f, -1.0f } -> { 5.0f, -1.0f } } and { { -0.1f, 1.0f } -> { 0.1f, -1.0f } } should not intersect.");
		}

		[TestMethod]
		public void IntersectionPointLine1TooFarLeft()
		{
			Vector2 intersectionPoint = Helper.IntersectionPoint(new Vector2(-2.0f, -1.0f), new Vector2(-2.0f, 1.0f), new Vector2(-0.1f, -1.0f), new Vector2(0.1f, 1.0f));

			Assert.IsTrue(Helper.IsNaV(intersectionPoint), "The two line segments { { 5.0f, -1.0f } -> { 5.0f, -1.0f } } and { { -0.1f, 1.0f } -> { 0.1f, -1.0f } } should not intersect.");
		}

		[TestMethod]
		public void IntersectionPointLine1TooFarUp()
		{
			Vector2 intersectionPoint = Helper.IntersectionPoint(new Vector2(-1.0f, 2.0f), new Vector2(1.0f, 2.0f), new Vector2(-1.0f, 0.0f), new Vector2(1.0f, 0.0f));

			Assert.IsTrue(Helper.IsNaV(intersectionPoint), "The two line segments { { -1.0f, -2.0f } -> { 1.0f, -2.0f } } and { { -1.0f, 0.0f } -> { 1.0f, 0.0f } } should not intersect.");
		}

		[TestMethod]
		public void IntersectionPointLine1TooFarDown()
		{
			Vector2 intersectionPoint = Helper.IntersectionPoint(new Vector2(-1.0f, -2.0f), new Vector2(1.0f, -2.0f), new Vector2(-1.0f, 0.0f), new Vector2(1.0f, 0.0f));

			Assert.IsTrue(Helper.IsNaV(intersectionPoint), "The two line segments { { -1.0f, -2.0f } -> { 1.0f, -2.0f } } and { { -1.0f, 0.0f } -> { 1.0f, 0.0f } } should not intersect.");
		}

		[TestMethod]
		public void IntersectionPointLine2TooFarRight()
		{
			Vector2 intersectionPoint = Helper.IntersectionPoint(new Vector2(-0.1f, -1.0f), new Vector2(0.1f, 1.0f), new Vector2(2.0f, -1.0f), new Vector2(2.0f, 1.0f));

			Assert.IsTrue(Helper.IsNaV(intersectionPoint), "The two line segments { { -0.1f, -1.0f } -> { 0.1f, 1.0f } } and { { 2.0f, -1.0f } -> { 2.0f, 1.0f } } should not intersect.");
		}

		[TestMethod]
		public void IntersectionPointLine2TooFarLeft()
		{
			Vector2 intersectionPoint = Helper.IntersectionPoint(new Vector2(-0.1f, -1.0f), new Vector2(0.1f, 1.0f), new Vector2(-2.0f, -1.0f), new Vector2(-2.0f, 1.0f));

			Assert.IsTrue(Helper.IsNaV(intersectionPoint), "The two line segments { { -0.1f, -1.0f } -> { 0.1f, 1.0f } } and { { -2.0f, -1.0f } -> { -2.0f, 1.0f } } should not intersect.");
		}

		[TestMethod]
		public void IntersectionPointLine2TooFarUp()
		{
			Vector2 intersectionPoint = Helper.IntersectionPoint(new Vector2(-1.0f, -0.1f), new Vector2(1.0f, 0.1f), new Vector2(-1.0f, 2.0f), new Vector2(1.0f, 2.0f));

			Assert.IsTrue(Helper.IsNaV(intersectionPoint), "The two line segments { { -1.0f, -0.1f } -> { 1.0f, 0.1f } } and { { -1.0f, 2.0f } -> { 1.0f, 2.0f } } should not intersect.");
		}

		[TestMethod]
		public void IntersectionPointLine2TooFarDown()
		{
			Vector2 intersectionPoint = Helper.IntersectionPoint(new Vector2(-1.0f, -0.1f), new Vector2(1.0f, 0.1f), new Vector2(-1.0f, -2.0f), new Vector2(1.0f, -2.0f));

			Assert.IsTrue(Helper.IsNaV(intersectionPoint), "The two line segments { { -1.0f, -0.1f } -> { 1.0f, 0.1f } } and { { -1.0f, -2.0f } -> { 1.0f, -2.0f } } should not intersect.");
		}

		[TestMethod]
		public void IntersectionPointSimpleFloats()
		{
			Vector2 intersectionPoint = Helper.IntersectionPoint(new Vector2(0.0f, 1.0f), new Vector2(0.0f, -1.0f), new Vector2(-1.0f, 0.0f), new Vector2(1.0f, 0.0f));

			Assert.AreEqual(Vector2.zero, intersectionPoint, "The intersection point of the two line segments { { 0.0f, 1.0f } -> { 0.0f, -1.0f } } and { { -1.0f, 0.0f } -> { 1.0f, 0.0f } } should be { 0.0f, 0.0f }");
		}

		[TestMethod]
		public void IntersectionPointCrossFloats()
		{
			Vector2 intersectionPoint = Helper.IntersectionPoint(1.0f, 1.0f, -1.0f, -1.0f, -1.0f, 1.0f, 1.0f, -1.0f);

			Assert.AreEqual(Vector2.zero, intersectionPoint, "The intersection point of the two line segments { { 1.0f, 1.0f } -> { -1.0f, -1.0f } } and { { -1.0f, 1.0f } -> { 1.0f, -1.0f } } should be { 0.0f, 0.0f }");
		}

		[TestMethod]
		public void IntersectionPointParallelVerticalFloats()
		{
			Vector2 intersectionPoint = Helper.IntersectionPoint(1.0f, 1.0f, 1.0f, -1.0f, -1.0f, 1.0f, -1.0f, -1.0f);

			Assert.IsTrue(Helper.IsNaV(intersectionPoint), "The two line segments { { 1.0f, 1.0f } -> { 1.0f, -1.0f } } and { { -1.0f, 1.0f } -> { -1.0f, -1.0f } } should not intersect.");
		}

		[TestMethod]
		public void IntersectionPointParallelHorizontalFloats()
		{
			Vector2 intersectionPoint = Helper.IntersectionPoint(1.0f, 1.0f, -1.0f, 1.0f, -1.0f, -1.0f, 1.0f, -1.0f);

			Assert.IsTrue(Helper.IsNaV(intersectionPoint), "The two line segments { { 1.0f, 1.0f } -> { -1.0f, 1.0f } } and { { -1.0f, -1.0f } -> { 1.0f, -1.0f } } should not intersect.");
		}

		[TestMethod]
		public void AngleBetweenNormalVectorsAsRadiansAndLowerBoundsAsZero()
		{
			double radians = Helper.AngleBetween(new Vector2(0.0f, 1.0f), new Vector2(1.0f, 0.0f), true, true);
			Assert.AreEqual(Helper.Pi / 2.0, radians, 0.001, "The angle between { 0, 1 } and { 1, 0 } should be an angle of Pi/2 radians, with a lower bound of zero.");
		}

		[TestMethod]
		public void AngleBetweenNormalVectorsAsDegreesAndLowerBoundsAsZero()
		{
			double degrees = Helper.AngleBetween(new Vector2(0.0f, 1.0f), new Vector2(1.0f, 0.0f), false, true);
			Assert.AreEqual(90.0, degrees, 0.001, "The angle between { 0, 1 } and { 1, 0 } should be an angle of 90 degrees, with a lower bound of zero.");
		}

		[TestMethod]
		public void AngleBetweenNormalVectorsAsRadiansAndNotLowerBoundsAsZero()
		{
			double radians = Helper.AngleBetween(new Vector2(0.0f, 1.0f), new Vector2(-1.0f, 0.0f), true, false);
			Assert.AreEqual(-Helper.Pi / 2.0, radians, 0.001, "The angle between { 0, 1 } and { -1, 0 } should be an angle of -Pi/2 radians, without a lower bound of zero.");
		}

		[TestMethod]
		public void AngleBetweenNormalVectorsAsDegreesAndNotLowerBoundsAsZero()
		{
			double degrees = Helper.AngleBetween(new Vector2(0.0f, 1.0f), new Vector2(-1.0f, 0.0f), false, false);
			Assert.AreEqual(-90.0, degrees, 0.001, "The angle between { 0, 1 } and { -1, 0 } should be an angle of -90 degrees, without a lower bound of zero.");
		}

		[TestMethod]
		public void AngleBetweenNormalVectorsGreaterThan180DegreesShouldntBeNegative()
		{
			double degrees = Helper.AngleBetween(new Vector2(0.0f, 1.0f), new Vector2(-1.0f, 0.0f), false, true);
			Assert.AreEqual(270.0, degrees, 0.001, "The angle between { 0, 1 } and { -1, 0 } should be an angle of 270 degrees, with a lower bound of zero.");
		}

		[TestMethod]
		public void AngleBetweenNonNormalisedVectorsAsDegreesAndNotLowerBoundsAsZero()
		{
			double degrees = Helper.AngleBetween(new Vector2(0.0f, 2.0f), new Vector2(-3.0f, 0.0f), false, false);
			Assert.AreEqual(-90.0, degrees, 0.001, "The angle between { 0, 2 } and { -3, 0 } should be an angle of -90 degrees, without a lower bound of zero.");
		}

		[TestMethod]
		public void AngleBetweenInvalidVectors()
		{
			double degrees = Helper.AngleBetween(Helper.NaV2, new Vector2(0.0f, 0.0f), false, false);
			Assert.IsTrue(double.IsNaN(degrees), "The angle returned from the calculation if the first vector is not valid should be double.NaN.");

			degrees = Helper.AngleBetween(new Vector2(0.0f, 0.0f), Helper.NaV2, false, false);
			Assert.IsTrue(double.IsNaN(degrees), "The angle returned from the calculation if the second vector is not valid should be double.NaN.");

			degrees = Helper.AngleBetween(Helper.NaV2, Helper.NaV2, false, false);
			Assert.IsTrue(double.IsNaN(degrees), "The angle returned from the calculation if both vectors are not valid should be double.NaN.");
		}

		[TestMethod]
		public void Vector2HalfwayBetweenNormalisedVectors()
		{
			Vector2 halfway = Helper.HalfwayBetween(new Vector2(0.0f, 1.0f), new Vector2(1.0f, 0.0f));
			Assert.AreEqual(Helper.OneOverRootTwoF, halfway.x, 0.001, "The x-component of the normalised vector halfway between { 0.0f, 1.0f } and { 1.0f, 0.0f } should be equal to 1/√2.");
			Assert.AreEqual(Helper.OneOverRootTwoF, halfway.y, 0.001, "The y-component of the normalised vector halfway between { 0.0f, 1.0f } and { 1.0f, 0.0f } should be equal to 1/√2.");
		}

		[TestMethod]
		public void Vector2HalfwayBetweenNonNormalisedVectors()
		{
			Vector2 halfway = Helper.HalfwayBetween(new Vector2(0.0f, 7.0f), new Vector2(2.0f, 0.0f));
			Assert.AreEqual(Helper.OneOverRootTwoF, halfway.x, 0.001, "The x-component of the normalised vector halfway between { 0.0f, 7.0f } and { 2.0f, 0.0f } should be equal to 1/√2.");
			Assert.AreEqual(Helper.OneOverRootTwoF, halfway.y, 0.001, "The y-component of the normalised vector halfway between { 0.0f, 7.0f } and { 2.0f, 0.0f } should be equal to 1/√2.");
		}

		[TestMethod]
		public void Vector2HalfwayBetweenInvalidVectors()
		{
			Vector2 halfway = Helper.HalfwayBetween(Helper.NaV2, Helper.NaV2);
			Assert.IsTrue(float.IsNaN(halfway.x), "The x-component of the normalised vector halfway between { float.NaN, float.NaN } and { float.NaN, float.NaN } should be equal to float.NaN, but was: " + halfway.x + ".");
			Assert.IsTrue(float.IsNaN(halfway.y), "The y-component of the normalised vector halfway between { float.NaN, float.NaN } and { float.NaN, float.NaN } should be equal to float.NaN, but was: " + halfway.y + ".");

			halfway = Helper.HalfwayBetween(new Vector2(0.0f, float.NaN), new Vector2(float.NaN, 0.0f));
			Assert.IsTrue(float.IsNaN(halfway.x), "The x-component of the normalised vector halfway between { 0.0f, float.NaN } and { float.NaN, 0.0f } should be equal to float.NaN, but was: " + halfway.x + ".");
			Assert.IsTrue(float.IsNaN(halfway.y), "The y-component of the normalised vector halfway between { 0.0f, float.NaN } and { float.NaN, 0.0f } should be equal to float.NaN, but was: " + halfway.y + ".");
		}

		[TestMethod]
		public void Vector3HalfwayBetweenNormalisedVectors()
		{
			Vector3 halfway = Helper.HalfwayBetween(new Vector3(0.0f, 1.0f, 0.0f), new Vector3(1.0f, 0.0f, 0.0f));
			Assert.AreEqual(Helper.OneOverRootTwoF, halfway.x, 0.001, "The x-component of the normalised vector halfway between { 0.0f, 1.0f, 0.0f } and { 1.0f, 0.0f, 0.0f } should be equal to 1/√2.");
			Assert.AreEqual(Helper.OneOverRootTwoF, halfway.y, 0.001, "The y-component of the normalised vector halfway between { 0.0f, 1.0f, 0.0f } and { 1.0f, 0.0f, 0.0f } should be equal to 1/√2.");
			Assert.AreEqual(0.0f, halfway.z, 0.001, "The z-component of the normalised vector halfway between { 0.0f, 1.0f, 0.0f } and { 1.0f, 0.0f, 0.0f } should be equal to zero.");
		}

		[TestMethod]
		public void Vector3HalfwayBetweenNonNormalisedVectors()
		{
			Vector3 halfway = Helper.HalfwayBetween(new Vector3(0.0f, 7.0f, 0.0f), new Vector3(2.0f, 0.0f, 0.0f));
			Assert.AreEqual(Helper.OneOverRootTwoF, halfway.x, 0.001, "The x-component of the normalised vector halfway between { 0.0f, 7.0f, 0.0f } and { 2.0f, 0.0f, 0.0f } should be equal to 1/√2.");
			Assert.AreEqual(Helper.OneOverRootTwoF, halfway.y, 0.001, "The y-component of the normalised vector halfway between { 0.0f, 7.0f, 0.0f } and { 2.0f, 0.0f, 0.0f } should be equal to 1/√2.");
			Assert.AreEqual(0.0f, halfway.z, 0.001, "The z-component of the normalised vector halfway between { 0.0f, 7.0f, 0.0f } and { 2.0f, 0.0f, 0.0f } should be equal to zero.");
		}

		[TestMethod]
		public void Vector3HalfwayBetweenInvalidVectors()
		{
			Vector3 halfway = Helper.HalfwayBetween(Helper.NaV3, Helper.NaV3);
			Assert.IsTrue(float.IsNaN(halfway.x), "The x-component of the normalised vector halfway between { float.NaN, float.NaN, float.NaN } and { float.NaN, float.NaN, float.NaN } should be equal to float.NaN, but was: " + halfway.x + ".");
			Assert.IsTrue(float.IsNaN(halfway.y), "The y-component of the normalised vector halfway between { float.NaN, float.NaN, float.NaN } and { float.NaN, float.NaN, float.NaN } should be equal to float.NaN, but was: " + halfway.y + ".");
			Assert.IsTrue(float.IsNaN(halfway.z), "The y-component of the normalised vector halfway between { float.NaN, float.NaN, float.NaN } and { float.NaN, float.NaN, float.NaN } should be equal to float.NaN, but was: " + halfway.z + ".");

			halfway = Helper.HalfwayBetween(new Vector3(0.0f, float.NaN, 0.0f), new Vector3(float.NaN, 0.0f, 0.0f));
			Assert.IsTrue(float.IsNaN(halfway.x), "The x-component of the normalised vector halfway between { 0.0f, float.NaN, 0.0f } and { float.NaN, 0.0f, 0.0f } should be equal to float.NaN, but was: " + halfway.x + ".");
			Assert.IsTrue(float.IsNaN(halfway.y), "The y-component of the normalised vector halfway between { 0.0f, float.NaN, 0.0f } and { float.NaN, 0.0f, 0.0f } should be equal to float.NaN, but was: " + halfway.y + ".");
			Assert.IsTrue(float.IsNaN(halfway.z), "The y-component of the normalised vector halfway between { 0.0f, float.NaN, 0.0f } and { float.NaN, 0.0f, 0.0f } should be equal to float.NaN, but was: " + halfway.z + ".");
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentOutOfRangeException))]
		public void RandomIntInvalidRange()
		{
			int rand = Helper.RandomRange(10, 0);
		}

		[TestMethod]
		public void RandomIntValidRange()
		{
			int rand = Helper.RandomRange(0, 10);

			Assert.IsTrue(rand < 10, "The random number generated between 0 -> 10 should be less than 10, but was: " + rand + ".");
			Assert.IsTrue(rand >= 0, "The random number generated between 0 -> 10 should be greater than or equal to 0, but was: " + rand + ".");
		}

		[TestMethod]
		public void RandomFloatInvalidRangeMinGreaterThanMax()
		{
			float rand = Helper.RandomRange(10.0f, 0.0f);
			Assert.IsTrue(float.IsNaN(rand), "The random number generated between 10.0f -> 0.0f should be float.NaN, but was: " + rand + ".");
		}

		[TestMethod]
		public void RandomFloatInvalidRangeMinMaxNaN()
		{
			float rand = Helper.RandomRange(0.0f, float.NaN);
			Assert.IsTrue(float.IsNaN(rand), "The random number generated between 0.0f -> float.NaN should be float.NaN, but was: " + rand + ".");
			rand = Helper.RandomRange(float.NaN, 0.0f);
			Assert.IsTrue(float.IsNaN(rand), "The random number generated between float.NaN -> 0.0f should be float.NaN, but was: " + rand + ".");
		}

		[TestMethod]
		public void RandomFloatInvalidRangeMinMaxInfinity()
		{
			float rand = Helper.RandomRange(0.0f, float.PositiveInfinity);
			Assert.IsTrue(float.IsNaN(rand), "The random number generated between 0.0f -> float.PositiveInfinity should be float.NaN, but was: " + rand + ".");
			rand = Helper.RandomRange(float.PositiveInfinity, 0.0f);
			Assert.IsTrue(float.IsNaN(rand), "The random number generated between float.PositiveInfinity -> 0.0f should be float.NaN, but was: " + rand + ".");
			rand = Helper.RandomRange(0.0f, float.NegativeInfinity);
			Assert.IsTrue(float.IsNaN(rand), "The random number generated between 0.0f -> float.NegativeInfinity should be float.NaN, but was: " + rand + ".");
			rand = Helper.RandomRange(float.NegativeInfinity, 0.0f);
			Assert.IsTrue(float.IsNaN(rand), "The random number generated between float.NegativeInfinity -> 0.0f should be float.NaN, but was: " + rand + ".");
		}

		[TestMethod]
		public void RandomFloatValidRange()
		{
			float rand = Helper.RandomRange(0.0f, 10.0f);

			Assert.IsTrue(rand < 10.0f, "The random number generated between 0.0f -> 10.0f should be less than 10.0f, but was: " + rand + ".");
			Assert.IsTrue(rand >= 0.0f, "The random number generated between 0.0f -> 10.0f should be greater than or equal to 0.0f, but was: " + rand + ".");
		}

		[TestMethod]
		public void RandomDoubleInvalidRangeMinGreaterThanMax()
		{
			double rand = Helper.RandomRange(10.0, 0.0);
			Assert.IsTrue(double.IsNaN(rand), "The random number generated between 10.0 -> 0.0 should be double.NaN, but was: " + rand + ".");
		}

		[TestMethod]
		public void RandomDoubleInvalidRangeMinMaxNaN()
		{
			double rand = Helper.RandomRange(0.0, double.NaN);
			Assert.IsTrue(double.IsNaN(rand), "The random number generated between 0.0 -> double.NaN should be double.NaN, but was: " + rand + ".");
			rand = Helper.RandomRange(double.NaN, 0.0);
			Assert.IsTrue(double.IsNaN(rand), "The random number generated between double.NaN -> 0.0 should be double.NaN, but was: " + rand + ".");
		}

		[TestMethod]
		public void RandomDoubleInvalidRangeMinMaxInfinity()
		{
			double rand = Helper.RandomRange(0.0, double.PositiveInfinity);
			Assert.IsTrue(double.IsNaN(rand), "The random number generated between 0.0 -> double.PositiveInfinity should be double.NaN, but was: " + rand + ".");
			rand = Helper.RandomRange(double.PositiveInfinity, 0.0);
			Assert.IsTrue(double.IsNaN(rand), "The random number generated between double.PositiveInfinity -> 0.0 should be double.NaN, but was: " + rand + ".");
			rand = Helper.RandomRange(0.0, double.NegativeInfinity);
			Assert.IsTrue(double.IsNaN(rand), "The random number generated between 0.0 -> double.NegativeInfinity should be double.NaN, but was: " + rand + ".");
			rand = Helper.RandomRange(double.NegativeInfinity, 0.0);
			Assert.IsTrue(double.IsNaN(rand), "The random number generated between double.NegativeInfinity -> 0.0 should be double.NaN, but was: " + rand + ".");
		}

		[TestMethod]
		public void RandomDoubleValidRange()
		{
			double rand = Helper.RandomRange(0.0, 10.0);

			Assert.IsTrue(rand < 10.0, "The random number generated between 0.0 -> 10.0 should be less than 10.0, but was: " + rand + ".");
			Assert.IsTrue(rand >= 0.0, "The random number generated between 0.0 -> 10.0 should be greater than or equal to 0.0, but was: " + rand + ".");
		}

		[TestMethod]
		public void MonotoneChainComparer()
		{
			List<Vector2> points = new List<Vector2>();

			points.Add(new Vector2(2.0f, 2.0f));
			points.Add(new Vector2(2.0f, 4.0f));
			points.Add(new Vector2(2.0f, 0.0f));
			points.Add(new Vector2(0.0f, 3.0f));
			points.Add(new Vector2(7.0f, 0.0f));

			points.Sort(new MonotoneChainConvexHullComparer());

			Assert.AreEqual(new Vector2(0.0f, 3.0f), points[0], "The 1st element in the sorted List should be { 0.0f, 3.0f }.");
			Assert.AreEqual(new Vector2(2.0f, 0.0f), points[1], "The 2nd element in the sorted List should be { 2.0f, 0.0f }.");
			Assert.AreEqual(new Vector2(2.0f, 2.0f), points[2], "The 3rd element in the sorted List should be { 2.0f, 2.0f }.");
			Assert.AreEqual(new Vector2(2.0f, 4.0f), points[3], "The 4th element in the sorted List should be { 2.0f, 4.0f }.");
			Assert.AreEqual(new Vector2(7.0f, 0.0f), points[4], "The 5th element in the sorted List should be { 7.0f, 0.0f }.");
		}

		[TestMethod]
		public void ConvexHullValidList()
		{
			List<Vector2> points = new List<Vector2>();

			points.Add(new Vector2(2.0f, 2.0f));
			points.Add(new Vector2(3.0f, 3.0f));
			points.Add(new Vector2(4.0f, 4.0f));
			points.Add(new Vector2(5.0f, 5.0f));
			points.Add(new Vector2(6.0f, 6.0f));
			points.Add(new Vector2(7.0f, 7.0f));
			points.Add(new Vector2(8.0f, 8.0f));
			points.Add(new Vector2(9.0f, 9.0f));
			points.Add(new Vector2(9.0f, 2.0f));
			points.Add(new Vector2(2.0f, 9.0f));

			HashSet<Vector2> convexHull = new HashSet<Vector2>(Helper.ConvexHull(points));
			Assert.AreEqual(4, convexHull.Count, "The convex hull should only have 4 points.");
			Assert.IsTrue(convexHull.Contains(new Vector2(2.0f, 2.0f)), "The convex hull of the supplied points should contain { 2.0f, 2.0f }.");
			Assert.IsTrue(convexHull.Contains(new Vector2(9.0f, 9.0f)), "The convex hull of the supplied points should contain { 9.0f, 9.0f }.");
			Assert.IsTrue(convexHull.Contains(new Vector2(2.0f, 9.0f)), "The convex hull of the supplied points should contain { 2.0f, 9.0f }.");
			Assert.IsTrue(convexHull.Contains(new Vector2(9.0f, 2.0f)), "The convex hull of the supplied points should contain { 9.0f, 2.0f }.");
		}

		[TestMethod]
		public void ConvexHullNullList()
		{
			List<Vector2> convexHull = Helper.ConvexHull(null);
			Assert.AreEqual(0, convexHull.Count, "The convex hull of a null list should have no points.");
		}

		[TestMethod]
		public void ConvexHullEmptyList()
		{
			List<Vector2> points = new List<Vector2>();

			List<Vector2> convexHull = Helper.ConvexHull(points);
			Assert.AreEqual(0, convexHull.Count, "The convex hull of an empty list should have no points.");
		}

		[TestMethod]
		public void ConvexHullOnePointList()
		{
			List<Vector2> points = new List<Vector2>();

			points.Add(new Vector2(2.0f, 2.0f));

			HashSet<Vector2> convexHull = new HashSet<Vector2>(Helper.ConvexHull(points));
			Assert.AreEqual(1, convexHull.Count, "The convex hull should only have 1 point.");
			Assert.IsTrue(convexHull.Contains(new Vector2(2.0f, 2.0f)), "The convex hull of the supplied point should contain { 2.0f, 2.0f }.");
		}
	}
}
