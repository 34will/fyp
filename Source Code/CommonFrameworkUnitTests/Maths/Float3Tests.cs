﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CommonFramework.Maths;

namespace CommonFrameworkUnitTests
{
    [TestClass]
    public class Float3Tests
    {
        [TestMethod]
        public void ConstructorEmpty()
        {
            Float3 vector = new Float3();
            Assert.AreEqual(0.0f, vector.X);
            Assert.AreEqual(0.0f, vector.Y);
            Assert.AreEqual(0.0f, vector.Z);
        }

        [TestMethod]
        public void ConstructorFloats()
        {
            Float3 vector = new Float3(1.2f, 2.3f, 3.6f);
            Assert.AreEqual(1.2f, vector.X);
            Assert.AreEqual(2.3f, vector.Y);
            Assert.AreEqual(3.6f, vector.Z);
        }

        [TestMethod]
        public void SetDimension()
        {
            // Prepare vector
            Float3 vector = new Float3(1.2f, 2.3f, 3.6f);
            Assert.AreEqual(1.2f, vector.X);
            Assert.AreEqual(2.3f, vector.Y);
            Assert.AreEqual(3.6f, vector.Z);

            // Change the dimension
            vector.SetDimension(0, 2.2f);
            vector.SetDimension(1, 3.3f);
            vector.SetDimension(2, 9.4f);
            Assert.AreEqual(2.2f, vector.X, "The value set on dimension zero not expected");
            Assert.AreEqual(3.3f, vector.Y, "The value set on dimension one not expected");
            Assert.AreEqual(9.4f, vector.Z, "The value set on dimension two not expected");
        }

        [TestMethod]
        public void GetDimension()
        {
            // Prepare vector
            Float3 vector = new Float3(1.2f, 2.3f, 3.6f);
            Assert.AreEqual(1.2f, vector.X);
            Assert.AreEqual(2.3f, vector.Y);
            Assert.AreEqual(3.6f, vector.Z);

            // Change the dimension
            Assert.AreEqual(1.2f, vector.GetDimension(0), "The value got on dimension zero not expected");
            Assert.AreEqual(2.3f, vector.GetDimension(1), "The value got on dimension one not expected");
            Assert.AreEqual(3.6f, vector.GetDimension(2), "The value got on dimension two not expected");
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void SetInvalidDimension()
        {
            Float3 vector = new Float3(1.2f, 2.3f, 3.6f);
            vector.SetDimension(3, 2.0f);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetInvalidDimension()
        {
            Float3 vector = new Float3(1.2f, 2.3f, 3.6f);
            float value = vector.GetDimension(3);
        }

        [TestMethod]
        public void Properties()
        {
            Float3 vector = new Float3(1.2f, 2.3f, 3.5f);
            Assert.AreEqual(1.2f, vector.X);
            Assert.AreEqual(2.3f, vector.Y);
            Assert.AreEqual(3.5f, vector.Z);

            // Use setters to modify values
            vector.X = 3.4f;
            vector.Y = 6.2f;
            vector.Z = 6.7f;

            // Check the changed values
            Assert.AreEqual(3.4f, vector.X, "The value set on the X component did not change value returned by getter");
            Assert.AreEqual(6.2f, vector.Y, "The value set on the Y component did not change value returned by getter");
            Assert.AreEqual(6.7f, vector.Z, "The value set on the Z component did not change value returned by getter");

            Assert.AreEqual(3, vector.Dimensions, "The number of dimensions should be two");
        }

        [TestMethod]
        public void LengthSquared()
        {
            Float3 vector = new Float3(5.0f, 6.0f, 7.0f);
            Assert.AreEqual(110.0f, vector.LengthSquared);
        }

        [TestMethod]
        public void Length()
        {
            Float3 vector = new Float3(5.0f, 6.0f, 7.0f);
            Assert.AreEqual(10.48808f, vector.Length, 0.001f);
        }

        [TestMethod]
        public void Normalise()
        {
            Float3 vector = new Float3(-5.0f, 6.0f, 7.0f);
            vector.Normalise();

            // Make sure the vector is now a unit vector
            Assert.AreEqual(1.0f, vector.Length);
            Assert.AreEqual(-0.47673f, vector.X, 0.001f);
            Assert.AreEqual(0.57207f, vector.Y, 0.001f);
            Assert.AreEqual(0.66742f, vector.Z, 0.001f);
        }

        [TestMethod]
        public void Dot()
        {
            Float3 a = new Float3(2.0f, 3.0f, 4.0f);
            Float3 b = new Float3(5.0f, 6.0f, 7.0f);
            Assert.AreEqual(56.0f, a.Dot(b), "The inner product should be the sum of the multiplied parts");
        }

        [TestMethod]
        public void DotStatic()
        {
            Float3 a = new Float3(2.0f, 3.0f, 4.0f);
            Float3 b = new Float3(5.0f, 6.0f, 7.0f);
            Assert.AreEqual(56.0f, Float3.Dot(a, b), "The inner product should be the sum of the multiplied parts");
        }

        [TestMethod]
        public void OperatorAdd()
        {
            Float3 a = new Float3(2.0f, 3.0f, 4.0f);
            Float3 b = new Float3(5.0f, 6.0f, 7.0f);
            Float3 c = a + b;

            Assert.AreEqual(7.0f, c.X);
            Assert.AreEqual(9.0f, c.Y);
            Assert.AreEqual(11.0f, c.Z);
        }

        [TestMethod]
        public void OperatorSubtract()
        {
            Float3 a = new Float3(10.0f, 3.0f, 5.5f);
            Float3 b = new Float3(4.0f, 5.0f, 4.0f);
            Float3 c = a - b;

            Assert.AreEqual(6.0f, c.X);
            Assert.AreEqual(-2.0f, c.Y);
            Assert.AreEqual(1.5f, c.Z);
        }

        [TestMethod]
        public void OperatorMultiplyScalar()
        {
            Float3 a = new Float3(10.0f, 3.0f, 1.5f);

            // Assert a first
            Float3 c = a * 2.0f;
            Assert.AreEqual(20.0f, c.X);
            Assert.AreEqual(6.0f, c.Y);
            Assert.AreEqual(3.0f, c.Z);

            // Assert a second
            c = 2.0f * a;
            Assert.AreEqual(20.0f, c.X);
            Assert.AreEqual(6.0f, c.Y);
            Assert.AreEqual(3.0f, c.Z);
        }

        [TestMethod]
        public void OperatorAddValue()
        {
            Float3 a = new Float3(2.0f, 3.0f, 1.5f);

            // Assert vector first
            Float3 c = a + 0.5f;
            Assert.AreEqual(2.5f, c.X);
            Assert.AreEqual(3.5, c.Y);
            Assert.AreEqual(2.0f, c.Z);

            // Assert vector second
            c = 0.5f + a;
            Assert.AreEqual(2.5f, c.X);
            Assert.AreEqual(3.5, c.Y);
            Assert.AreEqual(2.0f, c.Z);
        }

        [TestMethod]
        public void ArrayAccessor()
        {
            Float3 vector = new Float3(1.0f, 2.0f, 3.0f);
            Assert.AreEqual(1.0f, vector[0]);
            Assert.AreEqual(2.0f, vector[1]);
            Assert.AreEqual(3.0f, vector[2]);
            vector[0] = 4.0f;
            vector[1] = 5.0f;
            vector[2] = 6.0f;
            Assert.AreEqual(4.0f, vector[0]);
            Assert.AreEqual(5.0f, vector[1]);
            Assert.AreEqual(6.0f, vector[2]);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void ArrayAccessorSetUpperInvalidDimension()
        {
            Float3 vector = new Float3(1.0f, 2.0f, 3.0f);
            vector[3] = 3.0f;
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void ArrayAccessorSetLowerInvalidDimension()
        {
            Float3 vector = new Float3(1.0f, 2.0f, 3.0f);
            vector[-1] = 3.0f;
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void ArrayAccessorGetUpperInvalidDimension()
        {
            Float3 vector = new Float3(1.0f, 2.0f, 3.0f);
            float value = vector[3];
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void ArrayAccessorGetLowerInvalidDimension()
        {
            Float3 vector = new Float3(1.0f, 2.0f, 3.0f);
            float value = vector[-1];
        }
    }
}
