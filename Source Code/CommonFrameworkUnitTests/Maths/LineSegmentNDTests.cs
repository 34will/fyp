﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CommonFramework.Maths;


namespace CommonFrameworkUnitTests
{
    [TestClass]
    public class LineSegmentNDTests
    {
        [TestMethod]
        public void ConstructorIPositionables()
        {
            LineSegmentND segment = new LineSegmentND(new FloatN(new float[] { 1.0f, 2.0f, 3.0f }), new FloatN(new float[] { 4.0f, 5.0f, 6.0f }));
            Assert.AreEqual(1.0f, segment.A.GetDimension(0), "The segment should have the A positions X value set by the constructor");
            Assert.AreEqual(2.0f, segment.A.GetDimension(1), "The segment should have the A positions Y value set by the constructor");
            Assert.AreEqual(3.0f, segment.A.GetDimension(2), "The segment should have the A positions Z value set by the constructor");
            Assert.AreEqual(4.0f, segment.B.GetDimension(0), "The segment should have the B positions X value set by the constructor");
            Assert.AreEqual(5.0f, segment.B.GetDimension(1), "The segment should have the B positions Y value set by the constructor");
            Assert.AreEqual(6.0f, segment.B.GetDimension(2), "The segment should have the B positions Z value set by the constructor");
        }
    }
}
