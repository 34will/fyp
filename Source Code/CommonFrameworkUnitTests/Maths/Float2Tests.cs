﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CommonFramework.Maths;

namespace CommonFrameworkUnitTests
{
    [TestClass]
    public class Float2Tests
    {
        [TestMethod]
        public void ConstructorEmpty()
        {
            Float2 vector = new Float2();
            Assert.AreEqual(0.0f, vector.X);
            Assert.AreEqual(0.0f, vector.Y);
        }

        [TestMethod]
        public void ConstructorFloats()
        {
            Float2 vector = new Float2(1.2f, 2.3f);
            Assert.AreEqual(1.2f, vector.X);
            Assert.AreEqual(2.3f, vector.Y);
        }

        [TestMethod]
        public void SetDimension()
        {
            // Prepare vector
            Float2 vector = new Float2(1.2f, 2.3f);
            Assert.AreEqual(1.2f, vector.X);
            Assert.AreEqual(2.3f, vector.Y);

            // Change the dimension
            vector.SetDimension(0, 2.2f);
            vector.SetDimension(1, 3.3f);
            Assert.AreEqual(2.2f, vector.X, "The value set on dimension zero not expected");
            Assert.AreEqual(3.3f, vector.Y, "The value set on dimension one not expected");
        }

        [TestMethod]
        public void GetDimension()
        {
            // Prepare vector
            Float2 vector = new Float2(1.2f, 2.3f);
            Assert.AreEqual(1.2f, vector.X);
            Assert.AreEqual(2.3f, vector.Y);

            // Change the dimension
            Assert.AreEqual(1.2f, vector.GetDimension(0), "The value got on dimension zero not expected");
            Assert.AreEqual(2.3f, vector.GetDimension(1), "The value got on dimension one not expected");
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void SetInvalidDimension()
        {
            Float2 vector = new Float2(1.2f, 2.3f);
            vector.SetDimension(2, 2.0f);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetInvalidDimension()
        {
            Float2 vector = new Float2(1.2f, 2.3f);
            float value = vector.GetDimension(2);
        }

        [TestMethod]
        public void Properties()
        {
            Float2 vector = new Float2(1.2f, 2.3f);
            Assert.AreEqual(1.2f, vector.X);
            Assert.AreEqual(2.3f, vector.Y);

            // Use setters to modify values
            vector.X = 3.4f;
            vector.Y = 6.2f;

            // Check the changed values
            Assert.AreEqual(3.4f, vector.X, "The value set on the X component did not change value returned by getter");
            Assert.AreEqual(6.2f, vector.Y, "The value set on the Y component did not change value returned by getter");

            Assert.AreEqual(2, vector.Dimensions, "The number of dimensions should be two");
        }

        [TestMethod]
        public void LengthSquared()
        {
            Float2 vector = new Float2(5.0f, 6.0f);
            Assert.AreEqual(61.0f, vector.LengthSquared);
        }

        [TestMethod]
        public void Length()
        {
            Float2 vector = new Float2(5.0f, 6.0f);
            Assert.AreEqual(7.81025f, vector.Length);
        }

        [TestMethod]
        public void Normalise()
        {
            Float2 vector = new Float2(-5.0f, 6.0f);
            vector.Normalise();

            // Make sure the vector is now a unit vector
            Assert.AreEqual(1.0f, vector.Length);
            Assert.AreEqual(-0.6401844f, vector.X, 0.001f);
            Assert.AreEqual(0.7682213f, vector.Y, 0.001f);
        }

        [TestMethod]
        public void Dot()
        {
            Float2 a = new Float2(2.0f, 3.0f);
            Float2 b = new Float2(4.0f, 5.0f);
            Assert.AreEqual(23.0f, a.Dot(b), "The inner product should be the sum of the multiplied parts");
        }

        [TestMethod]
        public void DotStatic()
        {
            Float2 a = new Float2(2.0f, 3.0f);
            Float2 b = new Float2(4.0f, 5.0f);
            Assert.AreEqual(23.0f, Float2.Dot(a, b), "The inner product should be the sum of the multiplied parts");
        }

        [TestMethod]
        public void OperatorAdd()
        {
            Float2 a = new Float2(2.0f, 3.0f);
            Float2 b = new Float2(4.0f, 5.0f);
            Float2 c = a + b;

            Assert.AreEqual(6.0f, c.X);
            Assert.AreEqual(8.0f, c.Y);
        }

        [TestMethod]
        public void OperatorSubtract()
        {
            Float2 a = new Float2(10.0f, 3.0f);
            Float2 b = new Float2(4.0f, 5.0f);
            Float2 c = a - b;

            Assert.AreEqual(6.0f, c.X);
            Assert.AreEqual(-2.0f, c.Y);
        }

        [TestMethod]
        public void OperatorMultiplyScalar()
        {
            Float2 a = new Float2(10.0f, 3.0f);
            
            // Assert a first
            Float2 c = a * 2.0f;
            Assert.AreEqual(20.0f, c.X);
            Assert.AreEqual(6.0f, c.Y);

            // Assert a second
            c = 2.0f * a;
            Assert.AreEqual(20.0f, c.X);
            Assert.AreEqual(6.0f, c.Y);
        }

        [TestMethod]
        public void OperatorAddValue()
        {
            Float2 a = new Float2(2.0f, 3.0f);

            // Assert vector first
            Float2 c = a + 0.5f;
            Assert.AreEqual(2.5f, c.X);
            Assert.AreEqual(3.5, c.Y);

            // Assert vector second
            c = 0.5f + a;
            Assert.AreEqual(2.5f, c.X);
            Assert.AreEqual(3.5, c.Y);
        }

        [TestMethod]
        public void ArrayAccessor()
        {
            Float2 vector = new Float2(1.0f, 2.0f);
            Assert.AreEqual(1.0f, vector[0]);
            Assert.AreEqual(2.0f, vector[1]);
            vector[0] = 3.0f;
            vector[1] = 4.0f;
            Assert.AreEqual(3.0f, vector[0]);
            Assert.AreEqual(4.0f, vector[1]);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void ArrayAccessorSetUpperInvalidDimension()
        {
            Float2 vector = new Float2(1.0f, 2.0f);
            vector[2] = 3.0f;
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void ArrayAccessorSetLowerInvalidDimension()
        {
            Float2 vector = new Float2(1.0f, 2.0f);
            vector[-1] = 3.0f;
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void ArrayAccessorGetUpperInvalidDimension()
        {
            Float2 vector = new Float2(1.0f, 2.0f);
            float value = vector[2];
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void ArrayAccessorGetLowerInvalidDimension()
        {
            Float2 vector = new Float2(1.0f, 2.0f);
            float value = vector[-1];
        }
    }
}
