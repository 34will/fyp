﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CommonFramework.Maths;

namespace CommonFrameworkUnitTests
{
    [TestClass]
    public class RectangleTests
    {
        [TestMethod]
        public void ConstructorFloats()
        {
            Rectangle rectangle = new Rectangle(1.0f, 2.0f, 3.0f, 4.0f);
            Assert.AreEqual(1.0f, rectangle.X, "The X component should be set by the constructor");
            Assert.AreEqual(2.0f, rectangle.Y, "The Y component should be set by the constructor");
            Assert.AreEqual(3.0f, rectangle.Width, "The Width component should be set by the constructor");
            Assert.AreEqual(4.0f, rectangle.Height, "The Height component should be set by the constructor");
        }

        [TestMethod]
        public void ConstructorVectors()
        {
            Rectangle rectangle = new Rectangle(new Float2(1.0f, 2.0f), new Float2(3.0f, 4.0f));
            Assert.AreEqual(1.0f, rectangle.X, "The X component should be set by the constructor");
            Assert.AreEqual(2.0f, rectangle.Y, "The Y component should be set by the constructor");
            Assert.AreEqual(3.0f, rectangle.Width, "The Width component should be set by the constructor");
            Assert.AreEqual(4.0f, rectangle.Height, "The Height component should be set by the constructor");
        }

        [TestMethod]
        public void ContainsFloats()
        {
            Rectangle rectangle = new Rectangle(2.0f, 2.0f, 4.0f, 4.0f);

            // Check one inside the cube
            Assert.IsTrue(rectangle.Contains(3.0f, 3.0f), "The inside square assert has failed");

            // Check one on the edge of the cube
            Assert.IsTrue(rectangle.Contains(6.0f, 4.0f), "The edge of square assert has failed");

            // Check one on the outside
            Assert.IsFalse(rectangle.Contains(7.0f, 4.0f), "The outside square assert has failed");
        }

        [TestMethod]
        public void ContainsVectors()
        {
            Rectangle rectangle = new Rectangle(2.0f, 2.0f, 4.0f, 4.0f);

            // Check one inside the cube
            Assert.IsTrue(rectangle.Contains(new Float2(3.0f, 3.0f)), "The inside square assert has failed");

            // Check one on the edge of the cube
            Assert.IsTrue(rectangle.Contains(new Float2(6.0f, 4.0f)), "The edge of square assert has failed");

            // Check one on the outside
            Assert.IsFalse(rectangle.Contains(new Float2(7.0f, 4.0f)), "The outside square assert has failed");
        }

        [TestMethod]
        public void Intersects()
        {
            Rectangle a = new Rectangle(2.0f, 2.0f, 4.0f, 4.0f);
            Rectangle b = new Rectangle(2.0f, 2.0f, 4.0f, 4.0f);

            // Check perfect overlapping
            Assert.IsTrue(a.Intersects(b), "A intersects B, perfect overlap assert failed");
            Assert.IsTrue(b.Intersects(a), "B intersects A, perfect overlap assert failed");

            // Check partial overlap
            b.X = 4.0f;
            b.Y = 4.0f;
            Assert.IsTrue(a.Intersects(b), "A intersects B, partial overlap assert failed");
            Assert.IsTrue(b.Intersects(a), "B intersects A, partial overlap assert failed");

            // Check no overlap
            b.X = 7.0f;
            b.Y = 4.0f;
            Assert.IsFalse(a.Intersects(b), "A intersects B, no overlap assert failed");
            Assert.IsFalse(b.Intersects(a), "B intersects A, no overlap assert failed");
        }

        [TestMethod]
        public void Properties()
        {
            // Standard rectangle
            Rectangle rectangle = new Rectangle(1.0f, 2.0f, 3.0f, 4.0f);

			// Normal properties handled by the constructor
			Assert.AreEqual(1.0f, rectangle.X);
			Assert.AreEqual(2.0f, rectangle.Y);
			Assert.AreEqual(3.0f, rectangle.Width);
			Assert.AreEqual(4.0f, rectangle.Height);

			// Accumulated properties
			Assert.AreEqual(1.0f, rectangle.Left);
			Assert.AreEqual(2.0f, rectangle.Top);
			Assert.AreEqual(4.0f, rectangle.Right);
			Assert.AreEqual(6.0f, rectangle.Bottom);
		}

		[TestMethod]
		public void PropertiesSetAndGet()
		{
			// Standard cuboid
			Rectangle rectangle = new Rectangle(1.0f, 2.0f, 3.0f, 4.0f);

			rectangle.X = 2.0f;
			rectangle.Y = 3.0f;
			rectangle.Width = 4.0f;
			rectangle.Height = 5.0f;

			Assert.AreEqual(2.0f, rectangle.X);
			Assert.AreEqual(3.0f, rectangle.Y);
			Assert.AreEqual(4.0f, rectangle.Width);
			Assert.AreEqual(5.0f, rectangle.Height);
		}

        [TestMethod]
        public void SizeSetProperties()
        {
            // Standard rectangle
            Rectangle rectangle = new Rectangle(1.0f, 2.0f, 3.0f, 4.0f);

            // Normal properties handled by the constructor
            Assert.AreEqual(3.0f, rectangle.Width);
            Assert.AreEqual(4.0f, rectangle.Height);

            // Set new values
            rectangle.Width = 5.0f;
            rectangle.Height = 6.0f;

            // Check values now setter used
            Assert.AreEqual(5.0f, rectangle.Width);
            Assert.AreEqual(6.0f, rectangle.Height);
        }
    }
}
