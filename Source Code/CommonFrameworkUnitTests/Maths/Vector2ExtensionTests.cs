﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using UnityEngine;

using CommonFramework.Maths;

namespace CommonFrameworkUnitTests
{
	[TestClass]
	public class Vector2ExtensionTests
	{
		[TestMethod]
		public void ToV3XYNoZ()
		{
			Vector2 vectorTwo = new Vector2(5.0f, -10.0f);
			Vector3 vectorThr = vectorTwo.ToV3XY();

			Assert.AreEqual(5.0f, vectorThr.x, "The x-component of the converted Vector3 should be 5.0f.");
			Assert.AreEqual(-10.0f, vectorThr.y, "The y-component of the converted Vector3 should be -10.0f.");
			Assert.AreEqual(0.0f, vectorThr.z, "The z-component of the converted Vector3 should be 0.0f.");
		}

		[TestMethod]
		public void ToV3XYSpecifiedZ()
		{
			Vector2 vectorTwo = new Vector2(5.0f, -10.0f);
			Vector3 vectorThr = vectorTwo.ToV3XY(7.5f);

			Assert.AreEqual(5.0f, vectorThr.x, "The x-component of the converted Vector3 should be 5.0f.");
			Assert.AreEqual(-10.0f, vectorThr.y, "The y-component of the converted Vector3 should be -10.0f.");
			Assert.AreEqual(7.5f, vectorThr.z, "The z-component of the converted Vector3 should be 7.5f.");
		}

		[TestMethod]
		public void ToV3YXNoZ()
		{
			Vector2 vectorTwo = new Vector2(5.0f, -10.0f);
			Vector3 vectorThr = vectorTwo.ToV3YX();

			Assert.AreEqual(-10.0f, vectorThr.x, "The x-component of the converted Vector3 should be -10.0f.");
			Assert.AreEqual(5.0f, vectorThr.y, "The y-component of the converted Vector3 should be 5.0f.");
			Assert.AreEqual(0.0f, vectorThr.z, "The z-component of the converted Vector3 should be 0.0f.");
		}

		[TestMethod]
		public void ToV3YXSpecifiedZ()
		{
			Vector2 vectorTwo = new Vector2(5.0f, -10.0f);
			Vector3 vectorThr = vectorTwo.ToV3YX(7.5f);

			Assert.AreEqual(-10.0f, vectorThr.x, "The x-component of the converted Vector3 should be -10.0f.");
			Assert.AreEqual(5.0f, vectorThr.y, "The y-component of the converted Vector3 should be 5.0f.");
			Assert.AreEqual(7.5f, vectorThr.z, "The z-component of the converted Vector3 should be 7.5f.");
		}

		[TestMethod]
		public void ToV3XZNoY()
		{
			Vector2 vectorTwo = new Vector2(5.0f, -10.0f);
			Vector3 vectorThr = vectorTwo.ToV3XZ();

			Assert.AreEqual(5.0f, vectorThr.x, "The x-component of the converted Vector3 should be 5.0f.");
			Assert.AreEqual(0.0f, vectorThr.y, "The y-component of the converted Vector3 should be 0.0f.");
			Assert.AreEqual(-10.0f, vectorThr.z, "The z-component of the converted Vector3 should be -10.0f.");
		}

		[TestMethod]
		public void ToV3XZSpecifiedY()
		{
			Vector2 vectorTwo = new Vector2(5.0f, -10.0f);
			Vector3 vectorThr = vectorTwo.ToV3XZ(7.5f);

			Assert.AreEqual(5.0f, vectorThr.x, "The x-component of the converted Vector3 should be 5.0f.");
			Assert.AreEqual(7.5f, vectorThr.y, "The y-component of the converted Vector3 should be 7.5f.");
			Assert.AreEqual(-10.0f, vectorThr.z, "The z-component of the converted Vector3 should be -10.0f.");
		}

		[TestMethod]
		public void ToV3ZXNoY()
		{
			Vector2 vectorTwo = new Vector2(5.0f, -10.0f);
			Vector3 vectorThr = vectorTwo.ToV3ZX();

			Assert.AreEqual(-10.0f, vectorThr.x, "The x-component of the converted Vector3 should be -10.0f.");
			Assert.AreEqual(0.0f, vectorThr.y, "The y-component of the converted Vector3 should be 0.0f.");
			Assert.AreEqual(5.0f, vectorThr.z, "The z-component of the converted Vector3 should be 5.0f.");
		}

		[TestMethod]
		public void ToV3ZXSpecifiedY()
		{
			Vector2 vectorTwo = new Vector2(5.0f, -10.0f);
			Vector3 vectorThr = vectorTwo.ToV3ZX(7.5f);

			Assert.AreEqual(-10.0f, vectorThr.x, "The x-component of the converted Vector3 should be -10.0f.");
			Assert.AreEqual(7.5f, vectorThr.y, "The y-component of the converted Vector3 should be 7.5f.");
			Assert.AreEqual(5.0f, vectorThr.z, "The z-component of the converted Vector3 should be 5.0f.");
		}

		[TestMethod]
		public void ToV3YZNoX()
		{
			Vector2 vectorTwo = new Vector2(5.0f, -10.0f);
			Vector3 vectorThr = vectorTwo.ToV3YZ();

			Assert.AreEqual(0.0f, vectorThr.x, "The x-component of the converted Vector3 should be 0.0f.");
			Assert.AreEqual(5.0f, vectorThr.y, "The y-component of the converted Vector3 should be 5.0f.");
			Assert.AreEqual(-10.0f, vectorThr.z, "The z-component of the converted Vector3 should be -10.0f.");
		}

		[TestMethod]
		public void ToV3YZSpecifiedX()
		{
			Vector2 vectorTwo = new Vector2(5.0f, -10.0f);
			Vector3 vectorThr = vectorTwo.ToV3YZ(7.5f);

			Assert.AreEqual(7.5f, vectorThr.x, "The x-component of the converted Vector3 should be 7.5f.");
			Assert.AreEqual(5.0f, vectorThr.y, "The y-component of the converted Vector3 should be 5.0f.");
			Assert.AreEqual(-10.0f, vectorThr.z, "The z-component of the converted Vector3 should be -10.0f.");
		}

		[TestMethod]
		public void ToV3ZYNoX()
		{
			Vector2 vectorTwo = new Vector2(5.0f, -10.0f);
			Vector3 vectorThr = vectorTwo.ToV3ZY();

			Assert.AreEqual(0.0f, vectorThr.x, "The x-component of the converted Vector3 should be 0.0f.");
			Assert.AreEqual(-10.0f, vectorThr.y, "The y-component of the converted Vector3 should be -10.0f.");
			Assert.AreEqual(5.0f, vectorThr.z, "The z-component of the converted Vector3 should be 5.0f.");
		}

		[TestMethod]
		public void ToV3ZYSpecifiedX()
		{
			Vector2 vectorTwo = new Vector2(5.0f, -10.0f);
			Vector3 vectorThr = vectorTwo.ToV3ZY(7.5f);

			Assert.AreEqual(7.5f, vectorThr.x, "The x-component of the converted Vector3 should be 7.5f.");
			Assert.AreEqual(-10.0f, vectorThr.y, "The y-component of the converted Vector3 should be -10.0f.");
			Assert.AreEqual(5.0f, vectorThr.z, "The z-component of the converted Vector3 should be 5.0f.");
		}
	}
}
