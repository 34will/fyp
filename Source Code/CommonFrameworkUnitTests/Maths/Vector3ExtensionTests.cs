﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using UnityEngine;

using CommonFramework.Maths;

namespace CommonFrameworkUnitTests
{
	[TestClass]
	public class Vector3ExtensionTests
	{
		[TestMethod]
		public void ToV2XY()
		{
			Vector3 vectorThr = new Vector3(5.0f, -10.0f, 7.5f);
			Vector2 vectorTwo = vectorThr.ToV2XY();

			Assert.AreEqual(5.0f, vectorTwo.x, "The x-component of the converted Vector2 should be 5.0f.");
			Assert.AreEqual(-10.0f, vectorTwo.y, "The y-component of the converted Vector2 should be -10.0f.");
		}

		[TestMethod]
		public void ToV2YX()
		{
			Vector3 vectorThr = new Vector3(5.0f, -10.0f, 7.5f);
			Vector2 vectorTwo = vectorThr.ToV2YX();

			Assert.AreEqual(-10.0f, vectorTwo.x, "The x-component of the converted Vector2 should be -10.0f.");
			Assert.AreEqual(5.0f, vectorTwo.y, "The y-component of the converted Vector2 should be 5.0f.");
		}

		[TestMethod]
		public void ToV2XZ()
		{
			Vector3 vectorThr = new Vector3(5.0f, -10.0f, 7.5f);
			Vector2 vectorTwo = vectorThr.ToV2XZ();

			Assert.AreEqual(5.0f, vectorTwo.x, "The x-component of the converted Vector2 should be 5.0f.");
			Assert.AreEqual(7.5f, vectorTwo.y, "The y-component of the converted Vector2 should be 7.5f.");
		}

		[TestMethod]
		public void ToV2ZX()
		{
			Vector3 vectorThr = new Vector3(5.0f, -10.0f, 7.5f);
			Vector2 vectorTwo = vectorThr.ToV2ZX();

			Assert.AreEqual(7.5f, vectorTwo.x, "The x-component of the converted Vector2 should be 7.5f.");
			Assert.AreEqual(5.0f, vectorTwo.y, "The y-component of the converted Vector2 should be 5.0f.");
		}

		[TestMethod]
		public void ToV2YZ()
		{
			Vector3 vectorThr = new Vector3(5.0f, -10.0f, 7.5f);
			Vector2 vectorTwo = vectorThr.ToV2YZ();

			Assert.AreEqual(-10.0f, vectorTwo.x, "The x-component of the converted Vector2 should be -10.0f.");
			Assert.AreEqual(7.5f, vectorTwo.y, "The y-component of the converted Vector2 should be 7.5f.");
		}

		[TestMethod]
		public void ToV2ZY()
		{
			Vector3 vectorThr = new Vector3(5.0f, -10.0f, 7.5f);
			Vector2 vectorTwo = vectorThr.ToV2ZY();

			Assert.AreEqual(7.5f, vectorTwo.x, "The x-component of the converted Vector2 should be 7.5f.");
			Assert.AreEqual(-10.0f, vectorTwo.y, "The y-component of the converted Vector2 should be -10.0f.");
		}

        [TestMethod]
        public void ToFloat3()
        {
            Vector3 input = new Vector3(1.0f, 2.0f, 3.0f);
            Float3 result = input.ToFloat3();
            Assert.AreEqual(1.0f, result.X);
            Assert.AreEqual(2.0f, result.Y);
            Assert.AreEqual(3.0f, result.Z);
        }
	}
}
