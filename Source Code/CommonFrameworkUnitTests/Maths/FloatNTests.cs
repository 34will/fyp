﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CommonFramework.Maths;

namespace CommonFrameworkUnitTests
{
    [TestClass]
    public class FloatNTests
    {
        [TestMethod]
        public void ConstructorIPositionableND()
        {
            IPositionableND float3 = new Float3(1.2f, 2.2f, 3.3f);

            FloatN vector = new FloatN(float3);

            Assert.AreEqual(3, vector.Dimensions, "The number of dimensions in the FloatN is not that of the input IPositionableND");
            Assert.AreEqual(1.2f, vector.GetDimension(0), "The axis zero dimension did not have the same value as the one in the constructors input IPositionableND");
            Assert.AreEqual(2.2f, vector.GetDimension(1), "The axis one dimension did not have the same value as the one in the constructors input IPositionableND");
            Assert.AreEqual(3.3f, vector.GetDimension(2), "The axis two dimension did not have the same value as the one in the constructors input IPositionableND");
        }

        [TestMethod]
        public void ConstructorFloatArray()
        {
            float[] data = new float[] { 1.1f, 2.2f, 3.3f, 4.4f, 5.5f, 6.6f };

            FloatN vector = new FloatN(data);

            Assert.AreEqual(6, vector.Dimensions, "The number of dimensions in the FloatN is not that of the input array");
            Assert.AreEqual(1.1f, vector.GetDimension(0), "The axis zero dimension did not have the same value as the one in the constructors input array");
            Assert.AreEqual(2.2f, vector.GetDimension(1), "The axis one dimension did not have the same value as the one in the constructors input array");
            Assert.AreEqual(3.3f, vector.GetDimension(2), "The axis two dimension did not have the same value as the one in the constructors input array");
            Assert.AreEqual(4.4f, vector.GetDimension(3), "The axis zero dimension did not have the same value as the one in the constructors input array");
            Assert.AreEqual(5.5f, vector.GetDimension(4), "The axis one dimension did not have the same value as the one in the constructors input array");
            Assert.AreEqual(6.6f, vector.GetDimension(5), "The axis two dimension did not have the same value as the one in the constructors input array");
        }

        [TestMethod]
        public void ConstructorDimensions()
        {
            FloatN vector = new FloatN(2);
            Assert.AreEqual(2, vector.Dimensions, "The number of dimensions in the FloatN is not that of the input");
            Assert.AreEqual(0.0f, vector.GetDimension(0), "The axis zero dimension does not have the default zero value");
            Assert.AreEqual(0.0f, vector.GetDimension(1), "The axis one dimension does not have the default zero value");
        }

        [TestMethod]
        public void SetDimension()
        {
            FloatN vector = new FloatN(2);
            Assert.AreEqual(2, vector.Dimensions, "The number of dimensions in the FloatN is not that of the input");
            Assert.AreEqual(0.0f, vector.GetDimension(0), "The axis zero dimension does not have the default zero value");
            Assert.AreEqual(0.0f, vector.GetDimension(1), "The axis one dimension does not have the default zero value");

            vector.SetDimension(0, 1.1f);
            vector.SetDimension(1, 2.2f);

            Assert.AreEqual(1.1f, vector.GetDimension(0), "The set for axis zero did not produce the correct get value");
            Assert.AreEqual(2.2f, vector.GetDimension(1), "The set for axis one did not produce the correct get value");
        }

        [TestMethod]
        public void GetDimension()
        {
            FloatN vector = new FloatN(new float[] { 1.1f, 2.2f, 3.3f });

            Assert.AreEqual(1.1f, vector.GetDimension(0), "The get for axis zero did not get the correct value");
            Assert.AreEqual(2.2f, vector.GetDimension(1), "The get for axis one did not get the correct value");
            Assert.AreEqual(3.3f, vector.GetDimension(2), "The get for axis two did not get the correct value");
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void SetInvalidDimensionUpper()
        {
            FloatN vector = new FloatN(new float[] { 1.1f, 2.2f, 3.3f });
            vector.SetDimension(3, 4.4f);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetInvalidDimensionUpper()
        {
            FloatN vector = new FloatN(new float[] { 1.1f, 2.2f, 3.3f });
            float value = vector.GetDimension(3);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void SetInvalidDimensionLower()
        {
            FloatN vector = new FloatN(new float[] { 1.1f, 2.2f, 3.3f });
            vector.SetDimension(-1, 4.4f);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetInvalidDimensionLower()
        {
            FloatN vector = new FloatN(new float[] { 1.1f, 2.2f, 3.3f });
            float value = vector.GetDimension(-1);
        }

        [TestMethod]
        public void Clone()
        {
            // Make a FloatN and it's clone
            FloatN a = new FloatN(new float[] { 1.1f, 2.2f, 3.3f });
            FloatN b = a.Clone();

            // Assert the clone has the same values as the original
            for (int i = 0; i < 3; i++)
                Assert.AreEqual(a.GetDimension(i), b.GetDimension(i), "The clone should have the same value on axis " + i);

            // Assert they are not the same object instance
            Assert.IsFalse(Object.ReferenceEquals(a, b));

            // Change the original
            for (int i = 0; i < 3; i++)
                a.SetDimension(i, a.GetDimension(i) + 1.0f);

            // Assert the clone has now got different values to the original
            for (int i = 0; i < 3; i++)
                Assert.AreNotEqual(a.GetDimension(i), b.GetDimension(i), "The clone should not have the same value on axis " + i);
        }

        [TestMethod]
        public void ArrayOperator()
        {
            // Create a sample vector
            FloatN vector = new FloatN(new float[] { 1.1f, 2.2f, 3.3f });

            // Check using array operator
            Assert.AreEqual(1.1f, vector[0]);
            Assert.AreEqual(2.2f, vector[1]);
            Assert.AreEqual(3.3f, vector[2]);

            // Modify using array operator
            vector[0] = 2.4f;
            vector[1] = 4.0f;
            vector[2] = 6.6f;

            // Check changes array operator
            Assert.AreEqual(2.4f, vector[0]);
            Assert.AreEqual(4.0f, vector[1]);
            Assert.AreEqual(6.6f, vector[2]);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void ArrayOperatorGetInvalidDimension()
        {
            FloatN vector = new FloatN(new float[] { 1.1f, 2.2f, 3.3f });
            float value = vector[3];
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void ArrayOperatorSetInvalidDimension()
        {
            FloatN vector = new FloatN(new float[] { 1.1f, 2.2f, 3.3f });
            vector[3] = 4.4f;
        }

        [TestMethod]
        public void LengthSquared()
        {
            FloatN vector = new FloatN(new float[]{ 5.0f, 6.0f });
            Assert.AreEqual(61.0f, vector.LengthSquared);
        }

        [TestMethod]
        public void Length()
        {
            FloatN vector = new FloatN(new float[]{5.0f, 6.0f});
            Assert.AreEqual(7.81025f, vector.Length);
        }

        [TestMethod]
        public void Normalise()
        {
            FloatN vector = new FloatN(new float[]{-5.0f, 6.0f});
            vector.Normalise();

            // Make sure the vector is now a unit vector
            Assert.AreEqual(1.0f, vector.Length, 0.001f);
            Assert.AreEqual(-0.6401844f, vector.GetDimension(0), 0.001f);
            Assert.AreEqual(0.7682213f, vector.GetDimension(1), 0.001f);
        }

        [TestMethod]
        public void Dot()
        {
            FloatN a = new FloatN(new float[]{2.0f, 3.0f});
            FloatN b = new FloatN(new float[]{4.0f, 5.0f});
            Assert.AreEqual(23.0f, a.Dot(b), "The inner product should be the sum of the multiplied parts");
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void DotInvalidDimensions()
        {
            FloatN a = new FloatN(new float[] { 2.0f, 3.0f });
            FloatN b = new FloatN(new float[] { 4.0f, 5.0f, 6.0f });
            float value = a.Dot(b);
        }

        [TestMethod]
        public void DotStatic()
        {
            FloatN a = new FloatN(new float[]{2.0f, 3.0f});
            FloatN b = new FloatN(new float[]{4.0f, 5.0f});
            Assert.AreEqual(23.0f, FloatN.Dot(a, b), "The inner product should be the sum of the multiplied parts");
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void DotStaticInvalidDimensions()
        {
            FloatN a = new FloatN(new float[] { 2.0f, 3.0f });
            FloatN b = new FloatN(new float[] { 4.0f, 5.0f, 6.0f });
            float value = FloatN.Dot(a, b);
        }

        [TestMethod]
        public void ArrayAccessor()
        {
            FloatN vector = new FloatN(new Float2(1.0f, 2.0f));
            Assert.AreEqual(1.0f, vector[0]);
            Assert.AreEqual(2.0f, vector[1]);
            vector[0] = 3.0f;
            vector[1] = 4.0f;
            Assert.AreEqual(3.0f, vector[0]);
            Assert.AreEqual(4.0f, vector[1]);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void ArrayAccessorSetUpperInvalidDimension()
        {
            FloatN vector = new FloatN(new Float2(1.0f, 2.0f));
            vector[2] = 3.0f;
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void ArrayAccessorSetLowerInvalidDimension()
        {
            FloatN vector = new FloatN(new Float2(1.0f, 2.0f));
            vector[-1] = 3.0f;
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void ArrayAccessorGetUpperInvalidDimension()
        {
            FloatN vector = new FloatN(new Float2(1.0f, 2.0f));
            float value = vector[2];
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void ArrayAccessorGetLowerInvalidDimension()
        {
            FloatN vector = new FloatN(new Float2(1.0f, 2.0f));
            float value = vector[-1];
        }

        [TestMethod]
        public void OperatorAdd()
        {
            FloatN a = new FloatN(new Float2(1.0f, 2.0f));
            FloatN b = new FloatN(new Float2(3.0f, 4.0f));
            FloatN c = a + b;
            Assert.AreEqual(4.0f, c[0]);
            Assert.AreEqual(6.0f, c[1]);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void OperatorAddInvalidDimensions()
        {
            FloatN a = new FloatN(new Float3(1.0f, 2.0f, 3.0f));
            FloatN b = new FloatN(new Float2(3.0f, 4.0f));
            FloatN c = a + b;
        }

        [TestMethod]
        public void OperatorSubtract()
        {
            FloatN a = new FloatN(new Float2(1.0f, 6.0f));
            FloatN b = new FloatN(new Float2(3.0f, 4.0f));
            FloatN c = a - b;
            Assert.AreEqual(-2.0f, c[0]);
            Assert.AreEqual(2.0f, c[1]);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void OperatorSubtractInvalidDimensions()
        {
            FloatN a = new FloatN(new Float3(1.0f, 2.0f, 3.0f));
            FloatN b = new FloatN(new Float2(3.0f, 4.0f));
            FloatN c = a - b;
        }

        [TestMethod]
        public void OperatorMultiplyScalarRight()
        {
            FloatN a = new FloatN(new Float2(1.0f, 6.0f));
            FloatN c = a * 1.5f;
            Assert.AreEqual(1.5f, c[0]);
            Assert.AreEqual(9.0f, c[1]);
        }

        [TestMethod]
        public void OperatorMultiplyScalarLeft()
        {
            FloatN a = new FloatN(new Float2(1.0f, 6.0f));
            FloatN c = 1.5f * a;
            Assert.AreEqual(1.5f, c[0]);
            Assert.AreEqual(9.0f, c[1]);
        }

        [TestMethod]
        public void OperatorAddConstantRight()
        {
            FloatN a = new FloatN(new Float2(1.0f, 6.0f));
            FloatN c = a + 1.5f;
            Assert.AreEqual(2.5f, c[0]);
            Assert.AreEqual(7.5f, c[1]);
        }

        [TestMethod]
        public void OperatorAddConstantLeft()
        {
            FloatN a = new FloatN(new Float2(1.0f, 6.0f));
            FloatN c = 1.5f + a;
            Assert.AreEqual(2.5f, c[0]);
            Assert.AreEqual(7.5f, c[1]);
        }

    }
}
