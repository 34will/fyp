﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CommonFramework.Maths;

namespace CommonFrameworkUnitTests
{
    [TestClass]
    public class LineSegment3DTests
    {
        [TestMethod]
        public void ConstructorFloats()
        {
            LineSegment3D segment = new LineSegment3D(1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f);
            Assert.AreEqual(1.0f, segment.A.X, "The segment should have the A positions X value set by the constructor");
            Assert.AreEqual(2.0f, segment.A.Y, "The segment should have the A positions Y value set by the constructor");
            Assert.AreEqual(3.0f, segment.A.Z, "The segment should have the A positions Z value set by the constructor");
            Assert.AreEqual(4.0f, segment.B.X, "The segment should have the B positions X value set by the constructor");
            Assert.AreEqual(5.0f, segment.B.Y, "The segment should have the B positions Y value set by the constructor");
            Assert.AreEqual(6.0f, segment.B.Z, "The segment should have the B positions Z value set by the constructor");
        }

        [TestMethod]
        public void ConstructorVectors()
        {
            LineSegment3D segment = new LineSegment3D(new Float3(1.0f, 2.0f, 3.0f), new Float3(4.0f, 5.0f, 6.0f));
            Assert.AreEqual(1.0f, segment.A.X, "The segment should have the A positions X value set by the constructor");
            Assert.AreEqual(2.0f, segment.A.Y, "The segment should have the A positions Y value set by the constructor");
            Assert.AreEqual(3.0f, segment.A.Z, "The segment should have the A positions Z value set by the constructor");
            Assert.AreEqual(4.0f, segment.B.X, "The segment should have the B positions X value set by the constructor");
            Assert.AreEqual(5.0f, segment.B.Y, "The segment should have the B positions Y value set by the constructor");
            Assert.AreEqual(6.0f, segment.B.Z, "The segment should have the B positions Z value set by the constructor");
        }
    }
}