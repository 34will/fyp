﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using UnityEngine;

using CommonFramework.LineDrawer;
using CommonFramework.MessageSystem;

namespace CommonFrameworkUnitTests
{
	[TestClass]
	public class DrawLineMessageTests
	{
		[TestMethod]
		public void TwoVector3Constructor()
		{
			DrawLineMessage dlm = new DrawLineMessage(new Vector3(10.0f, -6.5f, 100000.2f), new Vector3(-40.4f, 30.0f, -0.0004f));

			Assert.AreEqual(new Vector3(10.0f, -6.5f, 100000.2f), dlm.Start, "The Start point of the line should be { 10.0f, -6.5f, 100000.2f }.");
			Assert.AreEqual(new Vector3(-40.4f, 30.0f, -0.0004f), dlm.End, "The End point of the line should be { -40.4f, 30.0f, -0.0004f }.");
			Assert.AreEqual(Color.white, dlm.StartColour, "The Start point colour should be the default of Color.white.");
			Assert.AreEqual(Color.white, dlm.EndColour, "The Start point colour should be the default of Color.white.");
		}

		[TestMethod]
		public void TwoVector3OneColourConstructor()
		{
			DrawLineMessage dlm = new DrawLineMessage(new Vector3(10.0f, -6.5f, 100000.2f), new Vector3(-40.4f, 30.0f, -0.0004f), Color.red);

			Assert.AreEqual(new Vector3(10.0f, -6.5f, 100000.2f), dlm.Start, "The Start point of the line should be { 10.0f, -6.5f, 100000.2f }.");
			Assert.AreEqual(new Vector3(-40.4f, 30.0f, -0.0004f), dlm.End, "The End point of the line should be { -40.4f, 30.0f, -0.0004f }.");
			Assert.AreEqual(Color.red, dlm.StartColour, "The Start point colour should be Color.red.");
			Assert.AreEqual(Color.red, dlm.EndColour, "The Start point colour should be Color.red.");
		}

		[TestMethod]
		public void TwoVector3TwoColourConstructor()
		{
			DrawLineMessage dlm = new DrawLineMessage(new Vector3(10.0f, -6.5f, 100000.2f), Color.blue, new Vector3(-40.4f, 30.0f, -0.0004f), Color.green);

			Assert.AreEqual(new Vector3(10.0f, -6.5f, 100000.2f), dlm.Start, "The Start point of the line should be { 10.0f, -6.5f, 100000.2f }.");
			Assert.AreEqual(new Vector3(-40.4f, 30.0f, -0.0004f), dlm.End, "The End point of the line should be { -40.4f, 30.0f, -0.0004f }.");
			Assert.AreEqual(Color.blue, dlm.StartColour, "The Start point colour should be the default of Color.blue.");
			Assert.AreEqual(Color.green, dlm.EndColour, "The Start point colour should be the default of Color.green.");
		}

		[TestMethod]
		public void SourceSetAndSet()
		{
			IMessageable m = new TestMessageable();
			DrawLineMessage dlm = new DrawLineMessage(new Vector3(10.0f, -6.5f, 100000.2f), new Vector3(-40.4f, 30.0f, -0.0004f));
			dlm.Source = m;

			Assert.AreEqual(m, dlm.Source, "The Source should be the provided TestMessageable.");
		}

		[TestMethod]
		public void MessageNameGet()
		{
			DrawLineMessage dlm = new DrawLineMessage(new Vector3(10.0f, -6.5f, 100000.2f), new Vector3(-40.4f, 30.0f, -0.0004f));

			Assert.AreEqual("DrawLineMessage", dlm.MessageName, "The MessageName should be \"DrawLineMessage\".");
		}
	}
}
