﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using UnityEngine;

using CommonFramework.LineDrawer;
using CommonFramework.MessageSystem;

namespace CommonFrameworkUnitTests
{
    [TestClass]
    public class DrawLineStatusMessageTests
    {
        [TestMethod]
        public void Constructor()
        {
            TestMessageable messageable = new TestMessageable();
            DrawLineStatusMessage message = new DrawLineStatusMessage(true, messageable);
            Assert.ReferenceEquals(messageable, message.Source);
            Assert.AreEqual(true, message.DrawLines);
        }

        [TestMethod]
        public void MessageName()
        {
            DrawLineStatusMessage message = new DrawLineStatusMessage(false);
            Assert.AreEqual("DrawLineStatusMessage", message.MessageName);
        }

        [TestMethod]
        public void MessageSource()
        {
            TestMessageable messageable = new TestMessageable();
            DrawLineStatusMessage message = new DrawLineStatusMessage(false);
            Assert.AreEqual(false, message.DrawLines);
            Assert.AreEqual(null, message.Source);
            message.Source = messageable;
            Assert.ReferenceEquals(messageable, message.Source);
        }
    }
}
