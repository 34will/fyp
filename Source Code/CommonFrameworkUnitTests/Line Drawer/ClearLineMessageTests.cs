﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using UnityEngine;

using CommonFramework.LineDrawer;
using CommonFramework.MessageSystem;

namespace CommonFrameworkUnitTests
{
    [TestClass]
    public class ClearLineMessageTests
    {
        [TestMethod]
        public void Constructor()
        {
            TestMessageable messageable = new TestMessageable();
            ClearLinesMessage message = new ClearLinesMessage(messageable);
            Assert.ReferenceEquals(messageable, message.Source);
        }

        [TestMethod]
        public void MessageName()
        {
            ClearLinesMessage message = new ClearLinesMessage();
            Assert.AreEqual("ClearLinesMessage", message.MessageName);
        }

        [TestMethod]
        public void MessageSource()
        {
            TestMessageable messageable = new TestMessageable();
            ClearLinesMessage message = new ClearLinesMessage();
            Assert.AreEqual(null, message.Source);
            message.Source = messageable;
            Assert.ReferenceEquals(messageable, message.Source);
        }
    }
}