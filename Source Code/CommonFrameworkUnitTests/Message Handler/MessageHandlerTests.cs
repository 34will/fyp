﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CommonFramework.MessageSystem;

namespace CommonFrameworkUnitTests
{
    [TestClass]
    public class MessageHandlerTests
	{
		[TestMethod]
		public void RegisterAndRemoveIMessageable()
		{
			TestMessageable testMessageable = new TestMessageable();

			MessageHandler.Instance.RegisterMessageable(testMessageable);

			Assert.AreEqual(1, MessageHandler.Instance.NumberOfRegisteredIMessageables, "There should only be one registered IMessageable.");

			MessageHandler.Instance.RemoveMessageable(testMessageable);

			Assert.AreEqual(0, MessageHandler.Instance.NumberOfRegisteredIMessageables, "There shouldn't be any registered IMessageables.");
		}

		[TestMethod]
		public void DoubleRegisterIMessageable()
		{
			TestMessageable testMessageable = new TestMessageable();

			MessageHandler.Instance.RegisterMessageable(testMessageable);
			MessageHandler.Instance.RegisterMessageable(testMessageable);

			Assert.AreEqual(1, MessageHandler.Instance.NumberOfRegisteredIMessageables, "There should only be one registered IMessageable.");

			MessageHandler.Instance.RemoveMessageable(testMessageable);
		}

		[TestMethod]
		public void SendMessage()
		{
			TestMessageable testMessageable = new TestMessageable();

			MessageHandler.Instance.RegisterMessageable(testMessageable);

			MessageHandler.Instance.SendMessage(new TestMessage("Hello, World!"));
			MessageHandler.Instance.Update();

			Assert.AreEqual("TestMessage.Hello, World!", testMessageable.LastMessage.MessageName, "The name of the last message recieved should be \"TestMessage.Hello, World!\".");
			Assert.IsNull(testMessageable.LastMessage.Source, "The last message recieved should not have a source.");

			MessageHandler.Instance.RemoveMessageable(testMessageable);
		}

		[TestMethod]
		public void SendSpecific()
		{
			TestMessageable testMessageable = new TestMessageable();
			TargetableMessageable targetMessageable = new TargetableMessageable();

			MessageHandler.Instance.RegisterMessageable(testMessageable);
			MessageHandler.Instance.RegisterMessageable(targetMessageable);

			MessageHandler.Instance.SendMessage(new TestMessage("Hello, World!"));
			MessageHandler.Instance.Update();

			MessageHandler.Instance.SendMessage(new TestMessage("Hello, Target!"), "TargetableMessageable");
			MessageHandler.Instance.Update();

			Assert.AreEqual("TestMessage.Hello, World!", testMessageable.LastMessage.MessageName, "The name of the last message received by testMessageable should be \"TestMessage.Hello, World!\".");
			Assert.IsNull(testMessageable.LastMessage.Source, "The last message recieved by testMessageable should not have a source.");

			Assert.AreEqual("TestMessage.Hello, Target!", targetMessageable.LastMessage.MessageName, "The name of the last message received by targetMessageable should be \"TestMessage.Hello, Target!\".");
			Assert.IsNull(targetMessageable.LastMessage.Source, "The last message recieved by targetMessageable should not have a source.");

			MessageHandler.Instance.RemoveMessageable(testMessageable);
			MessageHandler.Instance.RemoveMessageable(targetMessageable);
		}

		[TestMethod]
		public void PendingMessages()
		{
			TestMessageable testMessageable = new TestMessageable();
			TestMessageable otherMessageable = new TestMessageable();

			testMessageable.PendingMessages.Enqueue(new TestMessage("Hello, World!, from testMessageable!"));

			Assert.AreEqual(1, testMessageable.PendingMessages.Count, "There should be one pending messages.");

			MessageHandler.Instance.RegisterMessageable(testMessageable);
			MessageHandler.Instance.RegisterMessageable(otherMessageable);
			MessageHandler.Instance.Update();

			Assert.AreNotEqual("Hello, World!, from testMessageable!", otherMessageable.LastMessage.MessageName, "The last message received by otherMessageable should be the message sent from testMessageable.");

			Assert.IsNull(testMessageable.LastMessage, "The last message recieved by testMessageable should be null.");

			MessageHandler.Instance.RemoveMessageable(testMessageable);
			MessageHandler.Instance.RemoveMessageable(otherMessageable);
		}
    }
}
