﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CommonFramework.Maths;
using CommonFramework.Containers.Spatial;

namespace CommonFrameworkUnitTests
{
    [TestClass]
    public class QuadTreeTests
    {
        private List<Float2> points = new List<Float2>(new Float2[]{ new Float2(1.0f, 1.0f),
                                                                     new Float2(2.0f, 1.5f),
                                                                     new Float2(3.0f, 1.0f),
                                                                     new Float2(4.0f, 1.5f),
                                                                     new Float2(1.0f, 1.0f), });

        [TestMethod]
        public void ConstructorFloats()
        {
            QuadTree<Float2> tree = new QuadTree<Float2>(1.0f, 2.0f, 3.0f, 4.0f);
            Rectangle bounds = tree.Bounds;
            Assert.AreEqual(1.0f, bounds.X);
            Assert.AreEqual(2.0f, bounds.Y);
            Assert.AreEqual(3.0f, bounds.Width);
            Assert.AreEqual(4.0f, bounds.Height);
        }

        [TestMethod]
        public void ConstructorRectangle()
        {
            QuadTree<Float2> tree = new QuadTree<Float2>(new Rectangle(1.0f, 2.0f, 3.0f, 4.0f));
            Rectangle bounds = tree.Bounds;
            Assert.AreEqual(1.0f, bounds.X);
            Assert.AreEqual(2.0f, bounds.Y);
            Assert.AreEqual(3.0f, bounds.Width);
            Assert.AreEqual(4.0f, bounds.Height);
        }

        [TestMethod]
        public void Insert()
        {
            QuadTree<Float2> tree = new QuadTree<Float2>(new Rectangle(0.0f, 0.0f, 10.0f, 10.0f));
            foreach (Float2 point in points)
                Assert.IsTrue(tree.Insert(point));
            Assert.AreEqual(points.Count, tree.ItemCount, "The tree should be able to count the added items");
        }

        [TestMethod]
        public void InsertFail()
        {
            QuadTree<Float2> tree = new QuadTree<Float2>(new Rectangle(0.0f, 0.0f, 10.0f, 10.0f));
            Assert.IsFalse(tree.Insert(new Float2(-0.5f, 5.0f)));
            Assert.IsFalse(tree.Insert(new Float2(11.0f, 5.0f)));
            Assert.IsFalse(tree.Insert(new Float2(5.0f, -1.0f)));
            Assert.IsFalse(tree.Insert(new Float2(5.0f, 10.5f)));
            Assert.AreEqual(0, tree.ItemCount, "The tree should not be able to count the added items");
        }

        [TestMethod]
        public void Enumerator()
        {
            // Create tree
            QuadTree<Float2> tree = new QuadTree<Float2>(new Rectangle(0.0f, 0.0f, 10.0f, 10.0f));
            foreach (Float2 point in points)
                Assert.IsTrue(tree.Insert(point));

            // Check the correct points have been added
            int count = 0;
            foreach(Float2 point in tree)
            {
                Assert.IsTrue(points.Contains(point));
                count++;
            }
            Assert.AreEqual(points.Count, count);
        }

        [TestMethod]
        public void SearchFloats()
        {
            // Create tree
            QuadTree<Float2> tree = new QuadTree<Float2>(new Rectangle(0.0f, 0.0f, 10.0f, 10.0f));
            foreach (Float2 point in points)
                Assert.IsTrue(tree.Insert(point));

            // Search for the points
            int count = 0;
            foreach(Float2 point in tree.Search(0.5f, 0.5f, 2.0f, 2.0f))
            {
                Assert.IsTrue(points.Contains(point));
                count++;
            }

            Assert.AreEqual(3, count, "The number of found points in the scenario should be 3");
        }

        [TestMethod]
        public void SearchRectangle()
        {
            // Create tree
            QuadTree<Float2> tree = new QuadTree<Float2>(new Rectangle(0.0f, 0.0f, 10.0f, 10.0f));
            foreach (Float2 point in points)
                Assert.IsTrue(tree.Insert(point));

            // Search for the points
            int count = 0;
            foreach (Float2 point in tree.Search(new Rectangle(0.5f, 0.5f, 2.0f, 2.0f)))
            {
                Assert.IsTrue(points.Contains(point));
                count++;
            }

            Assert.AreEqual(3, count, "The number of found points in the scenario should be 3");
        }

        [TestMethod]
        public void SubTreeItems()
        {
            // Create tree
            QuadTree<Float2> tree = new QuadTree<Float2>(new Rectangle(0.0f, 0.0f, 10.0f, 10.0f));
            foreach (Float2 point in points)
                Assert.IsTrue(tree.Insert(point));

            // Make sure the tree contains all the inserted items in the sub-tree
            int count = 0;
            foreach (Float2 point in tree.SubTreeItems)
            {
                Assert.IsTrue(points.Contains(point));
                count++;
            }
        }

        [TestMethod]
        public void SubTreeQuads()
        {
            // Create tree
            QuadTree<Float2> tree = new QuadTree<Float2>(new Rectangle(0.0f, 0.0f, 10.0f, 10.0f));
            Assert.IsTrue(tree.Insert(new Float2(1.0f, 1.0f)));
            Assert.IsTrue(tree.Insert(new Float2(9.0f, 1.0f)));
            Assert.IsTrue(tree.Insert(new Float2(9.0f, 9.0f)));
            Assert.IsTrue(tree.Insert(new Float2(1.0f, 9.0f)));

            // Check the quads after the insert
            List<Rectangle> quads = new List<Rectangle>(tree.SubTreeQuads);
            Assert.AreEqual(4, quads.Count, "The inserted points should be arranged as to produce 4 quads");

            Assert.IsTrue(tree.Insert(new Float2(6.0f, 6.0f)));

            // Check the quads after the insert
            quads = new List<Rectangle>(tree.SubTreeQuads);
            Assert.AreEqual(8, quads.Count, "The inserted points should be arranged as to produce 8 quads");
        }

        [TestMethod]
        public void ItemCount()
        {
            // Create tree
            QuadTree<Float2> tree = new QuadTree<Float2>(new Rectangle(0.0f, 0.0f, 10.0f, 10.0f));
            foreach (Float2 point in points)
                Assert.IsTrue(tree.Insert(point));

            // Check the property against the list count
            Assert.AreEqual(points.Count, tree.ItemCount, "The trees item count should contain the same number of Float2 values as the list they were inserted from");
        }

        [TestMethod]
        public void MinimumQuadArea()
        {
            // Create tree
            QuadTree<Float2> tree = new QuadTree<Float2>(new Rectangle(0.0f, 0.0f, 10.0f, 10.0f));
            tree.MinimumQuadArea = 0.5f;
            Assert.AreEqual(0.5f, tree.MinimumQuadArea);
        }

        [TestMethod]
        public void Bounds()
        {
            Rectangle inBounds = new Rectangle(1.0f, 2.0f, 3.0f, 4.0f);
            QuadTree<Float2> tree = new QuadTree<Float2>(inBounds);
            Rectangle bounds = tree.Bounds;
            Assert.IsFalse(Object.ReferenceEquals(inBounds, bounds));
            Assert.AreEqual(1.0f, bounds.X);
            Assert.AreEqual(2.0f, bounds.Y);
            Assert.AreEqual(3.0f, bounds.Width);
            Assert.AreEqual(4.0f, bounds.Height);
        }
    }
}
