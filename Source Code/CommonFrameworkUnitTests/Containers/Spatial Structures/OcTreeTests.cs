﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CommonFramework.Maths;
using CommonFramework.Containers.Spatial;

namespace CommonFrameworkUnitTests
{
    [TestClass]
    public class OcTreeTests
    {
        private List<Float3> points = new List<Float3>(new Float3[]{ new Float3(1.0f, 1.0f, 1.0f),
                                                                     new Float3(2.0f, 1.5f, 1.0f),
                                                                     new Float3(3.0f, 1.0f, 1.5f),
                                                                     new Float3(4.0f, 1.5f, 1.0f),
                                                                     new Float3(1.0f, 1.0f, 2.0f), });

        [TestMethod]
        public void ConstructorFloats()
        {
            OcTree<Float3> tree = new OcTree<Float3>(1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f);
            Cuboid bounds = tree.Bounds;
            Assert.AreEqual(1.0f, bounds.X);
            Assert.AreEqual(2.0f, bounds.Y);
            Assert.AreEqual(3.0f, bounds.Z);
            Assert.AreEqual(4.0f, bounds.Width);
            Assert.AreEqual(5.0f, bounds.Height);
            Assert.AreEqual(6.0f, bounds.Depth);
        }

        [TestMethod]
        public void ConstructorCuboid()
        {
            OcTree<Float3> tree = new OcTree<Float3>(new Cuboid(1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f));
            Cuboid bounds = tree.Bounds;
            Assert.AreEqual(1.0f, bounds.X);
            Assert.AreEqual(2.0f, bounds.Y);
            Assert.AreEqual(3.0f, bounds.Z);
            Assert.AreEqual(4.0f, bounds.Width);
            Assert.AreEqual(5.0f, bounds.Height);
            Assert.AreEqual(6.0f, bounds.Depth);
        }

        [TestMethod]
        public void Insert()
        {
            OcTree<Float3> tree = new OcTree<Float3>(0.0f, 0.0f, 0.0f, 10.0f, 10.0f, 10.0f);
            foreach (Float3 point in points)
                Assert.IsTrue(tree.Insert(point));
            Assert.AreEqual(points.Count, tree.ItemCount, "The tree should be able to count the added items");
        }

        [TestMethod]
        public void InsertFail()
        {
            OcTree<Float3> tree = new OcTree<Float3>(0.0f, 0.0f, 0.0f, 10.0f, 10.0f, 10.0f);
            Assert.IsFalse(tree.Insert(new Float3(-0.5f, 5.0f, 5.0f)));
            Assert.IsFalse(tree.Insert(new Float3(11.0f, 5.0f, 5.0f)));
            Assert.IsFalse(tree.Insert(new Float3(5.0f, -1.0f, 5.0f)));
            Assert.IsFalse(tree.Insert(new Float3(5.0f, 10.5f, 5.0f)));
            Assert.IsFalse(tree.Insert(new Float3(5.0f, 5.0f, -0.5f)));
            Assert.IsFalse(tree.Insert(new Float3(5.0f, 5.0f, 10.1f)));
            Assert.AreEqual(0, tree.ItemCount, "The tree should not be able to count the added items");
        }

        [TestMethod]
        public void Enumerator()
        {
            OcTree<Float3> tree = new OcTree<Float3>(0.0f, 0.0f, 0.0f, 10.0f, 10.0f, 10.0f);
            foreach (Float3 point in points)
                Assert.IsTrue(tree.Insert(point));

            // Check the correct points have been added
            int count = 0;
            foreach (Float3 point in tree)
            {
                Assert.IsTrue(points.Contains(point));
                count++;
            }
            Assert.AreEqual(points.Count, count);
        }

        [TestMethod]
        public void SearchFloats()
        {
            OcTree<Float3> tree = new OcTree<Float3>(0.0f, 0.0f, 0.0f, 10.0f, 10.0f, 10.0f);
            foreach (Float3 point in points)
                Assert.IsTrue(tree.Insert(point));

            // Search for the points
            int count = 0;
            foreach (Float3 point in tree.Search(0.5f, 0.5f, 0.5f, 2.0f, 2.0f, 2.0f))
            {
                Assert.IsTrue(points.Contains(point));
                count++;
            }

            Assert.AreEqual(3, count, "The number of found points in the scenario should be 3");
        }

        [TestMethod]
        public void SearchCuboid()
        {
            OcTree<Float3> tree = new OcTree<Float3>(0.0f, 0.0f, 0.0f, 10.0f, 10.0f, 10.0f);
            foreach (Float3 point in points)
                Assert.IsTrue(tree.Insert(point));

            // Search for the points
            int count = 0;
            foreach (Float3 point in tree.Search(new Cuboid(0.5f, 0.5f, 0.5f, 2.0f, 2.0f, 2.0f)))
            {
                Assert.IsTrue(points.Contains(point));
                count++;
            }

            Assert.AreEqual(3, count, "The number of found points in the scenario should be 3");
        }

        [TestMethod]
        public void NearestNeighbourShallow()
        {
            // Create tree
            OcTree<Float3> tree = new OcTree<Float3>(0.0f, 0.0f, 0.0f, 10.0f, 10.0f, 10.0f);
            foreach (Float3 point in points)
                Assert.IsTrue(tree.Insert(point));

            Float3 nearest = tree.NearestNeighbour(new Float3(4.0f, 1.0f, 1.0f));
            Assert.ReferenceEquals(points[3], nearest);
        }

        [TestMethod]
        public void NearestNeighbourExistingItem()
        {
            // Create tree
            OcTree<Float3> tree = new OcTree<Float3>(0.0f, 0.0f, 0.0f, 10.0f, 10.0f, 10.0f);
            Float3[] points = new Float3[] { new Float3(1.0f, 1.0f, 1.0f), new Float3(2.0f, 2.0f, 2.0f), new Float3(3.5f, 3.5f, 3.5f), new Float3(7.5f, 7.5f, 7.5f) };
            foreach (Float3 point in points)
                tree.Insert(point);

            Float3 nearest = tree.NearestNeighbour(points[0]);
            Assert.ReferenceEquals(points[1], nearest);
        }

        [TestMethod]
        public void NearestNeighbourNullSubItems()
        {
            OcTree<Float3> tree = new OcTree<Float3>(0.0f, 0.0f, 0.0f, 10.0f, 10.0f, 10.0f);
            Float3 nearest = tree.NearestNeighbour(points[0]);
            Assert.IsNull(nearest);
        }

        [TestMethod]
        public void NearestNeighbourOppositeCell()
        {
            OcTree<Float3> tree = new OcTree<Float3>(0.0f, 0.0f, 0.0f, 10.0f, 10.0f, 10.0f);
            Float3 point = new Float3(9.0f, 9.0f, 9.0f);
            Assert.IsTrue(tree.Insert(point));
            Float3 foundPoint = tree.NearestNeighbour(new Float3(1.0f, 1.0f, 1.0f));
            Assert.ReferenceEquals(point, foundPoint);
        }

        [TestMethod]
        public void NearestNeighbourOppositeCellDeep()
        {
			// TODO - Meet last 5.41% of code coverage.
            OcTree<Float3> tree = new OcTree<Float3>(0.0f, 0.0f, 0.0f, 10.0f, 10.0f, 10.0f);
            Float3 point = new Float3(4.5f, 4.5f, 4.5f);
            Assert.IsTrue(tree.Insert(point));
            Assert.IsTrue(tree.Insert(new Float3(4.56f, 4.56f, 4.56f)));
            Assert.IsTrue(tree.Insert(new Float3(4.6f, 4.6f, 4.6f)));
            Assert.IsTrue(tree.Insert(new Float3(4.7f, 4.7f, 4.7f)));
            Float3 foundPoint = tree.NearestNeighbour(new Float3(1.0f, 1.0f, 1.0f));
            Assert.ReferenceEquals(point, foundPoint);
        }

        [TestMethod]
        public void SubTreeItems()
        {
            // Create tree
            OcTree<Float3> tree = new OcTree<Float3>(0.0f, 0.0f, 0.0f, 10.0f, 10.0f, 10.0f);
            foreach (Float3 point in points)
                Assert.IsTrue(tree.Insert(point));

            // Make sure the tree contains all the inserted items in the sub-tree
            int count = 0;
            foreach (Float3 point in tree.SubTreeItems)
            {
                Assert.IsTrue(points.Contains(point));
                count++;
            }
        }

        [TestMethod]
        public void SubTreeCubes()
        {
            // Create tree
            OcTree<Float3> tree = new OcTree<Float3>(0.0f, 0.0f, 0.0f, 10.0f, 10.0f, 10.0f);
            Assert.IsTrue(tree.Insert(new Float3(1.0f, 1.0f, 1.0f)));
            Assert.IsTrue(tree.Insert(new Float3(9.0f, 1.0f, 1.0f)));
            Assert.IsTrue(tree.Insert(new Float3(9.0f, 9.0f, 1.0f)));
            Assert.IsTrue(tree.Insert(new Float3(1.0f, 9.0f, 1.0f)));

            // Check the quads after the insert
            List<Cuboid> quads = new List<Cuboid>(tree.SubTreeCubes);
            Assert.AreEqual(8, quads.Count, "The inserted points should be arranged as to produce 4 quads");

            Assert.IsTrue(tree.Insert(new Float3(6.0f, 6.0f, 1.0f)));

            // Check the quads after the insert
            quads = new List<Cuboid>(tree.SubTreeCubes);
            Assert.AreEqual(16, quads.Count, "The inserted points should be arranged as to produce 8 quads");
        }

        [TestMethod]
        public void ItemCount()
        {
            // Create tree
            OcTree<Float3> tree = new OcTree<Float3>(0.0f, 0.0f, 0.0f, 10.0f, 10.0f, 10.0f);
            foreach (Float3 point in points)
                Assert.IsTrue(tree.Insert(point));

            // Check the property against the list count
            Assert.AreEqual(points.Count, tree.ItemCount, "The trees item count should contain the same number of Float3 values as the list they were inserted from");
        }

        [TestMethod]
        public void MinimumCubeVolume()
        {
            // Create tree
            OcTree<Float3> tree = new OcTree<Float3>(0.0f, 0.0f, 0.0f, 10.0f, 10.0f, 10.0f);
            tree.MinimumCellVolume = 0.5f;
            Assert.AreEqual(0.5f, tree.MinimumCellVolume);
        }

        [TestMethod]
        public void Bounds()
        {
            Cuboid inBounds = new Cuboid(1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f);
            OcTree<Float3> tree = new OcTree<Float3>(inBounds);
            Cuboid bounds = tree.Bounds;
            Assert.IsFalse(Object.ReferenceEquals(inBounds, bounds));
            Assert.AreEqual(1.0f, bounds.X);
            Assert.AreEqual(2.0f, bounds.Y);
            Assert.AreEqual(3.0f, bounds.Z);
            Assert.AreEqual(4.0f, bounds.Width);
            Assert.AreEqual(5.0f, bounds.Height);
            Assert.AreEqual(6.0f, bounds.Depth);
        }
    }
}
