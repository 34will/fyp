﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CommonFramework.Maths;
using CommonFramework.Containers.Spatial;

namespace CommonFrameworkUnitTests
{
    [TestClass]
    public class KDTreeTests
    {
        private List<Float2> points = new List<Float2>(new Float2[]{ new Float2(1.0f, 1.0f),
                                                                     new Float2(2.0f, 1.5f),
                                                                     new Float2(3.0f, 1.0f),
                                                                     new Float2(4.0f, 1.5f),
                                                                     new Float2(1.02f, 1.01f), });

        [TestMethod]
        public void Constructor()
        {
            KDTree<Float2> tree = new KDTree<Float2>(2);
            Assert.AreEqual(2, tree.Dimensions, "The number of dimensions for a KDTree<Float2> should be 2");
        }

        [TestMethod]
        public void Insert()
        {
            KDTree<Float2> tree = new KDTree<Float2>(2);
            foreach (Float2 point in points)
                Assert.IsTrue(tree.Insert(point));
            Assert.AreEqual(points.Count, tree.ItemCount, "The tree should be able to count the added items");
        }

        [TestMethod]
        public void InsertDuplicate()
        {
            KDTree<Float2> tree = new KDTree<Float2>(2);
            foreach (Float2 point in points)
                Assert.IsTrue(tree.Insert(point));
            Assert.IsFalse(tree.Insert(points[0]));
            Assert.AreEqual(points.Count, tree.ItemCount, "The tree should be able to count the added items");
        }

        [TestMethod]
        public void NearestNeighbours()
        {
            // Create tree
            KDTree<Float2> tree = new KDTree<Float2>(2, 2);
            foreach (Float2 point in points)
                Assert.IsTrue(tree.Insert(point));

            SortedList<float, IPositionableND> nearest = tree.NearestNeighbours(new Float2(4.0f, 1.0f), 1);
            Assert.ReferenceEquals(points[3], nearest.Values[0]);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void NearestNeighboursIncorrectDimensions()
        {
            KDTree<Float2> tree = new KDTree<Float2>(2, 1);
            foreach (Float2 point in points)
                Assert.IsTrue(tree.Insert(point));
            SortedList<float, IPositionableND> nearest = tree.NearestNeighbours(new Float3(4.0f, 1.0f, 2.0f), 1);
        }
        
        [TestMethod]
        public void NearestNeighboursItemsMaxInfinite()
        {
            // Create tree
            KDTree<Float2> tree = new KDTree<Float2>(2, 1);
            foreach (Float2 point in points)
                Assert.IsTrue(tree.Insert(point));

            SortedList<float, IPositionableND> nearest = tree.NearestNeighbours(new Float2(4.0f, 1.0f), 8);
            Assert.ReferenceEquals(points[3], nearest.Values[0]);
        }
        
        [TestMethod]
        public void Enumerator()
        {
            KDTree<Float2> tree = new KDTree<Float2>(2);
            foreach (Float2 point in points)
                Assert.IsTrue(tree.Insert(point));

            // Check the correct points have been added
            int count = 0;
            foreach (Float2 point in tree)
            {
                Assert.IsTrue(points.Contains(point));
                count++;
            }
            Assert.AreEqual(points.Count, count);
        }

        [TestMethod]
        public void Dimensions()
        {
            KDTree<Float2> a = new KDTree<Float2>(2);
            Assert.AreEqual(2, a.Dimensions);
            KDTree<Float3> b = new KDTree<Float3>(3);
            Assert.AreEqual(3, b.Dimensions);
            KDTree<FloatN> c = new KDTree<FloatN>(256);
            Assert.AreEqual(256, c.Dimensions);
        }

        [TestMethod]
        public void ItemCount()
        {
            // Create tree
            KDTree<Float2> tree = new KDTree<Float2>(2, 1);
            foreach (Float2 point in points)
                Assert.IsTrue(tree.Insert(point));

            // Check the property against the list count
            Assert.AreEqual(points.Count, tree.ItemCount, "The trees item count should contain the same number of Float2 values as the list they were inserted from");
        }

        [TestMethod]
        public void SubTreeItems()
        {
            // Create tree
            KDTree<Float2> tree = new KDTree<Float2>(2, 1);
            foreach (Float2 point in points)
                Assert.IsTrue(tree.Insert(point));

            // Make sure the tree contains all the inserted items in the sub-tree
            int count = 0;
            foreach (Float2 point in tree.SubTreeItems)
            {
                Assert.IsTrue(points.Contains(point));
                count++;
            }
        }

        [TestMethod]
        public void SubTreeAxes()
        {
            // Create tree
            KDTree<Float2> tree = new KDTree<Float2>(2, 1);
            foreach (Float2 point in points)
                Assert.IsTrue(tree.Insert(point));

            // Make sure the tree contains all the inserted items in the sub-tree
            int count = 0;
            foreach (var segments in tree.SubTreeAxes)
                count++;

            Assert.AreEqual(9, count, "The number of segments for the points inserted should be 9");
        }
    }
}
