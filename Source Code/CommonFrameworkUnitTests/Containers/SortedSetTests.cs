﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CommonFramework.Containers;

using IntSortedSet = CommonFramework.Containers.SortedSet<int>;

namespace CommonFrameworkUnitTests
{
	[TestClass]
	public class SortedSetTests
	{
		[TestMethod]
		public void Add()
		{
			IntSortedSet sortedSet = new IntSortedSet();

			Assert.IsTrue(sortedSet.Add(5), "Should be able to insert an item into an emtpy set.");
			Assert.AreEqual(1, sortedSet.Count, "Size of the set after one Add should be 1.");
		}

		[TestMethod]
		public void MultipleAdd()
		{
			IntSortedSet sortedSet = new IntSortedSet();

			Assert.IsTrue(sortedSet.Add(5), "Should be able to insert an item into an emtpy set.");
			Assert.IsTrue(sortedSet.Add(6), "Should be able to insert 6 into a set of { 5 }.");
			Assert.IsTrue(sortedSet.Add(7), "Should be able to insert 7 into a set of { 5, 6 }.");
			Assert.IsTrue(sortedSet.Add(8), "Should be able to insert 8 into a set of { 5, 6, 7 }.");
			Assert.AreEqual(4, sortedSet.Count, "Size of the set after four non-conflicting Adds should be 4.");
		}

		[TestMethod]
		public void MultipleConflictingAdd()
		{
			IntSortedSet sortedSet = new IntSortedSet();

			sortedSet.Add(5);
			sortedSet.Add(6);

			Assert.IsFalse(sortedSet.Add(6), "Should not be able to insert 6 into a set of { 5, 6 }.");
			Assert.AreEqual(2, sortedSet.Count, "Size of the set after two non-conflicting and one conflicting Add should be 2.");
		}

		[TestMethod]
		public void ICollectionAdd()
		{
			IntSortedSet sortedSet = new IntSortedSet();
			ICollection<int> coll = sortedSet;

			coll.Add(5);

			Assert.AreEqual(1, sortedSet.Count, "Size of the set after one ICollection Add should be 1.");
		}

		[TestMethod]
		public void SortingSortedInput()
		{
			IntSortedSet sortedSet = new IntSortedSet();

			sortedSet.Add(5);
			sortedSet.Add(6);
			sortedSet.Add(7);
			sortedSet.Add(9);

			string output = "";
			foreach(int i in sortedSet)
				output += i;

			Assert.AreEqual("5679", output, "Output concatenated string of sorted inputs should be \"5679\".");
			Assert.AreEqual(5, sortedSet.Minimum, "The minimum value of the set should be 5.");
			Assert.AreEqual(9, sortedSet.Maximum, "The maximum value of the set should be 9.");
		}

		[TestMethod]
		public void SortingUnsortedInput()
		{
			IntSortedSet sortedSet = new IntSortedSet();

			sortedSet.Add(2);
			sortedSet.Add(8);
			sortedSet.Add(5);
			sortedSet.Add(7);

			string output = "";
			foreach(int i in sortedSet)
				output += i;

			Assert.AreEqual("2578", output, "Output concatenated string of unsorted inputs should be \"2578\".");
		}

		[TestMethod]
		public void SortingSortedMinMax()
		{
			IntSortedSet sortedSet = new IntSortedSet();

			sortedSet.Add(2);
			sortedSet.Add(5);
			sortedSet.Add(6);
			sortedSet.Add(7);

			Assert.AreEqual(2, sortedSet.Minimum, "The minimum value of the set should be 2.");
			Assert.AreEqual(7, sortedSet.Maximum, "The maximum value of the set should be 7.");
		}

		[TestMethod]
		public void SortingUnsortedMinMax()
		{
			IntSortedSet sortedSet = new IntSortedSet();

			sortedSet.Add(2);
			sortedSet.Add(8);
			sortedSet.Add(5);
			sortedSet.Add(7);

			Assert.AreEqual(2, sortedSet.Minimum, "The minimum value of the set should be 2.");
			Assert.AreEqual(8, sortedSet.Maximum, "The maximum value of the set should be 8.");
		}

		[TestMethod]
		public void Clear()
		{
			IntSortedSet sortedSet = new IntSortedSet();

			sortedSet.Add(5);
			sortedSet.Add(6);
			sortedSet.Add(7);

			sortedSet.Clear();

			Assert.AreEqual(0, sortedSet.Count, "Size of the set after three non-conflicting Adds and then Clearing should be 0.");
		}

		[TestMethod]
		public void RemovePresentItem()
		{
			IntSortedSet sortedSet = new IntSortedSet();

			sortedSet.Add(5);

			Assert.IsTrue(sortedSet.Remove(5), "Should succeed when trying to Remove an object present in the set.");
			Assert.AreEqual(0, sortedSet.Count, "Size of the set after Removing the only object in the set should be 0.");
		}

		[TestMethod]
		public void RemoveAbsentItem()
		{
			IntSortedSet sortedSet = new IntSortedSet();

			sortedSet.Add(5);

			Assert.IsFalse(sortedSet.Remove(6), "Should fail when trying to Remove an object not present in the set.");
			Assert.AreEqual(1, sortedSet.Count, "Size of the set after failing to Remove the only object in the set should be 1.");
		}

		[TestMethod]
		public void RemoveNodeWithNoChildren()
		{
			IntSortedSet sortedSet = new IntSortedSet();

			sortedSet.Add(5);
			sortedSet.Add(9);
			sortedSet.Add(2);
			sortedSet.Add(3);
			sortedSet.Add(-4);

			Assert.IsTrue(sortedSet.Remove(-4), "Should succeed when trying to Remove an object present in the set.");
			Assert.AreEqual(4, sortedSet.Count, "Size of the set after Removing one in the set of 5 should be 4.");

			string output = "";
			foreach(int i in sortedSet)
				output += i;

			Assert.AreEqual("2359", output, "Output concatenated string of objects should be \"2359\".");
		}

		[TestMethod]
		public void RemoveRightNodeWithOneChild()
		{
			IntSortedSet sortedSet = new IntSortedSet();

			sortedSet.Add(5);
			sortedSet.Add(18);
			sortedSet.Add(19);
			sortedSet.Add(21);
			sortedSet.Add(25);
			sortedSet.Add(2);
			sortedSet.Add(3);
			sortedSet.Add(-4);

			Assert.IsTrue(sortedSet.Remove(18), "Should succeed when trying to Remove an object present in the set.");
			Assert.AreEqual(7, sortedSet.Count, "Size of the set after Removing one in the set of 8 should be 7.");

			string output = "";
			foreach(int i in sortedSet)
				output += i + " ";

			Assert.AreEqual("-4 2 3 5 19 21 25 ", output, "Output concatenated string of objects should be \"-4 2 3 5 19 21 25 \".");
		}

		[TestMethod]
		public void RemoveLeftNodeWithOneChild()
		{
			IntSortedSet sortedSet = new IntSortedSet();

			sortedSet.Add(5);
			sortedSet.Add(18);
			sortedSet.Add(19);
			sortedSet.Add(21);
			sortedSet.Add(25);
			sortedSet.Add(2);
			sortedSet.Add(-4);

			Assert.IsTrue(sortedSet.Remove(2), "Should succeed when trying to Remove an object present in the set.");
			Assert.AreEqual(6, sortedSet.Count, "Size of the set after Removing one in the set of 7 should be 6.");

			string output = "";
			foreach(int i in sortedSet)
				output += i + " ";

			Assert.AreEqual("-4 5 18 19 21 25 ", output, "Output concatenated string of objects should be \"-4 5 18 19 21 25 \".");
		}

		[TestMethod]
		public void RemoveZigZagNodeWithOneChild()
		{
			IntSortedSet sortedSet = new IntSortedSet();

			sortedSet.Add(5);
			sortedSet.Add(18);
			sortedSet.Add(19);
			sortedSet.Add(21);
			sortedSet.Add(20);
			sortedSet.Add(2);
			sortedSet.Add(3);
			sortedSet.Add(-4);

			Assert.IsTrue(sortedSet.Remove(21), "Should succeed when trying to Remove an object present in the set.");
			Assert.AreEqual(7, sortedSet.Count, "Size of the set after Removing one in the set of 8 should be 7.");

			string output = "";
			foreach(int i in sortedSet)
				output += i + " ";

			Assert.AreEqual("-4 2 3 5 18 19 20 ", output, "Output concatenated string of objects should be \"-4 2 3 5 18 19 20 \".");
		}

		[TestMethod]
		public void RemoveNodeWithTwoChildren()
		{
			IntSortedSet sortedSet = new IntSortedSet();

			sortedSet.Add(5);
			sortedSet.Add(12);
			sortedSet.Add(9);
			sortedSet.Add(21);
			sortedSet.Add(19);
			sortedSet.Add(25);
			sortedSet.Add(2);
			sortedSet.Add(3);
			sortedSet.Add(-4);

			Assert.IsTrue(sortedSet.Remove(12), "Should succeed when trying to Remove an object present in the set.");
			Assert.AreEqual(8, sortedSet.Count, "Size of the set after Removing one in the set of 9 should be 8.");

			string output = "";
			foreach(int i in sortedSet)
				output += i + " ";

			Assert.AreEqual("-4 2 3 5 9 19 21 25 ", output, "Output concatenated string of objects should be \"-4 2 3 5 9 19 21 25 \".");
		}

		[TestMethod]
		public void CustomComparerConstructor()
		{
			TestReverseIntComparer tric = new TestReverseIntComparer();
			IntSortedSet sortedSet = new IntSortedSet(tric);

			sortedSet.Add(5);
			sortedSet.Add(6);
			sortedSet.Add(7);
			sortedSet.Add(9);

			string output = "";
			foreach(int i in sortedSet)
				output += i;

			Assert.AreEqual("9765", output, "Output concatenated string of sorted inputs should be \"9765\", due to the inverted comparer.");
			Assert.AreEqual(tric, sortedSet.Comparer, "Comparer of set should be equal to the one set from outside.");
			Assert.AreEqual(9, sortedSet.Minimum, "The minimum value of the set should be 9, due to the inverted comparer.");
			Assert.AreEqual(5, sortedSet.Maximum, "The maximum value of the set should be 5, due to the inverted comparer.");
		}

		[TestMethod]
		public void IEnumerableConstructor()
		{
			int[] intArray = new int[] { 5, 6, 7, 8 };
			IntSortedSet sortedSet = new IntSortedSet(intArray);

			string output = "";
			foreach(int i in sortedSet)
				output += i;

			Assert.AreEqual("5678", output, "Output concatenated string of sorted inputs should be \"5678\".");
			Assert.AreEqual(5, sortedSet.Minimum, "The minimum value of the set should be 5.");
			Assert.AreEqual(8, sortedSet.Maximum, "The maximum value of the set should be 8.");
		}

		[TestMethod]
		public void CustomComparerAndIEnumerableConstructor()
		{
			int[] intArray = new int[] { 5, 6, 7, 8 };
			TestReverseIntComparer tric = new TestReverseIntComparer();
			IntSortedSet sortedSet = new IntSortedSet(tric, intArray);

			string output = "";
			foreach(int i in sortedSet)
				output += i;

			Assert.AreEqual("8765", output, "Output concatenated string of sorted inputs should be \"8765\", due to the inverted comparer.");
			Assert.AreEqual(tric, sortedSet.Comparer, "Comparer of set should be equal to the one set from outside.");
			Assert.AreEqual(8, sortedSet.Minimum, "The minimum value of the set should be 8, due to the inverted comparer.");
			Assert.AreEqual(5, sortedSet.Maximum, "The maximum value of the set should be 5, due to the inverted comparer.");
		}

		[TestMethod]
		public void CopyTo()
		{
			int[] intArray = new int[5];
			IntSortedSet sortedSet = new IntSortedSet();

			sortedSet.Add(18);
			sortedSet.Add(9);
			sortedSet.Add(-10);
			sortedSet.Add(32);
			sortedSet.Add(0);

			sortedSet.CopyTo(intArray);

			Assert.AreEqual(-10, intArray[0], "1st element of array should be -10.");
			Assert.AreEqual(0, intArray[1], "2nd element of array should be 0.");
			Assert.AreEqual(9, intArray[2], "3rd element of array should be 9.");
			Assert.AreEqual(18, intArray[3], "4th element of array should be 18.");
			Assert.AreEqual(32, intArray[4], "5th element of array should be 32.");
		}

		[TestMethod]
		public void CopyToWithArrayTooSmall()
		{
			int[] intArray = new int[4];
			IntSortedSet sortedSet = new IntSortedSet();

			sortedSet.Add(18);
			sortedSet.Add(9);
			sortedSet.Add(-10);
			sortedSet.Add(32);
			sortedSet.Add(0);

			sortedSet.CopyTo(intArray);

			Assert.AreEqual(0, intArray[0], "1st element of array should be 0.");
			Assert.AreEqual(0, intArray[1], "2nd element of array should be 0.");
			Assert.AreEqual(0, intArray[2], "3rd element of array should be 0.");
			Assert.AreEqual(0, intArray[3], "4th element of array should be 0.");
		}

		[TestMethod]
		public void CopyToWithNullArray()
		{
			int[] intArray = null;
			IntSortedSet sortedSet = new IntSortedSet();

			sortedSet.Add(18);
			sortedSet.Add(9);
			sortedSet.Add(-10);
			sortedSet.Add(32);
			sortedSet.Add(0);

			sortedSet.CopyTo(intArray);

			Assert.IsNull(intArray, "Array should be null.");
		}

		[TestMethod]
		public void CopyToArrayIndex()
		{
			int[] intArray = new int[7];
			for(int i = 0; i < 7; i++)
				intArray[i] = int.MaxValue;

			IntSortedSet sortedSet = new IntSortedSet();

			sortedSet.Add(18);
			sortedSet.Add(9);
			sortedSet.Add(-10);
			sortedSet.Add(32);
			sortedSet.Add(0);

			sortedSet.CopyTo(intArray, 2);

			Assert.AreEqual(int.MaxValue, intArray[0], "1st element of array should be int.MaxValue.");
			Assert.AreEqual(int.MaxValue, intArray[1], "2nd element of array should be int.MaxValue.");
			Assert.AreEqual(-10, intArray[2], "3rd element of array should be -10.");
			Assert.AreEqual(0, intArray[3], "4th element of array should be 0.");
			Assert.AreEqual(9, intArray[4], "5th element of array should be 9.");
			Assert.AreEqual(18, intArray[5], "6th element of array should be 18.");
			Assert.AreEqual(32, intArray[6], "7th element of array should be 32.");
		}

		[TestMethod]
		public void CopyToArrayIndexWithArrayTooSmall()
		{
			int[] intArray = new int[5];
			for(int i = 0; i < 5; i++)
				intArray[i] = int.MaxValue;

			IntSortedSet sortedSet = new IntSortedSet();

			sortedSet.Add(18);
			sortedSet.Add(9);
			sortedSet.Add(-10);
			sortedSet.Add(32);
			sortedSet.Add(0);

			sortedSet.CopyTo(intArray, 2);

			Assert.AreEqual(int.MaxValue, intArray[0], "1st element of array should be int.MaxValue.");
			Assert.AreEqual(int.MaxValue, intArray[1], "2nd element of array should be int.MaxValue.");
			Assert.AreEqual(int.MaxValue, intArray[2], "3rd element of array should be int.MaxValue.");
			Assert.AreEqual(int.MaxValue, intArray[3], "4th element of array should be int.MaxValue.");
			Assert.AreEqual(int.MaxValue, intArray[4], "5th element of array should be int.MaxValue.");
		}

		[TestMethod]
		public void CopyToArrayIndexWithNullArray()
		{
			int[] intArray = null;
			IntSortedSet sortedSet = new IntSortedSet();

			sortedSet.Add(18);
			sortedSet.Add(9);
			sortedSet.Add(-10);
			sortedSet.Add(32);
			sortedSet.Add(0);

			sortedSet.CopyTo(intArray, 2);

			Assert.IsNull(intArray, "Array should be null.");
		}

		[TestMethod]
		public void CopyNumberToArrayIndex()
		{
			int[] intArray = new int[7];
			for(int i = 0; i < 7; i++)
				intArray[i] = int.MaxValue;

			IntSortedSet sortedSet = new IntSortedSet();

			sortedSet.Add(18);
			sortedSet.Add(9);
			sortedSet.Add(-10);
			sortedSet.Add(32);
			sortedSet.Add(0);

			sortedSet.CopyTo(intArray, 2, 3);

			Assert.AreEqual(int.MaxValue, intArray[0], "1st element of array should be int.MaxValue.");
			Assert.AreEqual(int.MaxValue, intArray[1], "2nd element of array should be int.MaxValue.");
			Assert.AreEqual(-10, intArray[2], "3rd element of array should be -10.");
			Assert.AreEqual(0, intArray[3], "4th element of array should be 0.");
			Assert.AreEqual(9, intArray[4], "5th element of array should be 9.");
			Assert.AreEqual(int.MaxValue, intArray[5], "6th element of array should be int.MaxValue.");
			Assert.AreEqual(int.MaxValue, intArray[6], "7th element of array should be int.MaxValue.");
		}

		[TestMethod]
		public void CopyNumberToArrayIndexWithArrayTooSmall()
		{
			int[] intArray = new int[4];
			for(int i = 0; i < 4; i++)
				intArray[i] = int.MaxValue;

			IntSortedSet sortedSet = new IntSortedSet();

			sortedSet.Add(18);
			sortedSet.Add(9);
			sortedSet.Add(-10);
			sortedSet.Add(32);
			sortedSet.Add(0);

			sortedSet.CopyTo(intArray, 2, 3);

			Assert.AreEqual(int.MaxValue, intArray[0], "1st element of array should be int.MaxValue.");
			Assert.AreEqual(int.MaxValue, intArray[1], "2nd element of array should be int.MaxValue.");
			Assert.AreEqual(int.MaxValue, intArray[2], "3rd element of array should be int.MaxValue.");
			Assert.AreEqual(int.MaxValue, intArray[3], "4th element of array should be int.MaxValue.");
		}

		[TestMethod]
		public void CopyNumberToArrayIndexWithNullArray()
		{
			int[] intArray = null;
			IntSortedSet sortedSet = new IntSortedSet();

			sortedSet.Add(18);
			sortedSet.Add(9);
			sortedSet.Add(-10);
			sortedSet.Add(32);
			sortedSet.Add(0);

			sortedSet.CopyTo(intArray, 2, 3);

			Assert.IsNull(intArray, "Array should be null.");
		}

		[TestMethod]
		public void Contains()
		{
			IntSortedSet sortedSet = new IntSortedSet();

			sortedSet.Add(18);
			sortedSet.Add(9);
			sortedSet.Add(-10);
			sortedSet.Add(32);
			sortedSet.Add(0);

			Assert.IsTrue(sortedSet.Contains(-10), "Set should contain the number -10.");
			Assert.IsTrue(sortedSet.Contains(0), "Set should contain the number 0.");
			Assert.IsTrue(sortedSet.Contains(18), "Set should contain the number 18.");
		}

		[TestMethod]
		public void IsReadOnly()
		{
			IntSortedSet sortedSet = new IntSortedSet();

			Assert.IsFalse(sortedSet.IsReadOnly, "The set should not be read only.");
		}
	}
}
