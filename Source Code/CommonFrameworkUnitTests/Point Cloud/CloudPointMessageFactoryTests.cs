﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CommonFramework.DataStream;
using CommonFramework.PointCloud;
using CommonFramework.Maths;
using CommonFramework.MessageSystem;

namespace CommonFrameworkUnitTests
{
    [TestClass]
    public class CloudPointMessageFactoryTests
    {
        [TestMethod]
        public void CreateMessage()
        {
            CloudPointMessageFactory factory = new CloudPointMessageFactory();

            // Create the input packet
            List<byte> bytes = new List<byte>();
            bytes.AddRange(BitConverter.GetBytes(1.1f));
            bytes.AddRange(BitConverter.GetBytes(2.2f));
            bytes.AddRange(BitConverter.GetBytes(3.3f));
            Packet packet = new Packet(bytes.ToArray(), null);

            IMessage message = factory.CreateMessage(packet);
            Assert.AreEqual("CloudPointMessage", message.MessageName);
            Assert.AreEqual(null, message.Source);
            CloudPointMessage pointMessage = message as CloudPointMessage;
            Assert.IsNotNull(pointMessage);
            Assert.AreEqual(1.1f, pointMessage.CloudPoint.X);
            Assert.AreEqual(2.2f, pointMessage.CloudPoint.Y);
            Assert.AreEqual(3.3f, pointMessage.CloudPoint.Z);
        }

        [TestMethod]
        public void InvalidData()
        {
            CloudPointMessageFactory factory = new CloudPointMessageFactory();

            // Create the input packet
            List<byte> bytes = new List<byte>();
            bytes.AddRange(BitConverter.GetBytes(1.1f));
            bytes.AddRange(BitConverter.GetBytes(2.2f));
            Packet packet = new Packet(bytes.ToArray(), null);

            IMessage message = factory.CreateMessage(packet);
            Assert.IsNull(message);
        }
    }
}
