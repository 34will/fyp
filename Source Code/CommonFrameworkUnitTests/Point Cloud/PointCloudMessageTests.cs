﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CommonFramework.PointCloud;
using CommonFramework.Maths;

namespace CommonFrameworkUnitTests
{
    [TestClass]
    public class PointCloudMessageTests
    {
        [TestMethod]
        public void Constructor()
        {
            CloudPoint point = new CloudPoint(new Float3(), new Float3(), new Float3(), new Float3());
            TestMessageable messageable = new TestMessageable();
            CloudPointMessage message = new CloudPointMessage(point, messageable);
            Assert.ReferenceEquals(point, message.CloudPoint);
            Assert.ReferenceEquals(messageable, message.Source);
        }

        [TestMethod]
        public void MessageName()
        {
            CloudPoint point = new CloudPoint(new Float3(), new Float3(), new Float3(), new Float3());
            CloudPointMessage message = new CloudPointMessage(point);
            Assert.AreEqual("CloudPointMessage", message.MessageName);
        }

        [TestMethod]
        public void Source()
        {
            CloudPoint point = new CloudPoint(new Float3(), new Float3(), new Float3(), new Float3());
            TestMessageable messageable = new TestMessageable();
            CloudPointMessage message = new CloudPointMessage(point);
            Assert.ReferenceEquals(point, message.CloudPoint);

            Assert.IsNull(message.Source);
            message.Source = messageable;
            Assert.ReferenceEquals(messageable, message.Source);
        }
    }
}
