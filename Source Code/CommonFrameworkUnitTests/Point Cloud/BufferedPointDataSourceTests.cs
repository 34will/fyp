﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CommonFramework.PointCloud;
using CommonFramework.Maths;

namespace CommonFrameworkUnitTests
{
    [TestClass]
    public class BufferedPointDataSourceTests
    {
        [TestMethod]
        public void Constructor()
        {
            BufferedPointDataSource source = new BufferedPointDataSource();
            List<CloudPoint> points = new List<CloudPoint>(source.ReadAvailablePointData());
            Assert.AreEqual(0, source.Count, "The number of points in an empty buffer should be zero");
            Assert.AreEqual(false, source.PointDataAvailable, "There should be no data available in an empty buffer");
            Assert.AreEqual(0, points.Count, "The number of points read from the buffer should be zero");
        }

        [TestMethod]
        public void AddVector3()
        {
            BufferedPointDataSource source = new BufferedPointDataSource();
            source.Add(new Vector3(1.0f, 2.0f, 3.0f));
            source.Add(new Vector3(4.0f, 5.0f, 6.0f));
            Assert.IsTrue(source.PointDataAvailable, "After adding 2 points data should be available to read");
            Assert.AreEqual(2, source.Count);
            List<CloudPoint> points = new List<CloudPoint>(source.ReadAvailablePointData());
            Assert.AreEqual(2, points.Count, "The number read from the buffer should be 2");
            Assert.AreEqual(false, source.PointDataAvailable, "Once the data has been read from the stream, point data should not be available");
        }

        [TestMethod]
        public void AddCloudPoint()
        {
            BufferedPointDataSource source = new BufferedPointDataSource();
            source.Add(new CloudPoint(new Float3(1.0f, 2.0f, 3.0f), new Float3(), new Float3(), new Float3()));
            source.Add(new CloudPoint(new Float3(4.0f, 5.0f, 6.0f), new Float3(), new Float3(), new Float3()));
            Assert.IsTrue(source.PointDataAvailable, "After adding 2 points data should be available to read");
            List<CloudPoint> points = new List<CloudPoint>(source.ReadAvailablePointData());
            Assert.AreEqual(2, points.Count, "The number read from the buffer should be 2");
            Assert.AreEqual(false, source.PointDataAvailable, "Once the data has been read from the stream, point data should not be available");
        }

        [TestMethod]
        public void AddRangeCloudPoint()
        {
            Vector3[] inputData = new Vector3[]
            {
                new Vector3(1.0f, 2.0f, 3.0f),
                new Vector3(4.0f, 5.0f, 6.0f)
            };

            BufferedPointDataSource source = new BufferedPointDataSource();
            source.AddRange(inputData);
            Assert.IsTrue(source.PointDataAvailable, "After adding 2 points data should be available to read");
            Assert.AreEqual(2, source.Count);
            List<CloudPoint> points = new List<CloudPoint>(source.ReadAvailablePointData());
            Assert.AreEqual(2, points.Count, "The number read from the buffer should be 2");
            Assert.AreEqual(false, source.PointDataAvailable, "Once the data has been read from the stream, point data should not be available");
        }

        [TestMethod]
        public void AddRangeVector3()
        {
            CloudPoint[] inputData = new CloudPoint[]
            {
                new CloudPoint(new Float3(1.0f, 2.0f, 3.0f), new Float3(), new Float3(), new Float3()),
                new CloudPoint(new Float3(4.0f, 5.0f, 6.0f), new Float3(), new Float3(), new Float3())
            };

            BufferedPointDataSource source = new BufferedPointDataSource();
            source.AddRange(inputData);
            Assert.IsTrue(source.PointDataAvailable, "After adding 2 points data should be available to read");
            Assert.AreEqual(2, source.Count);
            List<CloudPoint> points = new List<CloudPoint>(source.ReadAvailablePointData());
            Assert.AreEqual(2, points.Count, "The number read from the buffer should be 2");
            Assert.AreEqual(false, source.PointDataAvailable, "Once the data has been read from the stream, point data should not be available");
        }
    }
}
