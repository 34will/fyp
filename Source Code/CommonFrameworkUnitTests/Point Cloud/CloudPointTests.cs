﻿using UnityEngine;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CommonFramework.PointCloud;
using CommonFramework.Maths;

namespace CommonFrameworkUnitTests
{
    [TestClass]
    public class CloudPointTests
    {
        [TestMethod]
        public void ConstructorFloat3s()
        {
            CloudPoint point = new CloudPoint(new Float3(1.1f, 2.2f, 3.3f), new Float3(4.4f, 5.5f, 6.6f), new Float3(7.7f, 8.8f, 9.9f), new Float3(0.3f, 0.6f, 0.7f));
            Assert.AreEqual(1.1f, point.Position.x);
            Assert.AreEqual(2.2f, point.Position.y);
            Assert.AreEqual(3.3f, point.Position.z);
            Assert.AreEqual(4.4f, point.Normal.x);
            Assert.AreEqual(5.5f, point.Normal.y);
            Assert.AreEqual(6.6f, point.Normal.z);
            Assert.AreEqual(7.7f, point.Tangent.x);
            Assert.AreEqual(8.8f, point.Tangent.y);
            Assert.AreEqual(9.9f, point.Tangent.z);
            Assert.AreEqual(1.0f, point.Tangent.w);
            Assert.AreEqual(0.3f, point.Colour.r);
            Assert.AreEqual(0.6f, point.Colour.g);
            Assert.AreEqual(0.7f, point.Colour.b);
            Assert.AreEqual(1.0f, point.Colour.a);
        }

        [TestMethod]
        public void ConstructorVectors()
        {
            CloudPoint point = new CloudPoint(new Vector3(1.1f, 2.2f, 3.3f), new Vector3(4.4f, 5.5f, 6.6f), new Vector4(7.7f, 8.8f, 9.9f, 1.0f), new Color(0.3f, 0.6f, 0.7f, 1.0f));
            Assert.AreEqual(1.1f, point.Position.x);
            Assert.AreEqual(2.2f, point.Position.y);
            Assert.AreEqual(3.3f, point.Position.z);
            Assert.AreEqual(4.4f, point.Normal.x);
            Assert.AreEqual(5.5f, point.Normal.y);
            Assert.AreEqual(6.6f, point.Normal.z);
            Assert.AreEqual(7.7f, point.Tangent.x);
            Assert.AreEqual(8.8f, point.Tangent.y);
            Assert.AreEqual(9.9f, point.Tangent.z);
            Assert.AreEqual(1.0f, point.Tangent.w);
            Assert.AreEqual(0.3f, point.Colour.r);
            Assert.AreEqual(0.6f, point.Colour.g);
            Assert.AreEqual(0.7f, point.Colour.b);
            Assert.AreEqual(1.0f, point.Colour.a);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void SetDimensionInvalidUpper()
        {
            CloudPoint point = new CloudPoint(new Float3(), new Float3(), new Float3(), new Float3());
            point.SetDimension(3, 1.0f);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void SetDimensionInvalidLower()
        {
            CloudPoint point = new CloudPoint(new Float3(), new Float3(), new Float3(), new Float3());
            point.SetDimension(-1, 1.0f);
        }

        [TestMethod]
        public void SetDimension()
        {
            CloudPoint point = new CloudPoint(new Float3(1.0f, 2.0f, 3.0f), new Float3(), new Float3(), new Float3());
            Assert.AreEqual(1.0f, point.Position.x);
            Assert.AreEqual(2.0f, point.Position.y);
            Assert.AreEqual(3.0f, point.Position.z);
            point.SetDimension(0, 2.0f);
            point.SetDimension(1, 3.0f);
            point.SetDimension(2, 4.0f);
            Assert.AreEqual(2.0f, point.Position.x);
            Assert.AreEqual(3.0f, point.Position.y);
            Assert.AreEqual(4.0f, point.Position.z);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetDimensionInvalidUpper()
        {
            CloudPoint point = new CloudPoint(new Float3(), new Float3(), new Float3(), new Float3());
            float value = point.GetDimension(3);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetDimensionInvalidLower()
        {
            CloudPoint point = new CloudPoint(new Float3(), new Float3(), new Float3(), new Float3());
            float value = point.GetDimension(-1);
        }

        [TestMethod]
        public void GetDimension()
        {
            CloudPoint point = new CloudPoint(new Float3(1.0f, 2.0f, 3.0f), new Float3(), new Float3(), new Float3());
            Assert.AreEqual(1.0f, point.GetDimension(0));
            Assert.AreEqual(2.0f, point.GetDimension(1));
            Assert.AreEqual(3.0f, point.GetDimension(2));
        }

        [TestMethod]
        public void Properties()
        {
            CloudPoint point = new CloudPoint(new Float3(1.0f, 2.0f, 3.0f), new Float3(), new Float3(), new Float3());
            Assert.AreEqual(1.0f, point.X);
            Assert.AreEqual(2.0f, point.Y);
            Assert.AreEqual(3.0f, point.Z);
            point.X = 2.0f;
            point.Y = 3.0f;
            point.Z = 4.0f;
            Assert.AreEqual(2.0f, point.X);
            Assert.AreEqual(3.0f, point.Y);
            Assert.AreEqual(4.0f, point.Z);
            Assert.AreEqual(3, point.Dimensions);
        }

        [TestMethod]
        public void ArrayAccessor()
        {
            CloudPoint point = new CloudPoint(new Float3(1.0f, 2.0f, 3.0f), new Float3(), new Float3(), new Float3());
            Assert.AreEqual(1.0f, point[0]);
            Assert.AreEqual(2.0f, point[1]);
            Assert.AreEqual(3.0f, point[2]);
            point[0] = 4.0f;
            point[1] = 5.0f;
            point[2] = 6.0f;
            Assert.AreEqual(4.0f, point[0]);
            Assert.AreEqual(5.0f, point[1]);
            Assert.AreEqual(6.0f, point[2]);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void ArrayAccessorGetInvalidUpper()
        {
            CloudPoint point = new CloudPoint(new Float3(), new Float3(), new Float3(), new Float3());
            float value = point[3];
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void ArrayAccessorGetInvalidLower()
        {
            CloudPoint point = new CloudPoint(new Float3(), new Float3(), new Float3(), new Float3());
            float value = point[-1];
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void ArrayAccessorSetInvalidUpper()
        {
            CloudPoint point = new CloudPoint(new Float3(), new Float3(), new Float3(), new Float3());
            point[3] = 1.4f;
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void ArrayAccessorSetInvalidLower()
        {
            CloudPoint point = new CloudPoint(new Float3(), new Float3(), new Float3(), new Float3());
            point[-1] = 1.4f;
        }

        [TestMethod]
        public void String()
        {
            CloudPoint point = new CloudPoint(new Vector3(1.0f, 1.5f, 2.0f), new Vector3(2.5f, 3.0f, 3.5f), new Vector4(4.0f, 4.5f, 5.0f, 5.5f), new Color(0.3f, 0.6f, 0.9f, 1.0f));
            string expected = "CloudPoint[position(1.0, 1.5, 2.0), normal(2.5, 3.0, 3.5), tangent(4.0, 4.5, 5.0, 5.5), colour(RGBA(0.300, 0.600, 0.900, 1.000))]";
            string actual = point.ToString();
            Assert.AreEqual(expected, actual);
        }
    }
}
