﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CommonFramework.Logging;

namespace CommonFrameworkUnitTests
{
    [TestClass]
    public class LogMessageTests
    {
        /// <summary>
        /// Test the LogMessage constructor that takes a string representing the message contents as a parameter.
        /// Checks that the message contents are equal to the message passed to the constructor.
        /// Checks that the log classifcation is the default Information classification.
        /// Check that the IMessageable source is null.
        /// </summary>
        [TestMethod]
        public void LogMessageMessageContentsConstructor()
        {
            string logMessageContents = "It's all ogre now!";
            LogMessage logMessage = new LogMessage(logMessageContents);

            Assert.AreEqual(logMessageContents, logMessage.Message, string.Format("Expected: {0}, Actual: {1}.", logMessageContents, logMessage.Message));
            Assert.IsTrue(logMessage.Classification == LogClassification.Information, string.Format("Expected: {0}, Actual: {1}.", LogClassification.Information, logMessage.Classification));
            Assert.IsNull(logMessage.Source, "The source of the LogMessage should be null.");
        }

        /// <summary>
        /// Test the LogMessage constructor that takes a string representing the message contents and the LogClassification
        /// as parameters.
        /// Checks that the message contents are equal to the message passed to the constructor.
        /// Checks that the log classifcation has changed to the Warning classification.
        /// Check that the IMessageable source is null.
        /// </summary>
        [TestMethod]
        public void LogMessageMessageContentLogClassificationConstructor()
        {
            string logMessageContents = "Guten abend, Tschuess!";
            LogClassification classification = LogClassification.Warning;
            LogMessage logMessage = new LogMessage(logMessageContents, classification);

            Assert.AreEqual(logMessageContents, logMessage.Message, string.Format("Expected: {0}, Actual: {1}.", logMessageContents, logMessage.Message));
            Assert.IsTrue(logMessage.Classification == classification, string.Format("Expected: {0}, Actual: {1}.", classification, logMessage.Classification));
            Assert.IsNull(logMessage.Source, "The source of the LogMessage should be null.");
        }

        /// <summary>
        /// Test the LogMessage constructor that takes a string representing the message contents and the IMessageable source
        /// as parameters.
        /// Checks that the message contents are equal to the message passed to the constructor.
        /// Checks that the log classifcation is equal to the assigned default Information classification.
        /// Check that the IMessageable source is not null.
        /// Check that the IMessageable source is equal to the TestMessageable.
        /// </summary>
        [TestMethod]
        public void LogMessageMessageContentIMessageableConstructor()
        {
            string logMessageContents = "Hallo, Servus!";
            TestMessageable testMessageable = new TestMessageable();
            LogMessage logMessage = new LogMessage(logMessageContents, testMessageable);

            Assert.IsTrue(logMessage.Classification == LogClassification.Information, string.Format("The LogClassification of the LogMessage should be of type {0}", LogClassification.Information));
            Assert.AreEqual(logMessageContents, logMessage.Message, string.Format("Expected: {0}, Actual: {1}.", logMessageContents, logMessage.Message));
            Assert.IsNotNull(logMessage.Source, "The source of the LogMessage should not be null.");
            Assert.AreEqual(testMessageable, logMessage.Source, string.Format("Expected: {0}, Actual: {1}.", testMessageable, logMessage.Source));
        }

        /// <summary>
        /// Test the LogMessage constructor that takes a string representing the message contents, the LogClassification
        /// and the IMessageable source as parameters.
        /// as parameters.
        /// Checks that the message contents are equal to the message passed to the constructor.
        /// Checks that the log classifcation has changed to the Warning classification.
        /// Check that the IMessageable source is not null.
        /// Check that the IMessageable source is equal to the TestMessageable.
        /// </summary>
        [TestMethod]
        public void LogMessageMessageContentsLogClassificationIMessageableParameterConstructor()
        {
            string logMessageContents = "Mahlzeit!";
            TestMessageable testMessageable = new TestMessageable();
            LogClassification classification = LogClassification.Warning;
            LogMessage logMessage = new LogMessage(logMessageContents, classification, testMessageable);

            Assert.AreEqual(logMessageContents, logMessage.Message, string.Format("Expected: {0}, Actual: {1}.", logMessageContents, logMessage.Message));
            Assert.IsTrue(logMessage.Classification == classification, string.Format("Expected: {0}, Actual: {1}.", classification, logMessage.Classification));
            Assert.IsNotNull(logMessage.Source, "The source of the LogMessage should not be null.");
            Assert.AreEqual(testMessageable, logMessage.Source, string.Format("Expected: {0}, Actual: {1}.", testMessageable, logMessage.Source));
        }


        [TestMethod]
        public void LogMessageSetGetSourceIMessageable()
        {
            string logMessageContents = "Danke Heike, wunderbarrr!";
            TestMessageable testMessageable = new TestMessageable();
            LogMessage logMessage = new LogMessage(logMessageContents);
            logMessage.Source = testMessageable;

            Assert.IsNotNull(logMessage.Source, "The source of the LogMessage should not be null.");
            Assert.AreEqual(testMessageable, logMessage.Source, string.Format("Expected: {0}, Actual: {1}.", testMessageable, logMessage.Source));
        }
    }
}
