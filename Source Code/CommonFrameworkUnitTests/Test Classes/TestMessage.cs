﻿using System;

using CommonFramework.MessageSystem;

namespace CommonFrameworkUnitTests
{
    public class TestMessage : IMessage
    {
        private IMessageable source = null;
        private string message = null;

        public TestMessage(string message = "")
        {
            this.message = message;
        }

        // ----- Properties ----- //

        public string MessageName
        {
            get { return "TestMessage." + message; }
        }

        public IMessageable Source
        {
            set { source = value; }
            get { return source; }
        }
    }
}
