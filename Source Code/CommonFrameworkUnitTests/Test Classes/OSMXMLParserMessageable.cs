﻿using System;
using System.Collections.Generic;

using CommonFramework.MessageSystem;
using CommonFramework.Environment;
using CommonFramework.Roads;

namespace CommonFrameworkUnitTests
{
	public class OSMXmlParserMessageable : IMessageable
	{
		private Queue<IMessage> pendingMessages = new Queue<IMessage>();
		private int numRoadSegementMessages = 0, numBuildRoadGraphMessages = 0, numAssetMessages = 0;

		public void ReceiveMessage(IMessage message)
		{
			if(message.MessageName == AssetMessage.messageName)
			{
				AssetMessage assetMessage = message as AssetMessage;
				if(assetMessage == null) return;

				numAssetMessages++;
			}
			else if(message.MessageName == RoadSegmentMessage.messageName)
			{
				RoadSegmentMessage roadMessage = message as RoadSegmentMessage;
				if(roadMessage == null) return;

				numRoadSegementMessages++;
			}
			else if(message.MessageName == BuildRoadGraphMessage.messageName)
			{
				BuildRoadGraphMessage buildGraphMessage = message as BuildRoadGraphMessage;
				if(buildGraphMessage == null) return;

				numBuildRoadGraphMessages++;
			}
		}

		// ----- Properties ----- //

		public int NumberOfRoadSegementMessages
		{
			get { return numRoadSegementMessages; }
		}

		public int NumberOfBuildRoadGraphMessages
		{
			get { return numBuildRoadGraphMessages; }
		}

		public int NumberOfAssetMessages
		{
			get { return numAssetMessages; }
		}

		public Queue<IMessage> PendingMessages
		{
			get { return pendingMessages; }
		}

		public string MessageableName
		{
			get { return "OSMXmlParserMessageable"; }
		}
	}
}
