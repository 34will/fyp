﻿using System;
using System.Collections.Generic;
using System.Linq;

using CommonFramework.MessageSystem;

namespace CommonFrameworkUnitTests
{
    public class TestMessageable : IMessageable
    {
        private List<IMessage> receivedMessages = new List<IMessage>();
        private Queue<IMessage> pendingMessages = new Queue<IMessage>();

        public void ReceiveMessage(IMessage message)
        {
            receivedMessages.Add(message);
        }

        // ----- Properties ----- //

        public Queue<IMessage> PendingMessages
        {
            get { return pendingMessages; }
        }

        public virtual string MessageableName
        {
            get { return "TestMessageable"; }
        }

        public IMessage LastMessage
        {
            get { return receivedMessages.Count == 0 ? null : receivedMessages.Last(); }
        }
    }
}
