﻿using System;
using System.Collections.Generic;

namespace CommonFrameworkUnitTests
{
	public class TestReverseIntComparer : IComparer<int>
	{
		public int Compare(int x, int y)
		{
			return -x.CompareTo(y);
		}
	}
}
