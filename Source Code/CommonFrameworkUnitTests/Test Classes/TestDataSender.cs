﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace CommonFrameworkUnitTests
{
    public class TestDataSender
    {
        private int port = 0;
        private UdpClient client = null;

        public TestDataSender(int port)
        {
            this.port = port;
            this.client = new UdpClient(port);
        }

        public void Send(byte[] data, IPEndPoint endpoint)
        {
            client.Send(data, data.Length, endpoint);
        }

        public void Stop()
        {
            client.Close();
        }

        // ----- Properties ----- //

        public int ListenPort
        {
            get { return port; }
        }
    }
}
