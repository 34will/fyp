﻿using System;

using CommonFramework.MessageSystem;

namespace CommonFrameworkUnitTests
{
    public class TargetableMessageable : TestMessageable
    {
        public override string MessageableName
        {
            get { return "TargetableMessageable"; }
        }
    }
}
