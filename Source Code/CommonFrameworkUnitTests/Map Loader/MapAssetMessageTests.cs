﻿using System;
using System.Collections.Generic;
using CommonFramework.DataStream;
using CommonFramework.MapLoader;
using CommonFramework.MessageSystem;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnityEngine;

namespace CommonFrameworkUnitTests
{
    [TestClass]
    public class MapAssetMessageTests
    {
        List<Vector3> cartesianWaypoints = new List<Vector3>()
        {
            new Vector3(30.0f, 50.0f, 70.0f),
            new Vector3(70.0f, 50.0f, 30.0f),
            new Vector3(45.0f, 90.0f, 67.5f)
        };

        /// <summary>
        /// Tests the MapAssetMessage constructor taking a Structure Type as a string and a set of Cartesian Waypoints represented
        /// as a list of Vector3 as parameters where the IMessageable source is null.
        /// Check the Structure Type of the AssetMessage is equal to the parameterized Structure Type.
        /// Check the Cartesian Waypoints list of the AssetMessage is equal to the parameterized Cartesian Waypoints list.
        /// Check the IMessageable source of the MapAssetMessage is defaulted to null.
        /// </summary>
        [TestMethod]
        public void MapAssetMessageStructureTypeCartesianWaypointsConstructor()
        {
            string structureType = "road";
            MapAssetMessage mapAssetMessage = new MapAssetMessage(structureType, cartesianWaypoints);

            Assert.AreEqual(structureType, mapAssetMessage.StructureType);
            Assert.AreEqual(cartesianWaypoints, mapAssetMessage.CartesianWaypoints);
            Assert.IsNull(mapAssetMessage.Source);
        }

        /// <summary>
        /// Tests the MapAssetMessage constructor taking a Structure Type as a string, a set of Cartesian Waypoints represented
        /// as a list of Vector3 and the IMessageable source as parameters.
        /// Check the Structure Type of the AssetMessage is equal to the parameterized Structure Type.
        /// Check the Cartesian Waypoints list of the AssetMessage is equal to the parameterized Cartesian Waypoints list.
        /// Check the IMessageable source of the MapAssetMessage is not equal to null.
        /// Check the IMessageable source of the MapAssetMessage is equal to the paramaterized IMessageable of type TargetableMessageable.
        /// </summary>
        [TestMethod]
        public void MapAssetMessageStructureTypeCartesianWaypointsIMessageableConstructor()
        {
            TargetableMessageable targetableMessageable = new TargetableMessageable();
            string structureType = "road";
            MapAssetMessage mapAssetMessage = new MapAssetMessage(structureType, cartesianWaypoints, targetableMessageable);

            Assert.AreEqual(structureType, mapAssetMessage.StructureType);
            Assert.AreEqual(cartesianWaypoints, mapAssetMessage.CartesianWaypoints);
            Assert.IsNotNull(mapAssetMessage.Source);
            Assert.AreEqual(targetableMessageable, mapAssetMessage.Source);
        }

        /// <summary>
        /// Check the IMessageable source of the MapAssetMessage is null.
        /// Set IMessageable to be TestMessageable.
        /// Check the IMessageable source of the MapAssetMessage is not null.
        /// Check the IMessageable source of the MapAssetMessage is equal to the set IMessageable - TestMessageable.
        /// </summary>
        [TestMethod]
        public void MapAssetMessageSetGetSourceIMessageable()
        {
            MapAssetMessage mapAssetMessage = new MapAssetMessage("road", cartesianWaypoints);
            TestMessageable testMessageable = new TestMessageable();
            
            Assert.IsNull(mapAssetMessage.Source);
            mapAssetMessage.Source = testMessageable;
            Assert.IsNotNull(mapAssetMessage.Source);
            Assert.AreEqual(testMessageable, mapAssetMessage.Source);
        }
    }
}
