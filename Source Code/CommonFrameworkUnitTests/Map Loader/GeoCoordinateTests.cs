﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using UnityEngine;

using CommonFramework.MapLoader;
using CommonFramework.Maths;

namespace CommonFrameworkUnitTests
{
	[TestClass]
	public class GeoCoordinateTests
	{
		[TestMethod]
		public void EarthMeanRadiusConstant()
		{
			Assert.AreEqual(6376500.0, GeoCoordinate.EarthMeanRadius, 100, "The Earth Mean Radius constant should be 6376500.0.");
		}

		[TestMethod]
		public void ConstructorAndLongitudeLatitideProperties()
		{
			GeoCoordinate gc = new GeoCoordinate(52.4673, 2.0045);

			Assert.AreEqual(52.4673, gc.Latitude, "The latitiude should be 52.4673.");
			Assert.AreEqual(2.0045, gc.Longitude, "The longitude should be 2.0045.");
		}

		[TestMethod]
		public void DistanceBetweenOtherGeoCoordinate()
		{
			GeoCoordinate gcOne = new GeoCoordinate(52.4673, 2.0045);
			GeoCoordinate gcTwo = new GeoCoordinate(2.0045, 52.4673);

			Assert.AreEqual(7285208.894, gcOne.DistanceBetween(gcTwo), 0.001, "The distance between 52 46 73N, 02 00 45W and 02 00 45N, 52 46 73W is 7285208.894m.");
		}

		[TestMethod]
		public void DistanceBetweenOtherLongitudeLatitide()
		{
			GeoCoordinate gcOne = new GeoCoordinate(52.4673, 2.0045);

			Assert.AreEqual(7285208.894, gcOne.DistanceBetween(2.0045, 52.4673), 0.001, "The distance between 52 46 73N, 02 00 45W and 02 00 45N, 52 46 73W is 7285208.894m.");
		}

		[TestMethod]
		public void DistanceBetweenTwoGeoCoordinates()
		{
			GeoCoordinate gcOne = new GeoCoordinate(52.4673, 2.0045);
			GeoCoordinate gcTwo = new GeoCoordinate(2.0045, 52.4673);

			Assert.AreEqual(7285208.894, GeoCoordinate.DistanceBetween(gcOne, gcTwo), 0.001, "The distance between 52 46 73N, 02 00 45W and 02 00 45N, 52 46 73W is 7285208.894m.");
		}

		[TestMethod]
		public void DistanceBetweenGeoCoordinateAndLongitudeLatitide()
		{
			GeoCoordinate gcOne = new GeoCoordinate(52.4673, 2.0045);

			Assert.AreEqual(7285208.894, GeoCoordinate.DistanceBetween(gcOne, 2.0045, 52.4673), 0.001, "The distance between 52 46 73N, 02 00 45W and 02 00 45N, 52 46 73W is 7285208.894m.");
		}

		[TestMethod]
		public void DistanceBetweenTwoIdenticalGeoCoordinates()
		{
			GeoCoordinate gcOne = new GeoCoordinate(52.4673, 2.0045);
			GeoCoordinate gcTwo = new GeoCoordinate(52.4673, 2.0045);

			Assert.AreEqual(0.0, GeoCoordinate.DistanceBetween(gcOne, gcTwo), "The distance between two identical GeoCoordinates should be 0.0m.");
		}

		[TestMethod]
		public void DistanceBetweenInvalidGeoCoordinates()
		{
			GeoCoordinate gcOne = new GeoCoordinate(52.4673, 2.0045);
			GeoCoordinate gcTwo = new GeoCoordinate(double.NaN, 2.0045);

			Assert.IsTrue(double.IsNaN(GeoCoordinate.DistanceBetween(gcOne, gcTwo)), "The distance between a normal GeoCoordinate and an invalid one should be double.NaN.");

			gcOne = new GeoCoordinate(52.4673, double.NaN);
			gcTwo = new GeoCoordinate(52.4673, 2.0045);

			Assert.IsTrue(double.IsNaN(GeoCoordinate.DistanceBetween(gcOne, gcTwo)), "The distance between a normal GeoCoordinate and an invalid one should be double.NaN.");

			gcOne = new GeoCoordinate(double.PositiveInfinity, 52.4673);
			gcTwo = new GeoCoordinate(52.4673, double.PositiveInfinity);

			Assert.IsTrue(double.IsNaN(GeoCoordinate.DistanceBetween(gcOne, gcTwo)), "The distance between two invalid GeoCoordinates should be double.NaN.");

			gcOne = new GeoCoordinate(2.0045, 52.4673);
			gcTwo = new GeoCoordinate(52.4673, 2.0045);

			Assert.IsTrue(double.IsNaN(GeoCoordinate.DistanceBetween(gcOne, null)), "The distance between a normal GeoCoordinate and a null one should be double.NaN.");

			Assert.IsTrue(double.IsNaN(GeoCoordinate.DistanceBetween(null, gcTwo)), "The distance between a null one and a normal GeoCoordinate should be double.NaN.");
		}

		[TestMethod]
		public void EqualsNullObject()
		{
			GeoCoordinate gcOne = new GeoCoordinate(52.4673, 2.0045);

			Assert.IsFalse(gcOne.Equals(null), "A GeoCoordinate should not equal a null object.");
		}

		[TestMethod]
		public void EqualsObject()
		{
			GeoCoordinate gcOne = new GeoCoordinate(52.4673, 2.0045);

			Assert.IsFalse(gcOne.Equals(10), "A GeoCoordinate should not equal an int.");
		}

		[TestMethod]
		public void EqualsIdenticalGeoCoordinate()
		{
			GeoCoordinate gcOne = new GeoCoordinate(52.4673, 2.0045);
			GeoCoordinate gcTwo = new GeoCoordinate(52.4673, 2.0045);

			Assert.IsTrue(gcOne.Equals(gcTwo), "A GeoCoordinate should equal an identical GeoCoordinate.");
		}

		[TestMethod]
		public void EqualsDifferentGeoCoordinate()
		{
			GeoCoordinate gcOne = new GeoCoordinate(52.4673, 2.0045);
			GeoCoordinate gcTwo = new GeoCoordinate(2.0045, 52.4673);

			Assert.IsFalse(gcOne.Equals(gcTwo), "A GeoCoordinate should not equal a different GeoCoordinate.");
		}

		[TestMethod]
		public void EqualsEqualsTwoGeoCoordinates()
		{
			GeoCoordinate gcOne = new GeoCoordinate(52.4673, 2.0045);
			GeoCoordinate gcTwo = new GeoCoordinate(2.0045, 52.4673);

			Assert.IsFalse(gcOne == gcTwo, "A GeoCoordinate should not equal a different GeoCoordinate.");
		}

		[TestMethod]
		public void EqualsEqualsSameGeoCoordinates()
		{
			GeoCoordinate gcOne = new GeoCoordinate(52.4673, 2.0045);
			GeoCoordinate gcTwo = gcOne;

			Assert.IsTrue(gcOne == gcTwo, "A GeoCoordinate should equal itself.");
		}

		[TestMethod]
		public void EqualsEqualsLeftNullGeoCoordinate()
		{
			GeoCoordinate gcOne = null;
			GeoCoordinate gcTwo = new GeoCoordinate(52.4673, 2.0045);

			Assert.IsFalse(gcOne == gcTwo, "A null GeoCoordinate should not equal a valid GeoCoordinate.");
		}

		[TestMethod]
		public void EqualsEqualsRightNullGeoCoordinates()
		{
			GeoCoordinate gcOne = new GeoCoordinate(52.4673, 2.0045);
			GeoCoordinate gcTwo = null;

			Assert.IsFalse(gcOne == gcTwo, "A valid GeoCoordinate should not equal a null GeoCoordinate.");
		}

		[TestMethod]
		public void NotEqualsTwoGeoCoordinates()
		{
			GeoCoordinate gcOne = new GeoCoordinate(52.4673, 2.0045);
			GeoCoordinate gcTwo = new GeoCoordinate(2.0045, 52.4673);

			Assert.IsTrue(gcOne != gcTwo, "A GeoCoordinate should not equal a different GeoCoordinate.");
		}

		[TestMethod]
		public void GetHashCodeIdenticalGeoCoordinate()
		{
			GeoCoordinate gcOne = new GeoCoordinate(52.4673, 2.0045);
			GeoCoordinate gcTwo = new GeoCoordinate(52.4673, 2.0045);

			Assert.IsTrue(gcOne.GetHashCode() == gcTwo.GetHashCode(), "The HashCode of a GeoCoordinate should equal the HashCode of an indentical GeoCoordinate.");
		}

		[TestMethod]
		public void GetHashCodeSwappedLongLatGeoCoordinate()
		{
			GeoCoordinate gcOne = new GeoCoordinate(52.4673, 2.0045);
			GeoCoordinate gcTwo = new GeoCoordinate(2.0045, 52.4673);

			Assert.IsTrue(gcOne.GetHashCode() != gcTwo.GetHashCode(), "The HashCode of a GeoCoordinate should not equal the HashCode of a GeoCoordinate with swapped Longitude and Latitude of original GeoCoordinate.");
		}

		[TestMethod]
		public void GetHashCodeDifferentGeoCoordinate()
		{
			GeoCoordinate gcOne = new GeoCoordinate(55.9006, 26.0045);
			GeoCoordinate gcTwo = new GeoCoordinate(3.0045, 52.4543);

			Assert.IsTrue(gcOne.GetHashCode() != gcTwo.GetHashCode(), "The HashCode of a GeoCoordinate should not equal the HashCode of a different GeoCoordinate.");
		}

		[TestMethod]
		public void RelativePositionValidGeoCoordinate()
		{
			GeoCoordinate gcOne = new GeoCoordinate(55.9006, 26.0045);
			GeoCoordinate gcTwo = new GeoCoordinate(56.0000, 26.1000);
			Vector3 expected = new Vector3(5943.26f, 0.0f, 11062.3174f);

			Assert.AreEqual(expected, GeoCoordinate.RelativePosition(gcOne, gcTwo), "The position of { 55.9006, 26.0045 } relative to { 56.0000, 26.1000 } should be { 5943.26, 0.0, 11062.3174 }.");
		}

		[TestMethod]
		public void RelativePositionSameGeoCoordinate()
		{
			GeoCoordinate gcOne = new GeoCoordinate(55.9006, 26.0045);
			GeoCoordinate gcTwo = new GeoCoordinate(55.9006, 26.0045);

			Assert.IsTrue(Helper.IsZero(GeoCoordinate.RelativePosition(gcOne, gcTwo)), "The relative position of a normal GeoCoordinate to itself should be a zero vector.");
		}

		[TestMethod]
		public void RelativePositionInvalidGeoCoordinates()
		{
			GeoCoordinate gcOne = new GeoCoordinate(52.4673, 2.0045);
			GeoCoordinate gcTwo = new GeoCoordinate(double.NaN, 2.0045);

			Assert.IsFalse(Helper.IsValid(GeoCoordinate.RelativePosition(gcOne, gcTwo)), "The relative position of a normal GeoCoordinate to an invalid one should be an invalid vector.");

			gcOne = new GeoCoordinate(52.4673, double.NaN);
			gcTwo = new GeoCoordinate(52.4673, 2.0045);

			Assert.IsFalse(Helper.IsValid(GeoCoordinate.RelativePosition(gcOne, gcTwo)), "The relative position of an invalid GeoCoordinate to a normal one should be an invalid vector.");

			gcOne = new GeoCoordinate(double.PositiveInfinity, 52.4673);
			gcTwo = new GeoCoordinate(52.4673, double.PositiveInfinity);

			Assert.IsFalse(Helper.IsValid(GeoCoordinate.RelativePosition(gcOne, gcTwo)), "The relative position between two invalid GeoCoordinates should be an invalid vector.");

			gcOne = new GeoCoordinate(2.0045, 52.4673);
			gcTwo = new GeoCoordinate(52.4673, 2.0045);

			Assert.IsFalse(Helper.IsValid(GeoCoordinate.RelativePosition(gcOne, null)), "The relative position of a normal GeoCoordinate to a null one should be an invalid vector.");

			Assert.IsFalse(Helper.IsValid(GeoCoordinate.RelativePosition(null, gcTwo)), "The relative position of a null one to a normal GeoCoordinate should be an invalid vector.");
		}
	}
}
