﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using UnityEngine;

using CommonFramework.MapLoader;
using CommonFramework.Maths;

namespace CommonFrameworkUnitTests
{
	[TestClass]
	public class OSMNodeTests
	{
		[TestMethod]
		public void Constructor()
		{
			OSMNode node = new OSMNode(0, 0.0, 0.0);

			Assert.IsNotNull(node, "An OSMNode with a valid constructor should not be null.");
		}

		[TestMethod]
		public void Properties()
		{
			OSMNode node = new OSMNode(1, 52.0, 2.0);

			Assert.AreEqual(1, node.ID, "The ID of an OSMNode constructed with an ID of 1 should be 1.");
			Assert.AreEqual(52.0, node.Latitude, "The Latitude of an OSMNode constructed with an Latitude of 52.0 should be 52.0.");
			Assert.AreEqual(2.0, node.Longitude, "The Longitude of an OSMNode constructed with an Longitude of 2.0 should be 2.0.");
			Assert.AreEqual(Helper.NaV3, node.RelativePosition, "The RelativePosition of an OSMNode that has not had it's position calculated should be Helper.NaV3.");
		}

		[TestMethod]
		public void RelativePositionValidOSMNode()
		{
			OSMNode node = new OSMNode(1, 55.9006, 26.0045);
			OSMNode origin = new OSMNode(2, 56.0000, 26.1000);

			node.CalculateRelativePosition(origin);

			Vector3 expected = new Vector3(5943.26f, 0.0f, 11062.3174f);

			Assert.AreEqual(expected, node.RelativePosition, "The relative position of { 55.9006, 26.0045 } to { 56.0000, 26.1000 } should be { 5943.26, 0.0, 11062.3174 }.");
		}

		[TestMethod]
		public void RelativePositionInvalidOSMNode()
		{
			OSMNode node = new OSMNode(1, 55.9006, 26.0045);
			OSMNode origin = new OSMNode(2, double.NaN, 26.1000);

			node.CalculateRelativePosition(origin);

			Assert.IsFalse(Helper.IsValid(node.RelativePosition), "The relative position of a valid OSMNode to an invalid one should be an invalid vector.");

			node = new OSMNode(1, double.NaN, 26.0045);
			origin = new OSMNode(2, 56.0000, 26.1000);

			node.CalculateRelativePosition(origin);

			Assert.IsFalse(Helper.IsValid(node.RelativePosition), "The relative position of an invalid OSMNode to a valid one should be an invalid vector.");

			node = new OSMNode(1, double.PositiveInfinity, 26.0045);
			origin = new OSMNode(2, double.PositiveInfinity, 26.1000);

			node.CalculateRelativePosition(origin);

			Assert.IsFalse(Helper.IsValid(node.RelativePosition), "The relative position of an invalid OSMNode to another invalid one should be an invalid vector.");

			node = new OSMNode(1, 55.9006, 26.0045);
			origin = null;

			node.CalculateRelativePosition(origin);

			Assert.IsFalse(Helper.IsValid(node.RelativePosition), "The relative position of a valid OSMNode to a null one should be an invalid vector.");
		}

		[TestMethod]
		public void RelativePositionValidGeoCoordinate()
		{
			OSMNode node = new OSMNode(1, 55.9006, 26.0045);
			GeoCoordinate origin = new GeoCoordinate(56.0000, 26.1000);

			node.CalculateRelativePosition(origin);

			Vector3 expected = new Vector3(5943.26f, 0.0f, 11062.3174f);

			Assert.AreEqual(expected, node.RelativePosition, "The relative position of { 55.9006, 26.0045 } to { 56.0000, 26.1000 } should be { 5943.26, 0.0, 11062.3174 }.");
		}

		[TestMethod]
		public void RelativePositionInvalidGeoCoordinate()
		{
			OSMNode node = new OSMNode(1, 55.9006, 26.0045);
			GeoCoordinate origin = new GeoCoordinate(double.NaN, 26.1000);

			node.CalculateRelativePosition(origin);

			Assert.IsFalse(Helper.IsValid(node.RelativePosition), "The relative position of a valid OSMNode to an invalid GeoCoordinate should be an invalid vector.");

			node = new OSMNode(1, double.NaN, 26.0045);
			origin = new GeoCoordinate(56.0000, 26.1000);

			node.CalculateRelativePosition(origin);

			Assert.IsFalse(Helper.IsValid(node.RelativePosition), "The relative position of an invalid OSMNode to a valid GeoCoordinate should be an invalid vector.");

			node = new OSMNode(1, double.PositiveInfinity, 26.0045);
			origin = new GeoCoordinate(double.PositiveInfinity, 26.1000);

			node.CalculateRelativePosition(origin);

			Assert.IsFalse(Helper.IsValid(node.RelativePosition), "The relative position of an invalid OSMNode to an invalid GeoCoordinate should be an invalid vector.");

			node = new OSMNode(1, 55.9006, 26.0045);
			origin = null;

			node.CalculateRelativePosition(origin);

			Assert.IsFalse(Helper.IsValid(node.RelativePosition), "The relative position of a valid OSMNode to a null GeoCoordinate should be an invalid vector.");
		}
	}
}
