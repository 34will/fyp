﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CommonFramework.MessageSystem;
using CommonFramework.MapLoader;

namespace CommonFrameworkUnitTests
{
	[TestClass]
	public class OSMXmlParserTests
	{
		[TestMethod]
		public void InstanceInstantiation()
		{
			Assert.IsNotNull(OSMXmlParser.Instance, "The OSMXmlParser singleton instance should never be null.");
		}

        // TODO - FIX.
		/*[TestMethod]
		public void ParseUnclippedFile()
		{
			OSMXmlParserMessageable osmMessageable = new OSMXmlParserMessageable();

			MessageHandler.Instance.RegisterMessageable(osmMessageable);

			OSMXmlParser.Instance.ParseOSMFile("Test Assets/ModifiedTestManhattan.osm");

			Assert.AreEqual(4, osmMessageable.NumberOfAssetMessages, "The number of AssetMessages recieved from the unclipped .osm file should be 4.");
			Assert.AreEqual(71, osmMessageable.NumberOfRoadSegementMessages, "The number of RoadSegementMessages recieved from the unclipped .osm file should be 71.");
			Assert.AreEqual(1, osmMessageable.NumberOfBuildRoadGraphMessages, "The number of BuildRoadGraphMessages recieved from the unclipped .osm file should be 1.");

			MessageHandler.Instance.RemoveMessageable(osmMessageable);
		}

		[TestMethod]
		public void ParseClippedFile()
		{
            OSMXmlParserMessageable osmMessageable = new OSMXmlParserMessageable();

			MessageHandler.Instance.RegisterMessageable(osmMessageable);

			OSMXmlParser.Instance.ParseClippedOSMFile("Test Assets/ModifiedTestManhattan.osm", -74.1154700, 40.8275750, -74.1099630, 40.8314270);

			Assert.AreEqual(3, osmMessageable.NumberOfAssetMessages, "The number of AssetMessages recieved from the clipped .osm file should be 3.");
			Assert.AreEqual(21, osmMessageable.NumberOfRoadSegementMessages, "The number of RoadSegementMessages recieved from the clipped .osm file should be 21.");
			Assert.AreEqual(1, osmMessageable.NumberOfBuildRoadGraphMessages, "The number of BuildRoadGraphMessages recieved from the clipped .osm file should be 1.");

			MessageHandler.Instance.RemoveMessageable(osmMessageable);
		}

		[TestMethod]
		public void ParseClippedFileInvalidClipping()
        {
			OSMXmlParserMessageable osmMessageable = new OSMXmlParserMessageable();

			MessageHandler.Instance.RegisterMessageable(osmMessageable);

			OSMXmlParser.Instance.ParseClippedOSMFile("Test Assets/ModifiedTestManhattan.osm", double.NaN, double.NaN, double.NaN, double.NaN);

			Assert.AreEqual(4, osmMessageable.NumberOfAssetMessages, "The number of AssetMessages recieved from the invalidly clipped .osm file should be 4.");
			Assert.AreEqual(71, osmMessageable.NumberOfRoadSegementMessages, "The number of RoadSegementMessages recieved from the invalidly clipped .osm file should be 71.");
			Assert.AreEqual(1, osmMessageable.NumberOfBuildRoadGraphMessages, "The number of BuildRoadGraphMessages recieved from the invalidly clipped .osm file should be 1.");

			MessageHandler.Instance.RemoveMessageable(osmMessageable);
		}*/
	}
}
